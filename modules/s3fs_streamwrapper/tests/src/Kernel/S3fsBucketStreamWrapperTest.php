<?php

namespace Drupal\Tests\s3fs_streamwrapper\Kernel;

use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\s3fs_streamwrapper\StreamWrapper\S3fsBucketStream;

/**
 * Tests the S3fsBucketStream functions.
 *
 * @group s3fs
 * @group s3fs_streamwrapper
 *
 * @covers \Drupal\s3fs_streamwrapper\StreamWrapper\S3fsBucketStream
 */
class S3fsBucketStreamWrapperTest extends S3fsKernelTestBase {

  /**
   * A stream wrapper scheme to use for the test.
   *
   * @var string
   */
  protected string $schemeUnderTest = 's3';

  /**
   * Tests S3fsBucketStream specific behavior.
   */
  public function testS3fsBucketStreamBehavior(): void {
    $type = S3fsBucketStream::getType();
    $this->assertEquals(StreamWrapperInterface::NORMAL, $type);

    // Test that stat is working on the root folder.
    $this->assertTrue(is_dir($this->schemeUnderTest . '://'), 'Root folder is seen as a directory');

    // Generate a test file.
    $filename = $this->randomMachineName();
    $uri = $this->schemeUnderTest . '://' . $filename;
    file_put_contents($uri, 'This is a test file for S3fsBuckStreamTest.');

    $file_system = \Drupal::service('file_system');
    // Attempt to open a file in read/write mode.
    // Aws\S3\StreamWrapper does not support the + modes.
    // @see Aws\S3\StreamWrapper::validate()
    $handle = @fopen($uri, 'r+');
    $this->assertFalse($handle, 'Unable to open a file for reading and writing (r+) with the S3fs Bucket stream wrapper.');
    // NOTE: The Aws\S3\StreamWrapper strips binary/text conversion options.
    // Attempt to open a file in binary read mode.
    $handle = fopen($uri, 'rb');
    $this->assertNotFalse($handle, 'Able to open a file for reading in binary mode with the S3fs Bucket stream wrapper.');
    $this->assertTrue(fclose($handle), 'Able to close file opened in binary mode using the S3fs Bucket stream wrapper.');
    // Attempt to open a file in text read mode.
    $handle = fopen($uri, 'rt');
    $this->assertNotFalse($handle, 'Able to open a file for reading in text mode with the S3fs Bucket stream wrapper.');
    $this->assertTrue(fclose($handle), 'Able to close file opened in text mode using the S3fs Bucket stream wrapper.');
    // Attempt to open a file in read mode.
    $handle = fopen($uri, 'r');
    $this->assertNotFalse($handle, 'Able to open a file for reading with the S3fs Bucket stream wrapper.');
    // StreamWrapper does not support + modes.
    $handle = @fopen($uri, 'w+');
    $this->assertFalse($handle, 'Unable to open a file for reading and writing (w+) with the S3fs Bucket stream wrapper.');
    $handle = fopen($uri, 'w');
    $this->assertNotFalse($handle, 'Able to open a file for writing with the S3fs Bucket stream wrapper.');
    // StreamWrapper does not support + modes.
    $handle = @fopen($uri, 'a+');
    $this->assertFalse($handle, 'Unable to open a file for appended reading and writing (a+) with the S3fs Bucket stream wrapper.');
    $handle = fopen($uri, 'a');
    $this->assertNotFalse($handle, 'Able to open a file for appended writing with the S3fs Bucket stream wrapper.');

    // Attempt to change file permissions.
    $this->assertTrue(@chmod($uri, 0777), 'Able to call chmod without error, changes ignored');
    // Attempt to acquire an exclusive lock for writing.
    $this->assertFalse(@flock($handle, LOCK_EX | LOCK_NB), 'Unable to acquire an exclusive lock using the S3fs Bucket stream wrapper.');
    // Attempt to obtain a shared lock.
    $this->assertFalse(@flock($handle, LOCK_SH | LOCK_NB), 'Unable to acquire a shared lock using the S3fs Bucket stream wrapper.');
    // Attempt to release a shared lock.
    $this->assertFalse(@flock($handle, LOCK_UN | LOCK_NB), 'Unable to release a shared lock using the S3fs Bucket stream wrapper.');
    // Attempt to truncate the file.
    $this->assertFalse(@ftruncate($handle, 0), 'Unable to truncate using the S3fs Bucket stream wrapper.');
    // Attempt to write to the file.
    $this->assertEquals(8, @fwrite($handle, $this->randomMachineName()), 'Able to to write to an append-only stream using the S3fs Bucket stream wrapper.');
    // Attempt to flush output to the file.
    $this->assertTrue(fflush($handle), 'Able to flush output to file using the S3fs Bucket stream wrapper.');
    $this->assertTrue(fclose($handle), 'Able to close file using the S3fsBucket stream wrapper.');

    // Test the rename() function.
    $this->assertTrue(rename($uri, $this->schemeUnderTest . '://newname.txt'), 'Abble to rename files using the S3fs Bucket stream wrapper.');

    // Test the mkdir() function by attempting to create a directory.
    $dirname = $this->randomMachineName();
    $dir = $this->schemeUnderTest . '://' . $dirname;
    $recursive_dir = $this->schemeUnderTest . '://recursive/test/' . $dirname;
    $this->assertTrue(mkdir($dir), 'Able to make directory using S3fs Bucket stream wrapper');
    $this->assertTrue(mkdir($recursive_dir, 0777, TRUE), 'Able to recursively make directory using the S3fs Bucket stream wrapper');

    // Test scandir() while we have multiple files and folders.
    $result = scandir($this->schemeUnderTest . '://');
    $this->assertNotFalse($result, 'scandir did not return false');
    $this->assertCount(3, $result);

    // Test the unlink() function.
    $this->assertTrue($file_system->unlink($uri), 'Able to unlink file using S3fs Bucket stream wrapper.');
    $this->assertFileDoesNotExist($uri);

    // Test the rmdir() function by attempting to remove the directory.
    $this->assertTrue(rmdir($dir), 'Able to delete directory with the S3fs Bucket stream wrapper.');
    $this->assertFalse(rmdir($this->schemeUnderTest . '://recursive/test/'), 'Unable to delete non-empty directory with the S3fs Bucket stream wrapper');

    // Remove the directory.
    rmdir($recursive_dir);
    rmdir($this->schemeUnderTest . '://recursive/test/');
    rmdir($this->schemeUnderTest . '://recursive/');
    $this->assertDirectoryDoesNotExist($this->schemeUnderTest . '://recursive');

  }

}
