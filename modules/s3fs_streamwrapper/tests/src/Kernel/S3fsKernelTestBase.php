<?php

namespace Drupal\Tests\s3fs_streamwrapper\Kernel;

use Aws\Exception\AwsException;
use Aws\S3\Exception\S3Exception;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\KernelTests\Core\File\FileTestBase;

/**
 * Base class for s3fs_streamwrapper kernel tests.
 *
 * @group s3fs
 * @group s3fs_streamwrapper
 */
abstract class S3fsKernelTestBase extends FileTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    's3fs',
    'key',
    's3fs_bucket',
    's3fs_client',
    's3fs_streamwrapper',
    's3fs_bucket_localstack_config',
    's3fs_test_s3_streamwrapper',
  ];

  /**
   * S3 Credentials provided and bucket exists.
   *
   * @var bool
   */
  protected $bucketNotFound = FALSE;

  /**
   * Folder name to use for placing tests files.
   *
   * @var string
   */
  protected $remoteTestsFolder = '_s3fs_tests';

  /**
   * Full base key path for tests folder.
   *
   * @var string
   */
  protected $remoteTestsFolderKey = '';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('s3fs_bucket', ['s3fs_file']);
    $this->installEntitySchema('s3fs_bucket');
    $this->installEntitySchema('s3fs_streamwrapper');
    $this->installConfig('s3fs_bucket_localstack_config');
    $this->installConfig('s3fs_test_s3_streamwrapper');

    // Set the standardized root_folder.
    $this->remoteTestsFolderKey = '_s3fs_test/' . uniqid('', TRUE);
    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $s3_entity */
    $s3_entity = $this->container->get('entity_type.manager')->getStorage('s3fs_streamwrapper')->load('s3');
    $s3_config = $s3_entity->getStreamWrapperConfig();
    $s3_config['root_folder'] = $this->remoteTestsFolderKey . '/s3';
    $s3_entity->setStreamWrapperConfig($s3_config)->save();

    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $usedd_entity */
    $usedd_entity = $this->container->get('entity_type.manager')->getStorage('s3fs_streamwrapper')->load('usedd');
    $usedd_config = $usedd_entity->getStreamWrapperConfig();
    $usedd_config['root_folder'] = $this->remoteTestsFolderKey . '/usedd';
    $usedd_entity->setStreamWrapperConfig($usedd_config)->save();
    drupal_static_reset();

    // Rebuild container to trigger compilerpass rebuild for the installed
    // config.
    /** @var \Drupal\Core\DrupalKernel $kernel */
    $kernel = $this->container->get('kernel');
    $new_container = $kernel->rebuildContainer();
    assert(is_a($new_container, ContainerBuilder::class));
    $this->container = $new_container;

    // Register is called as part of booting the kernel, however config for
    // our test streamWrappers was not available until container rebuild.
    // Call register() to complete setup of s3fs streamWrappers.
    $this->container->get('stream_wrapper_manager')->register();

    $s3_bucket = $s3_entity->getBucket();
    $s3_client = $s3_bucket->getS3Client();

    $this->bucketNotFound = !$s3_client->doesBucketExistV2($s3_bucket->getBucketName(), FALSE);

    $connectAttempts = 0;
    while ($this->bucketNotFound && $connectAttempts <= 5) {
      try {
        $s3_client->createBucket([
          'Bucket' => $s3_bucket->getBucketName(),
        ]);
        $this->bucketNotFound = FALSE;
      }
      catch (S3Exception $e) {
        // Bucket possibly was created by another script between checking.
        $this->bucketNotFound = !$s3_client->doesBucketExistV2($s3_bucket->getBucketName(), FALSE);
      }
      catch (AwsException $e) {
        // No need to continue tests if can't access the bucket. Either the
        // credentials are incorrect or problem with S3Client.
        $this->fail("Unable to create bucket '{$s3_bucket->getBucketName()}' Please verify the settings.");
      }

      if ($this->bucketNotFound) {
        // Wait before we loop again.
        sleep(5);
      }
      $connectAttempts++;
    }

    if (!$this->bucketNotFound) {
      // Empty out the bucket before the test, to prevent unexpected errors.
      $s3_client->deleteMatchingObjects($s3_bucket->getBucketName(), $this->remoteTestsFolderKey);
    }
    else {
      $this->fail("Unable to create bucket '{$s3_bucket->getBucketName()}' Please verify the settings.");
    }
  }

  /**
   * Clean up S3 folder.
   */
  protected function tearDown(): void {
    if (!$this->bucketNotFound) {
      /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $s3_entity */
      $s3_entity = $this->container->get('entity_type.manager')
        ->getStorage('s3fs_streamwrapper')
        ->load('s3');
      $s3_bucket = $s3_entity->getBucket();
      $s3_bucket->getS3Client()->deleteMatchingObjects($s3_bucket->getBucketName(), $this->remoteTestsFolderKey);
    }
    parent::tearDown();
  }

}
