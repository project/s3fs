<?php

namespace Drupal\Tests\s3fs_streamwrapper\Kernel;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\file\Entity\File;
use Drupal\s3fs_streamwrapper\Controller\S3fsFileDownloadController;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Tests the S3fsFileDownloadController.
 *
 * @group s3fs
 * @group s3fs_streamwrapper
 *
 * @covers \Drupal\s3fs_streamwrapper\Controller\S3fsFileDownloadController
 */
class S3fsFileDownloadControllerTest extends S3fsKernelTestBase {

  use TestFileCreationTrait;
  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'user',
  ];

  /**
   * Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Stream Wrapper Manager service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected StreamWrapperManagerInterface $streamWrapperManager;

  /**
   * Path of source file to use for tests.
   *
   * @var string
   */
  protected string $rootPath;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('file', ['file_usage']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
    $this->installEntitySchema('user');

    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->streamWrapperManager = $this->container->get('stream_wrapper_manager');

    $img_localpath = __DIR__ . '/../../fixtures/test.png';
    $img_data = file_get_contents($img_localpath);
    $random_img_uri = $this->createUri(NULL, $img_data, 's3');
    $img_uri = $random_img_uri . '.png';
    rename($random_img_uri, $img_uri);
    copy($img_uri, 's3://image-test-do.png');
    copy($img_uri, 'usedd://image-test-do.png');

  }

  /**
   * Test the S3fsFileDownloadController.
   */
  public function testS3fsFileDownloadController(): void {

    $user = $this->setUpCurrentUser();

    $controller = S3fsFileDownloadController::create($this->container);
    $this->assertInstanceOf(S3fsFileDownloadController::class, $controller, "::Create returns controller");

    $request_parameters = [
      'file' => 'image-test-do.png',
    ];

    $controller = new S3fsFileDownloadController($this->entityTypeManager, $this->streamWrapperManager);
    $request = Request::create('', 'GET', $request_parameters);

    $exception_thrown = FALSE;
    try {
      $response = $controller->download($request, 'usedd');
    }
    catch (\Exception $e) {
      $exception_thrown = TRUE;
      $this->assertInstanceOf(AccessDeniedHttpException::class, $e, 'Controller threw expected exception');
    }

    $this->assertTrue($exception_thrown, 'Exception was thrown for file without access');

    // Attempt to access with drupal delivery valid auth.
    $file = File::create([
      'filename' => 'image-test-do.png',
      'uri' => 'usedd://image-test-do.png',
      'status' => 1,
      'uid' => $user->id(),
    ]);
    // Temporary so we don't have to create references.
    $file->setTemporary();
    $file->save();

    $request = Request::create('', 'GET', $request_parameters);
    $response = $controller->download($request, 'usedd');
    $this->assertInstanceOf(BinaryFileResponse::class, $response, 'Controller returns an instance of BinaryFileResponse::class');

    $derivative_image = $this->container->get('image.factory')->get('usedd://image-test-do.png');
    $this->assertEquals($derivative_image->getMimeType(), $response->headers->get('Content-Type'), "Binary response includes correct Content-Type");
    $this->assertEquals($derivative_image->getFileSize(), $response->headers->get('Content-Length'), "Binary response includes correct Content-Length");

  }

  /**
   * Test that invalid/disabled schemes are not served.
   *
   * @dataProvider providerTestInvalidScheme
   */
  public function testInvalidScheme(string $scheme): void {

    $controller = new S3fsFileDownloadController($this->entityTypeManager, $this->streamWrapperManager);
    $request = Request::create('', 'GET', []);

    try {
      $controller->download($request, $scheme);
    }
    catch (\Exception $e) {
      $this->assertInstanceOf(NotFoundHttpException::class, $e, 'Controller threw expected exception');
      return;
    }

    $this->fail('Controller did not throw required exception');
  }

  /**
   * Data provider for testInvalidScheme().
   *
   * @return array[]
   *   Array with a single scheme to test.
   */
  public function providerTestInvalidScheme(): array {
    return [
      'Not a s3fs scheme' => ['public'],
      'Not using Drupal Delivery' => ['s3'],
      'Disabled scheme' => ['disableds3'],
    ];
  }

}
