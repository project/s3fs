<?php

namespace Drupal\Tests\s3fs_streamwrapper\Kernel;

/**
 * Tests the admin listing fallback when views is not enabled.
 *
 * @group s3fs
 * @group s3fs_streamwrapper
 *
 * @covers \Drupal\s3fs_streamwrapper\S3StreamWrapperEntityListBuilder
 */
class S3StreamWrapperListBuilderTest extends S3fsKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user_role');
  }

  /**
   * Tests that the listBuilder returns.
   */
  public function testListBuilder(): void {
    /** @var \Drupal\Core\Entity\EntityListBuilderInterface $list_builder */
    $list_builder = $this->container->get('entity_type.manager')->getListBuilder('s3fs_streamwrapper');

    $build = $list_builder->render();
    $this->container->get('renderer')->renderRoot($build);

    $this->assertNotEmpty($build['#cache']['tags']);
    $this->assertContains('config:s3fs_streamwrapper_list', $build['#cache']['tags']);
    $this->assertNotEmpty($build['table']['#rows']);
  }

}
