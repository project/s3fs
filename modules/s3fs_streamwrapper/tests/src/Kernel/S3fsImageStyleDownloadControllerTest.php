<?php

namespace Drupal\Tests\s3fs_streamwrapper\Kernel;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageStyleInterface;
use Drupal\s3fs_streamwrapper\Controller\S3fsImageStyleDownloadController;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Tests the S3fsImageStyleDownloadController.
 *
 * @group s3fs
 * @group s3fs_streamwrapper
 *
 * @covers \Drupal\s3fs_streamwrapper\Controller\S3fsImageStyleDownloadController
 */
class S3fsImageStyleDownloadControllerTest extends S3fsKernelTestBase {

  use TestFileCreationTrait;
  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'user',
    'image',
  ];

  /**
   * Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Lock service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected LockBackendInterface $lockService;

  /**
   * Image Factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected ImageFactory $imageFactory;

  /**
   * Stream Wrapper Manager service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected StreamWrapperManagerInterface $streamWrapperManager;

  /**
   * File System service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * Path of source file to use for tests.
   *
   * @var string
   */
  protected string $rootPath;

  /**
   * The Large ImageStyle used for testing.
   *
   * @var \Drupal\image\ImageStyleInterface
   */

  protected ImageStyleInterface $largeImageStyle;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('file', ['file_usage']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
    $this->installEntitySchema('image_style');
    $this->installConfig('system');
    $this->installConfig('image');

    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->lockService = $this->container->get('lock');
    $this->imageFactory = $this->container->get('image.factory');
    $this->streamWrapperManager = $this->container->get('stream_wrapper_manager');
    $this->fileSystem = $this->container->get('file_system');

    $img_localpath = __DIR__ . '/../../fixtures/test.png';
    $img_data = file_get_contents($img_localpath);
    $random_img_uri = $this->createUri(NULL, $img_data, 's3');
    $img_uri = $random_img_uri . '.png';
    rename($random_img_uri, $img_uri);
    copy($img_uri, 's3://image-test-do.png');
    copy($img_uri, 'usedd://image-test-do.png');

    $large_image_style = ImageStyle::load('large');
    $this->assertInstanceOf(ImageStyleInterface::class, $large_image_style, 'Unable to load large image style');
    $this->largeImageStyle = $large_image_style;
  }

  /**
   * Test the S3fsImageStyleDownloadController.
   */
  public function testImageStyleDownloadController(): void {

    $controller = S3fsImageStyleDownloadController::create($this->container);
    $this->assertInstanceOf(S3fsImageStyleDownloadController::class, $controller, "::Create returns controller");

    $request_parameters = [
      'file' => 'image-test-do.png',
      'itok' => $this->largeImageStyle->getPathToken('s3://image-test-do.png'),
    ];

    $img_uri = 's3://' . $request_parameters['file'];
    $request_parameters['itok'] = $this->largeImageStyle->getPathToken($img_uri);

    $controller = new S3fsImageStyleDownloadController($this->entityTypeManager, $this->lockService, $this->imageFactory, $this->streamWrapperManager, $this->fileSystem);
    $request = Request::create('', 'GET', $request_parameters);

    $response = $controller->deliver($request, 's3', $this->largeImageStyle);
    $this->assertInstanceOf(Response::class, $response, 'Controller returns an instance of Response::class');
    $this->assertEquals(302, $response->getStatusCode(), "Controller response is correct status code");
    assert($response instanceof TrustedRedirectResponse);

    // Test with token validation disabled.
    $this->config('image.settings')->set('allow_insecure_derivatives', TRUE)->save();
    $request_parameters['itok'] = 'invalid_token';
    $request = Request::create('', 'GET', $request_parameters);
    $response = $controller->deliver($request, 's3', $this->largeImageStyle);
    $this->assertInstanceOf(Response::class, $response, 'Controller returns an instance of Response::class with itok disabled');
    $this->assertEquals(302, $response->getStatusCode(), "Controller response is correct status code with itok disabled");
    assert($response instanceof TrustedRedirectResponse);
    $this->config('image.settings')->set('allow_insecure_derivatives', FALSE)->save();

    $user = $this->setUpCurrentUser();

    // Attempt to access a derivative delivered by Drupal Delivery without
    // permissions.
    $request_parameters = [
      'file' => 'image-test-do.png',
      'itok' => $this->largeImageStyle->getPathToken('usedd://image-test-do.png'),
    ];
    $request = Request::create('', 'GET', $request_parameters);
    $exception_thrown = FALSE;
    try {
      $response = $controller->deliver($request, 'usedd', $this->largeImageStyle);
    }
    catch (\Exception) {
      $exception_thrown = TRUE;
    }
    $this->assertTrue($exception_thrown, 'Exception thrown');

    // Attempt to access with drupal delivery valid auth.
    $file = File::create([
      'filename' => 'image-test-do.png',
      'uri' => 'usedd://image-test-do.png',
      'status' => 1,
      'uid' => $user->id(),
    ]);
    // Temporary so we don't have to create references.
    $file->setTemporary();
    $file->save();

    $request = Request::create('', 'GET', $request_parameters);
    $response = $controller->deliver($request, 'usedd', $this->largeImageStyle);
    $this->assertInstanceOf(BinaryFileResponse::class, $response, 'Controller returns an instance of BinaryFileResponse::class');

    $derivative_image = $this->imageFactory->get('usedd://styles/large/usedd/image-test-do.png');
    $this->assertEquals($derivative_image->getMimeType(), $response->headers->get('Content-Type'), "Binary response includes correct Content-Type");
    $this->assertEquals($derivative_image->getFileSize(), $response->headers->get('Content-Length'), "Binary response includes correct Content-Length");

  }

  /**
   * Test scheme's that should not be processed.
   *
   * @dataProvider providerTestInvalidScheme
   */
  public function testInvalidScheme(string $scheme): void {

    $controller = new S3fsImageStyleDownloadController($this->entityTypeManager, $this->lockService, $this->imageFactory, $this->streamWrapperManager, $this->fileSystem);
    $request = Request::create('', 'GET', ['file' => 'test.jpg']);

    try {
      $controller->deliver($request, $scheme, $this->largeImageStyle);
    }
    catch (\Exception $e) {
      $this->assertInstanceOf(AccessDeniedHttpException::class, $e, 'Controller threw expected exception');
      return;
    }

    $this->fail('Controller did not throw required exception');
  }

  /**
   * Data provider for testInvalidScheme().
   *
   * @return array[]
   *   Array with a single scheme to test.
   */
  public function providerTestInvalidScheme(): array {
    return [
      'Not a s3fs scheme' => ['public'],
      'Disabled scheme' => ['disableds3'],
    ];
  }

  /**
   * Test invalid itok provided.
   */
  public function testInvalidItok(): void {

    $request_parameters = [
      'file' => 'test.jpg',
      'itok' => 'invalid_token',
    ];
    $controller = new S3fsImageStyleDownloadController($this->entityTypeManager, $this->lockService, $this->imageFactory, $this->streamWrapperManager, $this->fileSystem);
    $request = Request::create('', 'GET', $request_parameters);

    try {
      $controller->deliver($request, 's3', $this->largeImageStyle);
    }
    catch (\Exception $e) {
      $this->assertInstanceOf(AccessDeniedHttpException::class, $e, 'Controller threw expected exception');
      return;
    }

    $this->fail('Controller did not throw required exception');
  }

  /**
   * Tests that files stored in the root folder are converted properly.
   *
   * Imported from core.
   */
  public function testConvertFileInRoot(): void {
    // Create the test image style with a Convert effect.
    $image_style = ImageStyle::create([
      'name' => 'image_effect_test',
      'label' => 'Image Effect Test',
    ]);
    $this->assertEquals(SAVED_NEW, $image_style->save());
    $image_style->addImageEffect([
      'id' => 'image_convert',
      'data' => [
        'extension' => 'jpeg',
      ],
    ]);
    $this->assertEquals(SAVED_UPDATED, $image_style->save());

    // Create a copy of a test image file in root.
    $test_uri = 's3://image-test-do.png';
    $this->assertFileExists($test_uri);

    // Execute the image style on the test image.
    $derivative_uri = 's3://styles/image_effect_test/s3/image-test-do.png.jpeg';
    $this->assertFileDoesNotExist($derivative_uri);

    $this->assertInstanceOf(ImageStyleInterface::class, ImageStyle::load('image_effect_test'), 'image_effect_test style is an ImageStyleInterface');

    $request_parameters = [
      'file' => 'image-test-do.png.jpeg',
      'itok' => ImageStyle::load('image_effect_test')->getPathToken('s3://image-test-do.png.jpeg'),
    ];
    $controller = new S3fsImageStyleDownloadController($this->entityTypeManager, $this->lockService, $this->imageFactory, $this->streamWrapperManager, $this->fileSystem);
    $request = Request::create('', 'GET', $request_parameters);

    $response = $controller->deliver($request, 's3', ImageStyle::load('image_effect_test'));
    $this->assertInstanceOf(Response::class, $response, 'Controller provided a response object');
    $this->assertFileExists($derivative_uri);
  }

}
