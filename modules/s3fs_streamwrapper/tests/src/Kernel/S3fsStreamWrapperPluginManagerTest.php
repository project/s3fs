<?php

namespace Drupal\Tests\s3fs_streamwrapper\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the plugin.manager.s3fs_streamwrapper Service.
 *
 * @group s3fs
 * @group s3fs_streamwrapper
 *
 * @covers \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginManager
 */
class S3fsStreamWrapperPluginManagerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    's3fs_streamwrapper',
  ];

  /**
   * Test the s3fs_streamwrapper plugin manager.
   */
  public function testPluginManager(): void {

    /** @var \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginManager $pluginManager */
    $pluginManager = $this->container->get('plugin.manager.s3fs_s3streamwrapper');
    $bucketTypesOptions = $pluginManager->getDefinitions();
    $this->assertArrayHasKey('s3_custom', $bucketTypesOptions);
  }

}
