<?php

namespace Drupal\Tests\s3fs_streamwrapper\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Test S3fsStreamWrapperEntityForm.
 *
 * @group s3fs
 * @group s3fs_streamwrapper
 *
 * @covers \Drupal\s3fs_streamwrapper\Form\S3StreamWrapperEntityForm
 */
class S3fsStreamWrapperEntityFormTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    's3fs',
    'key',
    's3fs_bucket',
    's3fs_client',
    's3fs_streamwrapper',
    's3fs_bucket_localstack_config',
    's3fs_test_s3_streamwrapper',
  ];

  /**
   * Theme to use for testing.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Test adding a new bucket.
   */
  public function testEntityPages(): void {
    $this->markTestSkipped('Random Failures');
    $adminUser = $this->createUser(['administer s3fs_streamwrapper']);
    $this->drupalLogin($adminUser);

    // Validate the Edit page loads.
    $this->drupalGet('admin/config/s3/streamwrapper/s3/edit');
    $this->assertSession()->pageTextContains('Configure Custom StreamWrapper plugin');

    // Test adding a record.
    $this->drupalGet('admin/config/s3/streamwrapper/add');

    $page = $this->getSession()->getPage();
    $page->findById('edit-streamwrapper-type-s3-custom')->click();
    $this->assertSession()->waitForText('Configure Custom StreamWrapper plugin');
    $this->assertSession()->pageTextContains('Configure Custom StreamWrapper plugin');
    $values = [
      'label' => 'form-test-streamwrapper',
      'streamwrapper_type' => 's3_custom',
      'streamwrapper_config[bucketId]' => 'localstack',
    ];
    $this->submitForm($values, 'Save');
    $this->assertSession()->waitForText('The StreamWrapper was successfully saved.');
    $this->assertSession()->pageTextContains('The StreamWrapper was successfully saved. ');

    $expected_config = [
      'bucketId' => 'localstack',
      'root_folder' => '',
      'cache_control_header' => '',
      'encryption' => '',
      'use_drupal' => FALSE,
      'use_https' => TRUE,
      'redirect_styles_ttl' => 300,
      'presigned_urls' => '',
      'saveas' => '',
      'use_cname' => FALSE,
      'cname' => '',
      'domain_root' => 'none',
      'upload_as_private' => FALSE,
    ];

    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntity $entity */
    $entity = \Drupal::service('entity_type.manager')->getStorage('s3fs_streamwrapper')->load('form-test-streamwrapper');
    $saved_config = $entity->getStreamWrapperConfig();
    $this->assertNotEmpty($saved_config);
    foreach ($expected_config as $key => $value) {
      $this->assertEquals($value, $saved_config[$key], "$key was correctly saved in config");
    }

    $this->drupalGet('admin/config/s3/streamwrapper/');
    $this->assertSession()->waitForText('form-test-streamwrapper');
    $this->assertSession()->pageTextContains('form-test-streamwrapper');

    // Test disabling a record.
    $this->drupalGet('admin/config/s3/streamwrapper/form-test-streamwrapper/disable');
    $this->assertSession()->waitForText('Disabling a streamWrapper will disable all access to files on this scheme.');
    $this->assertSession()->pageTextContains('Disabling a streamWrapper will disable all access to files on this scheme.');
    $submit_button = $page->findById('edit-submit');
    $this->assertNotEmpty($submit_button);
    $submit_button->click();
    $this->assertSession()->waitForText('The streamWrapper form-test-streamwrapper has been disabled.');
    $this->assertSession()->pageTextContains('The streamWrapper form-test-streamwrapper has been disabled.');

  }

}
