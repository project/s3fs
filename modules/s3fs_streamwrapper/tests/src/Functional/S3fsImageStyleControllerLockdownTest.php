<?php

namespace Drupal\Tests\s3fs_streamwrapper\Functional;

use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * S3 File System Image Style Lockdown verification.
 *
 * Ensure that the core Image Style generation routes are blocked from usage
 * on paths that should be delivered by the s3fs_streamwrapper module.
 *
 * @group s3fs
 * @group s3fs_streamwrapper
 *
 * @coversNothing
 */
class S3fsImageStyleControllerLockdownTest extends S3fsStreamWrapperBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'image',
    'file',
  ];

  /**
   * Test that ImageStyleDownloadController routes are blocked.
   */
  public function testImageStyleRoutes(): void {
    /** @var \Drupal\Core\Routing\RouteProviderInterface $route_provider */
    $route_provider = \Drupal::service('router.route_provider');
    $expected_route_names = [
      'image.style_public_s3fs_lockdown_s3',
      'image.style_private_s3fs_lockdown_s3',
      'image.style_public_s3fs_lockdown_usedd',
      'image.style_private_s3fs_lockdown_usedd',
    ];

    $not_expected_route_names = [
      // Our routes should always be open.
      's3fs_streamwrapper.image_styles_lockdown_s3',
      's3fs_streamwrapper.image_styles_lockdown_usedd',
      's3fs_streamwrapper.image_styles_lockdown_disabled',
      // Disabled schemes should not be locked down.
      'image.style_public_s3fs_lockdown_disabled',
      'image.style_private_s3fs_lockdown_disabled',
      // We should not touch the systems.files routes.
      'system.files_lockdown_s3',
      'system.files_lockdown_usedd',
      'system.files_lockdown_disabled',
    ];

    $this->assertCount(4, $route_provider->getRoutesByNames($expected_route_names), 'Expected route names found');
    $this->assertCount(0, $route_provider->getRoutesByNames($not_expected_route_names), "No unexpected route names found");

    $site_path = $this->kernel->getSitePath();
    $public_route_pattern_base = '/' . $site_path . '/files/styles/{image_style}/';

    $image_styles_public_route = $route_provider->getRouteByName('image.style_public');
    $this->assertEquals($image_styles_public_route->getPath(), $public_route_pattern_base . '{scheme}', 'Core path is as expected for image.styles_public');

    // Validate the image.style_public routes takeover have priority.
    foreach (['s3', 'usedd'] as $scheme) {
      $routes = $route_provider->getRoutesByPattern($public_route_pattern_base . $scheme);
      $route_iterator = $routes->getIterator();
      $this->assertEquals('image.style_public_s3fs_lockdown_' . $scheme, $route_iterator->key(), "Takeover route is highest priority for public image styles route");
      $this->assertEquals('/' . $site_path . '/files/styles/{image_style}/' . $scheme, $route_iterator->current()->getPath(), 'Takeover path is as expected for public route');
      $this->assertEquals('FALSE', $route_iterator->current()->getRequirement('_access'), 'Lockdwon root prohibits access');
      $route_iterator->next();
      $this->assertEquals('image.style_public', $route_iterator->key(), 'Core public image style route is lower priority');

      $routes = $route_provider->getRoutesByPattern('/system/files/styles/{image_style}/' . $scheme);
      $route_iterator = $routes->getIterator();
      $this->assertEquals('image.style_private_s3fs_lockdown_' . $scheme, $route_iterator->key(), "Takeover route is highest priority for private image styles route");
      $this->assertEquals('/system/files/styles/{image_style}/' . $scheme, $route_iterator->current()->getPath(), 'Takeover path is as expected for private route');
      $this->assertEquals('FALSE', $route_iterator->current()->getRequirement('_access'), 'Lockdwon root prohibits access');
      $route_iterator->next();
      $this->assertEquals('image.style_private', $route_iterator->key(), 'Core private image style route is lower priority');
    }

    /*
     * The following code performs a full end-to-end check of the blocking
     * rules tested above
     */

    $file_repository = \Drupal::service('file.repository');

    $this->drupalLogin($this->rootUser);
    // Prevent issues with derivative tokens during test.
    $this->config('image.settings')->set('allow_insecure_derivatives', TRUE)->save();

    $img_uri1 = "{$this->remoteTestsFolderUri}/test.png";
    $img_localpath = __DIR__ . '/../../fixtures/test.png';

    // Upload the test image.
    $this->assertTrue(\Drupal::service('file_system')->mkdir($this->remoteTestsFolderUri), 'Created the testing directory in the DB.');
    $img_data = file_get_contents($img_localpath);
    $this->assertNotFalse($img_data);
    $img_file = $file_repository->writeData($img_data, $img_uri1);
    $this->assertNotFalse($img_file, "Copied the the test image to $img_uri1.");

    $private_image_name = $this->randomGenerator->word(15) . '.jpg';
    $this->randomGenerator->image('private://' . $private_image_name, '400x300', '800x600');
    // Manually create the file record for the private:// file as we want it
    // to be temporary to pass hook_download() acl's.
    $values = [
      'uid' => $this->rootUser->id(),
      'status' => 0,
      'filename' => $private_image_name,
      'uri' => 'private://' . $private_image_name,
      'filesize' => filesize('private://' . $private_image_name),
      'filemime' => 'image/jpeg',
    ];
    $private_file = File::create($values);
    $private_file->save();
    $this->assertNotFalse(getimagesize($private_file->getFileUri()));
    $public_image_name = $this->randomGenerator->word(15) . '.jpg';
    $temp_public_image = $this->randomGenerator->image('temporary://' . $public_image_name, '400x300', '800x600');
    $img_data = file_get_contents($temp_public_image);
    $this->assertNotFalse($img_data);
    $file_repository->writeData($img_data, 'public://' . $public_image_name);
    $this->assertNotFalse(getimagesize('public://' . $public_image_name));

    // Request a derivative. for s3 scheme via s3fs route.
    $private_route = Url::fromRoute(
      's3fs_streamwrapper.image_styles',
      [
        'image_style' => 'large',
        'scheme' => 's3',
      ],
    );
    $derivative = $this->drupalGet($private_route->setAbsolute()->toString() . '/' . $this->remoteTestsFolder . '/test.png');
    $this->assertNotFalse(imagecreatefromstring($derivative), 's3fs_streamwrapper.image_styles processes s3://');

    // Request a derivative. for s3 scheme via private route.
    $private_route = Url::fromRoute(
      'image.style_private',
      [
        'image_style' => 'large',
        'scheme' => 's3',
      ],
    );
    $derivative = $this->drupalGet($private_route->setAbsolute()->toString() . '/' . $this->remoteTestsFolder . '/test.png');
    $this->assertSession()->statusCodeEquals(403);

    // Request a derivative. for s3 scheme via public route.
    $private_route = Url::fromRoute(
      'image.style_public',
      [
        'image_style' => 'large',
        'scheme' => 's3',
      ],
    );
    $derivative = $this->drupalGet($private_route->setAbsolute()->toString() . '/' . $this->remoteTestsFolder . '/test.png');
    $this->assertSession()->statusCodeEquals(403);

    /*
     * Ensure core public:// and private:// image style still works.
     */

    // Request a derivative for private:// scheme via private route.
    $private_via_private_route = Url::fromRoute(
      'image.style_private',
      [
        'image_style' => 'large',
        'scheme' => 'private',
      ],
    );
    $derivative = $this->drupalGet($private_via_private_route->setAbsolute()->toString() . '/' . $private_image_name);
    $this->assertNotFalse(imagecreatefromstring($derivative), 'image.style_private does process private://');

    // Request a derivative for public:// scheme via public route.
    $public_via_public_route = Url::fromRoute(
      'image.style_public',
      [
        'image_style' => 'large',
        'scheme' => 'public',
      ],
    );
    $derivative = $this->drupalGet($public_via_public_route->setAbsolute()->toString() . '/' . $public_image_name);
    $this->assertNotFalse(imagecreatefromstring($derivative), 'image.style_public does process public://');

    // Enable public and private takeover.
    $settings = [];
    $settings['settings']['s3fs.use_s3_for_public'] = (object) [
      'value' => TRUE,
      'required' => TRUE,
    ];
    $settings['settings']['s3fs.use_s3_for_private'] = (object) [
      'value' => TRUE,
      'required' => TRUE,
    ];
    $this->writeSettings($settings);
    // Make sure the test runner and Drupal gets the new StreamWrappers.
    $this->rebuildAll();

    // Create new files in s3.
    $private_image_name = $this->randomGenerator->word(15) . '.jpg';
    $this->randomGenerator->image('private://' . $private_image_name, '400x300', '800x600');
    // Manually create the file record for the private:// file as we want it
    // to be temporary to pass hook_download() acl's.
    $values = [
      'uid' => $this->rootUser->id(),
      'status' => 0,
      'filename' => $private_image_name,
      'uri' => 'private://' . $private_image_name,
      'filesize' => filesize('private://' . $private_image_name),
      'filemime' => 'image/jpeg',
    ];
    $private_file = File::create($values);
    $private_file->save();
    $this->assertNotFalse(getimagesize($private_file->getFileUri()));
    $public_image_name = $this->randomGenerator->word(15) . '.jpg';
    $this->randomGenerator->image('public://' . $public_image_name, '400x300', '800x600');
    $this->assertNotFalse(getimagesize('public://' . $public_image_name));
  }

}
