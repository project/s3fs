<?php

namespace Drupal\Tests\s3fs_streamwrapper\Functional;

use Aws\Exception\AwsException;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3ClientInterface;
use Drupal\Core\Database\Connection;
use Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface;
use Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * S3 File System Test Base.
 *
 * Provides a base for BrowserTest to execute against.
 *
 * @group s3fs
 * @group s3fs_streamwrapper
 */
abstract class S3fsStreamWrapperBrowserTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    's3fs',
    'key',
    's3fs_bucket',
    's3fs_client',
    's3fs_streamwrapper',
    's3fs_bucket_localstack_config',
    's3fs_test_s3_streamwrapper',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * The s3fs module config.
   *
   * @var array
   */
  protected array $s3Config;

  /**
   * The AWS SDK for PHP S3Client object.
   *
   * @var \Aws\S3\S3ClientInterface
   */
  protected S3ClientInterface $s3;

  /**
   * S3 Credentials provided and bucket exists.
   *
   * @var bool
   */
  protected bool $bucketNotFound = FALSE;

  /**
   * Folder name to use for placing tests files.
   *
   * @var string
   */
  protected string $remoteTestsFolder = '_s3fs_tests';

  /**
   * Full base key path for tests folder.
   *
   * @var string
   */
  protected string $remoteTestsFolderKey = '';

  /**
   * URI for accessing the data via StreamWrapper.
   *
   * @var string
   */
  protected string $remoteTestsFolderUri = '';

  /**
   * The StreamWrapper Config Entity.
   *
   * @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface
   */
  protected S3StreamWrapperEntityInterface $wrapperEntity;

  /**
   * StreamWrapper Pluugin.
   *
   * @var \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginInterface
   */
  protected S3StreamWrapperPluginInterface $wrapperPlugin;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $wrapper_entity */
    $wrapper_entity = $this->container->get('entity_type.manager')
      ->getStorage('s3fs_streamwrapper')
      ->load('s3');
    $this->wrapperEntity = $wrapper_entity;

    $this->wrapperPlugin = $this->wrapperEntity->getStreamWrapperPlugin();

    $this->s3 = $this->wrapperPlugin->getS3Client();

    $this->prepareConfig();

    // Because of our StreamWrapperManager loading during parent::setUp()
    // before we can install the configs we need to reset the environment for
    // the StreamWrapperManager to load the new configs.
    $this->resetAll();

    if (empty($this->s3Config['bucket'])) {
      // No sense to test anything if credentials absent.
      $this->bucketNotFound = TRUE;
      $this->markTestSkipped('S3 not configured');
    }

    $this->connection = $this->container->get('database');

    $this->remoteTestsFolderKey = $this->s3Config['root_folder'];
    $this->remoteTestsFolderUri = "s3://{$this->remoteTestsFolder}";
    $this->bucketNotFound = !$this->s3->doesBucketExistV2($this->s3Config['bucket'], FALSE);

    $connectAttempts = 0;
    while ($this->bucketNotFound && $connectAttempts <= 5) {
      try {
        $this->s3->createBucket([
          'Bucket' => $this->s3Config['bucket'],
        ]);
        $this->bucketNotFound = FALSE;
      }
      catch (S3Exception $e) {
        // Bucket possibly was created by another script between checking.
        $this->bucketNotFound = !$this->s3->doesBucketExistV2($this->s3Config['bucket'], FALSE);
      }
      catch (AwsException $e) {
        // No need to continue tests if can't access the bucket. Either the
        // credentials are incorrect or problem with S3Client.
        $this->fail("Unable to create bucket '{$this->s3Config['bucket']}' in region '{$this->s3Config['region']}'.
          Please verify the S3 settings.");
      }

      if ($this->bucketNotFound) {
        // Wait before we loop again.
        sleep(5);
      }
      $connectAttempts++;
    }

    if (!$this->bucketNotFound) {
      // Empty out the bucket before the test, to prevent unexpected errors.
      $this->s3->deleteMatchingObjects($this->s3Config['bucket'], $this->remoteTestsFolderKey);
    }
    else {
      $this->fail("Unable to access bucket '{$this->s3Config['bucket']}' in region '{$this->s3Config['region']}'.
          Please verify the S3 settings.");
    }
  }

  /**
   * Clean up S3 folder.
   */
  protected function tearDown(): void {
    if (!$this->bucketNotFound) {
      $this->s3->deleteMatchingObjects($this->s3Config['bucket'], $this->remoteTestsFolderKey);
    }
    parent::tearDown();
  }

  /**
   * Prepares the config.
   */
  protected function prepareConfig(): void {

    // Set the standardized root_folder.
    $rootPath = $this->remoteTestsFolder . '/' . uniqid('', TRUE);
    $this->s3Config['root_folder'] = $rootPath;

    // Get the bucket name from the plugin.
    $this->assertNotNull($this->wrapperPlugin->getBucket());
    $this->s3Config['bucket'] = $this->wrapperPlugin->getBucket()->getBucketName();

    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $s3Entity */
    $s3Entity = $this->container->get('entity_type.manager')->getStorage('s3fs_streamwrapper')->load('s3');
    $s3Config = $s3Entity->getStreamWrapperConfig();
    $s3Config['root_folder'] = $rootPath;
    $s3Entity->setStreamWrapperConfig($s3Config);
    $s3Entity->save();
  }

}
