<?php

namespace Drupal\Tests\s3fs_streamwrapper\Functional;

/**
 * Tests s3fs_streamwrapper container compiler operations.
 *
 * @group s3fs
 * @group s3fs_streamwrapper
 *
 * @covers \Drupal\s3fs_streamwrapper\Compiler\S3fsRegisterStreamWrappers
 * @covers \Drupal\s3fs_streamwrapper\S3fsStreamwrapperServiceProvider
 */
class S3fsServiceProviderTest extends S3fsStreamWrapperBrowserTestBase {

  /**
   * Tests that the ServiceProvider registers the s3fs streamWrappers.
   */
  public function testRegisterStreamWrapper(): void {
    $this->assertTrue($this->container->has('stream_wrapper.s3'), "s3:// streamWrapper service exists");
    $this->assertContains('s3', stream_get_wrappers(), 's3:// scheme has been registered');
  }

}
