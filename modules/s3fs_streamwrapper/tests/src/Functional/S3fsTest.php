<?php

namespace Drupal\Tests\s3fs_streamwrapper\Functional;

use Drupal\Component\Utility\UrlHelper;
use Drupal\file\FileInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageStyleInterface;

/**
 * S3 File System Tests.
 *
 * Ensure that the remote file system functionality provided by S3 File System
 * works correctly.
 *
 * @group s3fs
 * @group s3fs_streamwrapper
 *
 * @coversNothing
 */
class S3fsTest extends S3fsStreamWrapperBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    's3fs',
    'image',
    'key',
    's3fs_bucket',
    's3fs_client',
    's3fs_streamwrapper',
    's3fs_bucket_localstack_config',
    's3fs_test_s3_streamwrapper',
  ];

  /**
   * Coverage test for the stream wrapper.
   */
  public function testStreamWrapperCoverage(): void {
    $file_repository = \Drupal::service('file.repository');
    $file_url_generator = \Drupal::service('file_url_generator');

    $test_uri1 = "{$this->remoteTestsFolderUri}/test_file1.txt";
    $test_uri2 = "{$this->remoteTestsFolderUri}/test_file2.txt";

    $this->assertTrue($this->container->get('stream_wrapper_manager')->isValidScheme('s3'), '"s3" is a valid stream wrapper scheme.');
    $this->assertEquals('Drupal\s3fs_streamwrapper\StreamWrapper\S3fsBucketStream', $this->container->get('stream_wrapper_manager')->getClass('s3'), 'URIs with scheme "s3" should be handled by S3fsStream.');

    // The test.txt file contains enough data to force multiple calls
    // to write_stream().
    $file_contents = file_get_contents(__DIR__ . '/../../fixtures/test.txt');
    $this->assertNotFalse($file_contents);

    $this->assertTrue($this->container->get('file_system')->mkdir($this->remoteTestsFolderUri), 'Exercised mkdir to create the testing directory (in the DB).');
    $this->assertTrue(is_dir($this->remoteTestsFolderUri), 'Make sure the folder we just created correctly reports that it is a folder.');

    // Exercising file upload functionality.
    $s3_file1 = $file_repository->writeData($file_contents, $test_uri1);

    $this->assertTrue($this->container->get('stream_wrapper_manager')->isValidUri($s3_file1->getFileUri()), "Uploaded the first test file, $test_uri1.");

    // Exercising file copy functionality.
    $s3_file2 = $file_repository->copy($s3_file1, $test_uri2);
    $this->assertNotFalse($s3_file2, "Copied the the first test file to $test_uri2.");

    // Exercising the dir_*() functions.
    $files = $this->container->get('file_system')->scanDirectory($this->remoteTestsFolderUri, '#.*#');
    $this->assertTrue(isset($files[$test_uri1]), 'The first test file is in the tests directory.');
    $this->assertTrue(isset($files[$test_uri2]), 'The second test file is in the tests directory.');
    $this->assertEquals(2, count($files), "There are exactly two files in the tests directory.");

    // Exercising getExternalUrl()
    $url = $file_url_generator->generateAbsoluteString($test_uri1);
    $this->assertNotFalse($url, 'getExternalUrl test succeeded.');

    // Exercising unlink()
    $this->assertTrue(self::fileDelete($s3_file1), "Deleted the first test file.");
    $this->assertFalse(file_exists($test_uri1), 'The wrapper reports that the first test file no longer exists.');

    // Exercising rename()
    $this->assertTrue(rename($test_uri2, $test_uri1), "Renamed the second test file to the newly-vacated URI of $test_uri1.");
    $s3_file2->setFileUri($test_uri1);
    $s3_file2->save();

    // Rename a 'directory' should fail.
    $dir_move_test_src = $this->remoteTestsFolderUri . '/directoryToBeMoved';
    $dir_move_test_dest = $this->remoteTestsFolderUri . '/destinationDirectory';
    $this->assertTrue($this->container->get('file_system')->mkdir($dir_move_test_src), 'Created testing directory to attempt move.');
    $this->assertNotFalse(file_put_contents($dir_move_test_src . '/test.file', 'test'), "Created file in directory that will be moved.");
    $this->assertFalse(rename($dir_move_test_src, $dir_move_test_dest), 'Should not be able to move a directory.');
    $this->assertFalse(is_file($dir_move_test_dest . '/test.file'), 'Test file should not exist as directory moves are not supported.');
    $this->assertTrue(unlink($dir_move_test_src . '/test.file'), "Deleted the test move file.");

    // Exercising rmdir()
    $this->assertFalse($this->container->get('file_system')->rmdir($this->remoteTestsFolderUri), 'rmdir() did not delete the tests folder because it is not empty.');
    $this->assertTrue($this->container->get('file_system')->rmdir($dir_move_test_src), "Delete the move test directory");
    $this->assertTrue(self::fileDelete($s3_file2), 'Deleted the last test file.');
    $this->assertTrue($this->container->get('file_system')->rmdir($this->remoteTestsFolderUri), 'Deleted the tests folder.');
    $this->assertFalse(is_dir($this->remoteTestsFolderUri), 'The wrapper reports that the tests folder is gone.');

    // Testing max filename limits.
    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $s3_wrapper_entity */
    $s3_wrapper_entity = $this->container->get('entity_type.manager')->getStorage('s3fs_streamwrapper')->load('s3');
    $max_uri_length = $s3_wrapper_entity->getMaxUriLength();

    $base_path = 's3://' . $this->randomMachineName($max_uri_length - 15);
    $this->assertEquals($max_uri_length - 10, strlen($base_path), 'Base path is 10 char less than limit');

    // Max length mkdir().
    // The last / is stripped making max length limit safe.
    $path_directory_max_length = "{$base_path}/dir/12345/";
    $this->assertTrue($this->container->get('file_system')->mkdir($path_directory_max_length), 'Creating max path length directory');
    $this->assertTrue(is_dir($path_directory_max_length), 'Verify max path length directory exists');
    // Max Length + 1 characters long exceeds limit (the last / is stripped)
    $path_directory_exceed_limit = "{$base_path}/dir/123456/";
    $this->assertFalse($this->container->get('file_system')->mkdir($path_directory_exceed_limit), 'Creating directory that exceeds path length limit');
    $this->assertFalse(is_dir($path_directory_exceed_limit), 'Verify directory that exceeds max path length doesnt exist');

    // Max length stream_open().
    $path_file_max_length = "{$base_path}/12345.txt";
    $path_file_exceed_length = "{$base_path}/123456.txt";
    $this->assertNotFalse(file_put_contents($path_file_max_length, $file_contents), 'Creating max path length filename');
    $this->assertTrue(is_file($path_file_max_length), 'Verify max path length file exists');
    $this->assertFalse(@file_put_contents($path_file_exceed_length, $file_contents), 'Creating file exceeds max path length');
    $this->assertFalse(is_file($path_file_exceed_length), 'File that exceeds max path length doesnt exist');

    // Max length rename().
    $path_file_renamed_to_max_length = "{$base_path}/12345.ace";
    $this->assertTrue(rename($path_file_max_length, $path_file_renamed_to_max_length), 'Rename file to max path length');
    $this->assertFalse(rename($path_file_renamed_to_max_length, $path_file_exceed_length), 'Rename file to exceed max path length');

    // Set config and flush static to test cache-control header.
    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $s3Entity */
    $s3Entity = $this->container->get('entity_type.manager')->getStorage('s3fs_streamwrapper')->load('s3');

    $s3Config = $s3Entity->getStreamWrapperConfig();
    $s3Config['cache_control_header'] = 'public, max-age=300';
    $s3Entity->setStreamWrapperConfig($s3Config)->save();
    $this->resetAll();
    // Verify header exists on new files.
    $headerPutUri = 's3://' . $this->randomMachineName();
    file_put_contents($headerPutUri, $file_contents);
    $url = $file_url_generator->generateAbsoluteString($headerPutUri);
    $this->drupalGet($url);
    $this->assertSession()->responseHeaderEquals('cache-control', 'public, max-age=300');
    // Make sure the header exists and age changes on copy().
    $s3Config['cache_control_header'] = 'public, max-age=301';
    $s3Entity->setStreamWrapperConfig($s3Config)->save();
    $this->resetAll();
    $headerCopyUri = 's3://' . $this->randomMachineName();
    copy($headerPutUri, $headerCopyUri);
    $url = $file_url_generator->generateAbsoluteString($headerCopyUri);
    $this->drupalGet($url);
    $this->assertSession()->responseHeaderEquals('cache-control', 'public, max-age=301');

  }

  /**
   * Test the image derivative functionality.
   */
  public function testImageDerivatives(): void {
    $file_repository = \Drupal::service('file.repository');

    // Prevent issues with derivative tokens during test.
    $this->config('image.settings')->set('allow_insecure_derivatives', TRUE)->save();

    // @todo this should probably be in the test module config.
    // Use the large image style for for presigned tests.
    $s3Config = [
      'presigned_urls' => '6000|.*/large/.*',
    ];
    $s3Config += $this->wrapperEntity->getStreamWrapperConfig();
    $this->wrapperEntity->setStreamWrapperConfig($s3Config);

    $img_uri1 = "{$this->remoteTestsFolderUri}/test.png";
    $img_localpath = __DIR__ . '/../../fixtures/test.png';

    // Upload the test image.
    $this->assertTrue($this->container->get('file_system')->mkdir($this->remoteTestsFolderUri), 'Created the testing directory in the DB.');
    $img_data = file_get_contents($img_localpath);
    $this->assertNotFalse($img_data);
    $img_file = $file_repository->writeData($img_data, $img_uri1);
    $this->assertNotFalse($img_file, "Copied the the test image to $img_uri1.");

    // Request a derivative.
    // Parse query parameters to ensure they get passed.
    $this->assertInstanceOf(ImageStyleInterface::class, ImageStyle::load('thumbnail'), 'Image style thumbnail loads');
    $style_url_parsed = UrlHelper::parse(ImageStyle::load('thumbnail')->buildUrl($img_uri1));
    $derivative = $this->drupalGet($style_url_parsed['path'], ['query' => $style_url_parsed['query']]);
    $this->assertNotFalse(imagecreatefromstring($derivative), 'The returned derivative is a valid image.');

    $this->assertInstanceOf(ImageStyleInterface::class, ImageStyle::load('large'), 'Image style large loads');
    $style_presigned_url_parsed = UrlHelper::parse(ImageStyle::load('large')->buildUrl($img_uri1));
    $presigned_derivative = $this->drupalGet($style_presigned_url_parsed['path'], ['query' => $style_presigned_url_parsed['query']]);
    $this->assertNotFalse(imagecreatefromstring($presigned_derivative), 'The returned signed derivative is a valid image.');

  }

  /**
   * File delete wrapper that returns result.
   *
   * @param \Drupal\file\FileInterface $file
   *   A file object to delete.
   *
   * @return bool
   *   TRUE if file was deleted, FALSE otherwise.
   */
  protected static function fileDelete(FileInterface $file): bool {
    $file->delete();
    $exists = file_exists($file->getFileUri());
    return !$exists;
  }

}
