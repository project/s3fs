<?php

namespace Drupal\Tests\s3fs_streamwrapper\Unit;

use Aws\S3\S3ClientInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\s3fs_bucket\S3BucketInterface;
use Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginInterface;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Test base for StreamWrapper plugin testing.
 *
 * @group s3fs
 * @group s3fs_streamwrapper
 */
abstract class S3fsStreamWrapperUnitTestBase extends UnitTestCase {

  /**
   * Entity Type Manager mock.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected EntityTypeManagerInterface|MockObject $entityManagerMock;


  /**
   * ModuleHandler mock.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected ModuleHandlerInterface|MockObject $moduleHandlerMock;

  /**
   * S3fs_bucket Entity Mock.
   *
   * @var \Drupal\s3fs_bucket\S3BucketInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected S3BucketInterface|MockObject $bucketEntityMock;

  /**
   * An (optional) mock s3 client.
   *
   * @var \Aws\S3\S3ClientInterface|\PHPUnit\Framework\MockObject\MockObject|null
   */
  protected S3ClientInterface|MockObject|NULL $s3ClientMock = NULL;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Reset all static caches.
    drupal_static_reset();
    $this->moduleHandlerMock = $this->createMock(ModuleHandlerInterface::class);
    $this->bucketEntityMock = $this->getS3BucketEntityMock();

    $bucket_storage_mock = $this->createMock(EntityStorageInterface::class);
    $bucket_storage_mock->method('load')->with('mock_s3_bucket')->willReturn($this->bucketEntityMock);
    $bucket_storage_mock->method('loadByProperties')->willReturn([$this->bucketEntityMock]);
    $this->entityManagerMock = $this->createMock(EntityTypeManagerInterface::class);
    $this->entityManagerMock->method('getStorage')->with('s3fs_bucket')->willReturn($bucket_storage_mock);

  }

  /**
   * Set up the plugin under test.
   *
   * This function should be replaced by extenders to set up the real plugin.
   *
   * @return \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginInterface
   *   An instance of the plugin type under test.
   */
  abstract protected function setupPlugin(): S3StreamWrapperPluginInterface;

  /**
   * Returns a mocked S3Bucket Entity.
   *
   * @return \Drupal\s3fs_bucket\S3BucketInterface|\PHPUnit\Framework\MockObject\MockObject
   *   A base S3BucketInterface mock.
   */
  protected function getS3BucketEntityMock(): S3BucketInterface|MockObject {
    $mock = $this->createMock(S3BucketInterface::class);
    $mock->method('status')->willReturn(TRUE);
    $mock->method('getDescription')->willReturn('Mocked Bucket Entity');
    $mock->method('getPluginId')->willReturn('mocked_bucket_plugin');
    $mock->method('getBucketName')->willReturn('mock-s3-bucket');
    $mock->method('getMaxPathLength')->willReturn(230);
    $mock->method('getS3fsObject')
      ->with(
        $this->logicalAnd(
          $this->logicalNot($this->stringContains('://')),
          $this->logicalNot($this->stringStartsWith('/'))
        )
      )
      ->willReturnCallback([__CLASS__, 'callbackGetS3fsObject']);

    if ($this->s3ClientMock == NULL) {
      $mock->method('getS3Client')
        ->willReturn(
          $this->createMock(S3ClientInterface::class)
        );
    }
    else {
      $mock->method('getS3Client')->willReturn($this->s3ClientMock);
    }

    $callback_alter_path = function (string $path): string {
      $path = preg_replace('^ALTER_ME^', 'altered', $path);
      static::assertNotNull($path, 'ALTER_ME alter returned null');
      $path = preg_replace('^DELETE_ME/^', '', $path);
      static::assertNotNull($path, 'DELETE_ME alter returned null');
      return $path;
    };
    $mock->method('alterKeyedPath')
      ->with(
        $this->logicalAnd(
          $this->logicalNot($this->stringContains('://')),
          $this->logicalNot($this->stringStartsWith('/'))
        )
      )
      ->willReturnCallback($callback_alter_path);

    $callback_get_command_params = function (string $path, $params): array {
      $params['Bucket'] = 'mock-s3-bucket';
      $params['Key'] = $path;
      return $params;
    };
    $mock->method('getCommandParams')
      ->with(
        $this->logicalAnd(
          $this->logicalNot($this->stringContains('://')),
          $this->logicalNot($this->stringStartsWith('/'))
        )
      )
      ->willReturnCallback($callback_get_command_params);

    $mock->method('convertMetadata')
      ->willReturnCallback([__CLASS__, 'callbackConvertMetadata']);

    return $mock;
  }

  /**
   * Callback to mock getS3fsObject()
   *
   * @param string $path
   *   Path being queried.
   *
   * @return array|false
   *   False if $path contains 'NON_EXISTENT' else a metadata array.
   *   If $path contains IS_DIR the directory flag will be set.
   */
  public static function callbackGetS3fsObject(string $path): array|FALSE {
    $metadata = [
      'bucket' => 'mock-s3-bucket',
      'path' => $path,
      'filesize' => 0,
      'timestamp' => 160000000,
      'dir' => 0,
      'version' => '',
    ];

    if (str_contains($path, 'NON_EXISTENT')) {
      return FALSE;
    }

    if (str_contains($path, 'IS_DIR')) {
      $metadata['dir'] = 1;
    }
    else {
      $metadata['filesize'] = 4096;
      $metadata['version'] = '55bddc1b-4016-471e-b7e8-9e7a3e6e1f20';
    }

    return $metadata;

  }

  /**
   * Callback to mock listDir()
   *
   * @param string $path
   *   Path being queried.
   *
   * @return array
   *   An array of mocked paths.
   */
  public static function callbackListDir(string $path): array {
    return [
      "$path/object_1",
      "$path/object_2",
      "$path/object_3",
    ];
  }

  /**
   * Mock for bucket ConvertMetdata.
   */
  public static function callbackConvertMetadata(string $path, array $s3_metadata): array {

    $metadata = [
      'bucket' => 'mock_s3_bucket',
      'path' => $path,
      'filesize' => 0,
      'timestamp' => 1640000000,
      'dir' => 0,
      'version' => '',
    ];

    if (empty($s3_metadata)) {
      // The caller wants directory metadata.
      $metadata['dir'] = 1;
    }
    else {
      // The filesize value can come from either the Size or ContentLength
      // attribute, depending on which AWS API call built $s3_metadata.
      if (isset($s3_metadata['ContentLength'])) {
        $metadata['filesize'] = $s3_metadata['ContentLength'];
      }
      else {
        if (isset($s3_metadata['Size'])) {
          $metadata['filesize'] = $s3_metadata['Size'];
        }
      }

      if (isset($s3_metadata['LastModified'])) {
        $metadata['timestamp'] = date('U', strtotime($s3_metadata['LastModified']));
      }

      if (isset($s3_metadata['VersionId']) && $s3_metadata['VersionId'] != 'null') {
        $metadata['version'] = $s3_metadata['VersionId'];
      }
    }
    return $metadata;
  }

}
