<?php

namespace Drupal\Tests\s3fs_streamwrapper\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Form\FormState;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface;
use Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginInterface;

/**
 * Test base for StreamWrapper plugin testing.
 *
 * @group s3fs
 * @group s3fs_streamwrapper
 *
 * @covers \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginBase
 */
abstract class S3fsStreamWrapperPluginFormTestBase extends S3fsStreamWrapperUnitTestBase {

  /**
   * Base config for plugins.
   *
   * @var array
   */
  protected array $pluginConfig;

  /**
   * Id of plugin to test.
   *
   * @var string
   */
  protected string $pluginId;

  /**
   * Plugin definition.
   *
   * @var array
   */
  protected array $pluginDefinition;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $streamwrapper_entity_mock = $this->createMock(S3StreamWrapperEntityInterface::class);
    $streamwrapper_entity_mock->method('id')->willReturn('mock-streamwrapper');

    $this->pluginConfig = [
      '#streamwrapper' => $streamwrapper_entity_mock,
    ];

    $link_generator_mock = $this->createMock(LinkGeneratorInterface::class);
    $link_generator_mock->method('generateFromLink')->willReturn('http://www.example.org/some/link');
    $container = new ContainerBuilder();
    $container->set('link_generator', $link_generator_mock);
    \Drupal::setContainer($container);

  }

  /**
   * Validate buildConfigurationForm().
   */
  public function testBuildConfigurationForm(): void {
    $this->buildConfigurationForm();
  }

  /**
   * Build the configuration form from a plugin.
   *
   * @return array
   *   Result of building the configuration form
   */
  protected function buildConfigurationForm(): array {
    $form_state = new FormState();
    $plugin = $this->setupPlugin();
    if (!($plugin instanceof PluginFormInterface)) {
      $this->markTestSkipped('Plugin does not inherit PluginFormInterface');
    }
    $form = $plugin->buildConfigurationForm($this->pluginConfig, $form_state);
    $this->assertIsArray($form);
    return [$plugin, $form, $form_state];
  }

  /**
   * Set up the plugin under test.
   *
   * This function should be replaced by extenders to set up the real plugin.
   *
   * @return \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginInterface
   *   An instance of the plugin type under test.
   */
  abstract protected function setupPlugin(): S3StreamWrapperPluginInterface;

}
