<?php

namespace Drupal\Tests\s3fs_streamwrapper\Unit;

use Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapper\CustomS3StreamWrapperPlugin;

/**
 * Tests the CustomS3StreamWrapper plugin forms.
 *
 * @group s3fs
 * @group s3fs_streamwrapper
 *
 * @covers \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapper\CustomS3StreamWrapperPlugin
 * @covers \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginBase
 */
class S3fsCustomStreamWrapperPluginFormTest extends S3fsStreamWrapperPluginFormTestBase {

  /**
   * The config factory mock.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject&\Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactoryMock;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->pluginConfig += [
      'bucketId' => 'mock_s3_bucket',
    ];

    $this->pluginId = 's3_custom';
    $this->pluginDefinition = [
      'id' => $this->pluginId,
      'label' => 'Custom StreamWrapper',
      'description' => 'A fully customizable option',
      'class' => CustomS3StreamWrapperPlugin::class,
      'provider' => 'mock_s3_custom_streamwrapper',
    ];

    $config_mock_values = [
      'image.settings' => [
        'suppress_itok_output' => FALSE,
      ],
    ];
    $this->configFactoryMock = $this->getConfigFactoryStub($config_mock_values);

    /** @var  \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface&\PHPUnit\Framework\MockObject\MockObject $streamwraper_entity */
    $streamwraper_entity = $this->pluginConfig['#streamwrapper'];
    $streamwraper_entity->method('getStreamWrapperConfig')->willReturn([]);

  }

  /**
   * {@inheritdoc}
   */
  protected function setupPlugin(): CustomS3StreamWrapperPlugin {
    return new CustomS3StreamWrapperPlugin($this->pluginConfig, $this->pluginId, $this->pluginDefinition, $this->entityManagerMock, $this->moduleHandlerMock, $this->configFactoryMock, $this->getStringTranslationStub());
  }

}
