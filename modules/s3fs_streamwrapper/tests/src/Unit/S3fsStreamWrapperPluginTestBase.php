<?php

namespace Drupal\Tests\s3fs_streamwrapper\Unit;

use Aws\S3\S3Client;
use Aws\S3\S3ClientInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\image\ImageStyleInterface;
use Drupal\s3fs_client\S3ClientFactory;
use Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface;
use Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginInterface;

/**
 * Test base for StreamWrapper plugin testing.
 *
 * @group s3fs
 * @group s3fs_streamwrapper
 *
 * @covers \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginBase
 */
abstract class S3fsStreamWrapperPluginTestBase extends S3fsStreamWrapperUnitTestBase {

  /**
   * Base config for plugins.
   *
   * @var array
   */
  protected array $pluginConfig;

  /**
   * Id of plugin to test.
   *
   * @var string
   */
  protected string $pluginId;

  /**
   * Plugin definition.
   *
   * @var array
   */
  protected array $pluginDefinition;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $streamwrapper_entity_mock = $this->createMock(S3StreamWrapperEntityInterface::class);
    $streamwrapper_entity_mock->method('id')->willReturn('mock-streamwrapper');

    $this->pluginConfig = [
      '#streamwrapper' => $streamwrapper_entity_mock,
    ];

  }

  /**
   * Test basics functions of the plugin implementation.
   *
   * Tests the functions that do not require multiple scenarios to determine
   * functionality. Primarily those functions that return the same response
   * as what they were initialized with.
   *
   * In order to allow plugins to invoke their own logic we only validate
   * method form here.
   */
  public function testPluginBasics(): void {

    $plugin = $this->setupPlugin();

    $this->assertIsString($plugin->getPluginId(), 'PluginId name is a string');
    $this->assertNotEmpty($plugin->getPluginId(), "PluginId is not empty");

    $this->assertIsString($plugin->label(), 'Label is a string');
    $this->assertNotEmpty($plugin->label(), "label is not empty");

    $this->assertIsString($plugin->getDescription(), 'Bucket plugin description is a string');

    $plugin_running_config = $plugin->getConfiguration();
    foreach ($this->pluginConfig as $key => $value) {
      if ($key == '#streamwrapper') {
        continue;
      }
      $this->assertEquals($value, $plugin_running_config[$key], $key . ' is correctly set in plugin config.');
    }

    $this->assertTrue($plugin->getS3Client() instanceof S3ClientInterface);

  }

  /**
   * Tests getCommandParams().
   *
   * @dataProvider providerGetCommandParams
   */
  public function testGetCommandParams(array $new_config, string $uri, array $existing_params, string $expected_result): void {
    $this->pluginConfig = $new_config + $this->pluginConfig;
    $plugin = $this->setupPlugin();
    $result = $plugin->getCommandParams($uri, $existing_params);
    $this->assertArrayHasKey('Bucket', $result, 'Command params returns Bucket key');
    $this->assertEquals('mock-s3-bucket', $result['Bucket'], 'Bucket name is correctly set in command params');
  }

  /**
   * DataProvider for testGetCommandParams().
   *
   * @return array
   *   PhpUnit data provider array keyed by data set name or integer
   *     - array $new_config - Will be merged with $pluginConfig
   *     - string $uri - URI to test.
   *     - array $existing_params - Params to pass into getCommandParams.
   *     - array $expected_result - For use processing expected result.
   */
  abstract public function providerGetCommandParams(): array;

  /**
   * Tests convertUriToKeyedPath().
   *
   * @dataProvider providerConvertUriToKeyedPath
   */
  public function testconvertUriToKeyedPath(array $new_config, string $uri, bool $prepend_bucket, string $expected_result): void {
    $this->pluginConfig = $new_config + $this->pluginConfig;
    $plugin = $this->setupPlugin();
    $this->assertEquals($expected_result, $plugin->convertUriToKeyedPath($uri, $prepend_bucket));
  }

  /**
   * DataProvider for testConvertUriToKeyedPath().
   *
   * @return array
   *   PhpUnit data provider array keyed by data set name or integer
   *     - $new_config - Will be merged with $pluginConfig
   *     - $uri - URI to test.
   *     - $expected_result - For use processing expected result.
   */
  abstract public function providerConvertUriToKeyedPath(): array;

  /**
   * Tests getMaxUriLength.
   *
   * @dataProvider providerGetMaxUriLength
   */
  public function testGetMaxUriLength(array $new_config, int $expected_result): void {
    $this->pluginConfig = $new_config + $this->pluginConfig;
    $plugin = $this->setupPlugin();
    $this->assertEquals($expected_result, $plugin->getMaxUriLength(), 'Max length is as expected for configuration');
  }

  /**
   * DataProvider for testGetMaxUriLength().
   *
   * @return array
   *   PhpUnit data provider array keyed by data set name or integer
   *     - $new_config - Will be merged with $pluginConfig
   *     - $expected_result - Expected max path length.
   */
  public function providerGetMaxUriLength(): array {
    return [
      'No modification by StreamWrapper plugin, reduced by bucket' => [
        [],
        251,
      ],
    ];
  }

  /**
   * Tests getExternalUrl.
   *
   * @dataProvider providerGetExternalUrl
   */
  public function testGetExternalUrl(array $new_config, string $uri, bool $is_secure_request, string $expected_result): void {

    $this->setupUrlGeneratorMock();
    $this->setupUrlTestMocks();

    $this->pluginConfig = $new_config + $this->pluginConfig;
    $plugin = $this->setupPlugin();
    $this->assertEquals($expected_result, $plugin->getExternalUrl($uri, $is_secure_request), 'getExternalUrl returned expected string');
  }

  /**
   * DataProvider for testGetExternalUrl().
   *
   * @return array
   *   PhpUnit data provider array keyed by data set name or integer
   *     - $new_config - Will be merged with $pluginConfig
   *     - string $uri - URI to proccess
   *     - bool $is_secure_request - Should the URI be generated with https://.
   *     - string $expected_result - Expected result.
   */
  public function providerGetExternalUrl(): array {
    return [
      'No modification by StreamWrapper plugin' => [
        [],
        's3://test/object.txt',
        TRUE,
        'https://mock-s3-bucket.s3.amazonaws.com/test/object.txt',
      ],
      'Bucket altered pathc' => [
        [],
        's3://test/ALTER_ME/object.txt',
        TRUE,
        'https://mock-s3-bucket.s3.amazonaws.com/test/altered/object.txt',
      ],
      'Image Style, derivative already exists' => [
        [],
        's3://styles/mock_image_style/s3/object.jpg',
        TRUE,
        'https://mock-s3-bucket.s3.amazonaws.com/styles/mock_image_style/s3/object.jpg',
      ],
      'Image Style, derivative does not yet exist' => [
        [],
        's3://styles/mock_image_style/s3/NON_EXISTENT/object.jpg',
        TRUE,
        '/s3/styles/mock_image_style/s3/NON_EXISTENT/object.jpg',
      ],
      'Image Style does not exists' => [
        [],
        's3://styles/image_style_does_not_exist/object.jpg',
        TRUE,
        'https://mock-s3-bucket.s3.amazonaws.com/styles/image_style_does_not_exist/object.jpg',
      ],
    ];
  }

  /**
   * Tests stat().
   *
   * @dataProvider providerStat
   */
  public function testStat(array $new_config, string $uri): void {
    $this->pluginConfig = $new_config + $this->pluginConfig;
    $plugin = $this->setupPlugin();

    $result = $plugin->stat($uri);

    if (str_contains($uri, 'NON_EXISTENT')) {
      $this->assertFalse($result);
    }

    $this->assertIsArray($result, 'Result is an array');
    $this->assertArrayHasKey('dev', $result, 'Stat returns dev key');
    $this->assertArrayHasKey(0, $result, 'Stat returns 0 key');
    $this->assertArrayHasKey('ino', $result, 'Stat returns ino key');
    $this->assertArrayHasKey(1, $result, 'Stat returns 1 key');
    $this->assertArrayHasKey('mode', $result, 'Stat returns mode key');
    $this->assertArrayHasKey(2, $result, 'Stat returns 2 key');
    $this->assertArrayHasKey('nlink', $result, 'Stat returns nlink key');
    $this->assertArrayHasKey(3, $result, 'Stat returns 3 key');
    $this->assertArrayHasKey('uid', $result, 'Stat returns gid key');
    $this->assertArrayHasKey(4, $result, 'Stat returns 4 key');
    $this->assertArrayHasKey('gid', $result, 'Stat returns gid key');
    $this->assertArrayHasKey(5, $result, 'Stat returns 5 key');
    $this->assertArrayHasKey('rdev', $result, 'Stat returns rdev key');
    $this->assertArrayHasKey(6, $result, 'Stat returns 6 key');
    $this->assertArrayHasKey('size', $result, 'Stat returns size key');
    $this->assertArrayHasKey(7, $result, 'Stat returns 7 key');
    $this->assertArrayHasKey('atime', $result, 'Stat returns atime akey');
    $this->assertArrayHasKey(8, $result, 'Stat returns 8 key');
    $this->assertArrayHasKey('mtime', $result, 'Stat returns mtime key');
    $this->assertArrayHasKey(9, $result, 'Stat returns 9 key');
    $this->assertArrayHasKey('ctime', $result, 'Stat returns ctime key');
    $this->assertArrayHasKey(10, $result, 'Stat returns 10 key');
    $this->assertArrayHasKey('blksize', $result, 'Stat returns blksize key');
    $this->assertArrayHasKey(11, $result, 'Stat returns 11 key');
    $this->assertArrayHasKey('blocks', $result, 'Stat returns blocks key');
    $this->assertArrayHasKey(12, $result, 'Stat returns 12 key');

    // Make sure Index keys match associative key.
    $this->assertEquals($result[0], $result['dev'], 'Stat associative key dev and index key matches');
    $this->assertEquals($result[1], $result['ino'], 'Stat associative key ino and index key matches');
    $this->assertEquals($result[2], $result['mode'], 'Stat associative key mode and index key matches');
    $this->assertEquals($result[3], $result['nlink'], 'Stat associative key nlink and index key matches');
    $this->assertEquals($result[4], $result['uid'], 'Stat associative key uid and index key matches');
    $this->assertEquals($result[5], $result['gid'], 'Stat associative key gid and index key matches');
    $this->assertEquals($result[6], $result['rdev'], 'Stat associative key rdev and index key matches');
    $this->assertEquals($result[7], $result['size'], 'Stat associative key size and index key matches');
    $this->assertEquals($result[8], $result['atime'], 'Stat associative key atime and index key matches');
    $this->assertEquals($result[9], $result['mtime'], 'Stat associative key mtime and index key matches');
    $this->assertEquals($result[10], $result['ctime'], 'Stat associative key ctime and index key matches');
    $this->assertEquals($result[11], $result['blksize'], 'Stat associative key blksize and index key matches');
    $this->assertEquals($result[12], $result['blocks'], 'Stat associative key blocks and index key matches');

    if (str_contains($uri, 'IS_DIR') || str_ends_with($uri, '://')) {
      $this->assertTrue((bool) ($result['mode'] & 0040000), "Stat returns correct mode for directory");
    }
    else {
      $this->assertTrue((bool) ($result['mode'] & 0100000), "Stat returns correct mode for a file");
    }

  }

  /**
   * DataProvider for testStat().
   *
   * @return array
   *   PhpUnit data provider array keyed by data set name or integer
   *     - $new_config - Will be merged with $pluginConfig
   *     - string $uri - URI to proccess
   *     - bool $is_secure_request - Should the URI be generated with https://.
   *     - string $expected_result - Expected result.
   */
  public function providerStat(): array {
    return [
      'File' => [
        [],
        's3://path/to/file',
      ],
      'Directory' => [
        [],
        's3://path/to/IS_DIR',
      ],
      'Directory with trailing slash' => [
        [],
        's3://path/to/IS_DIR/',
      ],
      'File path that may be altered' => [
        [],
        's3://path/to/ALTER_ME/file',
      ],
      'No target' => [
        [],
        's3://',
      ],
    ];
  }

  /**
   * Tests isDir().
   *
   * @dataProvider providerIsDir
   */
  public function testIsDir(array $new_config, string $uri): void {
    $this->pluginConfig = $new_config + $this->pluginConfig;
    $plugin = $this->setupPlugin();

    $result = $plugin->isDir($uri);

    if (str_contains($uri, 'NON_EXISTENT')) {
      $this->assertFalse($result);
    }

    if (str_contains($uri, 'IS_DIR')) {
      $this->assertTrue($result);
    }
    else {
      $this->assertFalse($result);
    }

  }

  /**
   * DataProvider for testDir().
   *
   * @return array
   *   PhpUnit data provider array keyed by data set name or integer
   *     - array $new_config - Will be merged with $pluginConfig
   *     - string $uri - URI to process
   */
  public function providerIsDir(): array {
    return [
      'File' => [
        [],
        's3://path/to/file',
      ],
      'Directory' => [
        [],
        's3://path/to/IS_DIR',
      ],
      'Directory with trailing slash' => [
        [],
        's3://path/to/IS_DIR/',
      ],
      'File path that may be altered' => [
        [],
        's3://path/to/ALTER_ME/file',
      ],
      'File path that is non existent' => [
        [],
        's3://path/to/NON_EXISTENT/file',
      ],
    ];
  }

  /**
   * Tests getStreamOptions().
   *
   * @dataProvider providerGetStreamOptions
   */
  public function testGetStreamOptions(array $new_config, array $options, array $expected_result): void {
    $this->pluginConfig = $new_config + $this->pluginConfig;
    $plugin = $this->setupPlugin();

    $plugin->getStreamOptions($options);

    $this->assertEquals($expected_result, $options, 'Stream options match expected results');

  }

  /**
   * DataProvider for testGetStreamOptions().
   *
   * @return array
   *   PhpUnit data provider array keyed by data set name or integer
   *     - array $new_config - Will be merged with $pluginConfig
   *     - array $options - Existing options to be passed to getStreamOptions()
   *     - array $expected_result - Expected resulting data.
   */
  abstract public function providerGetStreamOptions(): array;

  /**
   * Tests listDir().
   *
   * @dataProvider providerListDir
   */
  public function testListDir(array $new_config, string $uri, array $expected_result): void {
    $this->bucketEntityMock->method('listDir')
      ->with($this->logicalNot($this->stringContains('://')))
      ->willReturnCallback([__CLASS__, 'callbackListDir']);

    $this->pluginConfig = $new_config + $this->pluginConfig;
    $plugin = $this->setupPlugin();

    $this->assertEquals($expected_result, $plugin->listDir($uri), 'Directory lists returns correct results');
  }

  /**
   * DataProvider for testListDir().
   *
   * @return array
   *   PhpUnit data provider array keyed by data set name or integer
   *     - array $new_config - Will be merged with $pluginConfig
   *     - string $uri - URI to process
   *     - array $expected_result - Expected resulting data.
   */
  public function providerListDir(): array {
    return [
      'Path lists director with no options' => [
        [],
        's3://path/to/directory',
        [
          's3://path/to/directory/object_1',
          's3://path/to/directory/object_2',
          's3://path/to/directory/object_3',
        ],
      ],
      'Path lists director with root_folder' => [
        [
          'root_folder' => 'my/root/folder',
        ],
        's3://path/to/directory',
        [
          's3://path/to/directory/object_1',
          's3://path/to/directory/object_2',
          's3://path/to/directory/object_3',
        ],
      ],
    ];
  }

  /**
   * Tests isDirEmpty().
   */
  public function testIsDirEmpty(): void {
    $plugin = $this->setupPlugin();

    $this->bucketEntityMock->method('isDirEmpty')
      ->with($this->logicalNot($this->stringContains('://')))
      ->willReturnOnConsecutiveCalls(FALSE, TRUE);

    $this->assertFalse($plugin->isDirEmpty('s3://path/to/full/directory'), 'isDirEmpty() correctly reports bucket not empty');
    $this->assertTrue($plugin->isDirEmpty('s3://path/to/full/directory'), 'isDirEmpty() correctly reports bucket empty');
  }

  /**
   * Set up the plugin under test.
   *
   * This function should be replaced by extenders to set up the real plugin.
   *
   * @return \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginInterface
   *   An instance of the plugin type under test.
   */
  abstract protected function setupPlugin(): S3StreamWrapperPluginInterface;

  /**
   * Setup common mocks for testing url generation.
   */
  protected function setupUrlTestMocks(): void {
    if ($this->s3ClientMock == NULL) {
      // Client not provided by parent class.
      $client_config = [
        'credentials' => [
          'key' => 'invalid_access_key',
          'secret' => 'invalid_secret_key',
        ],
        'region' => 'us-east-1',
        'version' => S3ClientFactory::DEFAULT_API_VERSION,
      ];
      $this->s3ClientMock = new S3Client($client_config);
    }

    // Rebuild the Bucket Entity Mock with a real S3Client for URL Generation.
    $this->bucketEntityMock = $this->getS3BucketEntityMock();
    $bucket_storage_mock = $this->createMock(EntityStorageInterface::class);
    $bucket_storage_mock->method('load')->with('mock_s3_bucket')->willReturn($this->bucketEntityMock);

    $image_style_mock = $this->createMock(ImageStyleInterface::class);
    $image_style_mock->method('getPathToken')->willReturn('123456');
    $image_style_storage_mock = $this->createMock(EntityStorageInterface::class);
    $image_style_storage_mock->method('load')->willReturnMap(
      [
        ['mock_image_style', $image_style_mock],
      ]
    );

    $this->entityManagerMock = $this->createMock(EntityTypeManagerInterface::class);
    $this->entityManagerMock->method('getStorage')->willReturnMap(
      [
        ['s3fs_bucket', $bucket_storage_mock],
        ['image_style', $image_style_storage_mock],
      ]
    );

  }

  /**
   * Configure and registers a mock url_generator service.
   */
  protected function setupUrlGeneratorMock(): void {
    $callback = function (string $route_name, array $route_parameters, array $options) {
      $path = '';
      switch ($route_name) {
        case 's3fs_streamwrapper.files.linkgen':
          $path = "/s3/files/{$route_parameters['scheme']}/{$route_parameters['file']}";
          break;

        case 's3fs_streamwrapper.image_styles.linkgen':
          $path = "/s3/styles/{$route_parameters['image_style']}/{$route_parameters['scheme']}/{$route_parameters['file']}";
          break;

        case '<front>':
          $path = '/';
          break;
      }

      return $path;
    };

    $url_generator_mock = $this->createMock(UrlGeneratorInterface::class);
    $url_generator_mock->method('generateFromRoute')->willReturnCallback($callback);
    $container = new ContainerBuilder();
    $container->set('url_generator', $url_generator_mock);
    \Drupal::setContainer($container);

  }

}
