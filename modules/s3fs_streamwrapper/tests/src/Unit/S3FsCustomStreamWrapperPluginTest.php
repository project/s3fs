<?php

namespace Drupal\Tests\s3fs_streamwrapper\Unit;

use Aws\S3\S3Client;
use Drupal\s3fs_client\S3ClientFactory;
use Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapper\CustomS3StreamWrapperPlugin;
use Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginInterface;

/**
 * Validate the S3fsCustomStreamWrapperPlugin.
 *
 * @group s3fs
 * @group s3fs_streamwrapper
 *
 * @covers \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapper\CustomS3StreamWrapperPlugin
 * @covers \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginBase
 */
class S3FsCustomStreamWrapperPluginTest extends S3fsStreamWrapperPluginTestBase {

  /**
   * The config factory mock.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject&\Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactoryMock;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->pluginConfig += [
      'bucketId' => 'mock_s3_bucket',
    ];

    $this->pluginId = 's3_custom';
    $this->pluginDefinition = [
      'id' => $this->pluginId,
      'label' => 'Custom StreamWrapper',
      'description' => 'A fully customizable option',
      'class' => CustomS3StreamWrapperPlugin::class,
      'provider' => 'mock_s3_custom_streamwrapper',
    ];

    $config_mock_values = [
      'image.settings' => [
        'suppress_itok_output' => FALSE,
      ],
    ];
    $this->configFactoryMock = $this->getConfigFactoryStub($config_mock_values);

  }

  /**
   * {@inheritdoc}
   */
  public function providerGetMaxUriLength(): array {
    return parent::providerGetMaxUriLength() + [
      'Root folder reduces max lenght' => [
        ['root_folder' => 'test_folder'],
        239,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function providerGetExternalUrl(): array {
    return parent::providerGetExternalUrl() + [
      'Use HTTPS False and secure request' => [
        [
          'use_https' => FALSE,
        ],
        's3://test/object.txt',
        TRUE,
        'https://mock-s3-bucket.s3.amazonaws.com/test/object.txt',
      ],
      'Use HTTPS FALSE and not secure request' => [
        [
          'use_https' => FALSE,
        ],
        's3://test/object.txt',
        FALSE,
        'http://mock-s3-bucket.s3.amazonaws.com/test/object.txt',
      ],
      'Use Drupal Delivery' => [
        [
          'use_drupal' => TRUE,
        ],
        's3://test/object.txt',
        TRUE,
        '/s3/files/s3/test/object.txt',
      ],
      'Use Drupal Delivery with root_folder' => [
        [
          'use_drupal' => TRUE,
          'root_folder' => 'folder_prefix',
        ],
        's3://test/object.txt',
        TRUE,
        '/s3/files/s3/test/object.txt',
      ],
      'Use CNAME' => [
        [
          'use_cname' => TRUE,
          'cname' => 'mock-s3-bucket.example.com',
        ],
        's3://test/object.txt',
        TRUE,
        'https://mock-s3-bucket.example.com/test/object.txt',
      ],
      'Use CNAME with port' => [
        [
          'use_cname' => TRUE,
          'cname' => 'mock-s3-bucket.example.com:8443',
        ],
        's3://test/object.txt',
        TRUE,
        'https://mock-s3-bucket.example.com:8443/test/object.txt',
      ],
      'CNAME, use_https TRUE, not secure request' => [
        [
          'use_https' => TRUE,
          'use_cname' => TRUE,
          'cname' => 'mock-s3-bucket.example.com',
        ],
        's3://test/object.txt',
        FALSE,
        'https://mock-s3-bucket.example.com/test/object.txt',
      ],
      'CNAME, use_https FALSE, secure request' => [
        [
          'use_https' => FALSE,
          'use_cname' => TRUE,
          'cname' => 'mock-s3-bucket.example.com',
        ],
        's3://test/object.txt',
        TRUE,
        'https://mock-s3-bucket.example.com/test/object.txt',
      ],
      'CNAME, use_https false, insecure request' => [
        [
          'use_https' => FALSE,
          'use_cname' => TRUE,
          'cname' => 'mock-s3-bucket.example.com',
        ],
        's3://test/object.txt',
        FALSE,
        'http://mock-s3-bucket.example.com/test/object.txt',
      ],
      'CNAME, with altered key path' => [
        [
          'use_cname' => TRUE,
          'cname' => 'mock-s3-bucket.example.com',
        ],
        's3://test/ALTER_ME/object.txt',
        TRUE,
        'https://mock-s3-bucket.example.com/test/altered/object.txt',
      ],
      'CNAME with root_folder' => [
        [
          'root_folder' => 'my_root_folder',
          'use_cname' => TRUE,
          'cname' => 'mock-s3-bucket.example.com',
        ],
        's3://test/object.txt',
        TRUE,
        'https://mock-s3-bucket.example.com/my_root_folder/test/object.txt',
      ],
      'CNAME with domain root stripping' => [
        [
          'domain_root' => 'root',
          'root_folder' => 'my_root_folder',
          'use_cname' => TRUE,
          'cname' => 'mock-s3-bucket.example.com',
        ],
        's3://test/object.txt',
        TRUE,
        'https://mock-s3-bucket.example.com/test/object.txt',
      ],
      'CNAME with domain root stripping but no root_folder' => [
        [
          'domain_root' => 'root',
          'use_cname' => TRUE,
          'cname' => 'mock-s3-bucket.example.com',
        ],
        's3://test/object.txt',
        TRUE,
        'https://mock-s3-bucket.example.com/test/object.txt',
      ],
    ];
  }

  /**
   * Tests additional getExternalUrl() calls.
   *
   * @dataProvider providerAdditionalGetExternalUrl
   */
  public function testPresignedGetExternalUrl(array $new_config, string $uri, bool $is_secure_request, int $expect_presigned, string $expected_result): void {

    $this->setupUrlGeneratorMock();

    $client_config = [
      'credentials' => [
        'key' => 'invalid_access_key',
        'secret' => 'invalid_secret_key',
      ],
      'region' => 'us-east-1',
      'version' => S3ClientFactory::DEFAULT_API_VERSION,
    ];
    $this->s3ClientMock = new S3Client($client_config);

    $this->setupUrlTestMocks();

    $this->pluginConfig = $new_config + $this->pluginConfig;
    $plugin = $this->setupPlugin();

    $result = $plugin->getExternalUrl($uri, $is_secure_request);
    $this->assertStringStartsWith($expected_result, $result);

    if ($expect_presigned == 0) {
      return;
    }

    $this->assertStringContainsString("X-Amz-Expires={$expect_presigned}", $result);
    $this->assertStringContainsString('X-Amz-Signature', $result);
    $this->assertStringNotContainsString('itok', $result, 'itok should always be stripped');

  }

  /**
   * DataProvider for testGetExternalUrl().
   *
   * @return array
   *   PhpUnit data provider array keyed by data set name or integer
   *     - $new_config - Will be merged with $pluginConfig
   *     - string $uri - URI to proccess
   *     - bool $is_secure_request - Should the URI be generated with https://.
   *     - int $expect_presigned - Expected timeout of link validity.
   *       0 if not signed.
   *     - string $expected_result - Expected result.
   */
  public function providerAdditionalGetExternalUrl(): array {
    return [
      'Presigning not matched ' => [
        [
          'presigned_urls' => '600|this/is/presigned/*',
        ],
        's3://test/object.txt',
        TRUE,
        0,
        'https://mock-s3-bucket.s3.amazonaws.com/test/object.txt',
      ],
      'Presigning matched ' => [
        [
          'presigned_urls' => "600|this/is/presigned/*",
        ],
        's3://this/is/presigned/object.txt',
        TRUE,
        600,
        'https://mock-s3-bucket.s3.amazonaws.com/this/is/presigned/object.txt',
      ],
      'Presigning with default time' => [
        [
          'presigned_urls' => "this/is/presigned/*",
        ],
        's3://this/is/presigned/object.txt',
        TRUE,
        60,
        'https://mock-s3-bucket.s3.amazonaws.com/this/is/presigned/object.txt',
      ],
      'Presigning with cname' => [
        [
          'use_cname' => TRUE,
          'cname' => 'mock-s3-bucket.example.com:8443',
          'presigned_urls' => "600|this/is/presigned/*",
        ],
        's3://this/is/presigned/object.txt',
        TRUE,
        600,
        'https://mock-s3-bucket.example.com:8443/this/is/presigned/object.txt',
      ],
      'Saveas time configured by presigning ' => [
        [
          'presigned_urls' => "3600|this/is/presigned/*",
          'saveas' => "this/is/presigned/saveas/*",
        ],
        's3://this/is/presigned/saveas/object.txt',
        TRUE,
        3600,
        'https://mock-s3-bucket.s3.amazonaws.com/this/is/presigned/saveas/object.txt?response-content-disposition=attachment',
      ],
      'Saveas without presigning configured' => [
        [
          'saveas' => "saveas/*",
        ],
        's3://saveas/object.txt',
        TRUE,
        21600,
        'https://mock-s3-bucket.s3.amazonaws.com/saveas/object.txt?response-content-disposition=attachment',
      ],
      'Presigned image style path' => [
        [
          'presigned_urls' => "600|styles/*",
        ],
        's3://styles/mock_image_style/s3/object.txt',
        TRUE,
        600,
        'https://mock-s3-bucket.s3.amazonaws.com/styles/mock_image_style/s3/object.txt',
      ],
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function providerConvertUriToKeyedPath(): array {
    return [
      'Simple test' => [
        [],
        's3://test/1/ALTER_ME/3',
        FALSE,
        'test/1/altered/3',
      ],
      'Target with root_folder' => [
        [
          'root_folder' => 'my/root/folder',
        ],
        's3://test/1/2/3',
        FALSE,
        'my/root/folder/test/1/2/3',
      ],
      'Prepend bucket' => [
        [],
        's3://test/1/ALTER_ME/3',
        TRUE,
        'mock-s3-bucket/test/1/altered/3',
      ],
      'Empty target' => [
        [],
        's3://',
        FALSE,
        '',
      ],
      'Empty target with prepend bucket' => [
        [],
        's3://',
        TRUE,
        'mock-s3-bucket',
      ],
      'Empty target with root_folder' => [
        [
          'root_folder' => 'my/root/folder',
        ],
        's3://',
        FALSE,
        'my/root/folder',
      ],
      'Empty target with root_folder prepend bucket' => [
        [
          'root_folder' => 'my/root/folder',
        ],
        's3://',
        TRUE,
        'mock-s3-bucket/my/root/folder',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function providerGetCommandParams(): array {
    return [
      'Simple test' => [
        [],
        's3://test/1/ALTER_ME/3',
        [],
        'test/1/altered/3',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function providerGetStreamOptions(): array {
    return [
      'No options provided' => [
       [],
       [],
       [],
      ],
      'Unrelated paramater not modified' => [
       [],
       [
         'Tagging' => 'key1=value1&key2=value2',
       ],
       [
         'Tagging' => 'key1=value1&key2=value2',
       ],
      ],
      'Upload as private is set' => [
       [
         'upload_as_private' => TRUE,
       ],
       [
         'ACL' => 'Public-Read',
       ],
       [],
      ],
      'Upload as private is not set' => [
       [
         'upload_as_private' => FALSE,
       ],
       [
         'ACL' => 'public-read',
       ],
       [
         'ACL' => 'public-read',
       ],
      ],
      'Encryption option configured.' => [
       [
         'encryption' => 'aws:kms',
       ],
       [],
       [
         'ServerSideEncryption' => 'aws:kms',
       ],
      ],
    ];
  }

  /**
   * Tests something.
   */
  public function testSomething(): void {
    self::assertTrue(TRUE, 'This is TRUE!');
  }

  /**
   * {@inheritdoc}
   */
  protected function setupPlugin(): S3StreamWrapperPluginInterface {
    return new CustomS3StreamWrapperPlugin($this->pluginConfig, $this->pluginId, $this->pluginDefinition, $this->entityManagerMock, $this->moduleHandlerMock, $this->configFactoryMock, $this->getStringTranslationStub());
  }

}
