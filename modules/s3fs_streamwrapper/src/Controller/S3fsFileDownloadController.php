<?php

namespace Drupal\s3fs_streamwrapper\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * S3fs file controller.
 */
class S3fsFileDownloadController extends ControllerBase {

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * S3fsFileDownloadController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $streamWrapperManager
   *   The stream wrapper manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, StreamWrapperManagerInterface $streamWrapperManager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->streamWrapperManager = $streamWrapperManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('stream_wrapper_manager')
    );
  }

  /**
   * Handles private file transfers for S3FS.
   *
   * Call modules that implement hook_file_download() to find out if a file is
   * accessible and what headers it should be transferred with. If one or more
   * modules returned headers the download will start with the returned headers.
   * If a module returns -1 an AccessDeniedHttpException will be thrown. If the
   * file exists but no modules responded an AccessDeniedHttpException will be
   * thrown. If the file does not exist a NotFoundHttpException will be thrown.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param string $scheme
   *   The file scheme.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   The transferred file as response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Thrown when the requested file does not exist.
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Thrown when the user does not have access to the file.
   *
   * @see hook_file_download()
   */
  public function download(Request $request, string $scheme): BinaryFileResponse {
    $target = $request->query->get('file');
    // Merge remaining path arguments into relative file path.
    $uri = $this->streamWrapperManager->normalizeUri($scheme . '://' . $target);

    $streamWrapperEntity = $this->getStreamWrapperEntity($scheme);

    if (
      // The scheme is configured.
      !is_null($streamWrapperEntity)
      // The scheme is enabled.
      && $streamWrapperEntity->status()
      // The scheme requests Drupal deliver the content.
      && $streamWrapperEntity->useDrupalDelivery()
      // The file exists.
      && is_file($uri)
    ) {
      // Let other modules provide headers and controls access to the file.
      $headers = $this->moduleHandler()->invokeAll('file_download', [$uri]);

      foreach ($headers as $result) {
        if ($result == -1) {
          throw new AccessDeniedHttpException();
        }
      }

      if (count($headers)) {
        // \Drupal\Core\EventSubscriber\FinishResponseSubscriber::onRespond()
        // sets response as not cacheable if the Cache-Control header is not
        // already modified. We pass in FALSE for non-private schemes for the
        // $public parameter to make sure we don't change the headers.
        return new BinaryFileResponse($uri, 200, $headers, FALSE);
      }

      throw new AccessDeniedHttpException();
    }

    throw new NotFoundHttpException();
  }

  /**
   * Obtain a S3StreamWrapperEntity for a specific scheme.
   *
   * @param string $scheme
   *   Scheme to retreive entity for.
   *
   * @return \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface|null
   *   Statically cached S3StreamWrapper entity.
   */
  protected function getStreamWrapperEntity($scheme): ?S3StreamWrapperEntityInterface {
    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface|null $staticStreamWrapperEntity */
    $staticStreamWrapperEntity = &drupal_static('s3fs_streamwrapper_entity_' . $scheme);

    if ($staticStreamWrapperEntity) {
      return $staticStreamWrapperEntity;
    }

    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $streamWrapperEntity */
    $streamWrapperEntity = $this->entityTypeManager
      ->getStorage('s3fs_streamwrapper')
      ->load($scheme);

    return $streamWrapperEntity;
  }

}
