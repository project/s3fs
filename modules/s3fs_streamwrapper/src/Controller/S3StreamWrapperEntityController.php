<?php

namespace Drupal\s3fs_streamwrapper\Controller;

use Drupal\Core\Entity\Controller\EntityController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Returns responses for S3 Bucket routes.
 */
class S3StreamWrapperEntityController extends EntityController {

  /**
   * Provides a generic delete title callback.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Entity\EntityInterface $_entity
   *   (optional) An entity, passed in directly from the request attributes, and
   *   set in \Drupal\Core\Entity\Enhancer\EntityRouteEnhancer.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The title for the entity delete page.
   */
  public function getDisableTitle(RouteMatchInterface $route_match, EntityInterface $_entity = NULL): TranslatableMarkup {
    if ($entity = $this->doGetEntity($route_match, $_entity)) {
      return $this->t('Disable %label', ['%label' => $entity->label()]);
    }

    return $this->t("Disable streamWrapper");
  }

}
