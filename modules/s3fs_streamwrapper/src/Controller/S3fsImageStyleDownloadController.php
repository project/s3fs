<?php

namespace Drupal\s3fs_streamwrapper\Controller;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Site\Settings;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\image\ImageStyleInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

/**
 * Defines a controller to serve image styles delivered by S3.
 */
class S3fsImageStyleDownloadController extends S3fsFileDownloadController {

  /**
   * The lock backend.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The image factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected ImageFactory $imageFactory;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemINterface $fileSystem;

  /**
   * Constructs an ImageStyleDownloadController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock backend.
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   *   The image factory.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The system service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LockBackendInterface $lock, ImageFactory $image_factory, StreamWrapperManagerInterface $stream_wrapper_manager, FileSystemInterface $file_system) {
    parent::__construct($entity_type_manager, $stream_wrapper_manager);
    $this->lock = $lock;
    $this->imageFactory = $image_factory;
    $this->logger = $this->getLogger('image');
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('lock'),
      $container->get('image.factory'),
      $container->get('stream_wrapper_manager'),
      $container->get('file_system')
    );
  }

  /**
   * Generates a derivative stored in S3 given a style and image path.
   *
   * After generating an image, redirect it to the requesting agent. Only used
   * for public delivery. Private delivery use the normal workflow:
   * \Drupal\image\Controller\ImageStyleDownloadController::deliver().
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param string $scheme
   *   The file scheme.
   * @param \Drupal\image\ImageStyleInterface $image_style
   *   The image style to deliver.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse|\Symfony\Component\HttpFoundation\Response
   *   The redirect response or some error response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Thrown when the user does not have access to the file.
   * @throws \Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException
   *   Thrown when the file is still being generated.
   * @throws \Exception
   *   Thrown when an unrecoverable processing error occurs.
   *
   * @see \Drupal\image\Controller\ImageStyleDownloadController::deliver()
   */
  public function deliver(Request $request, $scheme, ImageStyleInterface $image_style): TrustedRedirectResponse|Response {
    $target = $request->query->get('file');
    if (!is_string($target) || empty($target)) {
      throw new NotFoundHttpException('No file requested');
    }
    $image_uri = $scheme . '://' . $target;
    $image_uri = $this->streamWrapperManager->normalizeUri($image_uri);

    if ($this->streamWrapperManager->isValidScheme($scheme)) {
      /** @var FALSE|string $normalized_target */
      $normalized_target = $this->streamWrapperManager->getTarget($image_uri);
      if ($normalized_target !== FALSE) {
        if (!in_array($scheme, Settings::get('file_sa_core_2023_005_schemes', []))) {
          $parts = explode('/', $normalized_target);
          if (array_intersect($parts, ['.', '..'])) {
            throw new NotFoundHttpException();
          }
        }
      }
    }

    $streamWrapperEntity = $this->getStreamWrapperEntity($scheme);

    // Scheme is not a s3fs stream or is disabled.
    if (is_null($streamWrapperEntity) || !$streamWrapperEntity->status()) {
      throw new AccessDeniedHttpException();
    }

    // Check that the style is defined and the scheme is valid.
    $valid = !empty($image_style) && $this->streamWrapperManager->isValidScheme($scheme);

    // Also validate the derivative token. Sites which require image
    // derivatives to be generated without a token can set the
    // 'image.settings:allow_insecure_derivatives' configuration to TRUE to
    // bypass this check, but this will increase the site's vulnerability
    // to denial-of-service attacks. To prevent this variable from leaving the
    // site vulnerable to the most serious attacks, a token is always required
    // when a derivative of a style is requested.
    // The $target variable for a derivative of a style has
    // styles/<style_name>/... as structure, so we check if the $target variable
    // starts with styles/.
    $token = $request->query->get(IMAGE_DERIVATIVE_TOKEN, '');
    $token_is_valid = hash_equals($image_style->getPathToken($image_uri), $token)
      || hash_equals($image_style->getPathToken($scheme . '://' . $target), $token);
    if (!$this->config('image.settings')->get('allow_insecure_derivatives') || strpos(ltrim($target, '\/'), 'styles/') === 0) {
      $valid = $valid && $token_is_valid;
    }

    if (!$valid) {
      throw new AccessDeniedHttpException();
    }

    $derivative_uri = $image_style->buildUri($image_uri);

    // Don't try to generate file if source is missing.
    if (!file_exists($image_uri)) {
      // If the image st yle converted the extension, it has been added to the
      // original file, resulting in filenames like image.png.jpeg. So to find
      // the actual source image, we remove the extension and check if that
      // image exists.
      $target = StreamWrapperManager::getTarget($image_uri);
      if (is_bool($target)) {
        throw new NotFoundHttpException("Unable to determine requested file");
      }
      $path_info = pathinfo($target);
      if (empty($path_info['dirname']) || empty($path_info['filename'])) {
        throw new NotFoundHttpException("Unable to determine requested file");
      }
      $converted_image_uri = sprintf('%s://%s/%s', $this->streamWrapperManager->getScheme($derivative_uri), $path_info['dirname'], $path_info['filename']);
      if (!file_exists($converted_image_uri)) {
        $this->logger->notice('Source image at %source_image_path not found while trying to generate derivative image at %derivative_path.',
          [
            '%source_image_path' => $image_uri,
            '%derivative_path' => $derivative_uri,
          ]
        );
        return new Response($this->t('Error generating image, missing source file.'), 404);
      }
      else {
        // The converted file does exist, use it as the source.
        $image_uri = $converted_image_uri;
      }
    }

    $headers = [];

    if ($streamWrapperEntity->useDrupalDelivery()) {
      // Validate user access.
      $headers = $this->moduleHandler()->invokeAll('file_download', [$image_uri]);
      if (in_array(-1, $headers) || empty($headers)) {
        throw new AccessDeniedHttpException();
      }
    }

    // Don't start generating the image if the derivative already exists or if
    // generation is in progress in another thread.
    $lock_name = 's3fs_image_style_deliver:' . $image_style->id() . ':' . Crypt::hashBase64($image_uri);
    if (!file_exists($derivative_uri)) {
      $lock_acquired = $this->lock->acquire($lock_name);
      if (!$lock_acquired) {
        // Tell client to retry again in 3 seconds. Currently no browsers are
        // known to support Retry-After.
        throw new ServiceUnavailableHttpException(3, 'Image generation in progress. Try again shortly.');
      }
    }

    // Try to generate the image, unless another thread just did it while we
    // were acquiring the lock.
    $success = file_exists($derivative_uri);

    if (!$success) {
      $success = $image_style->createDerivative($image_uri, $derivative_uri);
    }

    if (!empty($lock_acquired)) {
      $this->lock->release($lock_name);
    }

    if ($success) {

      // Private streams are not served from the bucket.
      if ($streamWrapperEntity->useDrupalDelivery()) {
        $image = $this->imageFactory->get($derivative_uri);
        $uri = $image->getSource();
        $headers['Content-Type'] = $image->getMimeType();
        $headers['Content-Length'] = $image->getFileSize();

        // \Drupal\Core\EventSubscriber\FinishResponseSubscriber::onRespond()
        // sets response as not cacheable if the Cache-Control header is not
        // already modified. We pass in FALSE for non-private schemes for the
        // $public parameter to make sure we don't change the headers.
        return new BinaryFileResponse($uri, 200, $headers, FALSE);
      }

      $responseCacheTags = $image_style->getCacheTags();

      // Try to get a managed file and flush the cache.
      /** @var \Drupal\file\FileStorageInterface $storage */
      $storage = $this->entityTypeManager
        ->getStorage('file');
      $result = $storage
        ->getQuery()
        ->accessCheck(FALSE)
        ->condition('uri', $image_uri, '=')
        ->execute();
      if (!empty($result)) {
        $file = $storage->load(reset($result));
        if ($file) {
          Cache::invalidateTags($file->getCacheTags());
          $responseCacheTags = Cache::mergeTags($responseCacheTags, $file->getCacheTags());
        }
      }

      // Perform a 302 Redirect to the new image derivative in S3.
      // It must be TrustedRedirectResponse for external redirects.
      $response = new TrustedRedirectResponse($image_style->buildUrl($image_uri));
      $cacheableMetadata = $response->getCacheableMetadata();
      $cacheableMetadata->addCacheContexts(
        [
          'url.query_args:file',
          'url.query_args:itok',
        ]
      );
      $cacheableMetadata->setCacheMaxAge((int) $this->config('s3fs.settings')->get('redirect_styles_ttl'));
      $cacheableMetadata->setCacheTags($responseCacheTags);
      $response->addCacheableDependency($cacheableMetadata);
      return $response;
    }
    else {
      $this->logger->notice('Unable to generate the derived image located at %path.', ['%path' => $derivative_uri]);
      return new Response($this->t('Error generating image.'), 500);
    }
  }

}
