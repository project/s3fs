<?php

namespace Drupal\s3fs_streamwrapper\Entity;

use Aws\S3\S3ClientInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\s3fs_bucket\S3BucketInterface;
use Drupal\s3fs_bucket\S3Exception;
use Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginInterface;
use Drupal\s3fs_streamwrapper\Traits\S3fsPathsTrait;

/**
 * Defines the S3 StreamWrapper Config entity.
 *
 * @ConfigEntityType(
 *   id = "s3fs_streamwrapper",
 *   label = @Translation("S3fs StreamWrapper Config"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\s3fs_streamwrapper\S3StreamWrapperEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\s3fs_streamwrapper\Form\S3StreamWrapperEntityForm",
 *       "edit" = "Drupal\s3fs_streamwrapper\Form\S3StreamWrapperEntityForm",
 *       "delete" = "Drupal\s3fs_streamwrapper\Form\S3StreamWrapperEntityDeleteForm",
 *       "disable" = "Drupal\s3fs_streamwrapper\Form\S3StreamWrapperEntityDisableConfirmForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\s3fs_streamwrapper\S3StreamWrapperEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "s3fs_streamwrapper",
 *   admin_permission = "administer s3fs_streamwrapper",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "status" = "status"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/s3/streamwrapper/{s3fs_streamwrapper}",
 *     "add-form" = "/admin/config/s3/streamwrapper/add",
 *     "edit-form" = "/admin/config/s3/streamwrapper/{s3fs_streamwrapper}/edit",
 *     "delete-form" = "/admin/config/s3/streamwrapper/{s3fs_streamwrapper}/delete",
 *     "disable-form"     = "/admin/config/s3/streamwrapper/{s3fs_streamwrapper}/disable",
 *     "collection" = "/admin/config/s3/streamwrapper"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "streamwrapper_type",
 *     "streamwrapper_config"
 *   }
 * )
 *
 * @method string id()
 */
class S3StreamWrapperEntity extends ConfigEntityBase implements S3StreamWrapperEntityInterface {

  use S3fsPathsTrait;

  /**
   * The StreamWrapper description.
   *
   * @var string
   */
  protected $description = '';

  /**
   * The S3 StreamWrapper Config ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The S3 StreamWrapper Config label.
   *
   * @var string
   */
  protected $label;

  /**
   * The name of the S3 StreamWrapper Plugin for this entity.
   *
   * @var string
   */
  protected $streamwrapper_type;

  /**
   * Settings for this bucket set during plugin configuration.
   *
   * @var array
   */
  protected $streamwrapper_config = [];

  /**
   * The bucket plugin instance.
   *
   * @var \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginInterface|null
   */
  protected $streamWrapperPlugin = NULL;

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function hasValidStreamWrapperPlugin(): bool {

    if ($this->getPluginId() == NULL) {
      return FALSE;
    }

    $backend_plugin_definition = \Drupal::service('plugin.manager.s3fs_s3streamwrapper')->getDefinition($this->getPluginId(), FALSE);
    return !empty($backend_plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId(): ?string {
    return $this->streamwrapper_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getStreamWrapperPlugin(): S3StreamWrapperPluginInterface {
    if (!$this->streamWrapperPlugin) {
      /** @var \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginManager $streamWrapperPluginManager */
      $streamWrapperPluginManager = \Drupal::service('plugin.manager.s3fs_s3streamwrapper');
      $config = $this->streamwrapper_config;
      $config['#streamwrapper'] = $this;
      if ($this->getPluginId() == NULL) {
        throw new S3Exception("No plugin ID specified.");

      }
      try {
        $this->streamWrapperPlugin = $streamWrapperPluginManager->createInstance($this->getPluginId(), $config);
      }
      catch (PluginException $e) {
        $pluginId = $this->getPluginId();
        $label = $this->label();
        throw new S3Exception("The plugin with ID '$pluginId' could not be retrieved for StreamWrapper '$label'.");
      }
    }
    return $this->streamWrapperPlugin;
  }

  /**
   * {@inheritdoc}
   */
  public function getStreamWrapperConfig(): array {
    return $this->streamwrapper_config;
  }

  /**
   * {@inheritdoc}
   */
  public function setStreamWrapperConfig(array $streamWrapperConfig): static {
    $this->streamwrapper_config = $streamWrapperConfig;
    // In case the bucket plugin is already loaded, make sure the configuration
    // stays in sync.
    if ($this->streamWrapperPlugin
      && $this->getStreamWrapperPlugin()->getConfiguration() !== $streamWrapperConfig) {
      $this->getStreamWrapperPlugin()->setConfiguration($streamWrapperConfig);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {

    if (!$this->isNew()) {
      // Some updates are always disallowed.
      if ($this->id != $this->getOriginalId()) {
        throw new S3Exception("Cannot change an existing bucket's machine name.");
      }
    }

    parent::preSave($storage);
  }

  /*
   * The following functions are from S3StreamWrapperPluginInterface
   */

  /**
   * {@inheritdoc}
   */
  public function getS3Client(): S3ClientInterface {
    return $this->getStreamWrapperPlugin()->getS3Client();
  }

  /**
   * {@inheritdoc}
   */
  public function getCommandParams(string $uri, array $params = []): array {
    $uri = $this->resolvePath($uri);
    return $this->getStreamWrapperPlugin()->getCommandParams($uri, $params);
  }

  /**
   * {@inheritdoc}
   */
  public function getMaxUriLength(): int {
    return $this->getStreamWrapperPlugin()->getMaxUriLength();
  }

  /**
   * Drupal StreamWrapper functions.
   */

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl(string $uri, bool $isSecureRequest = FALSE): string {
    $uri = $this->resolvePath($uri);
    return $this->getStreamWrapperPlugin()->getExternalUrl($uri, $isSecureRequest);
  }

  /**
   * Helper functions for normal StreamWrappers.
   */

  /**
   * {@inheritdoc}
   */
  public function stat(string $uri): array|FALSE {
    $uri = $this->resolvePath($uri);
    return $this->getStreamWrapperPlugin()->stat($uri);
  }

  /**
   * {@inheritdoc}
   */
  public function isDir(string $uri): bool {
    $uri = $this->resolvePath($uri);
    return $this->getStreamWrapperPlugin()->isDir($uri);
  }

  /**
   * {@inheritdoc}
   */
  public function getStreamOptions(array &$options): void {
    $this->getStreamWrapperPlugin()->getStreamOptions($options);
  }

  /**
   * {@inheritdoc}
   */
  public function listDir(string $uri): array {
    $uri = $this->resolvePath($uri);
    return $this->getStreamWrapperPlugin()->listDir($uri);
  }

  /**
   * {@inheritdoc}
   */
  public function isDirEmpty(string $uri): bool {
    $uri = $this->resolvePath($uri);
    return $this->getStreamWrapperPlugin()->isDirEmpty($uri);
  }

  /**
   * {@inheritdoc}
   */
  public function convertUriToKeyedPath(string $uri, bool $prepend_bucket = TRUE): string {
    $uri = $this->resolvePath($uri);
    return $this->getStreamWrapperPlugin()->convertUriToKeyedPath($uri, $prepend_bucket);
  }

  /**
   * Metadata Cache related functions.
   */

  /**
   * {@inheritdoc}
   */
  public function convertMetadata(string $uri, array $metadata): array {
    $uri = $this->resolvePath($uri);
    return $this->getStreamWrapperPlugin()->convertMetadata($uri, $metadata);
  }

  /**
   * {@inheritdoc}
   */
  public function getS3Metadata(string $uri): array|false {
    $uri = $this->resolvePath($uri);
    return $this->getStreamWrapperPlugin()->getS3Metadata($uri);
  }

  /**
   * {@inheritdoc}
   */
  public function writeCache(array $metadata): void {
    $this->getStreamWrapperPlugin()->writeCache($metadata);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteCache($uri): int {
    $uri = $this->resolvePath($uri);
    return $this->getStreamWrapperPlugin()->deleteCache($uri);
  }

  /**
   * {@inheritdoc}
   */
  public function useDrupalDelivery(): bool {
    return $this->getStreamWrapperPlugin()->useDrupalDelivery();
  }

  /**
   * Additional functions.
   */

  /**
   * {@inheritdoc}
   */
  public function getBucket(): S3BucketInterface {
    return $this->getStreamWrapperPlugin()->getBucket();
  }

}
