<?php

namespace Drupal\s3fs_streamwrapper;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\s3fs_streamwrapper\StreamWrapper\S3fsBucketStream;
use Symfony\Component\DependencyInjection\Definition;

/**
 * Registers overrides to allow streamWrapper operations.
 */
class S3fsStreamwrapperServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {

    // We use config.storage.active as it is available before
    // config.storage. This early in the bootstrap service availability
    // is limited. We have seen config.storage be available
    // for s3fs at this time, however conflicting testing with flysystem and
    // in local dev lab has shown it may at times not be present.
    /** @var \Drupal\Core\Config\StorageInterface $config_storage */
    $config_storage = $container->get('config.storage.active');
    $entity_config_names = $config_storage->listAll('s3fs_streamwrapper.s3fs_streamwrapper');
    if(empty($entity_config_names)) {
      return;
    }

    // DatabaseStorage::readMultiple() errors on empty arrays
    // @see https://www.drupal.org/project/drupal/issues/3416525
    $configured_entities = $config_storage->readMultiple($entity_config_names);

    foreach ($configured_entities as $key) {
      if (empty($key)) {
        // This should never happen, however if it does skip this entry.
        continue;
      }

      if ($key['status'] !== TRUE) {
        // Scheme is not enabled.
        continue;
      }

      $service_definition = new Definition(S3fsBucketStream::class);
      $service_definition->setTags(
        [
          'stream_wrapper' => [
            [
              'scheme' => $key['id'],
            ],
          ],
        ],
      );
      $service_definition->setPublic(TRUE);
      $container->setDefinition('stream_wrapper.' . $key['id'], $service_definition);

    }

  }

}
