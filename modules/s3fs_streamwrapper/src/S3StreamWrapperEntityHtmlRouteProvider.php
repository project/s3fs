<?php

namespace Drupal\s3fs_streamwrapper;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for S3 StreamWrapper Config entities.
 *
 * @see Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class S3StreamWrapperEntityHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $entity_type_id = $entity_type->id();

    if ($disable_route = $this->getDisableFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.disable_form", $disable_route);
    }

    // Provide your custom entity routes here.
    return $collection;
  }

  /**
   * Gets the disable form route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getDisableFormRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('disable-form')) {
      $entity_type_id = $entity_type->id();
      $link_template = $entity_type->getLinkTemplate('disable-form');
      if (is_bool($link_template)) {
        return NULL;
      }
      $route = new Route($link_template);
      $route
        ->addDefaults([
          '_entity_form' => "{$entity_type_id}.disable",
          '_title_callback' => '\Drupal\s3fs_streamwrapper\Controller\S3StreamWrapperEntityController::getDisableTitle',
        ])
        ->setRequirement('_entity_access', "{$entity_type_id}.delete")
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ])
        ->setOption('_admin_route', TRUE);

      return $route;
    }
    return NULL;

  }

}
