<?php

namespace Drupal\s3fs_streamwrapper\Traits;

use Drupal\Core\StreamWrapper\StreamWrapperManager;

/**
 * S3fs path helper functions.
 *
 * @ingroup s3fs
 */
trait S3fsPathsTrait {

  /**
   * Resolve a (possibly) relative path to its non-relative form.
   *
   * Based on vfsStreamWrapper::resolvePath().
   *
   * @param string $path
   *   The path to resolve.
   *
   * @return string
   *   The resolved path.
   */
  protected function resolvePath(string $path): string {
    $scheme = StreamWrapperManager::getScheme($path);
    $target = StreamWrapperManager::getTarget($path);
    if (is_bool($scheme) || is_bool($target)) {
      throw new \InvalidArgumentException('$path must be a full URI');
    }
    $new_path = [];
    foreach (explode('/', $target) as $target_part) {
      if ('.' !== $target_part) {
        if ('..' !== $target_part) {
          $new_path[] = $target_part;
        }
        elseif (count($new_path) > 0) {
          array_pop($new_path);
        }
      }
    }

    $imploded_path = implode('/', $new_path);
    return $scheme . '://' . $imploded_path;
  }

}
