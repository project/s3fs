<?php

namespace Drupal\s3fs_streamwrapper;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of S3 StreamWrapper Config entities.
 */
class S3StreamWrapperEntityListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Name');
    $header['id'] = $this->t('Scheme');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id() . '://';
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity): array {
    $disable = [
      'title' => $this->t('Disable'),
      'weight' => 200,
      'url' => $entity->toUrl('disable-form'),
    ];

    $operations = parent::getDefaultOperations($entity) + [
      'disable' => $disable,
    ];

    return $operations;
  }

}
