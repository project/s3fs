<?php

namespace Drupal\s3fs_streamwrapper\StreamWrapper;

use Aws\S3\S3ClientInterface;
use Aws\S3\StreamWrapper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\s3fs_bucket\Entity\S3Bucket;
use Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface;
use Drupal\s3fs_streamwrapper\Traits\S3fsPathsTrait;
use Symfony\Component\Mime\MimeTypeGuesserInterface;
use Symfony\Component\Mime\MimeTypesInterface;

/**
 * StreamWrapper for use with a multiple bucket s3fs install.
 *
 * This streamWrapper relies on the s3fs_streamwrapper and s3fs_bucket modules
 * to provide vital interfaces to process requests.
 *
 * Core logic that will not be impacted by plugins is contained in this file
 * for efficiency and simplicity.
 *
 * Due to loading by PHP without the use of the Symfony framework this file
 * can not support Dependency Injection.
 *
 * @see \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginInterface
 * @see \Drupal\s3fs_bucket\S3BucketPluginInterface
 * @see \Drupal\Core\StreamWrapper\StreamWrapperManager
 */
class S3fsBucketStream extends StreamWrapper implements StreamWrapperInterface {

  use StringTranslationTrait;
  use S3fsPathsTrait;

  /**
   * Indicates the current error state in the wrapper.
   *
   * @var bool
   */
  protected bool $errorState = FALSE;

  /**
   * The opened protocol (e.g., "s3").
   *
   * @var string
   */
  private string $protocol = 's3';

  /**
   * Array of settings for each scheme.
   *
   * @var array<string, \Drupal\Core\StringTranslation\TranslatableMarkup|string|null>
   */
  protected array $settings;

  /**
   * AWS S3 Client.
   *
   * @var \Aws\S3\S3ClientInterface
   */
  private S3ClientInterface $s3Client;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityManager;

  /**
   * Drupal Module Handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Mime Type Guesser Service.
   *
   * @var \Symfony\Component\Mime\MimeTypeGuesserInterface|\Symfony\Component\Mime\MimeTypesInterface
   */
  protected MimeTypeGuesserInterface|MimeTypesInterface $mimeGuesser;

  /**
   * Instance uri referenced as "<scheme>://key".
   *
   * @var string|null
   */
  protected ?string $uri = NULL;

  /**
   * Directory listing used by the dir_* methods.
   *
   * @var string[]|null
   */
  protected ?array $dir = NULL;

  /**
   * {@inheritDoc}
   */
  public function __construct() {
    //phpcs:ignore DrupalPractice.Objects.GlobalDrupal.GlobalDrupal
    $this->entityManager = \Drupal::service('entity_type.manager');
    //phpcs:ignore DrupalPractice.Objects.GlobalDrupal.GlobalDrupal
    $this->moduleHandler = \Drupal::moduleHandler();
    //phpcs:ignore DrupalPractice.Objects.GlobalDrupal.GlobalDrupal
    $this->mimeGuesser = \Drupal::service('file.mime_type.guesser');

    // Since S3fsStreamWrapper is always constructed with the same inputs (the
    // file URI is not part of construction), we store the constructed settings
    // statically. This is important for performance because the way Drupal's
    // APIs are used causes stream wrappers to be frequently re-constructed.
    // Get the S3 Client object and register the stream wrapper again so it is
    // configured as needed.
    /** @var non-empty-array<string, \Drupal\Core\StringTranslation\TranslatableMarkup|string|null>|null $staticSettings */
    $staticSettings = &drupal_static('S3fsBucketStream_constructed_settings');

    if ($staticSettings !== NULL) {
      $this->settings = $staticSettings;
      return;
    }

    $settings = [];

    $wrappers = $this->entityManager
      ->getStorage('s3fs_streamwrapper')
      ->loadByProperties(['status' => TRUE]);

    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $wrapper */
    foreach ($wrappers as $wrapper) {
      $wrapperList[$wrapper->id()] = $wrapper->label();
      // Very basic check to avoid WSOD that may occur from attempting to use
      // a StreamWrapper that has no enabled bucket.
      $bucket = $wrapper->getBucket();
      if ($bucket instanceof S3Bucket) {
        $settings[$wrapper->id()] = $wrapper->label();
      }
    }

    $this->settings = $settings;

    // Save all the work we just did, so that subsequent S3fsStreamWrapper
    // constructions don't have to repeat it.
    $staticSettings = $settings;
  }

  /**
   * Sets the uri use later.
   *
   * @param string $uri
   *   The URI that is being accessed.
   */
  public function setUri($uri): void {
    // Use the resolved uri.
    $uri = $this->resolvePath($uri);
    $this->uri = $uri;
    $scheme = StreamWrapperManager::getScheme($uri);
    if (!is_string($scheme)) {
      throw new \InvalidArgumentException('$uri must be a valid URI.');
    }
    $this->setProtocol($scheme);
  }

  /**
   * Returns the stream resource URI, which looks like "<scheme>://filepath".
   *
   * @return string
   *   The current URI of the instance.
   */
  public function getUri(): string {
    if ($this->uri == NULL) {
      throw new \Exception('Call getUri() before callings setUri() is not supported');
    }
    return $this->uri;
  }

  /**
   * This wrapper does not support realpath().
   *
   * @return false
   *   Always returns FALSE.
   */
  public function realpath() {
    return FALSE;
  }

  /**
   * Sets the scheme and S3Client for use later.
   *
   * @param string $scheme
   *   The scheme this wrapper is used for.
   */
  public function setProtocol(string $scheme): void {
    $this->protocol = $scheme;
    $this->s3Client = $this->getClient($scheme);
    $this->register($this->s3Client, $this->protocol, NULL);
    $this->context = stream_context_get_default();
  }

  /**
   * Sets the scheme use later.
   *
   * @param string $scheme
   *   The scheme to obtain client for.
   *
   * @todo evaluate if we can remove passing in $scheme.
   */
  protected function getClient(string $scheme): S3ClientInterface {

    if (!isset($this->settings[$scheme])) {
      $this->triggerError(__CLASS__ . " called for unknown or disabled scheme $scheme");
    }

    // @todo error catching.
    $client = $this->getStreamWrapperEntity()->getS3Client();

    return $client;

  }

  /**
   * {@inheritdoc}
   */
  public static function getType(): int {
    return StreamWrapperInterface::NORMAL;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    // @todo do we pass this to plugins or keep it for us?
    return $this->t('S3fs Wrapper: @PROTOCOL', ['@PROTOCOL' => $this->protocol]);
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    // @todo do we pass this to plugins or keep it for us?
    return $this->t('S3 File System wrapper @PROTOCOL', ['@PROTOCOL' => $this->protocol]);
  }

  /**
   * Gets the path that the wrapper is responsible for.
   *
   * This function isn't part of DrupalStreamWrapperInterface, but the rest
   * of Drupal calls it as if it were, so we need to define it.
   *
   * @return string
   *   The empty string. Since this is a remote stream wrapper,
   *   it has no directory path.
   *
   * @see \Drupal\Core\File\LocalStream::getDirectoryPath()
   */
  public function getDirectoryPath(): string {
    return '';
  }

  /**
   * Obtain a configured plugin.
   *
   * @return \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface
   *   A configured s3fs_streamwrapper Entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getStreamWrapperEntity(): S3StreamWrapperEntityInterface {
    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface|NULL $staticStreamWrapperEntity */
    $staticStreamWrapperEntity = &drupal_static('s3fs_streamwrapper_entity_' . $this->protocol);

    if ($staticStreamWrapperEntity) {
      return $staticStreamWrapperEntity;
    }

    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $streamWrapperEntity */
    $streamWrapperEntity = $this->entityManager
      ->getStorage('s3fs_streamwrapper')
      ->load($this->protocol);

    $staticStreamWrapperEntity = $streamWrapperEntity;
    return $streamWrapperEntity;
  }

  /**
   * Write an object's (and its ancestor folders') metadata to the cache.
   *
   * @param array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir'?: 0|1, 'version'?: string} $metadata
   *   An associative array of file metadata in this format:
   *     'bucket' => The entity name associated with this bucket.
   *     'path' => The full URI of the file, including the scheme.
   *     'filesize' => The size of the file, in bytes.
   *     'timestamp' => The file's create/update timestamp.
   *     'dir' => A boolean indicating whether the object is a directory.
   *
   * @throws \Drupal\s3fs\S3fsException
   *   Exceptions which occur in the database call will percolate.
   */
  protected function writeCache(array $metadata): void {
    $this->getStreamWrapperEntity()->writeCache($metadata);
  }

  /**
   * Delete an object's metadata from the cache.
   *
   * @param string $uri
   *   A string containing the URI of the object
   *   to be deleted.
   *
   * @return int
   *   The number of records deleted.
   *
   * @throws \Drupal\s3fs\S3fsException
   *   Exceptions which occur in the database call will percolate.
   */
  protected function deleteCache(string $uri): int {
    $this->setUri($uri);
    return $this->getStreamWrapperEntity()->deleteCache($uri);
  }

  /**
   * Fetch an object from the file metadata cache table.
   *
   * @param string $uri
   *   The uri of the resource to check.
   *
   * @return array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir': 0|1, 'version': string}|false
   *   An array of metadata if the $uri is in the cache. Otherwise, FALSE.
   */
  public function readCache(string $uri): array|FALSE {
    $this->setUri($uri);
    $converted = $this->getStreamWrapperEntity()->convertUriToKeyedPath($uri, FALSE);
    return $this->getStreamWrapperEntity()->getBucket()->readCache($converted);
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl(): string {
    // We can't inject into this class and the Request service causes
    // serialization issues so we call the service from here.
    //phpcs:ignore DrupalPractice.Objects.GlobalDrupal.GlobalDrupal
    $request = \Drupal::request();
    return $this->getStreamWrapperEntity()->getExternalUrl($this->getUri(), $request->isSecure());
  }

  /**
   * Return bucket and key for a command array.
   *
   * @param string $uri
   *   Uri to the required object.
   *
   * @return array
   *   A modified path to the key in S3.
   */
  protected function getCommandParams(string $uri): array {
    $this->setUri($uri);
    $params = $this->getOptions(TRUE);
    return $this->getStreamWrapperEntity()->getCommandParams($uri, $params);
  }

  /**
   * Get the stream's context options or remove them if wanting default.
   *
   * @param bool $removeContextData
   *   Whether to remove the stream's context information.
   *
   * @return array
   *   An array of options.
   */
  private function getOptions(bool $removeContextData = FALSE): array {
    // Context is not set when doing things like stat.
    if (is_null($this->context)) {
      $this->context = stream_context_get_default();
    }
    $options = stream_context_get_options($this->context);

    if ($removeContextData) {
      unset($options['client'], $options['seekable'], $options['cache']);
    }

    return $options;
  }

  /**
   * StreamWrapper Default functions.
   */

  /**
   * Support for fopen(), file_get_contents(), file_put_contents() etc.
   *
   * @param string $uri
   *   The URI of the file to open.
   * @param string $mode
   *   The file mode. Only 'r', 'w', 'a', and 'x' are supported.
   * @param int $options
   *   A bit mask of STREAM_USE_PATH and STREAM_REPORT_ERRORS.
   * @param string $opened_path
   *   An OUT parameter populated with the path which was opened.
   *   This wrapper does not support this parameter.
   *
   * @return bool
   *   TRUE if file was opened successfully. Otherwise, FALSE.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-open.php
   */
// phpcs:disable
  public function stream_open($uri, $mode, $options, &$opened_path): BOOL {
// phpcs:
    $this->setUri($uri);
    $uri = $this->getUri();

    if (mb_strlen($uri) > $this->getStreamWrapperEntity()->getMaxUriLength()) {
      return FALSE;
    }
    $this->setUri($uri);
    $converted = $this->getSdkStreamPath($uri);

    $s3options = $this->getOptions();

    // Allow other modules to alter the stream open params.
    $this->moduleHandler->alter('s3fs_stream_open_params', $s3options[$this->protocol], $converted);

    stream_context_set_option($this->context, $s3options);

    return parent::stream_open($converted, $mode, $options, $opened_path);
  }

  /**
   * This wrapper does not support flock().
   *
   * @return false
   *   Always Returns FALSE.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-lock.php
   */
  // phpcs:ignore Drupal.NamingConventions.ValidFunctionName.ScopeNotCamelCaps,Drupal.Commenting.FunctionComment.Missing
  public function stream_lock($operation): BOOL {
    return FALSE;
  }

  /**
   * Support for fflush(). Flush current cached stream data to a file in S3.
   *
   * @return bool
   *   TRUE if data was successfully stored in S3.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-flush.php
   */
// phpcs:disable
  public function stream_flush(): BOOL {
// phpcs:enable
    // Prepare upload parameters.
    $options = $this->getOptions();
    $params = $this->getCommandParams($this->getUri());

    $contentType = $this->mimeGuesser->guessMimeType($params['Key']);

    $options[$this->protocol]['ContentType'] = $contentType;

    $this->getStreamWrapperEntity()->getStreamOptions($options[$this->protocol]);

    // Allow other modules to alter the upload params.
    $this->moduleHandler->alter('s3fs_upload_params', $options[$this->protocol]);

    stream_context_set_option($this->context, $options);

    if (parent::stream_flush()) {
      return $this->writeUriToCache($this->getUri());
    }
    return FALSE;
  }

  /**
   * Sets metadata on the stream.
   *
   * This wrapper does not support touch(), chmod(), chown(), or chgrp().
   *
   * Manual recommends return FALSE for not implemented options, but Drupal
   * require TRUE in some cases like chmod for avoid watchdog errors.
   *
   * @param string $uri
   *   A string containing the URI to the file to set metadata on.
   * @param int $option
   *   One of:
   *   - STREAM_META_TOUCH: The method was called in response to touch().
   *   - STREAM_META_OWNER_NAME: The method was called in response to chown()
   *     with string parameter.
   *   - STREAM_META_OWNER: The method was called in response to chown().
   *   - STREAM_META_GROUP_NAME: The method was called in response to chgrp().
   *   - STREAM_META_GROUP: The method was called in response to chgrp().
   *   - STREAM_META_ACCESS: The method was called in response to chmod().
   * @param mixed $value
   *   If option is:
   *   - STREAM_META_TOUCH: Array consisting of two arguments of the touch()
   *     function.
   *   - STREAM_META_OWNER_NAME or STREAM_META_GROUP_NAME: The name of the owner
   *     user/group as string.
   *   - STREAM_META_OWNER or STREAM_META_GROUP: The value of the owner
   *     user/group as integer.
   *   - STREAM_META_ACCESS: The argument of the chmod() as integer.
   *
   * @return bool
   *   Returns FALSE if the option is not included in bypassed_options array
   *   otherwise, TRUE.
   *
   * @see http://php.net/manual/streamwrapper.stream-metadata.php
   * @see http://php.net/manual/en/streamwrapper.stream-metadata.php
   * @see \Drupal\Core\File\FileSystem::chmod()
   */
  // phpcs:ignore Drupal.NamingConventions.ValidFunctionName.ScopeNotCamelCaps,Drupal.Commenting.FunctionComment.Missing
  public function stream_metadata($uri, $option, $value): BOOL {
    $bypassed_options = [STREAM_META_ACCESS];
    return in_array($option, $bypassed_options);
  }

  /**
   * Not supported: Change stream options.
   *
   * This method is called to set options on the stream.
   *
   * Since Windows systems do not allow it and it is not needed for most use
   * cases anyway, this method is not supported on local files and will trigger
   * an error and return false. If needed, custom subclasses can provide
   * OS-specific implementations for advanced use cases.
   *
   * @param int $option
   *   One of:
   *   - STREAM_OPTION_BLOCKING: The method was called in response to
   *     stream_set_blocking().
   *   - STREAM_OPTION_READ_TIMEOUT: The method was called in response to
   *     stream_set_timeout().
   *   - STREAM_OPTION_WRITE_BUFFER: The method was called in response to
   *     stream_set_write_buffer().
   * @param int $arg1
   *   If option is:
   *   - STREAM_OPTION_BLOCKING: The requested blocking mode:
   *     - 1 means blocking.
   *     - 0 means not blocking.
   *   - STREAM_OPTION_READ_TIMEOUT: The timeout in seconds.
   *   - STREAM_OPTION_WRITE_BUFFER: The buffer mode, STREAM_BUFFER_NONE or
   *     STREAM_BUFFER_FULL.
   * @param int $arg2
   *   If option is:
   *   - STREAM_OPTION_BLOCKING: This option is not set.
   *   - STREAM_OPTION_READ_TIMEOUT: The timeout in microseconds.
   *   - STREAM_OPTION_WRITE_BUFFER: The requested buffer size.
   *
   * @return false
   *   Not supported.
   */
  // phpcs:ignore Drupal.NamingConventions.ValidFunctionName.ScopeNotCamelCaps,Drupal.Commenting.FunctionComment.Missing
  public function stream_set_option($option, $arg1, $arg2): BOOL {
    trigger_error('stream_set_option() not supported for local file based stream wrappers', E_USER_WARNING);
    return FALSE;
  }

  /**
   * This wrapper does not support stream_truncate.
   *
   * Always returns FALSE.
   *
   * Will respond to truncation; e.g., through ftruncate().
   *
   * @param int $new_size
   *   The new size. Ignored.
   *
   * @return FALSE
   *   Unsupported operation, always FALSE.
   *
   * @see ftruncate()
   * @see http://php.net/manual/en/streamwrapper.stream-truncate.php
   */
  // phpcs:ignore Drupal.NamingConventions.ValidFunctionName.ScopeNotCamelCaps,Drupal.Commenting.FunctionComment.Missing
  public function stream_truncate($new_size): BOOL {
    return FALSE;
  }

  /**
   * This method is called in response to unlink().
   *
   * @param string $uri
   *   The uri of the resource to delete.
   *
   * @return bool
   *   TRUE if resource was successfully deleted even if it did not exist.
   *   FALSE if the call to S3 failed, in which case the file will not be
   *   removed from the cache.
   *
   * @see unlink()
   * @see PhpStreamWrapperInterface::rmdir()
   * @see http://php.net/manual/en/streamwrapper.unlink.php
   */
  public function unlink($uri): BOOL {
    $this->setUri($uri);
    // Use the resolved uri.
    $uri = $this->getUri();
    $converted = $this->getSdkStreamPath($uri);
    if (parent::unlink($converted)) {
      $this->deleteCache($uri);
      clearstatcache(TRUE, $uri);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Renames a file or directory.
   *
   * This method is called in response to rename(). Should attempt to rename
   * $path_from to $path_to.
   *
   * If $to_uri exists, this file will be overwritten. This behavior is
   * identical to the PHP rename() function
   *
   * Note, the streamWrapper::$context property is updated if a valid context is
   * passed to the caller function.
   *
   * @param string $from_uri
   *   The uri of the file to be renamed.
   * @param string $to_uri
   *   The new uri for the file.
   *
   * @return bool
   *   TRUE if file was successfully renamed. Otherwise, FALSE.
   *
   * @see rename()
   * @see http://php.net/manual/en/streamwrapper.rename.php
   */
  public function rename($from_uri, $to_uri) {

    $this->setUri($to_uri);
    $to_uri = $this->getUri();
    $from_uri = $this->resolvePath($from_uri);

    if ($this->isDir($from_uri)) {
      // AWS SDK doesn't support moving 'directories'.
      return FALSE;
    }

    if (mb_strlen($to_uri) > $this->getStreamWrapperEntity()->getMaxUriLength()) {
      return FALSE;
    }

    $from_key = $this->getSdkStreamPath($from_uri);
    $to_key = $this->getSdkStreamPath($to_uri);

    $rename_context = [
      'from_key' => $from_key,
      'to_key' => $to_key,
    ];

    $options = $this->getOptions();

    $this->getStreamWrapperEntity()->getStreamOptions($options[$this->protocol]);

    // Allow other modules to alter the rename params.
    $this->moduleHandler->alter('s3fs_copy_params', $options[$this->protocol], $rename_context);

    stream_context_set_option($this->context, $options);

    // If parent succeeds in renaming, updated local metadata and cache.
    if (parent::rename($from_key, $to_key)) {
      $metadata = $this->readCache($from_uri);
      if (!$metadata) {
        // File was moved, but we don't have metadata for source file.
        return FALSE;
      }
      $metadata['path'] = $this->getStreamWrapperEntity()->convertUriToKeyedPath($to_uri, FALSE);
      $this->writeCache($metadata);
      $this->deleteCache($from_uri);
      clearstatcache(TRUE, $from_uri);
      clearstatcache(TRUE, $to_uri);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Gets the name of the directory from a given path.
   *
   * This method is usually accessed through
   * \Drupal\Core\File\FileSystemInterface::dirname(), which wraps around the
   * normal PHP dirname() function, which does not support stream wrappers.
   *
   * @param string $uri
   *   An optional URI.
   *
   * @return string
   *   The directory name, or FALSE if not applicable.
   *
   * @see \Drupal\Core\File\FileSystemInterface::dirname()
   */
  public function dirname($uri = NULL): string {
    if (!isset($uri)) {
      $uri = $this->getUri();
    }
    else {
      $uri = $this->resolvePath($uri);
    }
    $scheme = StreamWrapperManager::getScheme($uri);
    $target = StreamWrapperManager::getTarget($uri);

    if (is_bool($scheme) || is_bool($target)) {
      return FALSE;
    }

    $dirname = dirname($target);

    // When the dirname() call above is given '$scheme://', it returns '.'.
    // But '$scheme://.' is an invalid uri, so we return "$scheme://" instead.
    if ($dirname == '.') {
      $dirname = '';
    }

    return "$scheme://$dirname";
  }

  /**
   * Support for mkdir().
   *
   * @param string $uri
   *   The URI to the directory to create.
   * @param int $mode
   *   Permission flags
   *     700-range permissions map to ACL_PUBLIC.
   *     600-range permissions map to ACL_AUTH_READ.
   *     All other permissions map to ACL_PRIVATE.
   *   Expects octal form.
   * @param int $options
   *   A bit mask of STREAM_REPORT_ERRORS and STREAM_MKDIR_RECURSIVE.
   *
   * @return bool
   *   TRUE if the directory was successfully created. Otherwise, FALSE.
   *
   * @see http://php.net/manual/en/streamwrapper.mkdir.php
   */
  public function mkdir($uri, $mode, $options): bool {
    $this->setUri($uri);
    $uri = $this->getUri();
    // Some Drupal plugins call mkdir with a trailing slash. We mustn't store
    // that slash in the cache.
    $uri = rtrim($uri, '/');

    if (mb_strlen($uri) > $this->getStreamWrapperEntity()->getMaxUriLength()) {
      return FALSE;
    }

    clearstatcache(TRUE, $uri);
    // If this URI already exists in the cache, return TRUE if it's a folder
    // (so that recursive calls won't improperly report failure when they
    // reach an existing ancestor), or FALSE if it's a file (failure).
    $test_metadata = $this->readCache($uri);
    if ($test_metadata) {
      return (bool) $test_metadata['dir'];
    }

    $metadata = $this->convertMetadata($uri, []);
    $this->writeCache($metadata);

    // If the STREAM_MKDIR_RECURSIVE option was specified, also create all the
    // ancestor folders of this uri, except for the root directory.
    $parent_dir = $this->dirname($uri);
    if (($options & STREAM_MKDIR_RECURSIVE) && StreamWrapperManager::getTarget($parent_dir) != '') {
      return $this->mkdir($parent_dir, $mode, $options);
    }
    return TRUE;
  }

  /**
   * Removes a directory.
   *
   * This method is called in response to rmdir().
   *
   * Note, in order for the appropriate error message to be returned this method
   * should not be defined if the wrapper does not support removing directories.
   *
   * Note, the streamWrapper::$context property is updated if a valid context is
   * passed to the caller function.
   *
   * @param string $uri
   *   The URI to the folder to delete.
   * @param int $options
   *   A bit mask of STREAM_REPORT_ERRORS.
   *
   * @return bool
   *   TRUE if folder is successfully removed.
   *   FALSE if $uri isn't a folder, or the folder is not empty.
   *
   * @see rmdir()
   * @see StreamWrapper::rmdir()
   * @see PhpStreamWrapperInterface::mkdir()
   * @see PhpStreamWrapperInterface::unlink()
   * @see http://php.net/manual/en/streamwrapper.rmdir.php
   */
  public function rmdir($uri, $options): BOOL {
    $this->setUri($uri);
    // Use the resolved uri.
    $uri = $this->getUri();

    if (!$this->isDir($uri)) {
      return FALSE;
    }

    $isEmpty = $this->getStreamWrapperEntity()->isDirEmpty($uri);

    // If the folder is empty at time of query it is eligible for deletion.
    if ($isEmpty) {
      // Suppress race triggerError('Subfolder is not empty').
      if (@parent::rmdir($this->getSdkStreamPath($uri), $options)) {
        $this->deleteCache($uri);
        clearstatcache(TRUE, $uri);
        return TRUE;
      }
    }

    // The folder is non-empty.
    return FALSE;
  }

  /**
   * Retrieve information about a file.
   *
   * This method is called in response to all stat() related functions.
   *
   * Note, the streamWrapper::$context property is updated if a valid context is
   * passed to the caller function.
   *
   * @param string $uri
   *   The URI to get information about.
   * @param int $flags
   *   A bit mask of STREAM_URL_STAT_LINK and STREAM_URL_STAT_QUIET.
   *   S3fsStreamWrapper ignores this value.
   *
   * @return array|FALSE
   *   An array with file status, or FALSE in case of an error.
   *
   * @see http://php.net/manual/en/streamwrapper.url-stat.php
   */
  // phpcs:ignore Drupal.NamingConventions.ValidFunctionName.ScopeNotCamelCaps,Drupal.Commenting.FunctionComment.Missing
  public function url_stat($uri, $flags): array|FALSE {
    $uri = $this->resolvePath($uri);
    return $this->stat($uri);
  }

  // @todo review for changes
  /**
   * Open directory handle.
   *
   * This method is called in response to opendir().
   *
   * @param string $uri
   *   The URI to the directory to open.
   * @param int $options
   *   A flag used to enable safe_mode.
   *   This wrapper doesn't support safe_mode, so this parameter is ignored.
   *
   * @return bool
   *   TRUE on success. Otherwise, FALSE.
   *
   * @see http://php.net/manual/en/streamwrapper.dir-opendir.php
   */
// phpcs:disable
  public function dir_opendir($uri, $options = NULL): BOOL {
// phpcs:enable
    $this->setUri($uri);
    // Use the resolved uri.
    $uri = $this->getUri();
    if (!$this->isDir($uri)) {
      return FALSE;
    }

    $child_paths = $this->getStreamWrapperEntity()->listDir($uri);
    $this->dir = [];
    foreach ($child_paths as $child_path) {
      $this->dir[] = basename($child_path);
    }
    return TRUE;
  }

  /**
   * Read entry from directory handle.
   *
   * This method is called in response to readdir().
   *
   * @return string|false
   *   Should return string representing the next filename, or FALSE if there
   *   is no next file. Note, the return value will be cast to string.
   *
   * @see readdir()
   * @see http://php.net/manual/en/streamwrapper.dir-readdir.php
   */
  // phpcs:ignore Drupal.NamingConventions.ValidFunctionName.ScopeNotCamelCaps,Drupal.Commenting.FunctionComment.Missing
  public function dir_readdir(): string|FALSE {
    if ($this->dir == NULL) {
      return FALSE;
    }
    $current = current($this->dir);
    if ($current) {
      next($this->dir);
    }
    return $current;
  }

  /**
   * Internal Functions.
   */

  /**
   * Obtain the metadata from bucket and write to cache for specified URI.
   *
   * @param string $uri
   *   The URI to be written to cache.
   *
   * @return bool
   *   TRUE if the file existed and written to the database, otherwise FALSE.
   */
  protected function writeUriToCache(string $uri): bool {
    $path = $this->getStreamWrapperEntity()->convertUriToKeyedPath($uri, FALSE);
    if ($this->getStreamWrapperEntity()->getBucket()->waitUntilFileExists($path)) {
      $metadata = $this->getS3Metadata($uri);
      if (!empty($metadata)) {
        $this->writeCache($metadata);
        clearstatcache(TRUE, $uri);
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Get the status of the file with the specified URI.
   *
   * Implementation of a stat method to ensure that remote files don't fail
   * checks when they should pass.
   *
   * @param string $uri
   *   The uri of the resource.
   *
   * @return array|false
   *   An array with file status, or FALSE if the file doesn't exist.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-stat.php
   */
  protected function stat(string $uri): array|FALSE {
    $this->setUri($uri);
    return $this->getStreamWrapperEntity()->stat($uri);
  }

  /**
   * Determine whether the $uri is a directory.
   *
   * @param string $uri
   *   The path of the resource to check.
   *
   * @return bool
   *   TRUE if the resource is a directory.
   */
  protected function isDir(string $uri): bool {
    $this->setUri($uri);
    return $this->getStreamWrapperEntity()->isDir($uri);
  }

  /**
   * Convert file metadata returned from S3 into a metadata cache array.
   *
   * @param string $uri
   *   The uri of the resource.
   * @param array $metadata
   *   An array containing the collective metadata for the object in S3.
   *   The caller may send an empty array here to indicate that the returned
   *   metadata should represent a directory.
   *
   * @return array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir': 0|1, 'version': string}
   *   A file metadata cache array.
   */
  protected function convertMetadata(string $uri, array $metadata): array {
    $this->setUri($uri);
    return $this->getStreamWrapperEntity()->convertMetadata($uri, $metadata);
  }

  /**
   * Returns the converted metadata for an object in S3.
   *
   * @param string $uri
   *   The URI for the object in S3.
   *
   * @return array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir': 0|1, 'version': string}|false
   *   An array of DB-compatible file metadata or empty array if lookup fails.*
   */
  protected function getS3Metadata(string $uri): array|false {
    $this->setUri($uri);
    return $this->getStreamWrapperEntity()->getS3Metadata($uri);
  }

  /**
   * Triggers one or more errors.
   *
   * @param string|array $errors
   *   Errors to trigger.
   * @param mixed $flags
   *   If set to STREAM_URL_STAT_QUIET, no error or exception is triggered.
   *
   * @return bool
   *   Always returns FALSE.
   */
  protected function triggerError(string|array $errors, mixed $flags = NULL): BOOL {
    if ($flags != STREAM_URL_STAT_QUIET) {
      trigger_error(implode("\n", (array) $errors), E_USER_ERROR);
    }
    $this->errorState = TRUE;
    return FALSE;
  }

  /**
   * Returns an AWS SDK Compatible StreamPath.
   *
   * @param string $uri
   *   URI to get converted path for.
   *
   * @return string
   *   Path in the form of <protocol>://<bucket_name>/<path_to_object>.
   */
  protected function getSdkStreamPath(string $uri): string {
    $this->setUri($uri);
    $keyedPath = $this->getStreamWrapperEntity()->convertUriToKeyedPath($uri, TRUE);
    return $this->protocol . '://' . $keyedPath;
  }

}
