<?php

namespace Drupal\s3fs_streamwrapper\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a S3 StreamWrapper Plugin item annotation object.
 *
 * @see \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class S3StreamWrapper extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
