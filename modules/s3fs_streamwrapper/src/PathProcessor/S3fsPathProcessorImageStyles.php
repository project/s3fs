<?php

namespace Drupal\s3fs_streamwrapper\PathProcessor;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a path processor to rewrite image styles URLs.
 *
 * As the route system does not allow arbitrary amount of parameters convert
 * the file path to a query parameter on the request.
 *
 * This processor handles Amazon S3 public image style callback:
 * - In order to allow the webserver to serve these files with dynamic args
 *   the route is registered under /s3/files/styles prefix and change internally
 *   to pass validation and move the file to query parameter. This file will be
 *   processed in S3fsImageStyleDownloadController::deliver().
 *
 *  Files using 'Drupal delivery' (private) are handled by
 *  S3fsDrupalFileDeliveryPathProcessor.
 *
 * @see \Drupal\s3fs_streamwrapper\Controller\S3fsImageStyleDownloadController::deliver()
 * @see \Drupal\image\Controller\ImageStyleDownloadController::deliver()
 * @see \Drupal\image\PathProcessor\PathProcessorImageStyles::processInbound()
 */
class S3fsPathProcessorImageStyles implements InboundPathProcessorInterface {

  const IMAGE_STYLE_PATH_PREFIX = '/s3/styles/';

  /**
   * Entity Type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct our new PathProccessor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request): string {
    if ($this->isImageStylePath($path)) {
      // Strip out path prefix.
      $rest = preg_replace('|^' . preg_quote(static::IMAGE_STYLE_PATH_PREFIX, '|') . '|', '', $path);
      if ($rest == NULL) {
        throw new \Exception('Unexpected fault removing path base');
      }

      // Get the image style, scheme and path.
      if (substr_count($rest, '/') >= 2) {
        [$image_style, $scheme, $file] = explode('/', $rest, 3);

        if ($this->isValidScheme($scheme)) {
          // Set the file as query parameter.
          $request->query->set('file', $file);
          $path = static::IMAGE_STYLE_PATH_PREFIX . $image_style . '/' . $scheme;
        }
        else {
          // If it is not a valid enabled scheme send a 404 page.
          $path = '/system/404';
        }
      }
    }

    return $path;
  }

  /**
   * Check if the path is a s3 image style path.
   *
   * @param string $path
   *   Path to be checked.
   *
   * @return bool
   *   TRUE if path starts with s3fs image style prefix, FALSE otherwise.
   */
  private function isImageStylePath(string $path): BOOL {
    return strpos($path, static::IMAGE_STYLE_PATH_PREFIX) === 0;
  }

  /**
   * Check if scheme is Amazon S3 image style supported.
   *
   * @param string $scheme
   *   Scheme (ex 's3' for s3://) to test.
   *
   * @return bool
   *   TRUE if s3fs will generate image styles, FALSE otherwise.
   */
  private function isValidScheme(string $scheme): BOOL {
    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface|null $entity */
    $entity = $this->entityTypeManager
      ->getStorage('s3fs_streamwrapper')
      ->load($scheme);

    return (!is_null($entity) && $entity->status());
  }

}
