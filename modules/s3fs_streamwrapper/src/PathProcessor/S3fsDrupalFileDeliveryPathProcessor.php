<?php

namespace Drupal\s3fs_streamwrapper\PathProcessor;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a path processor to allow Drupal to deliver the files.
 *
 * As the route system does not allow arbitrary amount of parameters convert
 * the file path to a query parameter on the request.
 *
 * This processor handles Amazon S3 files delivered by Drupal.
 * - In order to allow the webserver to serve these files with dynamic args
 *   the route is registered under /s3/files prefix and change internally
 *   to pass the file as a query parameter. This file will be
 *   processed in FileDownloadController::download().
 *
 * @see \Drupal\system\FileDownloadController::download()
 */
class S3fsDrupalFileDeliveryPathProcessor implements InboundPathProcessorInterface {

  /**
   * Path prefix to manage.
   */
  const S3FS_FILE_PATH = '/s3/files/';

  /**
   * Entity Type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct our new PathProccessor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    if ($this->isFileDownloadPath($path)) {
      // Strip out path prefix.
      $rest = preg_replace('|^' . preg_quote(static::S3FS_FILE_PATH, '|') . '|', '', $path);
      if ($rest == NULL) {
        throw new \Exception('Unexpected fault removing path base');
      }

      // Get the scheme and path.
      if (substr_count($rest, '/') >= 1) {
        [$scheme, $file] = explode('/', $rest, 2);

        if ($this->isValidScheme($scheme)) {
          // Set the file as query parameter.
          $request->query->set('file', $file);
          $path = static::S3FS_FILE_PATH . $scheme;
        }
        else {
          // If it is not a valid enabled scheme send a 404 page.
          $path = '/system/404';
        }
      }
    }

    return $path;
  }

  /**
   * Check if the path is a s3 file path.
   *
   * @param string $path
   *   Path to be checked.
   *
   * @return bool
   *   TRUE if path starts with s3 file prefix, FALSE otherwise.
   */
  private function isFileDownloadPath($path) {
    return strpos($path, static::S3FS_FILE_PATH) === 0;
  }

  /**
   * Check if scheme is an enabled S3fs_streamwrapper scheme.
   *
   * @param string $scheme
   *   Scheme (ex 's3' for s3://) to test.
   *
   * @return bool
   *   TRUE if an enabled s3fs streamwrapper, FALSE otherwise.
   */
  private function isValidScheme($scheme) {
    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface|null $entity */
    $entity = $this->entityTypeManager
      ->getStorage('s3fs_streamwrapper')
      ->load($scheme);

    return (!is_null($entity) && $entity->status());
  }

}
