<?php

namespace Drupal\s3fs_streamwrapper\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirm form for disabling a streamWrapper.
 */
class S3StreamWrapperDisableConfirmForm extends EntityConfirmFormBase {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface
   */
  protected $entity;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a S3StreamWrapperDisableConfirmForm object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $messenger = $container->get('messenger');

    return new static($messenger);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to disable the streamWrapper %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Disabling a streamWrapper will disable all access to files on this scheme.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.s3fs_streamwrapper.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Disable');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->entity->setStatus(FALSE)->save();

    $this->messenger->addStatus($this->t('The streamWrapper %name has been disabled.', ['%name' => $this->entity->label()]));
    $form_state->setRedirect('entity.s3fs_streamwrapper.collection');
  }

}
