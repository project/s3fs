<?php

namespace Drupal\s3fs_streamwrapper\Form;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Utility\Error;
use Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface;
use Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginManager;
use Drupal\s3fs_streamwrapper\Utility\Utility;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for S3StreamWrapper Config Entities.
 */
class S3StreamWrapperEntityForm extends EntityForm {

  /**
   * Stream Wrapper Plugin Manager helper service.
   *
   * @var \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginManager
   */
  protected S3StreamWrapperPluginManager $wrapperPluginManager;

  /**
   * Constructor for s3 bucket forms.
   *
   * @param \Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginManager $wrapperPluginManager
   *   Plugin Manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger Service.
   */
  public function __construct(S3StreamWrapperPluginManager $wrapperPluginManager, MessengerInterface $messenger) {
    $this->wrapperPluginManager = $wrapperPluginManager;
    $this->setMessenger($messenger);
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('plugin.manager.s3fs_s3streamwrapper'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $streamWrapperEntity */
    $streamWrapperEntity = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $streamWrapperEntity->label(),
      '#description' => $this->t("Label for the S3 StreamWrapper Config."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $streamWrapperEntity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntity::load',
      ],
      '#disabled' => !$streamWrapperEntity->isNew(),
    ];

    $this->buildEntityForm($form, $form_state, $streamWrapperEntity);
    // Skip adding the backend config form if we cleared the server form due to
    // an error.
    if ($form) {
      $this->buildPluginConfigForm($form, $form_state, $streamWrapperEntity);
    }

    return $form;
  }

  /**
   * Builds the form for the basic server properties.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $streamWrapperEntity
   *   The s3 bucket that is being created or edited.
   */
  public function buildEntityForm(array &$form, FormStateInterface $form_state, S3StreamWrapperEntityInterface $streamWrapperEntity): void {
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('Enter a label for this StreamWrapper configuration.'),
      '#default_value' => $streamWrapperEntity->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $streamWrapperEntity->isNew() ? NULL : $streamWrapperEntity->id(),
      '#maxlength' => 50,
      '#required' => TRUE,
      '#machine_name' => [
        'exists' => '\Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntity::load',
        'source' => ['label'],
        'replace' => '-',
        'replace_pattern' => '[^a-z0-9-+.]+',
      ],
      '#disabled' => !$streamWrapperEntity->isNew(),
    ];
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Only enabled StreamWrapeprs can be accessed by file operations.'),
      '#default_value' => $streamWrapperEntity->status(),
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Enter a description for the StreamWrapper.'),
      '#default_value' => $streamWrapperEntity->getDescription(),
    ];

    $plugins = $this->wrapperPluginManager->getDefinitions();
    $plugin_options = [];
    $descriptions = [];
    foreach ($plugins as $plugin_id => $definition) {
      $config = $plugin_id === $streamWrapperEntity->getPluginId() ? $streamWrapperEntity->getStreamWrapperConfig() : [];
      $config['#streamwrapper'] = $streamWrapperEntity;
      try {
        /** @var \Drupal\s3fs_bucket\S3BucketPluginInterface $plugin */
        $plugin = $this->wrapperPluginManager
          ->createInstance($plugin_id, $config);
      }
      catch (PluginException $e) {
        continue;
      }
      $plugin_options[$plugin_id] = Utility::escapeHtml($plugin->label());
      $descriptions[$plugin_id]['#description'] = Utility::escapeHtml($plugin->getDescription());
    }
    asort($plugin_options, SORT_NATURAL | SORT_FLAG_CASE);
    if (count($plugin_options) === 1) {
      $streamWrapperEntity->set('streamWrapperType', key($plugin_options));
    }
    $form['streamwrapper_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('StreamWrapper type'),
      '#description' => $this->t('Choose a plugin to use for this StreamWrapper.'),
      '#options' => $plugin_options,
      '#default_value' => $streamWrapperEntity->getPluginId(),
      '#required' => TRUE,
      '#disabled' => !$streamWrapperEntity->isNew(),
      '#ajax' => [
        'callback' => [get_class($this), 'buildAjaxStreamWrapperConfigForm'],
        'wrapper' => 's3-streamwrapper-plugin-config-form',
        'method' => 'replace',
        'effect' => 'fade',
      ],
    ];
  }

  /**
   * Builds the plugin-specific configuration form.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $streamWrapperEntity
   *   The bucket that is being created or edited.
   */
  public function buildPluginConfigForm(array &$form, FormStateInterface $form_state, S3StreamWrapperEntityInterface $streamWrapperEntity): void {
    $form['streamwrapper_config'] = [];
    if ($streamWrapperEntity->hasValidStreamWrapperPlugin()) {
      $plugin = $streamWrapperEntity->getStreamWrapperPlugin();
      $form_state->set('plugin', $plugin->getPluginId());
      if ($plugin instanceof PluginFormInterface) {
        if ($form_state->isRebuilding()) {
          $this->messenger->addWarning($this->t('Please configure the selected plugin.'));
        }
        // Attach the plugin configuration form.
        $plugin_form_state = SubformState::createForSubform($form['streamwrapper_config'], $form, $form_state);
        $form['streamwrapper_config'] = $plugin->buildConfigurationForm($form['streamwrapper_config'], $plugin_form_state);

        // Modify the bucket plugin configuration container element.
        $form['streamwrapper_config']['#type'] = 'details';
        $form['streamwrapper_config']['#title'] = $this->t('Configure %plugin plugin', ['%plugin' => $plugin->label()]);
        $form['streamwrapper_config']['#open'] = TRUE;
      }
    }
    // Only notify the user of a missing plugin if we're editing an
    // existing bucket.
    elseif (!$streamWrapperEntity->isNew()) {
      $this->messenger->addError($this->t('The StreamWrapper plugin is missing or invalid.'));
      return;
    }
    $form['streamwrapper_config'] += [
      '#type' => 'container',
    ];
    $form['streamwrapper_config']['#attributes']['id'] = 's3-streamwrapper-plugin-config-form';
    $form['streamwrapper_config']['#tree'] = TRUE;
  }

  /**
   * Handles switching the selected plugin.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The part of the form to return as AJAX.
   */
  public static function buildAjaxStreamWrapperConfigForm(array $form, FormStateInterface $form_state): array {
    // The work is already done in form(), where we rebuild the entity according
    // to the current form values and then create the plugin configuration form
    // based on that. So we just need to return the relevant part of the form
    // here.
    return $form['streamwrapper_config'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    // Schemes that have special meaning that s3fs should not provide.
    $prohibited_schemes = [
      'http',
      'https',
      'temporary',
      'ftp',
      'ftps',
      'compress.zlib',
      'compress.bzip2',
      'php',
      'file',
      'glob',
      'data',
      'sqlsrv',
      'zip',
      'phar',
    ];
    $machine_name = $form_state->getValue('id');
    if (in_array($machine_name, $prohibited_schemes)) {
      $form_state->setErrorByName('id', $this->t("'@name' is a reserved scheme.", ['@name' => $machine_name]));
    }

    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $server */
    $server = $this->getEntity();

    // Check if the backend plugin changed.
    $plugin_id = $server->getPluginId();
    if ($plugin_id != $form_state->get('plugin')) {
      // This can only happen during initial server creation, since we don't
      // allow switching the plugin afterwards. The user has selected a
      // different plugin, so any values entered for the other plugin should
      // be discarded.
      $input = &$form_state->getUserInput();
      $input['streamwrapper_config'] = [];
      $new_backend = $this->wrapperPluginManager->createInstance($form_state->getValues()['streamwrapper_type']);
      if ($new_backend instanceof PluginFormInterface) {
        $form_state->setRebuild();
      }
    }
    // Check before loading the bucket plugin so we don't throw an exception.
    elseif ($server->hasValidStreamWrapperPlugin()) {
      $backend = $server->getStreamWrapperPlugin();
      if ($backend instanceof PluginFormInterface) {
        $backend_form_state = SubformState::createForSubform($form['streamwrapper_config'], $form, $form_state);
        $backend->validateConfigurationForm($form['streamwrapper_config'], $backend_form_state);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $streamWrapperEntity */
    $streamWrapperEntity = $this->getEntity();
    // Check before loading the backend plugin so we don't throw an exception.
    if ($streamWrapperEntity->hasValidStreamWrapperPlugin()) {
      $backend = $streamWrapperEntity->getStreamWrapperPlugin();
      if ($backend instanceof PluginFormInterface) {
        $backend_form_state = SubformState::createForSubform($form['streamwrapper_config'], $form, $form_state);
        $backend->submitConfigurationForm($form['streamwrapper_config'], $backend_form_state);
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // Only save the bucket if the form doesn't need to be rebuilt.
    if (!$form_state->isRebuilding()) {
      try {
        $streamWrapperEntity = $this->getEntity();
        $streamWrapperEntity->save();
        $this->messenger->addStatus($this->t('The StreamWrapper was successfully saved.'));
        $form_state->setRedirect('entity.s3fs_streamwrapper.edit_form', ['s3fs_streamwrapper' => $streamWrapperEntity->id()]);
      }
      catch (EntityStorageException $e) {
        $form_state->setRebuild();

        $message = '%type: @message in %function (line %line of %file).';
        $variables = Error::decodeException($e);
        $this->getLogger('s3fs_streamwrapper')->error($message, $variables);

        $this->messenger->addError($this->t('The bucket could not be saved.'));
      }
    }
  }

}
