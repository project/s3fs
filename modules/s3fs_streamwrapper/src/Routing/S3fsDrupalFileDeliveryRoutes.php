<?php

namespace Drupal\s3fs_streamwrapper\Routing;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Defines a route subscriber to register a url for serving static files.
 */
class S3fsDrupalFileDeliveryRoutes implements ContainerInjectionInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Constructs a new S3fsImageStyleRoutes object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('module_handler')
    );
  }

  /**
   * Returns an array of route objects.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */
  public function routes(): array {
    $routes = [];

    // This route is the one that actually processes the file delivery.
    $routes['s3fs_streamwrapper.files.drupal_delivery'] = new Route(
      '/s3/files/{scheme}',
      [
        '_controller' => 'Drupal\s3fs_streamwrapper\Controller\S3fsFileDownloadController::download',
      ],
      [
        '_access' => 'TRUE',
        'scheme' => '.+',
      ]
    );

    // This route is used to generate links.
    $routes['s3fs_streamwrapper.files.linkgen'] = new Route(
      '/s3/files/{scheme}/{file}',
      [
        '_controller' => 'Drupal\s3fs_streamwrapper\Controller\S3fsFileDownloadController::download',
      ],
      [
        '_access' => 'TRUE',
        'scheme' => '.+',
        'file' => '.+',
      ]
    );

    // @see \Drupal\redirect\Routing\RouteSubscriber::alterRoutes()
    if ($this->moduleHandler->moduleExists('redirect')) {
      $routes['s3fs_streamwrapper.files.drupal_delivery']->setDefault('_disable_route_normalizer', TRUE);
      $routes['s3fs_streamwrapper.files.linkgen']->setDefault('_disable_route_normalizer', TRUE);
    }

    return $routes;
  }

}
