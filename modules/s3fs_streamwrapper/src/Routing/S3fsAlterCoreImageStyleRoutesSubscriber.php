<?php

namespace Drupal\s3fs_streamwrapper\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Ensure that ImageStyleDownloadController does not process s3fs images.
 *
 * Due to the core ImageStyleDownloadController() class having no access
 * controls built in for any system except private://, any controller path
 * that utilizes ImageStyleDownloadController() and a wildcard {scheme} in
 * route parameter path must have all s3fs managed paths overridden with a
 * path that denys access.
 */
class S3fsAlterCoreImageStyleRoutesSubscriber extends RouteSubscriberBase {

  /**
   * Constructs a new ImageStyleRoutes object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(protected EntityTypeManagerInterface $entityTypeManager) {
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {

    $storage = $this->entityTypeManager->getStorage('s3fs_streamwrapper');
    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntity[] $wrappers */
    $wrappers = $storage->loadByProperties(['status' => TRUE]);

    /** @var string[] $s3fs_schemes */
    $s3fs_schemes = [];
    foreach ($wrappers as $wrapper) {
      $s3fs_schemes[] = $wrapper->id();
    }
    foreach ($collection->getIterator() as $route_name => $route) {
      $path = $route->getPath();

      if ($path === '/s3/styles/{image_style}/{scheme}') {
        // Skip our own image style route.
        continue;
      }

      if (preg_match('{scheme}', $path)) {
        $controller = $route->getDefault('_controller');
        // We can only work modify string values.
        if (!is_string($controller)) {
          // @todo log an error.
          continue;
        }

        // We only want the class name, not class and method.
        $controller = preg_replace('/::.*/', '', $controller);
        if ($controller == NULL) {
          // @todo log an error
          continue;
        }

        // We can not trust any route that has a scheme parameter that inherits
        // the core ImageStyleDownloadController class.
        if (is_a($controller, 'Drupal\image\Controller\ImageStyleDownloadController', TRUE)) {
          foreach ($s3fs_schemes as $scheme) {
            $new_path = preg_replace('/{scheme}/', $scheme, $path);
            if ($new_path == NULL) {
              // @todo log an error or throw excpetion.
              continue;
            }

            $new_route = new Route(
              $new_path,
              [],
              [
                '_access' => 'FALSE',
              ]
            );
            $collection->add($route_name . '_s3fs_lockdown_' . $scheme, $new_route);
          }
        }
      }
    }
  }

}
