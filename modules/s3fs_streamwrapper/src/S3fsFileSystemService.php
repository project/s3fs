<?php

namespace Drupal\s3fs_streamwrapper;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\Exception\DirectoryNotReadyException;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\Exception\FileExistsException;
use Drupal\Core\File\Exception\FileNotExistsException;
use Drupal\Core\File\Exception\FileWriteException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\s3fs_streamwrapper\StreamWrapper\S3fsBucketStream;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mime\MimeTypeGuesserInterface;

/**
 * Provides helpers to operate on files and stream wrappers.
 *
 * PHP convince functions copy(),rename(), move_uploaded_file(), etc. do not
 * check that the write buffer is successfully flushed. As such we need to
 * handle the writes ourselves to ensure we can propagate any errors.
 *
 * Additionally by calling putObject and copyObject we avoid the
 * StreamWrapper creating a buffer copy of the source file in.
 *
 * @see https://www.drupal.org/project/s3fs/issues/2972161
 * @see https://www.drupal.org/project/s3fs/issues/3204635
 */
class S3fsFileSystemService implements FileSystemInterface {

  /**
   * The inner service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $decorated;

  /**
   * The file logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected StreamWrapperManagerInterface $streamWrapperManager;

  /**
   * The module_handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Mime Type Guessing Service.
   *
   * @var \Symfony\Component\Mime\MimeTypeGuesserInterface
   */
  protected MimeTypeGuesserInterface $mimeGuesser;

  /**
   * Entity Type Manager Service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityManager;

  /**
   * S3fsFileService constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $decorated
   *   FileSystem Service being decorated.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   StreamWrapper manager service.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logging service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module Handler service.
   * @param \Symfony\Component\Mime\MimeTypeGuesserInterface $mimeGuesser
   *   Mime type guesser service.
   */
  public function __construct(FileSystemInterface $decorated, StreamWrapperManagerInterface $stream_wrapper_manager, LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $moduleHandler, MimeTypeGuesserInterface $mimeGuesser) {
    $this->decorated = $decorated;
    $this->streamWrapperManager = $stream_wrapper_manager;
    $this->logger = $logger;
    $this->entityManager = $entity_type_manager;
    $this->moduleHandler = $moduleHandler;
    $this->mimeGuesser = $mimeGuesser;
  }

  /**
   * {@inheritdoc}
   */
  public function moveUploadedFile($filename, $uri): bool {
    $wrapper = $this->streamWrapperManager->getViaUri($uri);
    if ($wrapper instanceof S3fsBucketStream) {
      // Ensure the file was uploaded as part of HTTP POST.
      if (!is_uploaded_file($filename)) {
        return FALSE;
      }
      return $this->putObject($filename, $uri);
    }
    else {
      return $this->decorated->moveUploadedFile($filename, $uri);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function chmod($uri, $mode = NULL): bool {
    return $this->decorated->chmod($uri, $mode);
  }

  /**
   * {@inheritdoc}
   */
  public function unlink($uri, $context = NULL): bool {
    return $this->decorated->unlink($uri, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function realpath($uri): FALSE|string {
    return $this->decorated->realpath($uri);
  }

  /**
   * {@inheritdoc}
   */
  public function dirname($uri): string {
    return $this->decorated->dirname($uri);
  }

  /**
   * {@inheritdoc}
   */
  public function basename($uri, $suffix = NULL) {
    return $this->decorated->basename($uri, $suffix);
  }

  /**
   * {@inheritdoc}
   */
  public function mkdir($uri, $mode = NULL, $recursive = FALSE, $context = NULL): bool {
    return $this->decorated->mkdir($uri, $mode, $recursive, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function rmdir($uri, $context = NULL): bool {
    return $this->decorated->rmdir($uri, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function tempnam($directory, $prefix) {
    return $this->decorated->tempnam($directory, $prefix);
  }

  /**
   * {@inheritdoc}
   */
  public function copy($source, $destination, $replace = self::EXISTS_RENAME): string {
    $wrapper = $this->streamWrapperManager->getViaUri($destination);
    if ($wrapper instanceof S3fsBucketStream) {

      $this->prepareDestination($source, $destination, $replace);
      $srcScheme = $this->streamWrapperManager->getScheme($source);
      $dstScheme = $this->streamWrapperManager->getScheme($destination);

      if ($srcScheme == $dstScheme) {
        $result = $this->copyObject($source, $destination);
      }
      else {
        $result = $this->putObject($source, $destination);
      }

      if (!$result) {
        $this->logger->error("The specified file '%source' could not be copied to '%destination'.",
          [
            '%source' => $source,
            '%destination' => $destination,
          ]);
        throw new FileWriteException("The specified file '$source' could not be copied to '$destination'.");
      }

      return $destination;
    }
    else {
      return $this->decorated->copy($source, $destination, $replace);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function delete($path) {
    return $this->decorated->delete($path);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRecursive($path, callable $callback = NULL) {
    return $this->decorated->deleteRecursive($path, $callback);
  }

  /**
   * {@inheritdoc}
   */
  public function move($source, $destination, $replace = self::EXISTS_RENAME): string {
    $wrapper = $this->streamWrapperManager->getViaUri($destination);
    if ($wrapper instanceof S3fsBucketStream) {
      $this->prepareDestination($source, $destination, $replace);

      // Ensure compatibility with Windows.
      // @see \Drupal\Core\File\FileSystemInterface::unlink().
      if (!$this->streamWrapperManager->isValidUri($source) && (substr(PHP_OS, 0, 3) == 'WIN')) {
        chmod($source, 0600);
      }

      // Attempt to resolve the URIs. This is necessary in certain
      // configurations (see above) and can also permit fast moves across local
      // schemes.
      $real_source = $this->realpath($source) ?: $source;

      $srcScheme = $this->streamWrapperManager->getScheme($real_source);
      $dstScheme = $this->streamWrapperManager->getScheme($destination);

      if ($srcScheme == $dstScheme) {
        $result = $this->copyObject($real_source, $destination);
      }
      else {
        // Both sources are not on the same StreamWrapper.
        // Fall back to slow copy and unlink procedure.
        $result = $this->putObject($real_source, $destination);
      }

      if (!$result) {
        $this->logger->error("The specified file '%source' could not be moved to '%destination'.", [
          '%source' => $source,
          '%destination' => $destination,
        ]);
        throw new FileWriteException("The specified file '$source' could not be moved to '$destination'.");
      }
      else {
        if (!@unlink($real_source)) {
          $this->logger->error("The source file '%source' could not be unlinked after copying to '%destination'.", [
            '%source' => $source,
            '%destination' => $destination,
          ]);
          throw new FileException("The source file '$source' could not be unlinked after copying to '$destination'.");
        }
      }

      return $destination;
    }
    else {
      return $this->decorated->move($source, $destination, $replace);
    }
  }

  /**
   * Prepares the destination for a file copy or move operation.
   *
   * - Checks if $source and $destination are valid and readable/writable.
   * - Checks that $source is not equal to $destination; if they are an error
   *   is reported.
   * - If file already exists in $destination either the call will error out,
   *   replace the file or rename the file based on the $replace parameter.
   *
   * @param string $source
   *   A string specifying the filepath or URI of the source file.
   * @param string|null $destination
   *   A URI containing the destination that $source should be moved/copied to.
   *   The URI may be a bare filepath (without a scheme) and in that case the
   *   default scheme (file://) will be used.
   * @param int $replace
   *   Replace behavior when the destination file already exists:
   *   - FileSystemInterface::EXISTS_REPLACE - Replace the existing file.
   *   - FileSystemInterface::EXISTS_RENAME - Append _{incrementing number}
   *     until the filename is unique.
   *   - FileSystemInterface::EXISTS_ERROR - Do nothing and return FALSE.
   *
   * @see \Drupal\Core\File\FileSystemInterface::copy()
   * @see \Drupal\Core\File\FileSystemInterface::move()
   */
  protected function prepareDestination(string $source, &$destination, int $replace): void {
    $original_source = $source;

    if (!file_exists($source)) {
      if (($realpath = $this->realpath($original_source)) !== FALSE) {
        $this->logger->error("File '%original_source' ('%realpath') could not be copied because it does not exist.", [
          '%original_source' => $original_source,
          '%realpath' => $realpath,
        ]);
        throw new FileNotExistsException("File '$original_source' ('$realpath') could not be copied because it does not exist.");
      }
      else {
        $this->logger->error("File '%original_source' could not be copied because it does not exist.", [
          '%original_source' => $original_source,
        ]);
        throw new FileNotExistsException("File '$original_source' could not be copied because it does not exist.");
      }
    }

    // Prepare the destination directory.
    if ($this->prepareDirectory($destination)) {
      // The destination is already a directory, so append the source basename.
      $destination = $this->streamWrapperManager->normalizeUri($destination . '/' . $this->basename($source));
    }
    else {
      // Perhaps $destination is a dir/file?
      $dirname = $this->dirname($destination);
      if (!$this->prepareDirectory($dirname)) {
        $this->logger->error("The specified file '%original_source' could not be copied because the destination directory '%destination_directory' is not properly configured. This may be caused by a problem with file or directory permissions.", [
          '%original_source' => $original_source,
          '%destination_directory' => $dirname,
        ]);
        throw new DirectoryNotReadyException("The specified file '$original_source' could not be copied because the destination directory '$dirname' is not properly configured. This may be caused by a problem with file or directory permissions.");
      }
    }

    // Determine whether we can perform this operation based on overwrite rules.
    $destination = $this->getDestinationFilename($destination, $replace);
    if (is_bool($destination)) {
      $this->logger->error("File '%original_source' could not be copied because a file by that name already exists in the destination directory ('%destination').", [
        '%original_source' => $original_source,
        '%destination' => $destination,
      ]);
      throw new FileExistsException("File '$original_source' could not be copied because a file by that name already exists in the destination directory ('$destination').");
    }

    // Assert that the source and destination filenames are not the same.
    $real_source = $this->realpath($source);
    $real_destination = $this->realpath($destination);
    if ($source == $destination || ($real_source !== FALSE) && ($real_source == $real_destination)) {
      $this->logger->error("File '%source' could not be copied because it would overwrite itself.", [
        '%source' => $source,
      ]);
      throw new FileException("File '$source' could not be copied because it would overwrite itself.");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function saveData($data, $destination, $replace = self::EXISTS_RENAME): string|FALSE {
    // Write the data to a temporary file.
    $temp_name = $this->tempnam('temporary://', 'file');
    if (is_bool($temp_name)) {
      $this->logger->error('Unable to obtain temporary file name.');
      throw new FileWriteException("No temporary filename available.");
    }

    if (file_put_contents($temp_name, $data) === FALSE) {
      $this->logger->error("Temporary file '%temp_name' could not be created.", ['%temp_name' => $temp_name]);
      throw new FileWriteException("Temporary file '$temp_name' could not be created.");
    }

    // Move the file to its final destination.
    return $this->move($temp_name, $destination, $replace);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareDirectory(&$directory, $options = self::MODIFY_PERMISSIONS): bool {
    return $this->decorated->prepareDirectory($directory, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationFilename($destination, $replace) {
    return $this->decorated->getDestinationFilename($destination, $replace);
  }

  /**
   * {@inheritdoc}
   */
  public function createFilename($basename, $directory): string {
    return $this->decorated->createFilename($basename, $directory);
  }

  /**
   * {@inheritdoc}
   */
  public function getTempDirectory(): string {
    return $this->decorated->getTempDirectory();
  }

  /**
   * {@inheritdoc}
   */
  public function scanDirectory($dir, $mask, array $options = []): array {
    return $this->decorated->scanDirectory($dir, $mask, $options);
  }

  /**
   * Upload a file that is not in the bucket.
   *
   * @param string $source
   *   Source file to be copied.
   * @param string $destination
   *   Destination path in bucket.
   *
   * @return bool
   *   True if successful, else FALSE.
   */
  protected function putObject($source, $destination): BOOL {

    $scheme = StreamWrapperManager::getScheme($destination);

    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $streamWrapperEntity */
    $streamWrapperEntity = $this->entityManager
      ->getStorage('s3fs_streamwrapper')
      ->load($scheme);

    if (mb_strlen($destination) > $streamWrapperEntity->getMaxUriLength()) {
      $this->logger->error("The specified file '%destination' exceeds max URI length limit.",
        [
          '%destination' => $destination,
        ]);
      return FALSE;
    }

    $key_path = $this->streamWrapperManager->getTarget($destination);

    if (is_bool($key_path)) {
      $this->logger->error("Unable to get target from URI '%destination'",
        [
          '%destination' => $destination,
        ]);
      return FALSE;
    }

    $contentType = $this->mimeGuesser->guessMimeType($key_path);

    $uploadParams = $streamWrapperEntity->getCommandParams($destination);
    $uploadParams += [
      'SourceFile' => $source,
      'ContentType' => $contentType,
    ];

    $streamWrapperEntity->getStreamOptions($uploadParams);

    $this->moduleHandler->alter('s3fs_upload_params', $uploadParams);

    $s3 = $streamWrapperEntity->getS3Client();
    try {
      $s3->putObject($uploadParams);
    }
    catch (\Exception $e) {
      return FALSE;
    }

    if ($streamWrapperEntity->getBucket()->waitUntilFileExists($uploadParams['Key'])) {
      $metadata = $streamWrapperEntity->getBucket()->getS3Metadata($uploadParams['Key']);
      if (!empty($metadata)) {
        $streamWrapperEntity->writeCache($metadata);
        clearstatcache(TRUE, $destination);
      }
    }
    else {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Copy a file that is already in the the bucket.
   *
   * @param string $source
   *   Source file to be copied.
   * @param string $destination
   *   Destination path in bucket.
   *
   * @return bool
   *   True if successful, else FALSE.
   */
  protected function copyObject($source, $destination): BOOL {

    $scheme = StreamWrapperManager::getScheme($destination);

    /** @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $streamWrapperEntity */
    $streamWrapperEntity = $this->entityManager
      ->getStorage('s3fs_streamwrapper')
      ->load($scheme);

    if (mb_strlen($destination) > $streamWrapperEntity->getMaxUriLength()) {
      $this->logger->error("The specified file '%destination' exceeds max URI length limit.",
        [
          '%destination' => $destination,
        ]);
      return FALSE;
    }

    $key_path = $this->streamWrapperManager->getTarget($destination);

    if (is_bool($key_path)) {
      $this->logger->error("Unable to get target from URI '%destination'",
        [
          '%destination' => $destination,
        ]);
      return FALSE;
    }

    $contentType = $this->mimeGuesser->guessMimeType($key_path);

    $copyParams = $streamWrapperEntity->getCommandParams($destination);

    $srcKey = $streamWrapperEntity->convertUriToKeyedPath($source);
    $copyParams += [
      'CopySource' => $srcKey,
      'ContentType' => $contentType,
      'MetadataDirective' => 'REPLACE',
    ];

    $streamWrapperEntity->getStreamOptions($copyParams);

    $this->moduleHandler->alter('s3fs_copy_params_alter', $copyParams);

    $s3 = $streamWrapperEntity->getS3Client();
    try {
      $s3->copyObject($copyParams);
    }
    catch (\Exception $e) {
      return FALSE;
    }

    if ($streamWrapperEntity->getBucket()->waitUntilFileExists($copyParams['Key'])) {
      $metadata = $streamWrapperEntity->getBucket()->getS3Metadata($copyParams['Key']);
      if (!empty($metadata)) {
        $streamWrapperEntity->writeCache($metadata);
        clearstatcache(TRUE, $destination);
      }
    }
    else {
      return FALSE;
    }

    return TRUE;
  }

}
