<?php

namespace Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapper;

use Aws\Middleware;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginBase;
use Psr\Http\Message\RequestInterface;

/**
 * Plugin implementation of the s3fs_stremwrapper.
 *
 * Provides a fully customizable StreamWrapper configuration.
 *
 * @S3StreamWrapper(
 *   id = "s3_custom",
 *   label = @Translation("Custom StreamWrapper"),
 *   description = @Translation("A fully customizable option")
 * )
 *
 * @method array{'encryption': string, 'use_drupal': bool, 'use_https': bool, 'redirect_styles_ttl': int, 'presigned_urls': string, 'saveas': string, 'use_cname': bool, 'cname': string, 'domain_root': string, 'upload_as_private': bool, 'bucketId': string, 'root_folder': string, 'cache_control_header': string} getConfiguration()
 */
class CustomS3StreamWrapperPlugin extends S3StreamWrapperPluginBase implements PluginFormInterface {

  // The s3fs_bucket entity type causes a Database serialization.
  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Initialize the plugin with dependent services.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin Definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   Entity Type Manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module Handler Service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String Translation service.
   *
   * @todo Should we switch from hooks to events.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_manager, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory, TranslationInterface $string_translation) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_manager, $module_handler);
    $this->configFactory = $config_factory;
    $this->setStringTranslation($string_translation);
  }

  /**
   * {@inheritdoc}
   */
  public static function create($container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('config.factory'),
      $container->get('string_translation')
    );
  }

  /**
   * Gets default configuration for this plugin.
   *
   * @return array{'encryption': string, 'use_drupal': bool, 'use_https': bool, 'redirect_styles_ttl': int, 'presigned_urls': string, 'saveas': string, 'use_cname': bool, 'cname': string, 'domain_root': string, 'upload_as_private': bool, 'bucketId': string, 'root_folder': string, 'cache_control_header': string}
   *   An associative array with the default configuration.
   */
  public function defaultConfiguration(): array {
    $config = [
      'encryption' => '',
      'use_drupal' => FALSE,
      'use_https' => TRUE,
      'redirect_styles_ttl' => 300,
      'presigned_urls' => '',
      'saveas' => '',
      'use_cname' => FALSE,
      'cname' => '',
      'domain_root' => 'none',
      'upload_as_private' => FALSE,
    ];
    $config += parent::defaultConfiguration();

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $config = $this->streamWrapper?->getStreamWrapperConfig();
    $config += $this->defaultConfiguration();

    $form['bucket_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Bucket'),
      '#open' => TRUE,
      '#parents' => ['streamwrapper_config'],
    ];

    $enabledBuckets = [];
    $buckets = $this->entityManager
      ->getStorage('s3fs_bucket')
      ->loadByProperties(['status' => TRUE]);

    /** @var \Drupal\s3fs_bucket\S3BucketInterface $bucket */
    foreach ($buckets as $bucket) {
      $enabledBuckets[$bucket->id()] = $bucket->label();
    }

    $form['bucket_settings']['bucketId'] = [
      '#type' => 'select',
      '#title' => $this->t('Bucket'),
      '#default_value' => $config['bucketId'],
      '#description' => $this->t('Which bucket to utilize'),
      '#options' => $enabledBuckets,
    ];
    $form['bucket_settings']['root_folder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Root Folder'),
      '#default_value' => $config['root_folder'],
      '#description' => $this->t(
        "S3 File System uses the specified folder as the root of the file system within your bucket (if blank, the bucket
      root is used). This is helpful when your bucket is used by multiple sites, or has additional data in it which
      s3fs should not interfere with.<br>
      The metadata refresh function will not retrieve metadata for any files which are outside the Root Folder.<br>
      This setting is case sensitive. Do not include leading or trailing slashes.<br>
      Changing this setting <b>will not</b> move any files. If you've already uploaded files to S3 through S3 File
      System, you will need to manually move them into this folder."
      ),
    ];

    $form['storage_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Storage Settings'),
      '#description' => $this->t(
        "Settings that control how the objects will be stored and served from the S3 Bucket. ."
      ),
      '#open' => TRUE,
      '#parents' => ['streamwrapper_config'],
    ];
    $form['storage_settings']['cache_control_header'] = [
      '#type' => 'textfield',
      '#title' => $this->t('S3 Object Cache-Control Header'),
      '#default_value' => $config['cache_control_header'],
      '#description' => $this->t('The cache control header to set on all S3 objects for CDNs and browsers, e.g.
      "public, max-age=300".'
      ),
    ];
    $form['storage_settings']['encryption'] = [
      '#type' => 'select',
      '#options' => [
        '' => $this->t('None'),
        // phpcs:ignore DrupalPractice.General.OptionsT.TforValue
        'AES256' => 'AES256',
        // phpcs:ignore DrupalPractice.General.OptionsT.TforValue
        'aws:kms' => 'aws:kms',
      ],
      '#title' => $this->t('Server-Side Encryption'),
      '#default_value' => $config['encryption'],
      '#description' => $this->t(
        'If your bucket requires @ENCRYPTION, you can specify the encryption algorithm here',
        [
          '@ENCRYPTION' => Link::fromTextAndUrl($this->t('server-side encryption'),
            Url::fromUri('http://docs.aws.amazon.com/AmazonS3/latest/dev/UsingServerSideEncryption.html'
            ))->toString(),
        ]
      ),
    ];
    $form['storage_settings']['upload_as_private'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Upload all files as private in S3'),
      '#default_value' => $config['upload_as_private'],
      '#description' => $this->t(
        "Enable to store all files without public viewable permissions.</em>"
      ),
    ];

    $form['link_generation'] = [
      '#type' => 'details',
      '#title' => $this->t('Access Link Generation'),
      '#description' => $this->t(
        "Configure how objects will be accessed."
      ),
      '#open' => TRUE,
      '#parents' => ['streamwrapper_config'],
    ];
    $form['link_generation']['use_drupal'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Deliver files through Drupal'),
      '#default_value' => $config['use_drupal'],
      '#description' => $this->t(
        'Drupal will deliver the files to the user, similar to how private:// files are delivered.'
      ),
    ];
    $form['link_generation']['use_https'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Always serve files from S3 via HTTPS'),
      '#default_value' => $config['use_https'],
      '#description' => $this->t(
        'Forces S3 File System to always generate HTTPS URLs for files in your bucket,
      e.g. "https://mybucket.s3.amazonaws.com/smiley.jpg".<br>
      Without this setting enabled, URLs for your files will use the same scheme as the page they are served from.'
      ),
      '#states' => [
        'visible' => [
          ':input[id=edit-streamwrapper-config-use-drupal]' => ['checked' => FALSE],
        ],
      ],
    ];
    $form['link_generation']['use_cname'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use CNAME'),
      '#default_value' => $config['use_cname'],
      '#description' => $this->t('Serve files from a custom domain by using an appropriately named bucket, e.g. "mybucket.mydomain.com".'),
      '#states' => [
        'visible' => [
          ':input[id=edit-streamwrapper-config-use-drupal]' => ['checked' => FALSE],
        ],
      ],
    ];
    $form['link_generation']['cname_settings_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('CNAME Settings'),
      '#states' => [
        'visible' => [
          ':input[id=edit-streamwrapper-config-use-cname]' => ['checked' => TRUE],
          ':input[id=edit-streamwrapper-config-use-drupal]' => ['checked' => FALSE],
        ],
      ],
      '#parents' => ['streamwrapper_config'],
    ];
    $form['link_generation']['cname_settings_fieldset']['cname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain Name'),
      '#default_value' => $config['cname'],
      '#description' => $this->t('If serving files from a CDN, the bucket name can differ from the domain name.'),
    ];
    $form['link_generation']['cname_settings_fieldset']['domain_root'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Map Domain Name to specific path'),
      '#options'       => [
        'none' => $this->t('Entire bucket (mybucket)'),
        'root' => $this->t('Root folder (mybucket/root)'),
      ],
      '#default_value' => $config['domain_root'],
      '#description'   => $this->t("Map the domain name to the location from where the file should be pulled.
        This is useful when using a service such as Cloudfront where the origin path can be a specific
        folder in a bucket, rather than the entire bucket."
      ),
    ];
    $form['link_generation']['redirect_styles_ttl'] = [
      '#type' => 'number',
      '#title' => $this->t('The TTL of the redirect cache to the s3 styles'),
      '#default_value' => $config['redirect_styles_ttl'],
      '#description' => $this->t('Styles will be redirected to S3 and Dynamic Page Cache module will cache the response for the specified TTL.'),
    ];
    $form['link_generation']['path_specific'] = [
      '#type' => 'details',
      '#title' => $this->t('Path-specific Settings'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          ':input[id=edit-streamwrapper-config-use-drupal]' => ['checked' => FALSE],
        ],
      ],
      '#parents' => ['streamwrapper_config'],
    ];
    // @todo Should we consider making this a multi-entry form instead of a text area?
    $form['link_generation']['path_specific']['presigned_urls'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Presigned URLs'),
      '#default_value' => $config['presigned_urls'],
      '#rows' => 5,
      '#description' => $this->t(
        'A list of timeouts and paths that should be delivered through a presigned url.<br>
      Enter one value per line, in the format timeout|path. e.g. "60|private_files/*". Paths use regex patterns
      as per @link. If no timeout is provided, it defaults to 60 seconds.<br>',
        [
          '@link' => Link::fromTextAndUrl($this->t('preg_match'), Url::fromUri('http://php.net/preg_match'))
            ->toString(),
        ]
      ),
    ];
    // @todo should we consider making this a multi-entry form instead of a text area?
    $form['link_generation']['path_specific']['saveas'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Force Save As'),
      '#default_value' => $config['saveas'],
      '#rows' => 5,
      '#description' => $this->t(
        'A list of paths for which users will be forced to save the file, rather than displaying it in the browser.<br>
      Enter one value per line. e.g. "video/*". Paths use regex patterns as per @link.<br>',
        [
          '@link' => Link::fromTextAndUrl($this->t('preg_match'), Url::fromUri('http://php.net/preg_match'))
            ->toString(),
        ]
      ),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    // @todo Implement validateConfigurationForm() method.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    // @todo Implement submitConfigurationForm() method.
  }

  /**
   * {@inheritdoc}
   */
  public function writeCache(array $metadata): void {
    $this->getBucket()->writeCache($metadata);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteCache(string $uri): int {
    $converted = $this->convertUriToKeyedPath($uri, FALSE);
    return $this->getBucket()->deleteCache($converted);
  }

  /**
   * {@inheritdoc}
   */
  public function getCommandParams(string $uri, array $params = []): array {
    $convertedPath = $this->convertUriToKeyedPath($uri, FALSE);

    $params = $this->getBucket()->getCommandParams($convertedPath, $params);

    return $params;
  }

  /**
   * {@inheritdoc}
   */
  public function getS3Metadata(string $uri): array|false {
    $converted = $this->convertUriToKeyedPath($uri, FALSE);
    return $this->getBucket()->getS3Metadata($converted);
  }

  /**
   * Try to fetch an object from the metadata cache.
   *
   * If that file isn't in the cache, we assume it doesn't exist.
   *
   * @param string $uri
   *   The uri of the resource to check.
   *
   * @return array|false
   *   An array if the $uri exists, otherwise FALSE.
   */
  protected function getS3fsObject(string $uri): array|FALSE {

    // For the root directory, return metadata for a generic folder.
    if (StreamWrapperManager::getTarget($uri) == '') {
      return $this->convertMetadata('s3://', []);
    }

    if (mb_strlen($uri) > $this->getMaxUriLength()) {
      return FALSE;
    }

    $converted = $this->convertUriToKeyedPath($uri, FALSE);

    return $this->getBucket()->getS3fsObject($converted);
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl(string $uri, bool $isSecureRequest = FALSE): string {
    // Obtain current config and/ merge with default. Allows us to assume all
    // variables exist.
    $config = $this->getConfiguration();
    $s3Client = $this->getS3Client();

    $cname = [
      'port' => NULL,
      'host' => '',
      'scheme' => 'https',
    ];

    if ($config['use_cname']) {
      $domain = $config['cname'];
      if ($domain) {
        $domainpart = explode(':', $domain, 2);
        if (!empty($domainpart[1])) {
          $cname['port'] = (int) $domainpart[1];
        }
        $cname['host'] = $domainpart[0];
        if ($isSecureRequest) {
          $cname['scheme'] = 'https';
        }
        else {
          $cname['scheme'] = $config['use_https'] ? 'https' : 'http';
        }
      }
    }

    // Convert the presigned URLs string to an associative array like
    // [blob => timeout].
    $presignedUrls = [];
    if (!empty($config['presigned_urls'])) {
      foreach (explode(PHP_EOL, $config['presigned_urls']) as $line) {
        $blob = trim($line);
        if ($blob) {
          if (preg_match('/(.*)\|(.*)/', $blob, $matches)) {
            $blob = $matches[2];
            $timeout = $matches[1];
            $presignedUrls[$blob] = $timeout;
          }
          else {
            $presignedUrls[$blob] = 60;
          }
        }
      }
    }

    // Convert the forced save-as string to an array.
    $saveAs = [];
    if (!empty($config['saveas'])) {
      foreach (explode(PHP_EOL, $config['saveas']) as $line) {
        $blob = trim($line);
        if ($blob) {
          $saveAs[] = $blob;
        }
      }
    }

    // Note that $uri is the unaltered value of the File's URI, while
    // $s3_key may be changed at various points to account for implementation
    // details on the S3 side (e.g. root_folder).
    $s3_key = $this->convertUriToKeyedPath($uri, FALSE);
    $target = StreamWrapperManager::getTarget($uri);

    if (is_bool($target)) {
      return Url::fromUserInput('/')->toString();
    }

    // Is this an ImageStyle Path.
    $isImageStylePath = FALSE;
    $path_parts = explode('/', $target);
    if ($path_parts[0] == 'styles' && substr($s3_key, -4) != '.css') {
      $isImageStylePath = TRUE;
    }

    // S3fsFileDownloadController will serve the files from Drupal.
    if ($config['use_drupal'] && !$isImageStylePath) {
      return Url::fromRoute(
        's3fs_streamwrapper.files.linkgen',
        [
          'file' => $target,
          'scheme' => StreamWrapperManager::getScheme($uri),
        ],
        ['absolute' => TRUE, 'path_processing' => FALSE]
      )
        ->toString();
    }

    // Default for image style URL generation.
    $suppressItok = TRUE;
    $itok = '';

    // When generating an image derivative URL, e.g. styles/thumbnail/blah.jpg,
    // if the file doesn't exist, provide a URL to s3fs's special version of
    // image_style_deliver(), which will create the derivative when that URL
    // gets requested.  When the file does exist we need to calculate an itok
    // in case the link requires presigning.
    if ($isImageStylePath) {

      // Style derivative does not yet exist in the bucket.
      if ($config['use_drupal'] || !$this->getS3fsObject($uri)) {
        // The style delivery path looks like: s3/styles/thumbnail/...
        // And $path_parts looks like ['styles', 'thumbnail', ...].
        $style = $path_parts[1];
        $file = implode('/', array_slice($path_parts, 3));
        return Url::fromRoute(
          's3fs_streamwrapper.image_styles.linkgen',
          [
            'image_style' => $style,
            'file' => $file,
            'scheme' => StreamWrapperManager::getScheme($uri),
          ],
          ['absolute' => TRUE, 'path_processing' => FALSE]
        )
          ->toString();
      }

      // Generate itok key in case link that need to be presigned.
      $suppressItok = $this->configFactory->get('image.settings')->get('suppress_itok_output');
      if (!$suppressItok) {
        $imageStyleName = $path_parts[1];
        $srcScheme = $path_parts[2];
        // Strip off 'style', ImageStyleName and scheme.
        array_splice($path_parts, 0, 3);
        $srcImageUri = $srcScheme . '://' . implode('/', $path_parts);

        /** @var \Drupal\image\ImageStyleInterface|null $imageStyleEntity */
        $imageStyleEntity = $this->entityManager
          ->getStorage('image_style')
          ->load($imageStyleName);

        if ($imageStyleEntity) {
          $itok = $imageStyleEntity->getPathToken($srcImageUri);
        }
        else {
          // Not a known image style, do not attempt to add ITOK to URL.
          $suppressItok = TRUE;
        }
      }

    }

    // Set up the URL settings as specified in our settings page.
    $url_settings = [
      'torrent' => FALSE,
      'presigned_url' => FALSE,
      'timeout' => 60,
      'forced_saveas' => FALSE,
      'api_args' => [],
      'custom_GET_args' => [],
    ];

    // Presigned URLs.
    foreach ($presignedUrls as $blob => $timeout) {
      // ^ is used as the delimeter because it's an illegal character in URLs.
      if (preg_match("^$blob^", $target)) {
        $url_settings['presigned_url'] = TRUE;
        $url_settings['timeout'] = $timeout;
        break;
      }
    }
    // Forced Save As.
    foreach ($saveAs as $blob) {
      if (preg_match("^$blob^", $target)) {
        $filename = basename($s3_key);
        $url_settings['api_args']['ResponseContentDisposition'] = "attachment; filename=\"$filename\"";
        $url_settings['forced_saveas'] = TRUE;
        break;
      }
    }

    // Allow other modules to change the URL settings.
    $this->moduleHandler->alter('s3fs_url_settings', $url_settings, $s3_key);

    // Prepend root_folder if necessary.
    if (
      // We are generating the link using CNAME.
      $config['use_cname']
      // Domain root stripping is enabled.
      && $config['domain_root'] !== 'none'
      // There is actually a root folder to strip.
      && !empty($config['root_folder'])
    ) {
      if (str_starts_with($s3_key, $config['root_folder'])) {
        // Only if root folder is the start of the string do we consider
        // removing it.
        $s3_key = mb_substr($s3_key, mb_strlen($config['root_folder']) + 1);
      }
    }

    $commandSettings = [
      'Bucket' => $this->getBucket()->getBucketName(),
      'Key'    => $s3_key,
    ];

    // Handle presign expire timeout.
    $expires = NULL;
    if ($url_settings['presigned_url']) {
      $expires = "+{$url_settings['timeout']} seconds";

    }
    else {
      // Due to Amazon's security policies (see Request client parameters @
      // http://docs.aws.amazon.com/AmazonS3/latest/API/RESTObjectGET.html),
      // only signed requests can use request parameters.
      // Thus, we must provide an expiry time for any URLs which specify
      // Response* API args.
      foreach ($url_settings['api_args'] as $key => $arg) {
        if (strpos($key, 'Response') === 0) {
          // @see https://aws.amazon.com/premiumsupport/knowledge-center/presigned-url-s3-bucket-expiration/
          // Max limit: Instance Credential 6h, STS 3d, IAM 7d.
          $expires = "21600 seconds";
          break;
        }
      }
    }

    // If this file is versioned, attach the version number to
    // ensure that browser caches will be bypassed upon version changes.
    $meta = $this->getBucket()->readCache($uri);
    if (!empty($meta['version'])) {
      $commandSettings['VersionId'] = $meta['version'];
    }

    foreach ($url_settings['api_args'] as $key => $arg) {
      $commandSettings[$key] = $arg;
    }

    try {
      $command = $s3Client->getCommand('GetObject', $commandSettings);
    }
    catch (\Exception $e) {
      watchdog_exception('S3fs', $e);
      return Url::fromUserInput('/')->toString();
    }
    // Make sure the url scheme is set correctly.
    if ($isSecureRequest) {
      $scheme = 'https';
    }
    else {
      $scheme = $config['use_https'] ? 'https' : 'http';
    }

    $command->getHandlerList()->appendBuild(
      Middleware::mapRequest(function (RequestInterface $request) use (
        $scheme
      ) {
        $requestUri = $request->getUri();
        if ($requestUri->getPort() == 80 || $requestUri->getPort() == 443) {
          // Reset port value for use with scheme select.
          $requestUri = $requestUri->withPort(NULL);
        }
        $requestUri = $requestUri->withScheme($scheme);

        return $request->withUri($requestUri);
      }),
      'set-scheme'
    );

    if (!empty($url_settings['custom_GET_args'])) {
      // If another module added a 'custom_GET_args' array to the url settings,
      // add a build handler to process them.
      $command->getHandlerList()->appendBuild(
        Middleware::mapRequest(function (RequestInterface $request) use (
          $url_settings
        ) {
          $requestUri = $request
            ->getUri()
            ->withQueryValues($request->getUri(), $url_settings['custom_GET_args']);

          return $request->withUri($requestUri, TRUE);
        }),
        'add-getargs'
      );
    }

    if (!empty($config['use_cname'])) {
      $command->getHandlerList()->appendBuild(
        Middleware::mapRequest(function (RequestInterface $request) use (
          $cname
        ) {

          $requestUri = $request->getUri()
            ->withHost($cname['host']);

          if (!empty($cname['port'])) {
            $requestUri = $requestUri->withPort($cname['port']);
          }

          return $request->withUri($requestUri);
        }),
        'use-cname'
      );
    }

    if ($isImageStylePath && !$suppressItok) {
      $command->getHandlerList()->appendBuild(
        Middleware::mapRequest(function (RequestInterface $request) use (
          $itok
        ) {
          $requestUri = $request->getUri();
          $requestUri = $requestUri->withQueryValue($requestUri, 'itok', $itok);
          return $request->withUri($requestUri);
        }),
        'add-itok'
      );
    }

    if (!empty($expires)) {
      // Need to use a presign URL.
      try {
        $external_url = (string) $s3Client->createPresignedRequest($command, $expires)->getUri();
      }
      catch (\Exception $e) {
        watchdog_exception('S3FS', $e);
        return Url::fromUserInput('/')->toString();
      }

      if ($isImageStylePath && !$suppressItok) {
        $parsedUrl = UrlHelper::parse($external_url);
        $queryParams = UrlHelper::filterQueryParameters($parsedUrl['query'], ['itok']);
        $external_url = $parsedUrl['path'] . '?' . UrlHelper::buildQuery($queryParams);
      }

    }
    else {
      // No special request given, we can generate the link.
      if (!$config['use_cname']) {
        try {
          $external_url = $s3Client->getObjectUrl($this->getBucket()->getBucketName(), $s3_key);
        }
        catch (\Exception $e) {
          watchdog_exception('S3FS', $e);
          return Url::fromUserInput('/')->toString();
        }
        if (!$config['use_https'] && !$isSecureRequest) {
          // Forced HTTPS not enabled and not an HTTPS page load.
          /** @var string}null $external_url */
          $external_url = preg_replace('#^https:#', 'http:', $external_url);
        }
      }
      else {
        $external_url = $cname['scheme'] . '://' . $cname['host'];
        if (!empty($cname['port'])) {
          $external_url = $external_url . ':' . $cname['port'];
        }
        if ($this->getBucket()->isPathStyleEndpoint()) {
          $external_url = $external_url . '/' . $this->getBucket()->getBucketName();
        }
        $external_url = $external_url . '/' . UrlHelper::encodePath($s3_key);
      }

      if (!empty($meta['version'])) {
        $external_url = $this->appendGetArg($external_url, 'VersionId', $meta['version']);
      }

      foreach ($url_settings['custom_GET_args'] as $name => $value) {
        $external_url = $this->appendGetArg($external_url, $name, $value);
      }

    }

    return $external_url;
  }

  /**
   * Helper function to safely append a GET argument to a given base URL.
   *
   * @param string $base_url
   *   The URL onto which the GET arg will be appended.
   * @param string $name
   *   The name of the GET argument.
   * @param string|null $value
   *   The value of the GET argument. Optional.
   *
   * @return string
   *   The converted path GET argument.
   */
  protected static function appendGetArg(string $base_url, string $name, string $value = NULL): string {
    $separator = strpos($base_url, '?') === FALSE ? '?' : '&';
    $new_url = "{$base_url}{$separator}{$name}";
    if ($value !== NULL) {
      $new_url .= "=$value";
    }
    return $new_url;
  }

  /**
   * {@inheritdoc}
   */
  public function stat($uri): array|FALSE {
    $metadata = $this->getS3fsObject($uri);
    if ($metadata) {
      $stat = [];
      $stat[0] = $stat['dev'] = 0;
      $stat[1] = $stat['ino'] = 0;
      // Use the S_IFDIR posix flag for directories, S_IFREG for files.
      // All files are considered writable, so OR in 0777.
      $stat[2] = $stat['mode'] = ($metadata['dir'] ? 0040000 : 0100000) | 0777;
      $stat[3] = $stat['nlink'] = 0;
      $stat[4] = $stat['uid'] = 0;
      $stat[5] = $stat['gid'] = 0;
      $stat[6] = $stat['rdev'] = 0;
      $stat[7] = $stat['size'] = 0;
      $stat[8] = $stat['atime'] = 0;
      $stat[9] = $stat['mtime'] = 0;
      $stat[10] = $stat['ctime'] = 0;
      $stat[11] = $stat['blksize'] = 0;
      $stat[12] = $stat['blocks'] = 0;

      if (!$metadata['dir']) {
        $stat[4] = $stat['uid'] = 's3fs';
        $stat[7] = $stat['size'] = $metadata['filesize'];
        $stat[8] = $stat['atime'] = $metadata['timestamp'];
        $stat[9] = $stat['mtime'] = $metadata['timestamp'];
        $stat[10] = $stat['ctime'] = $metadata['timestamp'];
      }
      return $stat;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isDir(string $uri): bool {
    $metadata = $this->getS3fsObject($uri);
    return $metadata ? $metadata['dir'] : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getStreamOptions(array &$options): void {
    $this->getBucket()->getStreamOptions($options);

    $config = $this->getConfiguration();

    if ($config['upload_as_private']) {
      unset($options['ACL']);
    }
    // Set the Cache-Control header, if the user specified one.
    if (!empty($config['cache_control_header'])) {
      $options['CacheControl'] = $config['cache_control_header'];
    }

    if (!empty($config['encryption'])) {
      $options['ServerSideEncryption'] = $config['encryption'];
    }

  }

  /**
   * {@inheritdoc}
   */
  public function convertUriToKeyedPath(string $uri, bool $prepend_bucket = TRUE): string {
    $config = $this->getConfiguration();

    // Remove the protocol.
    $parts = explode('://', $uri);

    // Remove erroneous leading or trailing, forward-slashes and backslashes.
    $parts[1] = trim($parts[1], '\\/');

    $target_path = '';

    // If it's set, all files are placed in the root folder.
    if (!empty($config['root_folder'])) {
      $target_path = "{$config['root_folder']}/";
    }

    if (!empty($parts[1])) {
      $target_path .= "{$parts[1]}";
    }

    // Allow the Bucket to make changes.
    $target_path = $this->getBucket()->alterKeyedPath($target_path);

    // Prepend the uri with a bucket since AWS SDK expects this.
    if ($prepend_bucket) {
      $target_path = "{$this->getBucket()->getBucketName()}/{$target_path}";
    }

    // If $parts[1] was empty we might have a trailing '/';.
    $target_path = rtrim($target_path, '/');

    return $target_path;
  }

  /**
   * {@inheritdoc}
   */
  public function convertMetadata(string $uri, array $metadata): array {
    $path = $this->convertUriToKeyedPath($uri, FALSE);
    return $this->getBucket()->convertMetadata($path, $metadata);
  }

  /**
   * {@inheritdoc}
   */
  public function listDir(string $uri): array {
    $config = $this->getConfiguration();
    $scheme = StreamWrapperManager::getScheme($uri);

    $converted = $this->convertUriToKeyedPath($uri, FALSE);

    $pathListing = $this->getBucket()->listDir($converted);

    $listing = [];
    foreach ($pathListing as $path) {
      if (!empty($config['root_folder'])) {
        $path = substr($path, strlen($config['root_folder']) + 1);
      }
      $listing[] = "$scheme://$path";
    }
    return $listing;
  }

  /**
   * {@inheritdoc}
   */
  public function isDirEmpty(string $uri): bool {
    $converted = $this->convertUriToKeyedPath($uri, FALSE);
    return $this->getBucket()->isDirEmpty($converted);
  }

  /**
   * {@inheritdoc}
   */
  public function useDrupalDelivery(): bool {
    return $this->getConfiguration()['use_drupal'];
  }

  /**
   * {@inheritdoc}
   */
  public function getMaxUriLength(): int {
    $config = $this->getConfiguration();
    $max_length = parent::getMaxUriLength();
    if (!empty($config['root_folder'])) {
      return $max_length - strlen($config['root_folder']) - 1;
    }
    return $max_length;
  }

}
