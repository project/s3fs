<?php

namespace Drupal\s3fs_streamwrapper\Plugin;

use Aws\S3\S3ClientInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\s3fs_bucket\S3BucketInterface;
use Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface;

/**
 * Base class for S3 StreamWrapper Plugin plugins.
 *
 * @phpstan-property array{'bucketId': string, 'root_folder': string, 'cache_control_header': string} $configuration
 */
abstract class S3StreamWrapperPluginBase extends PluginBase implements S3StreamWrapperPluginInterface {

  /**
   * The server this backend is configured for.
   *
   * @var \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface|null
   */
  protected ?S3StreamWrapperEntityInterface $streamWrapper = NULL;

  /**
   * Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityManager;

  /**
   * Module Handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Initialize the plugin with dependent services.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin Definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   Entity Type Manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module Handler Service.
   *
   * @todo Should we switch from hooks to events.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_manager, ModuleHandlerInterface $module_handler) {
    if (($configuration['#streamwrapper'] ?? NULL) instanceof S3StreamWrapperEntityInterface) {
      $this->setStreamWrapper($configuration['#streamwrapper']);
      unset($configuration['#streamwrapper']);
    }
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entity_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create($container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    $plugin_definition = $this->getPluginDefinition();
    return $plugin_definition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $plugin_definition = $this->getPluginDefinition();
    return $plugin_definition['description'] ?? '';
  }

  /**
   * Gets default configuration for this plugin.
   *
   * @return array{'bucketId': string, 'root_folder': string, 'cache_control_header': string}
   *   An associative array with the default configuration.
   */
  public function defaultConfiguration(): array {
    return [
      'bucketId' => '',
      'root_folder' => '',
      'cache_control_header' => '',
    ];
  }

  /**
   * Gets this plugin's configuration.
   *
   * @return array{'bucketId': string, 'root_folder': string, 'cache_control_header': string}
   *   An array of this plugin's configuration.
   */
  public function getConfiguration(): array {
    return $this->configuration += $this->defaultConfiguration();

  }

  /**
   * Sets the configuration for this plugin instance.
   *
   * @param array{'bucketId': string, 'root_folder': string, 'cache_control_header': string} $configuration
   *   An associative array containing the plugin's configuration.
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration;

    if ($this->streamWrapper && $this->streamWrapper->getStreamWrapperConfig() !== $configuration) {
      $this->streamWrapper->setStreamWrapperConfig($configuration);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function useDrupalDelivery(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setStreamWrapper(S3StreamWrapperEntityInterface $streamWrapper): static {
    $this->streamWrapper = $streamWrapper;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBucketId(): string {
    $config = $this->getConfiguration();
    return $config['bucketId'];
  }

  /**
   * {@inheritdoc}
   */
  public function getBucket(): S3BucketInterface {
    /** @var \Drupal\s3fs_bucket\S3BucketInterface|NULL $staticBucket */
    $staticBucket = &drupal_static('s3fs_bucket_entity_' . $this->getBucketId());

    if ($staticBucket) {
      return $staticBucket;
    }

    /** @var \Drupal\s3fs_bucket\S3BucketInterface|null $bucket */
    $bucket = $this->entityManager
      ->getStorage('s3fs_bucket')
      ->load($this->getBucketId());

    if ($bucket && $bucket->status()) {
      $staticBucket = $bucket;
    }
    else {
      $staticBucket = NULL;
      throw new \Exception('Unable to load Bucket Entity');
    }

    return $staticBucket;
  }

  /**
   * {@inheritdoc}
   */
  public function getS3Client(): S3ClientInterface {
    return $this->getBucket()->getS3Client();
  }

  /**
   * {@inheritdoc}
   */
  public function getMaxUriLength(): int {
    // Max target length however strlen 'scheme://' does not impact the limit.
    return $this->getBucket()->getMaxPathLength() + strlen($this->getStreamWrapper()->id()) + 3;
  }

  /**
   * Get the entity that controls this plugin.
   *
   * @return \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface
   *   The streamWrapper entity.
   *
   * @throws \Exception
   */
  protected function getStreamWrapper(): S3StreamWrapperEntityInterface {
    if ($this->streamWrapper == NULL) {
      throw new \Exception('streamWrapper has not yet been set');
    }

    return $this->streamWrapper;
  }

}
