<?php

namespace Drupal\s3fs_streamwrapper\Plugin;

use Aws\S3\S3ClientInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\s3fs_bucket\S3BucketInterface;
use Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface;

/**
 * Defines an interface for S3 StreamWrapper Plugin plugins.
 */
interface S3StreamWrapperPluginInterface extends PluginInspectionInterface, ConfigurableInterface, ContainerFactoryPluginInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Returns the plugin's description.
   *
   * @return string
   *   A string describing the plugin. Might contain HTML and should be already
   *   sanitized for output.
   */
  public function getDescription();

  /**
   * Obtain a configured AWS S3Client that connects to this bucket.
   *
   * @return \Aws\S3\S3ClientInterface
   *   An AWS SDK S3 Client.
   */
  public function getS3Client(): S3ClientInterface;

  /**
   * Return bucket and key for a command array.
   *
   * @param string $uri
   *   Uri to the required object.
   * @param array $params
   *   Existing params.
   *
   * @return array
   *   A modified path to the key in S3.
   */
  public function getCommandParams(string $uri, array $params = []): array;

  /**
   * Returns the max length a URI can be.
   *
   * @return int
   *   Max length a URI can be.
   */
  public function getMaxUriLength(): int;

  /**
   * Drupal StreamWrapper functions.
   */

  /**
   * Returns a web accessible URL for the resource.
   *
   * The format of the returned URL will be different depending on how the S3
   * integration has been configured.
   *
   * @param string $uri
   *   The URI of the file to generate URL for.
   * @param bool $isSecureRequest
   *   Is the request loaded via a secure(https) page.
   *
   * @return string
   *   A web accessible URL for the resource.
   */
  public function getExternalUrl(string $uri, bool $isSecureRequest = FALSE): string;

  /**
   * Helper functions for normal StreamWrappers.
   */

  /**
   * Get the status of the file with the specified URI.
   *
   * Implementation of a stat method to ensure that remote files don't fail
   * checks when they should pass.
   *
   * @param string $uri
   *   The uri of the resource.
   *
   * @return array|false
   *   An array with file status, or FALSE if the file doesn't exist.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-stat.php
   */
  public function stat(string $uri): array|FALSE;

  /**
   * Determine whether the $uri is a directory.
   *
   * @param string $uri
   *   The path of the resource to check.
   *
   * @return bool
   *   TRUE if the resource is a directory.
   */
  public function isDir(string $uri): bool;

  /**
   * Obtains default stream options based on plugin config.
   *
   * @param array $options
   *   Existing context options by reference.
   */
  public function getStreamOptions(array &$options): void;

  /**
   * Obtain a list of files in the 'Directory' from the Metadata Cache.
   *
   * @todo Consider if this should only return the file names since basename() is called after.
   *
   * @param string $uri
   *   The uri of the directory.
   *
   * @return array
   *   An array of all file paths in the directory.
   */
  public function listDir(string $uri): array;

  /**
   * Determine if the 'directory' is 'empty'.
   *
   * @param string $uri
   *   The uri of the directory.
   *
   * @return bool
   *   Are any objects in the 'directory' or 'subdirectory' of the uri.
   */
  public function isDirEmpty(string $uri): bool;

  /**
   * Converts a Drupal URI path into an object path.
   *
   * @param string $uri
   *   An appropriate URI formatted like 'protocol://path'.
   * @param bool $prepend_bucket
   *   Whether to prepend the bucket name. S3's stream wrapper requires this for
   *   some functions.
   *
   * @return string
   *   A converted string ready for storage in the Database.
   *     Is of form:
   *     - path/to/object
   *     - bucket_name/path/to/object
   */
  public function convertUriToKeyedPath(string $uri, bool $prepend_bucket = TRUE): string;

  /**
   * Metadata Cache related functions.
   */

  /**
   * {@inheritdoc}
   *
   * Convert file metadata returned from S3 into a metadata cache array.
   *
   * @todo Evaluate if this can be moved to reside only inside the bucket plugin.
   *
   * @param string $uri
   *   The uri of the resource.
   * @param array{'ContentLength'?: non-negative-int, 'Size'?: non-negative-int, 'LastModified'?: string, 'VersionId'?: string}|array{} $metadata
   *   An array containing the collective metadata for the object in S3.
   *   The caller may send an empty array here to indicate that the returned
   *   metadata should represent a directory.
   *
   * @return array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir': 0|1, 'version': string}
   *   A file metadata cache array.
   */
  public function convertMetadata(string $uri, array $metadata): array;

  /**
   * Returns the converted metadata for an object in S3.
   *
   * @param string $uri
   *   The URI for the object in S3.
   *
   * @return array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir': 0|1, 'version': string}|false
   *   An array of DB-compatible file metadata or empty array if lookup fails.
   */
  public function getS3Metadata(string $uri): array|false;

  /**
   * Write an object's (and its ancestor folders') metadata to the cache.
   *
   * @param array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir'?: 0|1, 'version'?: string} $metadata
   *   An associative array of file metadata in this format:
   *     'bucket' => The entity name associated with this bucket.
   *     'path' => The full path of the file, including prefix.
   *     'filesize' => The size of the file, in bytes.
   *     'timestamp' => The file's create/update timestamp.
   *     'dir' => A boolean indicating whether the object is a directory.
   *
   * @throws \Drupal\s3fs\S3fsException
   *   Exceptions which occur in the database call will percolate.
   */
  public function writeCache(array $metadata): void;

  /**
   * Delete an object's metadata from the cache.
   *
   * @param string $uri
   *   A string containing the URI(s) of the object(s)
   *   to be deleted.
   *
   * @return int
   *   The number of records deleted.
   */
  public function deleteCache(string $uri): int;

  /**
   * Determine if Drupal should deliver the file instead of the bucket.
   *
   * When true Drupal will deliver the files in the same manner as private://
   * by fetching the content from the bucket and streaming it to the requester.
   *
   * @return bool
   *   True if Drupal should handle the file delivery (equivalent to private://)
   */
  public function useDrupalDelivery(): bool;

  /**
   * Additional functions.
   */

  /**
   * Sets the S3 StreamWrapperEntity into the plugin.
   *
   * @param \Drupal\s3fs_streamwrapper\Entity\S3StreamWrapperEntityInterface $streamWrapper
   *   The parent s3fs_streamwrapper entity associated with this plugin.
   *
   * @return $this
   *   Return $this instance.
   */
  public function setStreamWrapper(S3StreamWrapperEntityInterface $streamWrapper): static;

  /**
   * Obtains a configured Bucket entity to be used by the StreamWrapper.
   *
   * @return \Drupal\s3fs_bucket\S3BucketInterface
   *   A S3Bucket Entity.
   *
   * @throws \Exception
   */
  public function getBucket(): S3BucketInterface;

}
