<?php

namespace Drupal\s3fs_streamwrapper\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the S3 StreamWrapper Plugin plugin manager.
 *
 * @method S3StreamWrapperPluginInterface createInstance(string $plugin_id, array $configuration = [])
 */
class S3StreamWrapperPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new S3StreamWrapperManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/S3StreamWrapper', $namespaces, $module_handler, 'Drupal\s3fs_streamwrapper\Plugin\S3StreamWrapperPluginInterface', 'Drupal\s3fs_streamwrapper\Annotation\S3StreamWrapper');

    $this->alterInfo('s3fs_streamwrapper_S3StreamWrapper_info');
    $this->setCacheBackend($cache_backend, 's3fs_streamwrapper_S3StreamWrapper_plugins');
  }

}
