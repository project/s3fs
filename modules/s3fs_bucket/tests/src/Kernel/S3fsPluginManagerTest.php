<?php

namespace Drupal\Tests\s3fs_bucket\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the plugin.manager.s3fs_bucket Service.
 *
 * @group s3fs
 * @group s3fs_bucket
 *
 * @covers \Drupal\s3fs_bucket\S3BucketPluginManager
 */
class S3fsPluginManagerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    's3fs_bucket',
  ];

  /**
   * Test the s3fs_bucket plugin manager.
   */
  public function testPluginManager(): void {

    $expected = [
      'aws_simple' => 'Simple AWS bucket',
      's3_custom' => 'Custom Bucket',
    ];

    /** @var \Drupal\s3fs_bucket\S3BucketPluginManager $pluginManager */
    $pluginManager = $this->container->get('plugin.manager.s3fs_bucket');
    $bucketTypesOptions = $pluginManager->getBucketTypeOptions();
    $this->assertEquals($expected, $bucketTypesOptions);
  }

}
