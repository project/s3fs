<?php

namespace Drupal\Tests\s3fs_bucket\Kernel;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\s3fs_bucket\S3fsMetadataService;

/**
 * Tests the S3fsMetadata Service.
 *
 * @group s3fs
 * @group s3fs_bucket
 *
 * @covers \Drupal\s3fs_bucket\S3fsMetadataService
 */
class S3fsMetadataServiceTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    's3fs_client',
    's3fs_bucket',
    'system',
  ];

  /**
   * Count of test entries in kernelTestBucket.
   *
   * @var int
   */
  protected int $countKernelTestBucket;

  /**
   * Count of test entries NOT in kernelTestBucket.
   *
   * @var int
   */
  protected int $countUnusedEntries;

  /**
   * Count of all entries in metadata table.
   *
   * @var int
   */
  protected int $countTotal;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installSchema('s3fs_bucket', ['s3fs_file', 's3fs_file_temp']);

    $this->populateDatabase();

    $timeMock = $this->getMockBuilder(TimeInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $timeMock->method('getRequestTime')->willReturn(1640000000);

    $this->container->set('datetime.time', $timeMock);

  }

  /**
   * Evaluate writeCache()
   */
  public function testWriteCache(): void {
    /** @var \Drupal\s3fs_bucket\S3fsMetadataServiceInterface $s3fsMetadata */
    $s3fsMetadata = $this->container->get('s3fs_bucket.metadata');
    /** @var array{array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir'?: 0|1, 'version'?: string}} $data */
    $data = [
      [
        'path' => 'folder',
        'filesize' => 0,
        'dir' => 1,
        'timestamp' => '1600000000',
        'version' => '',
        'bucket' => 'newKernelTestBucket',
      ],
      [
        'path' => 'folder/test.txt',
        'filesize' => 12345,
        'dir' => 0,
        'timestamp' => 1600000000,
        'version' => 'SomeRandomVersion',
        'bucket' => 'newKernelTestBucket',
      ],
      [
        'path' => 'emptyFolder',
        'filesize' => 0,
        'dir' => 1,
        'timestamp' => 1600000000,
        'version' => 'SomeRandomVersion',
        'bucket' => 'newKernelTestBucket',
      ],
      [
        'path' => 'didNotExistFolder/test.txt',
        'filesize' => 12345,
        'dir' => 0,
        'timestamp' => 1600000000,
        'version' => 'SomeRandomVersion',
        'bucket' => 'newKernelTestBucket',
      ],
    ];
    foreach ($data as $record) {
      $s3fsMetadata->writeCache($record);
    }

    $expectedResult = [
      [
        'path' => 'didNotExistFolder',
        'filesize' => 0,
        'dir' => 1,
        'timestamp' => 1640000000,
        'version' => '',
        'bucket' => 'newKernelTestBucket',
      ],
      [
        'path' => 'didNotExistFolder/test.txt',
        'filesize' => 12345,
        'dir' => 0,
        'timestamp' => 1600000000,
        'version' => 'SomeRandomVersion',
        'bucket' => 'newKernelTestBucket',
      ],
      [
        'path' => 'emptyFolder',
        'filesize' => 0,
        'dir' => 1,
        'timestamp' => 1600000000,
        'version' => 'SomeRandomVersion',
        'bucket' => 'newKernelTestBucket',
      ],
      [
        'path' => 'folder',
        'filesize' => 0,
        'dir' => 1,
        'timestamp' => 1640000000,
        'version' => '',
        'bucket' => 'newKernelTestBucket',
      ],
      [
        'path' => 'folder/test.txt',
        'filesize' => 12345,
        'dir' => 0,
        'timestamp' => 1600000000,
        'version' => 'SomeRandomVersion',
        'bucket' => 'newKernelTestBucket',
      ],
    ];

    $this->assertEquals($expectedResult, $this->getAllRecords('newKernelTestBucket'));

  }

  /**
   * Exercise deleteCache()
   */
  public function testDeleteCache(): void {
    /** @var \Drupal\s3fs_bucket\S3fsMetadataServiceInterface $s3fsMetadata */
    $s3fsMetadata = $this->container->get('s3fs_bucket.metadata');

    $currentKeyedPaths = $this->getAllPathsKeyed('kernelTestBucket');
    $this->assertArrayHasKey('emptyFolder', $currentKeyedPaths);
    $this->assertArrayHasKey('folder/test.txt', $currentKeyedPaths);

    $s3fsMetadata->deleteCache('folder/test.txt', 'kernelTestBucket');

    $currentKeyedPaths = $this->getAllPathsKeyed('kernelTestBucket');
    $this->assertArrayHasKey('emptyFolder', $currentKeyedPaths);
    $this->assertArrayNotHasKey('folder/test.txt', $currentKeyedPaths);

    $deletePaths = [
      'emptyFolder',
    ];

    $s3fsMetadata->deleteCache($deletePaths, 'kernelTestBucket');

    $currentKeyedPaths = $this->getAllPathsKeyed('kernelTestBucket');
    $this->assertArrayNotHasKey('emptyFolder', $currentKeyedPaths);

  }

  /**
   * Exercise readCache()
   */
  public function testReadCache(): void {
    /** @var \Drupal\s3fs_bucket\S3fsMetadataServiceInterface $s3fsMetadata */
    $s3fsMetadata = $this->container->get('s3fs_bucket.metadata');

    $data = $this->getAllRecords('kernelTestBucket');
    foreach ($data as $record) {
      $result = $s3fsMetadata->readCache($record['path'], 'kernelTestBucket');
      $this->assertEquals($record, $result);
    }

    // Duplicate so we use the Cache instead of the database.
    foreach ($data as $record) {
      $result = $s3fsMetadata->readCache($record['path'], 'kernelTestBucket');
      $this->assertEquals($record, $result);
    }

    // Make sure we test the lock stampede.
    $lockMock = $this->getMockBuilder(LockBackendInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $lockMock->method('acquire')->willReturnOnConsecutiveCalls(FALSE, TRUE);

    /** @var \Drupal\s3fs_bucket\S3fsMetadataServiceInterface $oneTimeMetadata */
    $oneTimeMetadata = new S3fsMetadataService(
      $this->container->get('database'),
      $this->container->get('datetime.time'),
      $this->container->get('cache.s3fs_bucket_metadata'),
      $lockMock,
      $this->container->get('string_translation'),
      $this->container->get('logger.factory'),
      $this->container->get('messenger')
    );

    $expectedData = [
      'path' => 'folder/test.txt',
      'filesize' => 12345,
      'dir' => 0,
      'timestamp' => 1600000000,
      'version' => 'SomeRandomVersion',
      'bucket' => 'kernelTestBucket',
    ];
    $result = $oneTimeMetadata->readCache('folder/test.txt', 'kernelTestBucket');

    $this->assertEquals($expectedData, $result);
  }

  /**
   * Exercise listDir()
   */
  public function testListDir(): void {
    /** @var \Drupal\s3fs_bucket\S3fsMetadataServiceInterface $s3fsMetadata */
    $s3fsMetadata = $this->container->get('s3fs_bucket.metadata');
    $result = $s3fsMetadata->listDir('folder', 'kernelTestBucket');
    $this->assertEquals(['folder/test.txt'], $result);
    $result = $s3fsMetadata->listDir('', 'kernelTestBucket');
    $this->assertEquals(['emptyFolder', 'folder'], $result);
  }

  /**
   * Exercise isDirEmpty()
   */
  public function testIsDirEmpty(): void {
    /** @var \Drupal\s3fs_bucket\S3fsMetadataServiceInterface $s3fsMetadata */
    $s3fsMetadata = $this->container->get('s3fs_bucket.metadata');
    $this->assertTrue($s3fsMetadata->isDirEmpty('emptyFolder', 'kernelTestBucket'));
    $this->assertFalse($s3fsMetadata->isDirEmpty('folder', 'kernelTestBucket'));
  }

  /**
   * Exercise getExistingFolders()
   */
  public function testGetExistingFolders(): void {
    /** @var \Drupal\s3fs_bucket\S3fsMetadataServiceInterface $s3fsMetadata */
    $s3fsMetadata = $this->container->get('s3fs_bucket.metadata');
    $folders = $s3fsMetadata->getExistingFolders('kernelTestBucket');
    $this->assertArrayHasKey('emptyFolder', $folders);
    $this->assertArrayHasKey('folder', $folders);
    $this->assertEquals(2, count($folders));
  }

  /**
   * Excersie countStoredObjects().
   */
  public function testCountStoredObjects(): void {
    /** @var \Drupal\s3fs_bucket\S3fsMetadataServiceInterface $s3fsMetadata */
    $s3fsMetadata = $this->container->get('s3fs_bucket.metadata');

    $this->assertEquals($this->countKernelTestBucket, $s3fsMetadata->countStoredObjects('kernelTestBucket'));
    $this->assertEquals($this->countTotal, $s3fsMetadata->countStoredObjects(''));
  }

  /**
   * Exercise functions used for metadata cache refresh.
   */
  public function testCacheRefreshRoutines(): void {
    // Make sure the error gets triggered.
    $messengerMock = $this->getMockBuilder(MessengerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $messengerMock->expects($this->atLeastOnce())->method('addError');
    $this->container->set('messenger', $messengerMock);

    /** @var \Drupal\s3fs_bucket\S3fsMetadataServiceInterface $s3fsMetadata */
    $s3fsMetadata = $this->container->get('s3fs_bucket.metadata');

    $records = $this->getAllRecords('kernelTestBucket', TRUE);
    $this->assertEquals($this->countKernelTestBucket, count($records));
    $s3fsMetadata->prepareTempTable('kernelTestBucket');
    $records = $this->getAllRecords('kernelTestBucket', TRUE);
    $this->assertEquals(0, count($records));

    $fileRecords = [
      [
        'path' => 'folder/subfolder/test.txt',
        'filesize' => 12345,
        'dir' => 0,
        'timestamp' => 1600000000,
        'version' => 'SomeRandomVersion',
        'bucket' => 'kernelTestBucket',
      ],
      [
        'path' => '/newFolder/test.txt',
        'filesize' => 12345,
        'dir' => 0,
        'timestamp' => 1600000000,
        'version' => 'SomeRandomVersion',
        'bucket' => 'kernelTestBucket',
      ],
      [
        'path' => 'folderAndFileSamePath',
        'filesize' => 12345,
        'dir' => 0,
        'timestamp' => 1600000000,
        'version' => 'SomeRandomVersion',
        'bucket' => 'kernelTestBucket',
      ],
    ];

    $folders = $s3fsMetadata->getExistingFolders('kernelTestBucket');

    $s3fsMetadata->writeTemporaryMetadata($fileRecords, $folders);
    $this->assertEquals(4, count($folders));

    // Add a folder that is same path as a file to trigger an
    // IntegrityConstrationException.
    $folders['folderAndFileSamePath'] = TRUE;
    $this->assertEquals(5, count($folders));

    $s3fsMetadata->writeTemporaryFolders($folders, 'kernelTestBucket');
    $s3fsMetadata->moveTempMetadata('kernelTestBucket');

    $results = $this->getAllPathsKeyed('kernelTestBucket');
    $this->assertArrayHasKey('folder/subfolder/test.txt', $results);
    $this->assertArrayHasKey('/newFolder/test.txt', $results);
    $this->assertArrayHasKey('/newFolder', $results);
    $this->assertArrayHasKey('folderAndFileSamePath', $results);

    // Ensure a file that no longer exists in bucket is not in metadata.
    $this->assertArrayNotHasKey('folder/test.txt', $results);

    // The File should take priority over the folder when duplicates exist.
    $this->assertequals(0, $results['folderAndFileSamePath']);

  }

  /**
   * Populate the s3fs_file and s3fs_file_temp tables with data.
   */
  protected function populateDatabase(): void {
    // Default file entries in the database.
    $values = [
      [
        'path' => 'folder',
        'filesize' => 0,
        'dir' => 1,
        'timestamp' => 1600000000,
        'version' => '',
        'bucket' => 'kernelTestBucket',
      ],
      [
        'path' => 'folder/test.txt',
        'filesize' => 12345,
        'dir' => 0,
        'timestamp' => 1600000000,
        'version' => 'SomeRandomVersion',
        'bucket' => 'kernelTestBucket',
      ],
      [
        'path' => 'emptyFolder',
        'filesize' => 0,
        'dir' => 1,
        'timestamp' => 1600000000,
        'version' => 'SomeRandomVersion',
        'bucket' => 'kernelTestBucket',
      ],
      [
        'path' => 'folder',
        'filesize' => 0,
        'dir' => 1,
        'timestamp' => 1600000000,
        'version' => '',
        'bucket' => 'unusedKernelTestBucket',
      ],
      [
        'path' => 'folder/test.txt',
        'filesize' => 12345,
        'dir' => 0,
        'timestamp' => 1600000000,
        'version' => 'SomeRandomVersion',
        'bucket' => 'unusedKernelTestBucket',
      ],
    ];

    $countKernelTestBucket = 0;
    $countUnusedEntries = 0;
    foreach ($values as $record) {
      if ($record['bucket'] == 'kernelTestBucket') {
        $countKernelTestBucket++;
      }
      else {
        $countUnusedEntries++;
      }
    }

    $this->countKernelTestBucket = $countKernelTestBucket;
    $this->countUnusedEntries = $countUnusedEntries;
    $this->countTotal = $countKernelTestBucket + $countUnusedEntries;

    /** @var \Drupal\Core\Database\Connection $connection */
    $connection = $this->container->get('database');
    $connection->truncate('s3fs_file')->execute();
    $query = $connection->insert('s3fs_file')
      ->fields(['path', 'filesize', 'dir', 'timestamp', 'version', 'bucket']);
    foreach ($values as $record) {
      $query->values($record);
    }
    $query->execute();

    // Remove the entry with slash end as it is not allowed in temp table.
    $connection->truncate('s3fs_file_temp')->execute();
    $query = $connection->insert('s3fs_file_temp')
      ->fields(['path', 'filesize', 'dir', 'timestamp', 'version', 'bucket']);
    foreach ($values as $record) {
      $query->values($record);
    }
    $query->execute();

  }

  /**
   * Obtain current records from the s3fs file tables.
   *
   * @param string $bucketId
   *   BucketID to return records for.
   * @param bool $tempTable
   *   If true data is returned from s3fs_file_temp.
   *
   * @return array
   *   Integer indexed array of fields from Database.
   */
  protected function getAllRecords(string $bucketId, bool $tempTable = FALSE): array {
    /** @var \Drupal\Core\Database\Connection $connection */
    $connection = $this->container->get('database');
    if (!$tempTable) {
      $query = $connection->select('s3fs_file', 's');
    }
    else {
      $query = $connection->select('s3fs_file_temp', 's');
    }

    $results = $query->fields('s')
      ->condition('bucket', $bucketId, '=')
      ->execute()
      ?->fetchAll(\PDO::FETCH_ASSOC);

    return $results ?: [];
  }

  /**
   * Obtain current records from the s3fs file tables.
   *
   * @param string $bucketId
   *   BucketID to return records for.
   *
   * @return array
   *   Array keyed by file paths, value is truthful for directories.
   */
  protected function getAllPathsKeyed(string $bucketId): array {
    /** @var \Drupal\Core\Database\Connection $connection */
    $connection = $this->container->get('database');
    $results = $connection->select('s3fs_file', 's')
      ->fields('s', ['path', 'dir'])
      ->condition('bucket', $bucketId, '=')
      ->execute()
      ?->fetchAllKeyed();

    return $results ?: [];
  }

}
