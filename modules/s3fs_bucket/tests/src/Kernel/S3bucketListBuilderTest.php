<?php

namespace Drupal\Tests\s3fs_bucket\Kernel;

/**
 * Tests the admin listing fallback when views is not enabled.
 *
 * @group s3fs
 * @group s3fs_bucket
 *
 * @covers \Drupal\s3fs_bucket\S3BucketListBuilder
 */
class S3bucketListBuilderTest extends S3fsBucketKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'key',
    's3fs_bucket',
    's3fs_client',
    's3fs_bucket_localstack_config',
    'system',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user_role');
  }

  /**
   * Tests that the listBuilder returns.
   */
  public function testListBuilder(): void {
    /** @var \Drupal\Core\Entity\EntityListBuilderInterface $list_builder */
    $list_builder = $this->container->get('entity_type.manager')->getListBuilder('s3fs_bucket');

    $build = $list_builder->render();
    $this->container->get('renderer')->renderRoot($build);

    $this->assertNotEmpty($build['#cache']['tags']);
    $this->assertContains('config:s3fs_bucket_list', $build['#cache']['tags']);
    $this->assertNotEmpty($build['table']['#rows']);
  }

}
