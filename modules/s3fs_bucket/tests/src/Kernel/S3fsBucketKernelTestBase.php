<?php

namespace Drupal\Tests\s3fs_bucket\Kernel;

use Aws\Exception\AwsException;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3ClientInterface;
use Drupal\Core\Database\Connection;
use Drupal\KernelTests\KernelTestBase;
use Drupal\s3fs_bucket\S3BucketInterface;

/**
 * S3 File System Test Base.
 *
 * Provides a base for BrowserTest to execute against.
 *
 * This test is designed to execute against a local localstack instance.
 * Should you need to execute it against another provider the target may be
 * edited inside the s3fs_bucket_localstack_config test module.
 *
 * @group s3fs
 * @group s3fs_bucket
 */
abstract class S3fsBucketKernelTestBase extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'key',
    's3fs_bucket',
    's3fs_client',
    's3fs_bucket_localstack_config',
    'system',
  ];

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * The s3fs module config.
   *
   * @var array
   */
  protected array $s3Config;

  /**
   * The AWS SDK for PHP S3Client object.
   *
   * @var \Aws\S3\S3ClientInterface
   */
  protected S3ClientInterface $s3;

  /**
   * S3 Credentials provided and bucket exists.
   *
   * @var bool
   */
  protected bool $bucketNotFound = FALSE;

  /**
   * Folder name to use for placing tests files.
   *
   * @var string
   */
  protected string $remoteTestsFolder = '_s3fs_tests';

  /**
   * Full base key path for tests folder.
   *
   * @var string
   */
  protected string $remoteTestsFolderKey = '';

  /**
   * S3fs_bucket config entity.
   *
   * @var \Drupal\s3fs_bucket\S3BucketInterface
   */
  protected S3BucketInterface $s3Bucket;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('key');
    $this->installSchema('s3fs_bucket', ['s3fs_file', 's3fs_file_temp']);
    $this->installEntitySchema('s3fs_bucket');
    $this->installConfig('s3fs_bucket_localstack_config');

    $this->connection = $this->container->get('database');

    $this->remoteTestsFolderKey = $this->remoteTestsFolder . '/' . uniqid('', TRUE);
    /** @var \Drupal\s3fs_bucket\S3BucketInterface s3Bucket */
    $s3Bucket = $this->container->get('entity_type.manager')->getStorage('s3fs_bucket')->load('localstack');
    $this->s3Bucket = $s3Bucket;

    $this->s3 = $this->s3Bucket->getS3Client();

    $this->bucketNotFound = !$this->s3->doesBucketExistV2($this->s3Bucket->getBucketName(), FALSE);

    $connectAttempts = 0;
    while ($this->bucketNotFound && $connectAttempts <= 5) {
      try {
        $this->s3->createBucket([
          'Bucket' => $this->s3Bucket->getBucketName(),
        ]);
        $this->bucketNotFound = FALSE;
      }
      catch (S3Exception $e) {
        // Bucket possibly was created by another script between checking.
        $this->bucketNotFound = !$this->s3->doesBucketExistV2($this->s3Bucket->getBucketName(), FALSE);
      }
      catch (AwsException $e) {
        // No need to continue tests if we can't access the bucket. Either the
        // credentials are incorrect or problem with S3Client.
        $this->fail("Unable to create bucket '{$this->s3Bucket->getBucketName()}'. Please verify the S3 settings.");
      }

      if ($this->bucketNotFound) {
        // Wait before we loop again.
        sleep(5);
      }
      $connectAttempts++;
    }

    if (!$this->bucketNotFound) {
      // Empty out the bucket before the test, to prevent unexpected errors.
      $this->s3->deleteMatchingObjects($this->s3Bucket->getBucketName(), $this->remoteTestsFolderKey);
    }
    else {
      $this->fail("Unable to access bucket '{$this->s3Bucket->getBucketName()}'. Please verify the S3 settings.");
    }
  }

  /**
   * Clean up S3 folder.
   */
  protected function tearDown(): void {
    if (!$this->bucketNotFound) {
      $this->s3->deleteMatchingObjects($this->s3Bucket->getBucketName(), $this->remoteTestsFolderKey);
    }
    parent::tearDown();
  }

}
