<?php

namespace Drupal\Tests\s3fs_bucket\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * S3 File System Test Base.
 *
 * Provides a base for BrowserTest to execute against.
 *
 * This test is designed to execute against a local localstack instance.
 * Should you need to execute it against another provider the target may be
 * edited inside the s3fs_bucket_localstack_config test module.
 *
 * @group s3fs
 * @group s3fs_bucket
 *
 * @coversNothing
 */
class S3fsBucketEntityFormTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'key',
    's3fs_bucket',
    's3fs_client',
    's3fs_bucket_localstack_config',
  ];

  /**
   * Theme to use for testing.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Test adding a new bucket.
   */
  public function testEntityPages(): void {
    $this->markTestSkipped('Random Failures');
    $adminUser = $this->createUser(['administer s3fs_bucket']);
    $this->drupalLogin($adminUser);

    // Validate the Edit page loads.
    $this->drupalGet('admin/config/s3/s3-bucket/localstack');
    $this->assertSession()->pageTextContains('Configure Custom Bucket plugin');

    // Validate the Actions page loads.
    $this->drupalGet('admin/config/s3/s3-bucket/localstack/actions');
    $this->assertSession()->pageTextContains('File Metadata Cache');

    // Test adding a record.
    $this->drupalGet('admin/config/s3/s3fs_bucket/add');

    $page = $this->getSession()->getPage();
    $page->findById('edit-bucket-type-s3-custom')->click();
    $this->assertSession()->waitForText('Configure Custom Bucket plugin');
    $this->assertSession()->pageTextContains('Configure Custom Bucket plugin');
    $values = [
      'label' => 'form_test_bucket',
      'bucket_type' => 's3_custom',
      'bucket_config[bucket_name]' => 's3fs-test-bucket',
      'bucket_config[keymodule_access_key_name]' => 'localstack_test',
      'bucket_config[keymodule_secret_key_name]' => 'localstack_test',
      'bucket_config[use_custom_endpoint]' => TRUE,
      'bucket_config[endpoint]' => 'http://s3.localhost.localstack.cloud:4566',
    ];
    $this->submitForm($values, 'Save');
    $this->assertSession()->waitForText('The bucket was successfully saved.');
    $this->assertSession()->pageTextContains('The bucket was successfully saved.');

    $expected_config = [
      'bucket_name' => 's3fs-test-bucket',
      'region' => 'us-east-1',
      'keymodule_secret_key_name' => 'localstack_test',
      'keymodule_access_key_name' => 'localstack_test',
      'use_custom_endpoint' => TRUE,
      'endpoint' => 'http://s3.localhost.localstack.cloud:4566',
    ];
    /** @var \Drupal\s3fs_bucket\Entity\S3Bucket $entity */
    $entity = \Drupal::service('entity_type.manager')->getStorage('s3fs_bucket')->load('form_test_bucket');
    $saved_config = $entity->getBucketConfig();
    $this->assertNotEmpty($saved_config);
    foreach ($expected_config as $key => $value) {
      $this->assertEquals($value, $saved_config[$key], "$key was correctly saved in config");
    }

    $this->drupalGet('admin/config/s3/s3-bucket/');
    $this->assertSession()->waitForText('form_test_bucket');
    $this->assertSession()->pageTextContains('form_test_bucket');

    // Test disabling a record.
    $this->drupalGet('admin/config/s3/s3-bucket/form_test_bucket/disable');
    $this->assertSession()->waitForText('Disabling a bucket will disable all access to files on this bucket.');
    $this->assertSession()->pageTextContains('Disabling a bucket will disable all access to files on this bucket.');
    $submit_button = $page->findById('edit-submit');
    $this->assertNotEmpty($submit_button);
    $submit_button->click();
    $this->assertSession()->waitForText('The bucket form_test_bucket has been disabled.');
    $this->assertSession()->pageTextContains('The bucket form_test_bucket has been disabled.');

  }

}
