<?php

namespace Drupal\Tests\s3fs_bucket\Unit;

use Aws\Command;
use Aws\ResultInterface;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3ClientInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\s3fs_bucket\Exception\BucketIdEmptyException;
use Drupal\s3fs_bucket\S3BucketInterface;
use Drupal\s3fs_bucket\S3BucketPluginInterface;
use Drupal\s3fs_bucket\S3fsMetadataServiceInterface;
use PHPUnit\Framework\Constraint\IsType;

/**
 * Tests the Custom S3 Bucket Plugin.
 *
 * @group s3fs
 * @group s3fs_bucket
 */
abstract class S3fsBucketPluginTestBase extends S3fsBucketPluginUnitTestBase {

  /**
   * Per test results for parent class to inspect.
   *
   * @var array
   */
  protected array $results = [];

  /**
   * Test basics functions of the plugin implementation.
   *
   * Tests the functions that do not require multiple scenarios to determine
   * functionality. Primarily those functions that return the same response
   * as what they were initialized with.
   *
   * In order to allow plugins to invoke their own logic we only validate
   * method form here.
   */
  public function testPluginBasics(): void {

    $plugin = $this->setupPlugin();

    $this->assertIsString($plugin->getBucketName(), 'Bucket name is a string');
    $this->assertNotEmpty($plugin->getBucketName(), "Bucket name is not empty");
    $this->assertEquals('unit-test-bucket', $plugin->getBucketName(), 'Test bucket name is correctly set');

    $this->assertIsString($plugin->getPluginId(), 'PluginId name is a string');
    $this->assertNotEmpty($plugin->getPluginId(), "PluginId is not empty");

    $this->assertIsString($plugin->label(), 'Label is a string');
    $this->assertNotEmpty($plugin->label(), "label is not empty");

    $this->assertIsString($plugin->getDescription(), 'Bucket plugin description is a string');

    $this->assertIsInt($plugin->getMaxPathLength(), "getMaxPathLenght returns an integer");
    $this->assertLessThanOrEqual(S3fsMetadataServiceInterface::MAX_PATH_LENGTH, $plugin->getMaxPathLength(), 'Plugin max path length does not exceed Metadata Service max length');

    $pluginRunningConfig = $plugin->getConfiguration();
    foreach ($this->pluginConfig as $key => $value) {
      if ($key == '#bucket') {
        continue;
      }
      $this->assertEquals($value, $pluginRunningConfig[$key], $key . ' is correctly set in plugin config.');
    }
    $this->assertTrue($plugin->getS3Client() instanceof S3ClientInterface);

  }

  /**
   * Test functions intended to return based on config provided.
   *
   * @param string $function
   *   The function name to call.
   * @param mixed $expected
   *   The expected result.
   * @param array $new_config
   *   New config to include in test.
   *
   * @dataProvider pluginConfigDataProvider
   */
  public function testPluginConfig(string $function, mixed $expected, array $new_config = []): void {

    $this->pluginConfig = $new_config + $this->pluginConfig;

    $plugin = $this->setupPlugin();

    $this->assertEquals($expected, $plugin->$function());
  }

  /**
   * Validate setBucket.
   */
  public function testSetBucket(): void {
    $new_bucket_entity_mock = $this->createMock(S3BucketInterface::class);
    $new_bucket_entity_mock->method('id')->willReturn('new-unit-test-bucket');
    $plugin = $this->setupPlugin();
    $this->assertInstanceOf(S3BucketPluginInterface::class, $plugin->setBucket($new_bucket_entity_mock));
  }

  /**
   * Validate isPathStyleEndpoint()
   *
   * @param array $new_config
   *   New config to include in test.
   * @param bool $expected_value
   *   Expected return value.
   *
   * @dataProvider providerIsPathStyleEndpoint
   */
  public function testIsPathStyleEndpoint(array $new_config, bool $expected_value): void {
    $this->pluginConfig = $new_config += $this->pluginConfig;
    $plugin = $this->setupPlugin();
    $this->assertIsBool($plugin->isPathStyleEndpoint(), "isPathStyleEndpoint returns a boolean value");
    $this->assertEquals($expected_value, $plugin->isPathStyleEndpoint(), 'isPathStyleEndpoint matches expected value');
  }

  /**
   * DataProvider for isPathStyleEndpoint().
   *
   * @return array<array{array, bool}>
   *   PhpUnit data provider array keyed by data set name or integer
   *     - $new_config - Will be merged with $pluginConfig
   *     - $expected_result - Value expected from method.
   */
  abstract public function providerIsPathStyleEndpoint(): array;

  /**
   * Test getCommandParams().
   *
   * @param array $new_config
   *   New config to include in test.
   * @param string $path
   *   The path to get command parameters for.
   * @param array $params
   *   Existing params to be included returned response.
   * @param mixed $expected_result
   *   For use by parent class to pass in test expectations.
   *
   * @dataProvider providerGetCommandParams
   */
  public function testGetCommandParams(array $new_config, string $path, array $params, mixed $expected_result): void {
    $this->pluginConfig = $new_config + $this->pluginConfig;
    $plugin = $this->setupPlugin();
    $results = $plugin->getCommandParams($path, $params);
    $this->assertNotEmpty($results['Bucket']);
    $this->assertNotEmpty($results['Key']);
    $this->results['results'] = $results;
  }

  /**
   * DataProvider for getCommandParams().
   *
   * @return array<array{array, string, array, mixed}>
   *   PhpUnit data provider array keyed by data set name or integer
   *     - $new_config - Will be merged with $pluginConfig
   *     - $path - Path to test with.
   *     - $params - Params to pass in with test.
   *     - $expected_result - For use processing expected result.
   */
  abstract public function providerGetCommandParams(): array;

  /**
   * Test getStreamOptions().
   */
  abstract public function testGetStreamOptions(): void;

  /**
   * Validate alterKeyedPath().
   *
   * @param array $new_config
   *   New config to include in test.
   * @param string $path
   *   The path to test against.
   * @param string $expected_value
   *   The expected altered path.
   *
   * @dataProvider providerAlterKeyedPath
   */
  public function testAlterKeyedPath(array $new_config, string $path, string $expected_value): void {
    $this->pluginConfig = $new_config += $this->pluginConfig;
    $plugin = $this->setupPlugin();
    $returned_value = $plugin->alterKeyedPath($path);
    $this->assertIsString($returned_value, "alterKeyedPath returns a string value");
    $this->assertEquals($expected_value, $returned_value, 'alterKeyedPath matches expected value');
  }

  /**
   * DataProvider for alterKeyedPath().
   *
   * @return array<array{array, string, string}>
   *   PhpUnit data provider array keyed by data set name or integer
   *     - $new_config - Will be merged with $pluginConfig.
   *     - $path - Path to test.
   *     - $expected_result - Value expected from method.
   */
  abstract public function providerAlterKeyedPath(): array;

  /**
   * Validate convertMetadata() calls Metadata service.
   */
  public function testConvertMetadataCallsMetadataService(): void {
    $this->metadataServiceMock
      ->expects($this->atLeastOnce())
      ->method('convertMetadata')
      ->with($this->isType('string'), $this->isType('array'), 'unit-test-bucket')
      ->willReturn(
        [
          'bucket' => 'unit-test-bucket',
          'path' => 'object/exists',
          'filesize' => 1234567,
          'timestamp' => 1578014410,
          'dir' => 0,
          'version' => '44cff4ee-1370-11ec-82a8-0242ac130003',
        ]
      );

    $plugin = $this->setupPlugin();
    $result = $plugin->convertMetadata('object/exists', []);
    $this->assertIsArray($result, 'convertMetadata returns array');
  }

  /**
   * Validate ::waitUntilFileExists().
   */
  public function testWaitUntilFileExists(): void {

    $callback_has_object_key = function ($options) {
      $this->assertArrayHasKey('Key', $options, 'WaitUntil call has Key in options');
      return $options['Key'] === 'path/to/object';
    };

    $this->s3ClientMock
      ->method('waitUntil')
      ->with('ObjectExists', $this->callback($callback_has_object_key))
      ->willReturnOnConsecutiveCalls(
        $this->returnValue(TRUE),
        $this->throwException(new \Exception('NEED TO CHANGE THIS EXCEPTION'))
    );

    $plugin = $this->setupPlugin();

    $this->assertTrue($plugin->waitUntilFileExists('path/to/object'), 'Returns true when file exists');
    $this->assertFalse($plugin->waitUntilFileExists('path/to/object'), 'The file was not detected inside 10 attempts');
  }

  /**
   * Test refresh().
   *
   * @param array $new_config
   *   New config to include in test.
   *
   * @dataProvider providerRefreshCache
   */
  public function testRefreshCache(array $new_config): void {
    $this->pluginConfig = $new_config + $this->pluginConfig;

    $temporary_objects = [];
    $callback_write_temporary_metadata = function (array $metadata_list, array $folders) use (&$temporary_objects) {
      $temporary_objects = array_merge($temporary_objects, $metadata_list);
    };

    $temporary_folders = [];
    $callback_write_temporary_folders = function (array $folders, string $bucket_id) use (&$temporary_folders) {
      $temporary_folders = array_merge($temporary_folders, $folders);
    };

    $this->metadataServiceMock
      ->expects($this->atLeastOnce())
      ->method('getExistingFolders');
    $this->metadataServiceMock
      ->expects($this->atLeastOnce())
      ->method('countStoredObjects');
    $this->metadataServiceMock
      ->expects($this->atLeastOnce())
      ->method('prepareTempTable');
    $this->metadataServiceMock
      ->expects($this->atLeastOnce())
      ->method('writeTemporaryMetadata')
      ->willReturnCallback($callback_write_temporary_metadata);
    $this->metadataServiceMock
      ->expects($this->atLeastOnce())
      ->method('writeTemporaryFolders')
      ->willReturnCallback($callback_write_temporary_folders);
    $this->metadataServiceMock
      ->expects($this->once())
      ->method('moveTempMetadata');

    /* Setup for getObjectsV2 (unversioned) */
    $data = file_get_contents(__DIR__ . '/../../fixtures/list_objects_v2_page_1.json');
    $this->assertNotFalse($data);
    $result_list_objects_v2_page1 = json_decode($data, TRUE);
    $mock_listObjectsV2Response = $this->createMock(ResultInterface::class);
    $mock_listObjectsV2Response->method('count')->willReturn(1);
    $mock_listObjectsV2Response->method('toArray')->willReturn($result_list_objects_v2_page1);

    $data = file_get_contents(__DIR__ . '/../../fixtures/list_objects_v2_page_2.json');
    $this->assertNotFalse($data);
    $result_list_objects_v2_page2 = json_decode($data, TRUE);
    $mock_listObjectsV2Response_page_2 = $this->createMock(ResultInterface::class);
    $mock_listObjectsV2Response_page_2->method('count')->willReturn(1);
    $mock_listObjectsV2Response_page_2->method('toArray')->willReturn($result_list_objects_v2_page2);

    $this->s3ClientMock
      ->method('listObjectsV2')
      ->willReturnOnConsecutiveCalls($mock_listObjectsV2Response, $mock_listObjectsV2Response_page_2);

    /* Setup for ListObjectVersions */
    $data = file_get_contents(__DIR__ . '/../../fixtures/list_object_versions_page_1.json');
    $this->assertNotFalse($data);
    $result_list_object_versions = json_decode($data, TRUE);
    $mock_listObjectVersions_truncated = $this->createMock(ResultInterface::class);
    $mock_listObjectVersions_truncated->method('count')->willReturn(1);
    $mock_listObjectVersions_truncated->method('toArray')->willReturn($result_list_object_versions);

    $data = file_get_contents(__DIR__ . '/../../fixtures/list_object_versions_page_2.json');
    $this->assertNotFalse($data);
    $result_list_object_versions_page2 = json_decode($data, TRUE);
    $mock_listObjectVersions = $this->createMock(ResultInterface::class);
    $mock_listObjectVersions->method('count')->willReturn(1);
    $mock_listObjectVersions->method('toArray')->willReturn($result_list_object_versions_page2);

    $this->s3ClientMock
      ->method('listObjectVersions')
      ->willReturnOnConsecutiveCalls($mock_listObjectVersions_truncated, $mock_listObjectVersions);

    /* Run the tests and record the results. */
    $plugin = $this->setupPlugin();
    $context = [];
    $plugin->refreshCache($context);
    $this->results['context_1'] = $context;
    $plugin->refreshCache($context);
    $this->results['context_2'] = $context;
    $this->results['written_folders'] = $temporary_folders;
    $this->results['written_objects'] = $temporary_objects;
  }

  /**
   * Provide data for testRefreshCache()
   *
   * @return array
   *   New config to include in test.
   */
  abstract public function providerRefreshCache(): array;

  /**
   * Provide configuration data for testPluginConfig()
   *
   * This method should be replaced by the extending class to provide module
   * specific config tests.
   *
   * @return array<array{string, mixed, array}>
   *   Array keyed by either test name or integer
   *    - string $function
   *        The getter method to be called.
   *    - mixed $expected
   *        Expected response.
   *    - array $config
   *        Config be passed to plugin.
   */
  abstract public function pluginConfigDataProvider(): array;

  /**
   * Validate ::getS3Metdata.
   */
  public function testGetS3Metadata(): void {

    $this->metadataServiceMock
      ->method('convertMetadata')
      ->with('object/exists', $this->anything(), $this->anything())
      ->willReturn(
        [
          'bucket' => 'test',
          'path' => 'object/exists',
          'filesize' => 1234567,
          'timestamp' => 1578014410,
          'dir' => 0,
          'version' => '44cff4ee-1370-11ec-82a8-0242ac130003',
        ]
      );

    $callback_headObject = function ($params) {
      $object_key = $params['Key'];
      $aws_command_mock = self::createMock(Command::class);

      switch ($object_key) {
        case 'object/exists':
          $resultMock = $this->createMock('\Aws\ResultInterface');
          $resultMock->method('toArray')->willReturn(
            [
              'ContentLength' => '1234567',
              'LastModified' => 'Fri, 03 Jan 2020 01:20:10 GMT',
              'VersionId' => ' 44cff4ee-1370-11ec-82a8-0242ac130003',
            ]
          );
          return $resultMock;

        case 'object/DoesNotExist':
          $aws_command_mock = $this->createMock(Command::class);
          throw new S3Exception('Expected Exception', $aws_command_mock);

        case 'object/ThrowException':
          throw new \Exception("Requested exception");

        default:
          throw new \Exception("Unknown key requested");
      }
    };

    $this->s3ClientMock->method('headObject')->willReturnCallback($callback_headObject);

    $plugin = $this->setupPlugin();

    $this->assertFalse($plugin->getS3Metadata('object/DoesNotExist'));
    $this->assertFalse($plugin->getS3Metadata('object/ThrowException'));
    $this->assertNotFalse($plugin->getS3Metadata('object/exists'));

  }

  /*
   * Most of these will be to just be sure we handle error responses, the real
   * heavy lifting will be inside the S3fSMetadata service tests.
   *
   * We may even omit some of these since we make no changes to the data.
   */

  /**
   * Validate ::readCache.
   */
  public function testReadCache(): void {

    $existsData = [
      'bucket' => 'unit-test-bucket',
      'path' => 'object/exists',
      'filesize' => 1234567,
      'timestamp' => 1578014410,
      'dir' => 0,
      'version' => '44cff4ee-1370-11ec-82a8-0242ac130003',
    ];

    $readcache_map = [
      ['object/exists', 'unit-test-bucket', $existsData],
      ['object/doesNotExist', 'unit-test-bucket', FALSE],
    ];

    $this->metadataServiceMock
      ->method('readCache')
      ->willReturnMap($readcache_map);

    $plugin = $this->setupPlugin();

    $this->assertEquals($existsData, $plugin->readCache('object/exists',));
    $this->assertFalse($plugin->readCache('object/doesNotExist'));
  }

  /**
   * Validate ::writeCache().
   *
   * @param array $new_config
   *   New config to include in test.
   * @param array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir'?: 0|1, 'version'?: string} $submit_metadata
   *   Metadata to submit to writeCache().
   * @param mixed $context
   *   For use by classes extending this test to pass in additional data.
   *
   * @dataProvider providerWriteCache
   */
  public function testWriteCache(array $new_config, array $submit_metadata, mixed $context): void {
    $submitted_value = [];
    $write_cache_callback = function ($metadata) use (&$submitted_value) {
      $submitted_value[] = $metadata;
    };

    $this->metadataServiceMock
      ->expects($this->atLeastOnce())
      ->method('writeCache')
      ->with($this->isType(IsType::TYPE_ARRAY))
      ->willReturnCallback($write_cache_callback);

    $this->pluginConfig = $new_config + $this->pluginConfig;
    $plugin = $this->setupPlugin();
    $plugin->writeCache($submit_metadata);
    // Allow extending class to validate data submitted to metadata service.
    $this->results['write_callback'] = $submitted_value;
  }

  /**
   * DataProvider for writeCache().
   *
   * @return array<array{array, array, mixed}>
   *   PhpUnit data provider array keyed by data set name or integer
   *     - $new_config - Will be merged with $pluginConfig
   *     - $submit_metadata - Metadata to pass to writeCache()
   *     - $context - For usage by tests extending the base test.
   */
  public function providerWriteCache(): array {
    return [
      'Simple callback' => [
        [],
        [],
        NULL,
      ],
    ];
  }

  /**
   * Validate ::deleteCache().
   */
  public function testDeleteCache(): void {

    $deleteDataCallback = function ($paths, $bucket_id) {

      if (empty($bucket_id)) {
        throw new BucketIdEmptyException();
      }

      $validPaths = [
        'object/exists',
        'object/alsoExists',
      ];

      if (!is_array($paths)) {
        $paths = [$paths];
      }

      $count = 0;
      foreach ($paths as $path) {
        if (in_array($path, $validPaths)) {
          $count++;
        }
      }
      return $count;
    };

    $this->metadataServiceMock->method('deleteCache')->willReturnCallback($deleteDataCallback);

    $plugin = $this->setupPlugin();

    $this->assertEquals(1, $plugin->deleteCache('object/exists'));
    $this->assertEquals(0, $plugin->deleteCache('object/doesNotExist'));
    $this->assertEquals(2, $plugin->deleteCache(
      [
        'object/exists',
        'object/doesNotExist',
        'object/alsoExists',
      ]
    ));

  }

  /*
   * Test PluginFormInterface.
   *
   * @todo Move these to a bucketpluginform class.
   */

  /**
   * Validate buildConfigurationForm().
   */
  public function testBuildConfigurationForm(): void {
    $form_state = new FormState();
    $plugin = $this->setupPlugin();
    if (!($plugin instanceof PluginFormInterface)) {
      $this->markTestSkipped('Plugin does not inherit PluginFormInterface');
    }
    $result = $plugin->buildConfigurationForm($this->pluginConfig, $form_state);
    $this->assertIsArray($result);
    $this->results['build_form'] = $result;
  }

  /**
   * Helper for testing validateConfigurationForm().
   *
   * @param array $submitted_values
   *   Values submitted to be combined with defaultConfiguration().
   *
   * @return \Drupal\Core\Form\FormStateInterface
   *   The form state of the submited form.
   */
  protected function validateConfigurationForm(array $submitted_values): FormStateInterface {
    $plugin = $this->setupPlugin();
    if (!($plugin instanceof PluginFormInterface)) {
      $this->markTestSkipped('Plugin does not inherit PluginFormInterface');
    }
    $submitted_values += $plugin->defaultConfiguration();
    $form_state = new FormState();
    $form_state->setValues($submitted_values);
    $form = [];
    $plugin->validateConfigurationForm($form, $form_state);
    return $form_state;
  }

}
