<?php

namespace Drupal\Tests\s3fs_bucket\Unit;

use Aws\Command;
use Aws\S3\Exception\S3Exception;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\s3fs_bucket\Plugin\S3Bucket\CustomS3BucketPlugin;

/**
 * Tests the Custom S3 Bucket Plugin.
 *
 * @group s3fs
 * @group s3fs_bucket
 *
 * @covers \Drupal\s3fs_bucket\Plugin\S3Bucket\CustomS3BucketPlugin
 * @covers \Drupal\s3fs_bucket\S3BucketPluginBase
 */
class CustomS3BucketPluginConfigFormTest extends S3fsBucketPluginConfigFormTestBase {

  /**
   * Mock AWS Command for use with exceptions.
   *
   * @var \Aws\Command|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $mockCommand;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    $this->pluginId = 's3_custom';
    $this->pluginDefinition = [
      'id' => $this->pluginId,
      'label' => 'Custom Bucket',
      'description' => 'A fully customizable option',
      'class' => CustomS3BucketPlugin::class,
      'provider' => 's3fs_bucket',
    ];

    parent::setUp();

    $this->pluginConfig['bucket_name'] = 'unit-test-bucket';
    $this->pluginConfig['keymodule_access_key_name'] = 'test';
    $this->pluginConfig['keymodule_secret_key_name'] = 'test';

    $this->mockCommand = $this->createMock(Command::class);

  }

  /**
   * {@inheritdoc}
   */
  protected function setupPlugin(): CustomS3BucketPlugin {
    return new CustomS3BucketPlugin(
      $this->pluginConfig,
      $this->pluginId,
      $this->pluginDefinition,
      $this->clientFactoryMock,
      $this->metadataServiceMock,
      $this->s3fsBucketRefreshBatchMock,
      $this->keyServiceMock,
      $this->mimeTypeGuesserMocker,
      $this->moduleHandlerMock
    );
  }

  /**
   * {@inheritdoc}
   */
  public function testBuildConfigurationForm(): void {

    // We need to mock the url_generator for Url::fromRoute().
    $urlGenerator = $this->createMock(UrlGeneratorInterface::class);
    $urlGenerator->method('generateFromRoute')
      ->with('entity.key.collection', $this->anything(), $this->anything())
      ->willReturn((new GeneratedUrl())
        ->setGeneratedUrl('/admin/config/system/keys'));
    $container = \Drupal::getContainer();
    \Drupal::unsetContainer();
    $container->set('url_generator', $urlGenerator);
    \Drupal::setContainer($container);

    parent::testBuildConfigurationForm();
  }

  /**
   * Test validateConfigurationForm without errors.
   *
   * @todo We may want to move bucket validation to a public method
   * to make testing easier.
   */
  public function testValidateConfigurationFormNoErrors(): void {

    $this->s3ClientMock
      ->method('determineBucketRegion')
      ->with('test-bucket')
      ->willReturn('us-east-1');
    // @todo this may not be on spec
    $this->s3ClientMock
      ->method('getObject')
      ->willReturn(TRUE);

    $config = [
      'bucket_name' => 'test-bucket',
      'keymodule_access_key_name' => 'test',
      'keymodule_secret_key_name' => 'test',
    ];

    $form_state = $this->validateConfigurationForm($config);
    $this->assertEmpty($form_state->getErrors());
  }

  /**
   * Test validateConfigurationForm unable to obtain client.
   */
  public function testValidateConfigurationFormExceptionDetermineRegion(): void {
    $this->markTestSkipped('todo: Add an error when auto region fails and we use fallback');

    $this->s3ClientMock
      ->method('determineBucketRegion')
      ->willThrowException(new S3Exception('Simulate region error', $this->mockCommand));

    // @todo this may not be on spec
    $this->s3ClientMock
      ->method('getObject')
      ->willReturn(TRUE);

    $config = [
      'bucket_name' => 'test-bucket',
      'keymodule_access_key_name' => 'test',
      'keymodule_secret_key_name' => 'test',
    ];

    $form_state = $this->validateConfigurationForm($config);
    $this->assertNotEmpty($form_state->getErrors());
  }

  /**
   * Test validateConfigurationForm unable to obtain client.
   */
  public function testExceptionGetObject(): void {

    $this->s3ClientMock
      ->method('determineBucketRegion')
      ->willThrowException(new S3Exception('Simulate region error', $this->mockCommand));

    // @todo this may not be on spec
    $this->s3ClientMock
      ->method('getObject')
      ->willThrowException(new S3Exception('Simulated exception with getObject', $this->mockCommand));

    $config = [
      'bucket_name' => 'test-bucket',
      'keymodule_access_key_name' => 'test',
      'keymodule_secret_key_name' => 'test',
    ];

    $form_state = $this->validateConfigurationForm($config);
    $this->assertNotEmpty($form_state->getErrors());
  }

}
