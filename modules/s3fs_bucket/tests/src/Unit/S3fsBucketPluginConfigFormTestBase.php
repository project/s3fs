<?php

namespace Drupal\Tests\s3fs_bucket\Unit;

use Drupal\Core\Form\FormState;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Tests the Custom S3 Bucket Plugin.
 *
 * @group s3fs
 * @group s3fs_bucket
 */
abstract class S3fsBucketPluginConfigFormTestBase extends S3fsBucketPluginUnitTestBase {

  /**
   * Per test results for parent class to inspect.
   *
   * @var array
   */
  protected array $results = [];

  /* Test PluginFormInterface */

  /**
   * Validate buildConfigurationForm().
   */
  public function testBuildConfigurationForm(): void {
    $form_state = new FormState();
    $plugin = $this->setupPlugin();
    if (!($plugin instanceof PluginFormInterface)) {
      $this->markTestSkipped('Plugin does not inherit PluginFormInterface');
    }
    $result = $plugin->buildConfigurationForm($this->pluginConfig, $form_state);
    $this->assertIsArray($result);
    $this->results['build_form'] = $result;
  }

  /**
   * Helper for testing validateConfigurationForm().
   *
   * @return \Drupal\Core\Form\FormState
   *   $form_state after calling validateConfigurationForm().
   */
  protected function validateConfigurationForm(array $submitted_values): FormState {
    $plugin = $this->setupPlugin();
    if (!($plugin instanceof PluginFormInterface)) {
      $this->fail('Plugin does not inherit PluginFormInterface');
    }
    $submitted_values += $plugin->defaultConfiguration();
    $form_state = new FormState();
    $form_state->setValues($submitted_values);
    $form = [];
    $plugin->validateConfigurationForm($form, $form_state);
    return $form_state;
  }

}
