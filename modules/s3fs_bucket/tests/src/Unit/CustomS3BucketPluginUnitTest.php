<?php

namespace Drupal\Tests\s3fs_bucket\Unit;

use Drupal\Core\GeneratedUrl;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\s3fs_bucket\Plugin\S3Bucket\CustomS3BucketPlugin;
use Drupal\s3fs_client\S3ClientFactoryInterface;
use PHPUnit\Framework\Constraint\Constraint;
use PHPUnit\Framework\Constraint\TraversableContainsIdentical;

/**
 * Tests the Custom S3 Bucket Plugin.
 *
 * @group s3fs
 * @group s3fs_bucket
 *
 * @covers \Drupal\s3fs_bucket\Plugin\S3Bucket\CustomS3BucketPlugin
 * @covers \Drupal\s3fs_bucket\S3BucketPluginBase
 */
class CustomS3BucketPluginUnitTest extends S3fsBucketPluginTestBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    $this->pluginId = 's3_custom';
    $this->pluginDefinition = [
      'id' => $this->pluginId,
      'label' => 'Custom Bucket',
      'description' => 'A fully customizable option',
      'class' => CustomS3BucketPlugin::class,
      'provider' => 's3fs_bucket',
    ];

    parent::setUp();

    $this->pluginConfig['bucket_name'] = 'unit-test-bucket';
    $this->pluginConfig['keymodule_access_key_name'] = 'test';
    $this->pluginConfig['keymodule_secret_key_name'] = 'test';

  }

  /**
   * Test processClientConfig() using getS3Client().
   *
   * @param array $new_config
   *   New config to include in test.
   * @param string $expected_key
   *   The expected value for the access key.
   * @param string $expected_secret
   *   The expected value for the secret key.
   * @param string $expected_region
   *   The expected region.
   * @param \PHPUnit\Framework\Constraint\Constraint $expected_config
   *   The expected config.
   *
   * @dataProvider providerProcessClientConfig
   */
  public function testProcessClientConfig(array $new_config, string $expected_key, string $expected_secret, string $expected_region, Constraint $expected_config): void {
    $this->clientFactoryMock = $this->createMock(S3ClientFactoryInterface::class);
    $this->clientFactoryMock
      ->expects($this->once())
      ->method('createClient')
      ->with($expected_key, $expected_secret, $expected_region, $expected_config)->willReturn($this->s3ClientMock);

    $this->pluginConfig = $new_config + $this->pluginConfig;
    $plugin = $this->setupPlugin();
    $plugin->getS3Client();

  }

  /**
   * Provides test data for testProcessClientConfig()
   *
   * @return array<array{array, string, string, string, Constraint}>
   *   Array of test data.
   */
  public function providerProcessClientConfig(): array {
    return [
      'Default Config' => [
        [],
        'test',
        'test',
        'us-east-1',
        $this->logicalAnd(
          $this->arrayHasKey('signature'),
          new TraversableContainsIdentical('v4'),
          $this->arrayHasKey('version'),
          new TraversableContainsIdentical('2006-03-01'),
        ),
      ],
      'Access key token does not exist' => [
        [
          'keymodule_access_key_name' => 'non_existant_key',
        ],
        '',
        '',
        'us-east-1',
        $this->anything(),
      ],
      'Secret key token does not exist' => [
        [
          'keymodule_secret_key_name' => 'non_existant_key',
        ],
        '',
        '',
        'us-east-1',
        $this->anything(),
      ],
      'Alter region' => [
        [
          'region' => 'us-west-1',
        ],
        'test',
        'test',
        'us-west-1',
        $this->anything(),
      ],
      // @todo We may need a custom constraint for the following tests.
      'Use credential File' => [
        [
          'keymodule_access_key_name' => '',
          'keymodule_secret_key_name' => '',
          'credentials_file' => '/tmp/test.txt',
        ],
        '',
        '',
        'us-east-1',
        $this->anything(),
      ],
      'Use credential cache' => [
        [
          'keymodule_access_key_name' => '',
          'keymodule_secret_key_name' => '',
          'use_credentials_cache' => TRUE,
          'credentials_cache_dir' => '/tmp/',
        ],
        '',
        '',
        'us-east-1',
        $this->anything(),
      ],
      // End the todo on provider custom constraint.
      'Custom Endpoint enabled but not set' => [
        [
          'use_custom_endpoint' => TRUE,
          'endpoint' => NULL,
        ],
        'test',
        'test',
        'us-east-1',
        $this->logicalNot($this->arrayHasKey('endpoint')),
      ],
      'Custom Endpoint set but not enabled' => [
        [
          'use_custom_endpoint' => FALSE,
          'endpoint' => 'http://localhost.localstack.cloud',
        ],
        'test',
        'test',
        'us-east-1',
        $this->logicalNot($this->arrayHasKey('endpoint')),
      ],
      'Custom Endpoint set and enabled' => [
        [
          'use_custom_endpoint' => TRUE,
          'endpoint' => 'http://localhost.localstack.cloud',
        ],
        'test',
        'test',
        'us-east-1',
        $this->logicalAnd($this->arrayHasKey('endpoint'), new TraversableContainsIdentical('http://localhost.localstack.cloud')),
      ],
      'Do not use path style endpoint' => [
        [
          'use_path_style_endpoint' => FALSE,
        ],
        'test',
        'test',
        'us-east-1',
        $this->logicalNot($this->arrayHasKey('use_path_style_endpoint')),
      ],
      'Use path style endpoint' => [
        [
          'use_path_style_endpoint' => TRUE,
        ],
        'test',
        'test',
        'us-east-1',
        $this->logicalAnd($this->arrayHasKey('use_path_style_endpoint'), new TraversableContainsIdentical(TRUE)),
      ],
      'Do not disable certificate verification' => [
        [
          'disable_cert_verify' => FALSE,
        ],
        'test',
        'test',
        'us-east-1',
        $this->logicalnot($this->arrayHasKey('http')),
      ],
      'Disable certificate verification' => [
        [
          'disable_cert_verify' => TRUE,
        ],
        'test',
        'test',
        'us-east-1',
        $this->logicalAnd($this->arrayHasKey('http'), new TraversableContainsIdentical(['verify' => FALSE])),
      ],
      'Do not disable shared config files' => [
        [
          'disable_shared_config_files' => FALSE,
        ],
        'test',
        'test',
        'us-east-1',
        $this->logicalnot($this->arrayHasKey('use_aws_shared_config_files')),
      ],
      'Disable shared config files' => [
        [
          'disable_shared_config_files' => TRUE,
        ],
        'test',
        'test',
        'us-east-1',
        $this->logicalAnd($this->arrayHasKey('use_aws_shared_config_files'), new TraversableContainsIdentical(FALSE)),
      ],

    ];
  }

  /**
   * {@inheritdoc}
   */
  public function pluginConfigDataProvider(): array {
    return [
      'isPathStyleEndpoint True' => [
        'isPathStyleEndpoint',
        TRUE,
        [
          'use_path_style_endpoint' => TRUE,
        ],
      ],
      'isPathStyleEndpoint False' => [
        'isPathStyleEndpoint',
        FALSE,
        [
          'use_path_style_endpoint' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function testGetS3Metadata(): void {

    $data_object_exists = [
      'bucket' => 'unit-test-bucket',
      'path' => 'path/to/object',
      'filesize' => 1234567,
      'timestamp' => 1578014410,
      'dir' => 0,
      'version' => '44cff4ee-1370-11ec-82a8-0242ac130003',
    ];

    $data_directory_exists = [
      'bucket' => 'unit-test-bucket',
      'path' => 'path/to/object',
      'filesize' => 1234567,
      'timestamp' => 1578014410,
      'dir' => 1,
      'version' => '',
    ];

    // Each call to $plugin->readCache() should have one call to
    // S3fsMetadataService::refreshCache() for a total of 6 calls.
    $this->metadataServiceMock
      ->expects($this->exactly(6))
      ->method('readCache')
      ->with('path/to/object', 'unit-test-bucket')
      ->willReturnOnConsecutiveCalls(
        $data_object_exists,
        $data_directory_exists,
        FALSE,
        $data_object_exists,
        $data_directory_exists,
        FALSE
      );

    $this->metadataServiceMock
      ->method('convertMetadata')
      ->with($this->isType('string'), $this->isType('array'), 'unit-test-bucket')
      ->willReturn($data_object_exists);

    $headobject_result_mock = $this->createMock('\Aws\ResultInterface');
    $headobject_result_mock->method('toArray')->willReturn(
      [
        'ContentLength' => '1234567',
        'LastModified' => 'Fri, 03 Jan 2020 01:20:10 GMT',
        'VersionId' => '44cff4ee-1370-11ec-82a8-0242ac130003',
      ]
    );

    // Of the three 'ignore_cache' tests only the two that are not a
    // directory should trigger a headObject() lookup.
    $matcher = $this->exactly(2);
    $this->s3ClientMock
      ->expects($matcher)
      ->method('headObject')
      ->willReturn($headobject_result_mock);

    $plugin = $this->setupPlugin();

    $this->assertEquals($data_object_exists, $plugin->getS3fsObject('path/to/object'));
    $this->assertEquals($data_directory_exists, $plugin->getS3fsObject('path/to/object'));
    $this->assertFalse($plugin->getS3fsObject('path/to/object'));

    // Test with ignore cache enabled.
    $this->pluginConfig['ignore_cache'] = TRUE;
    $plugin->setConfiguration($this->pluginConfig);

    // Object is in metadata cache but should trigger lookup to headObject()
    $this->assertEquals($data_object_exists, $plugin->getS3fsObject('path/to/object'));
    $this->assertEquals('1', $matcher->getInvocationCount());
    // Object is a directory and should not cause lookup.
    $this->assertEquals($data_directory_exists, $plugin->getS3fsObject('path/to/object'));
    $this->assertEquals('1', $matcher->getInvocationCount());
    // No item in metadata cache, return via headObject();
    $this->assertEquals($data_object_exists, $plugin->getS3fsObject('path/to/object'));
    $this->assertEquals('2', $matcher->getInvocationCount());

  }

  /**
   * {@inheritdoc}
   */
  public function providerIsPathStyleEndpoint(): array {
    return [
      'Bucket defaults to not use path style endpoint' => [
        [],
        FALSE,
      ],
      'Bucket configured to not use path style endpoint' => [
        [
          'use_path_style_endpoint' => FALSE,
        ],
        FALSE,
      ],
      'Bucket configured to use path style endpoint' => [
        [
          'use_path_style_endpoint' => TRUE,
        ],
        TRUE,
      ],
    ];
  }

  /**
   * Test getCommandParams().
   *
   * @param array $new_config
   *   New config to include in test.
   * @param string $path
   *   The path to get command parameters for.
   * @param array $params
   *   Existing params to be included returned response.
   * @param array $expected_result
   *   For use by this class to pass in test expectations.
   *
   * @dataProvider providerGetCommandParams
   */
  public function testGetCommandParams(array $new_config, string $path, array $params, $expected_result): void {

    $alter_callback = function ($command_name, &$params, &$context) use ($path) {
      $this->assertEquals('s3fs_bucket_command_params', $command_name, 'Correct command used for alter');
      $this->assertIsArray($params, '$params is array for alter callback');
      $this->assertIsArray($context, '$context is an array for alter callback');
      $this->assertEquals($path, $context['key_path'], 'Alter calls provided correct key_path context');
      $this->assertEquals('unit-test-bucket', $context['bucket_id'], 'Bucket entity id provided to alter context');
      $this->assertArrayNotHasKey('Callback-Success', $params, 'Callback success tag is not present in alter call');
      $params['Callback-Success'] = TRUE;
    };

    $this->moduleHandlerMock
      ->expects($this->once())
      ->method('alter')
      ->willReturnCallback($alter_callback);

    parent::testGetCommandParams($new_config, $path, $params, $expected_result);
    $results = $this->results['results'];
    $this->assertEquals('unit-test-bucket', $results['Bucket'], 'Bucket name is correct');
    $this->assertEquals($path, $results['Key'], 'Key path is correct');
    $this->assertTrue($results['Callback-Success'], 'Params were altered by callback');

    if (!empty($expected_result['expected_params'])) {
      foreach ($expected_result['expected_params'] as $key => $expected_value) {
        $this->assertArrayHasKey($key, $results, "$key passed to params is retained");
        if (!empty($expected_value)) {
          $this->assertEquals($results[$key], $expected_value, "$key has expected value in params");
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function providerGetCommandParams(): array {
    return [
      'With empty config and params' => [
        [],
        'path/to/object',
        [],
        NULL,
      ],
      'Ensure Bucket and Key params are overwritten' => [
        [],
        'path/to/object',
        [
          'Bucket' => 'invalid-bucket',
          'Key' => 'Invalid-Key',
        ],
        NULL,
      ],
      'Passed in parameter is retained' => [
        [],
        'path/to/object',
        [
          'My Custom Param' => 'my value',
        ],
        [
          'expected_params' => [
            'My Custom Param' => 'my value',
          ],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function testGetStreamOptions(): void {
    $this->pluginConfig['upload_as_private'] = FALSE;
    $plugin = $this->setupPlugin();

    // ACL should always be set to public-read  if bucket isn't private.
    $options = [
      'ACL' => 'authenticated-read',
    ];
    $plugin->getStreamOptions($options);
    $this->assertArrayHasKey('ACL', $options);
    $this->assertEquals('public-read', $options['ACL']);

    // Ensure that ACL is removed if bucket is private.
    $this->pluginConfig['upload_as_private'] = TRUE;
    $plugin->setConfiguration($this->pluginConfig);
    $plugin->getStreamOptions($options);
    $this->assertArrayNotHasKey('ACL', $options);

  }

  /**
   * {@inheritdoc}
   */
  public function providerAlterKeyedPath(): array {
    return [
      'No alter required' => [
        [],
        '/path/to/some/object',
        '/path/to/some/object',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @dataProvider providerRefreshCache
   */
  public function testRefreshCache(array $new_config): void {
    parent::testRefreshCache($new_config);
    $this->assertCount(36, $this->results['written_objects']);
  }

  /**
   * {@inheritdoc}
   */
  public function providerRefreshCache(): array {
    return [
      'Version sync enabled' => [
        ['disable_version_sync' => FALSE],
      ],
      'Version sync disabled' => [
        ['disable_version_sync' => TRUE],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function testBuildConfigurationForm(): void {

    // We need to mock the url_generator for Url::fromRoute().
    $urlGenerator = $this->createMock(UrlGeneratorInterface::class);
    $urlGenerator->method('generateFromRoute')
      ->with('entity.key.collection', $this->anything(), $this->anything())
      ->willReturn((new GeneratedUrl())
        ->setGeneratedUrl('/admin/config/system/keys'));
    $container = \Drupal::getContainer();
    \Drupal::unsetContainer();
    $container->set('url_generator', $urlGenerator);
    \Drupal::setContainer($container);

    parent::testBuildConfigurationForm();
  }

  /**
   * Test validateConfigurationForm without errors.
   *
   * @todo We may want to move bucket validation to a public method
   * to make testing easier.
   */
  public function testValidateConfigurationFormNoErrors(): void {

    $this->s3ClientMock
      ->method('determineBucketRegion')
      ->with('test-bucket')
      ->willReturn('us-east-1');
    // @todo this may not be on spec
    $this->s3ClientMock
      ->method('getObject')
      ->willReturn(TRUE);

    $config = [
      'bucket_name' => 'test-bucket',
      'keymodule_access_key_name' => 'test',
      'keymodule_secret_key_name' => 'test',
    ];

    $form_state = $this->validateConfigurationForm($config);
    $this->assertEmpty($form_state->getErrors());

  }

  /**
   * {@inheritdoc}
   */
  protected function setupPlugin(): CustomS3BucketPlugin {
    return new CustomS3BucketPlugin(
      $this->pluginConfig,
      $this->pluginId,
      $this->pluginDefinition,
      $this->clientFactoryMock,
      $this->metadataServiceMock,
      $this->s3fsBucketRefreshBatchMock,
      $this->keyServiceMock,
      $this->mimeTypeGuesserMocker,
      $this->moduleHandlerMock
    );
  }

}
