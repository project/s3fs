<?php

namespace Drupal\Tests\s3fs_bucket\Unit;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\s3fs_bucket\Exception\BucketIdEmptyException;
use Drupal\s3fs_bucket\Exception\InvalidMetadataException;
use Drupal\s3fs_bucket\Exception\PathLengthExceededException;
use Drupal\s3fs_bucket\S3fsMetadataService;
use Drupal\s3fs_bucket\S3fsMetadataServiceInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the S3fsMetadata Service.
 *
 * @group s3fs
 * @group s3fs_bucket
 *
 * @covers \Drupal\s3fs_bucket\S3fsMetadataService
 */
class S3fsMetadataUnitTest extends UnitTestCase {

  /**
   * Request time service mock.
   *
   * @var \Drupal\Component\Datetime\TimeInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $timeMock;

  /**
   * Cache backend service mock.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $cacheBackendMock;

  /**
   * Lock backend service mock.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $lockBackendMock;

  /**
   * LoggerChannelFactory service mock.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $loggerChannelFactoryMock;

  /**
   * Messenger service mock.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $messengerMock;

  /**
   * Database connection service mock.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject|\Drupal\Core\Database\Connection
   */
  protected $connectionMock;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->connectionMock = $this->createMock('\Drupal\Core\Database\Connection');

    $this->timeMock = $this->createMock(TimeInterface::class);

    $this->timeMock->method('getRequestTime')->willReturn(1640000000);
    $this->cacheBackendMock = $this->createMock(CacheBackendInterface::class);
    $this->lockBackendMock = $this->createMock(LockBackendInterface::class);
    $this->loggerChannelFactoryMock = $this->createMock(LoggerChannelFactoryInterface::class);
    $this->loggerChannelFactoryMock->method('get')->willReturn(
      $this->createMock(LoggerChannelInterface::class)
    );
    $this->messengerMock = $this->createMock(MessengerInterface::class);
  }

  /**
   * Ensure convertMetadata properly converts metadata to standardized format.
   *
   * @param string $path
   *   Path to test against.
   * @param string $bucketId
   *   BucketId to use for test.
   * @param array $metadata
   *   Metadata to be converted to standardized format.
   * @param array $expected
   *   Expected results.
   * @param class-string<\Throwable>|null $expected_exception
   *   Expected exception or NULL for success.
   *
   * @dataProvider providerConvertMetadata
   */
  public function testConvertMetadata(string $path, string $bucketId, array $metadata, array $expected, ?string $expected_exception = NULL): void {
    $s3fsMetadata = new S3fsMetadataService($this->connectionMock, $this->timeMock, $this->cacheBackendMock, $this->lockBackendMock, $this->getStringTranslationStub(), $this->loggerChannelFactoryMock, $this->messengerMock);
    if ($expected_exception !== NULL) {
      $this->expectException($expected_exception);
    }
    $converted = $s3fsMetadata->convertMetadata($path, $metadata, $bucketId);
    $this->assertEquals($converted, $expected);
  }

  /**
   * DataProvider for testConvertMetadata().
   *
   * @return array
   *   An array of test options.
   */
  public function providerConvertMetadata(): array {
    return [
      'Empty Metadata' => [
        '',
        '',
        [],
        [
          'bucket' => '',
          'path' => '',
          'filesize' => 0,
          'timestamp' => 1640000000,
          'dir' => 1,
          'version' => '',
        ],
        BucketIdEmptyException::class,
      ],
      'Provide bucketId only' => [
        '',
        'unitTestBucket',
        [],
        [
          'bucket' => 'unitTestBucket',
          'path' => '',
          'filesize' => 0,
          'timestamp' => 1640000000,
          'dir' => 1,
          'version' => '',
        ],
      ],
      'Directory path' => [
        '/tmp/test/',
        'unitTestBucket',
        [],
        [
          'bucket' => 'unitTestBucket',
          'path' => '/tmp/test/',
          'filesize' => 0,
          'timestamp' => 1640000000,
          'dir' => 1,
          'version' => '',
        ],
      ],
      'Metadata ContentLength' => [
        '/tmp/test.txt',
        'unitTestBucket',
        [
          'ContentLength' => 12345,
        ],
        [
          'bucket' => 'unitTestBucket',
          'path' => '/tmp/test.txt',
          'filesize' => 12345,
          'timestamp' => 1640000000,
          'dir' => 0,
          'version' => '',
        ],
      ],
      'Metadata Size' => [
        '/tmp/test.txt',
        'unitTestBucket',
        [
          'Size' => 12345,
        ],
        [
          'bucket' => 'unitTestBucket',
          'path' => '/tmp/test.txt',
          'filesize' => 12345,
          'timestamp' => 1640000000,
          'dir' => 0,
          'version' => '',
        ],
      ],
      'Metadata LastModified' => [
        '/tmp/test.txt',
        'unitTestBucket',
        [
          'LastModified' => '2021-10-01T06:05:58.501Z',
        ],
        [
          'bucket' => 'unitTestBucket',
          'path' => '/tmp/test.txt',
          'filesize' => 0,
          'timestamp' => 1633068358,
          'dir' => 0,
          'version' => '',
        ],
      ],
      'Metadata VersionId' => [
        '/tmp/test.txt',
        'unitTestBucket',
        [
          'VersionId' => 'RandomVersionString',
        ],
        [
          'bucket' => 'unitTestBucket',
          'path' => '/tmp/test.txt',
          'filesize' => 0,
          'timestamp' => 1640000000,
          'dir' => 0,
          'version' => 'RandomVersionString',
        ],
      ],
    ];
  }

  /**
   * Test writeCache() throws expected exceptions.
   *
   * @param array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir'?: 0|1, 'version'?: string} $metadata
   *   Metadata array.
   * @param class-string<\Throwable> $expected_exception
   *   Expected exception.
   *
   * @dataProvider providerWriteCacheException
   */
  public function testWriteCacheException(array $metadata, string $expected_exception): void {
    $s3fsMetadata = new S3fsMetadataService($this->connectionMock, $this->timeMock, $this->cacheBackendMock, $this->lockBackendMock, $this->getStringTranslationStub(), $this->loggerChannelFactoryMock, $this->messengerMock);
    $this->expectException($expected_exception);
    $s3fsMetadata->writeCache($metadata);
  }

  /**
   * DataProvider for testWriteCacheException.
   *
   * @return array[]
   *   Callback data.
   */
  public function providerWriteCacheException(): array {
    return [
      'Empty metadata bucket' => [
        [
          'bucket' => '',
          'path' => '/tmp/test.txt',
          'filesize' => 0,
          'timestamp' => 1640000000,
          'dir' => 0,
          'version' => 'RandomVersionString',
        ],
        InvalidMetadataException::class,
      ],
      'Empty metadata path' => [
        [
          'bucket' => 'unitTestBucket',
          'path' => '',
          'filesize' => 0,
          'timestamp' => 1640000000,
          'dir' => 0,
          'version' => 'RandomVersionString',
        ],
        InvalidMetadataException::class,
      ],
      'Path too long' => [
        [
          'bucket' => 'unitTestBucket',
          'path' => $this->randomMachineName(S3fsMetadataServiceInterface::MAX_PATH_LENGTH + 1),
          'filesize' => 0,
          'timestamp' => 1640000000,
          'dir' => 0,
          'version' => 'RandomVersionString',
        ],
        PathLengthExceededException::class,
      ],
    ];
  }

}
