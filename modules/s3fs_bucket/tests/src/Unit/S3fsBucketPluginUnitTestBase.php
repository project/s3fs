<?php

namespace Drupal\Tests\s3fs_bucket\Unit;

use Aws\S3\S3Client;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\key\KeyInterface;
use Drupal\key\KeyRepositoryInterface;
use Drupal\s3fs_bucket\Batch\S3fsBucketRefreshCacheBatchInterface;
use Drupal\s3fs_bucket\S3BucketInterface;
use Drupal\s3fs_bucket\S3BucketPluginInterface;
use Drupal\s3fs_client\S3ClientFactoryInterface;
use Drupal\Tests\s3fs_bucket\Traits\S3fsMetadataServiceTestTrait;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\Mime\MimeTypeGuesserInterface;

/**
 * Tests the Custom S3 Bucket Plugin.
 *
 * @group s3fs
 * @group s3fs_bucket
 */
abstract class S3fsBucketPluginUnitTestBase extends UnitTestCase {

  use S3fsMetadataServiceTestTrait;

  /**
   * Id of plugin to test.
   *
   * @var string
   */
  protected string $pluginId;

  /**
   * Plugin definition.
   *
   * @var array
   */
  protected array $pluginDefinition;

  /**
   * S3fs Metadata service mock.
   *
   * @var \Drupal\s3fs_bucket\S3fsMetadataServiceInterface&\PHPUnit\Framework\MockObject\MockObject
   */
  protected $metadataServiceMock;

  /**
   * MimeTypeGuesser service mock.
   *
   * @var \Symfony\Component\Mime\MimeTypeGuesserInterface&\PHPUnit\Framework\MockObject\MockObject
   */
  protected $mimeTypeGuesserMocker;

  /**
   * Key Repository mock.
   *
   * @var \Drupal\key\KeyRepositoryInterface&\PHPUnit\Framework\MockObject\MockObject
   */
  protected $keyServiceMock;

  /**
   * ModuleHandler mock.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface&\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moduleHandlerMock;

  /**
   * Base config for plugins.
   *
   * @var array
   */
  protected $pluginConfig;

  /**
   * The S3Client mock.
   *
   * @var \Aws\S3\S3Client&\PHPUnit\Framework\MockObject\MockObject
   */
  protected $s3ClientMock;

  /**
   * S3Client Factory mock.
   *
   * @var \Drupal\s3fs_client\S3ClientFactoryInterface&\PHPUnit\Framework\MockObject\MockObject
   */
  protected $clientFactoryMock;

  /**
   * S3fsBucketRefreshBatch service mock.
   *
   * @var \Drupal\s3fs_bucket\Batch\S3fsBucketRefreshCacheBatchInterface&\PHPUnit\Framework\MockObject\MockObject
   */

  protected $s3fsBucketRefreshBatchMock;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Key Service.
    $keyValue = $this->createMock(KeyInterface::class);
    $keyValue->method('getKeyValue')->willReturn('test');
    $this->keyServiceMock = $this->createMock(KeyRepositoryInterface::class);
    $this->keyServiceMock
      ->method('getKey')
      ->willReturnMap(
        [
          ['test', $keyValue],
        ]
      );

    $entityMock = $this->createMock(S3BucketInterface::class);
    $entityMock->method('id')->willReturn('unit-test-bucket');

    $s3client_mock_additional_methods = [
      'abortMultipartUpload',
      'abortMultipartUploadAsync',
      'completeMultipartUpload',
      'completeMultipartUploadAsync',
      'copyObject',
      'copyObjectAsync',
      'createBucket',
      'createBucketAsync',
      'createMultipartUpload',
      'createMultipartUploadAsync',
      'deleteBucket',
      'deleteBucketAsync',
      'deleteBucketAnalyticsConfiguration',
      'deleteBucketAnalyticsConfigurationAsync',
      'deleteBucketCors',
      'deleteBucketCorsAsync',
      'deleteBucketEncryption',
      'deleteBucketEncryptionAsync',
      'deleteBucketIntelligentTieringConfiguration',
      'deleteBucketIntelligentTieringConfigurationAsync',
      'deleteBucketInventoryConfiguration',
      'deleteBucketInventoryConfigurationAsync',
      'deleteBucketLifecycle',
      'deleteBucketLifecycleAsync',
      'deleteBucketMetricsConfiguration',
      'deleteBucketMetricsConfigurationAsync',
      'deleteBucketOwnershipControls',
      'deleteBucketOwnershipControlsAsync',
      'deleteBucketPolicy',
      'deleteBucketPolicyAsync',
      'deleteBucketReplication',
      'deleteBucketReplicationAsync',
      'deleteBucketTagging',
      'deleteBucketTaggingAsync',
      'deleteBucketWebsite',
      'deleteBucketWebsiteAsync',
      'deleteObject',
      'deleteObjectAsync',
      'deleteObjectTagging',
      'deleteObjectTaggingAsync',
      'deleteObjects',
      'deleteObjectsAsync',
      'deletePublicAccessBlock',
      'deletePublicAccessBlockAsync',
      'getBucketAccelerateConfiguration',
      'getBucketAccelerateConfigurationAsync',
      'getBucketAcl',
      'getBucketAclAsync',
      'getBucketAnalyticsConfiguration',
      'getBucketAnalyticsConfigurationAsync',
      'getBucketCors',
      'getBucketCorsAsync',
      'getBucketEncryption',
      'getBucketEncryptionAsync',
      'getBucketIntelligentTieringConfiguration',
      'getBucketIntelligentTieringConfigurationAsync',
      'getBucketInventoryConfiguration',
      'getBucketInventoryConfigurationAsync',
      'getBucketLifecycle',
      'getBucketLifecycleAsync',
      'getBucketLifecycleConfiguration',
      'getBucketLifecycleConfigurationAsync',
      'getBucketLocation',
      'getBucketLocationAsync',
      'getBucketLogging',
      'getBucketLoggingAsync',
      'getBucketMetricsConfiguration',
      'getBucketMetricsConfigurationAsync',
      'getBucketNotification',
      'getBucketNotificationAsync',
      'getBucketNotificationConfiguration',
      'getBucketNotificationConfigurationAsync',
      'getBucketOwnershipControls',
      'getBucketOwnershipControlsAsync',
      'getBucketPolicy',
      'getBucketPolicyAsync',
      'getBucketPolicyStatus',
      'getBucketPolicyStatusAsync',
      'getBucketReplication',
      'getBucketReplicationAsync',
      'getBucketRequestPayment',
      'getBucketRequestPaymentAsync',
      'getBucketTagging',
      'getBucketTaggingAsync',
      'getBucketVersioning',
      'getBucketVersioningAsync',
      'getBucketWebsite',
      'getBucketWebsiteAsync',
      'getObject',
      'getObjectAsync',
      'getObjectAcl',
      'getObjectAclAsync',
      'getObjectAttributes',
      'getObjectAttributesAsync',
      'getObjectLegalHold',
      'getObjectLegalHoldAsync',
      'getObjectLockConfiguration',
      'getObjectLockConfigurationAsync',
      'getObjectRetention',
      'getObjectRetentionAsync',
      'getObjectTagging',
      'getObjectTaggingAsync',
      'getObjectTorrent',
      'getObjectTorrentAsync',
      'getPublicAccessBlock',
      'getPublicAccessBlockAsync',
      'headBucket',
      'headBucketAsync',
      'headObject',
      'headObjectAsync',
      'listBucketAnalyticsConfigurations',
      'listBucketAnalyticsConfigurationsAsync',
      'listBucketIntelligentTieringConfigurations',
      'listBucketIntelligentTieringConfigurationsAsync',
      'listBucketInventoryConfigurations',
      'listBucketInventoryConfigurationsAsync',
      'listBucketMetricsConfigurations',
      'listBucketMetricsConfigurationsAsync',
      'listBuckets',
      'listBucketsAsync',
      'listMultipartUploads',
      'listMultipartUploadsAsync',
      'listObjectVersions',
      'listObjectVersionsAsync',
      'listObjects',
      'listObjectsAsync',
      'listObjectsV2',
      'listObjectsV2Async',
      'listParts',
      'listPartsAsync',
      'putBucketAccelerateConfiguration',
      'putBucketAccelerateConfigurationAsync',
      'putBucketAcl',
      'putBucketAclAsync',
      'putBucketAnalyticsConfiguration',
      'putBucketAnalyticsConfigurationAsync',
      'putBucketCors',
      'putBucketCorsAsync',
      'putBucketEncryption',
      'putBucketEncryptionAsync',
      'putBucketIntelligentTieringConfiguration',
      'putBucketIntelligentTieringConfigurationAsync',
      'putBucketInventoryConfiguration',
      'putBucketInventoryConfigurationAsync',
      'putBucketLifecycle',
      'putBucketLifecycleAsync',
      'putBucketLifecycleConfiguration',
      'putBucketLifecycleConfigurationAsync',
      'putBucketLogging',
      'putBucketLoggingAsync',
      'putBucketMetricsConfiguration',
      'putBucketMetricsConfigurationAsync',
      'putBucketNotification',
      'putBucketNotificationAsync',
      'putBucketNotificationConfiguration',
      'putBucketNotificationConfigurationAsync',
      'putBucketOwnershipControls',
      'putBucketOwnershipControlsAsync',
      'putBucketPolicy',
      'putBucketPolicyAsync',
      'putBucketReplication',
      'putBucketReplicationAsync',
      'putBucketRequestPayment',
      'putBucketRequestPaymentAsync',
      'putBucketTagging',
      'putBucketTaggingAsync',
      'putBucketVersioning',
      'putBucketVersioningAsync',
      'putBucketWebsite',
      'putBucketWebsiteAsync',
      'putObject',
      'putObjectAsync',
      'putObjectAcl',
      'putObjectAclAsync',
      'putObjectLegalHold',
      'putObjectLegalHoldAsync',
      'putObjectLockConfiguration',
      'putObjectLockConfigurationAsync',
      'putObjectRetention',
      'putObjectRetentionAsync',
      'putObjectTagging',
      'putObjectTaggingAsync',
      'putPublicAccessBlock',
      'putPublicAccessBlockAsync',
      'restoreObject',
      'restoreObjectAsync',
      'selectObjectContent',
      'selectObjectContentAsync',
      'uploadPart',
      'uploadPartAsync',
      'uploadPartCopy',
      'uploadPartCopyAsync',
      'writeGetObjectResponse',
      'writeGetObjectResponseAsync',
    ];
    $this->s3ClientMock = $this->getMockBuilder(S3Client::class)
      ->disableOriginalConstructor()
      ->disableOriginalClone()
      ->disableArgumentCloning()
      ->addMethods($s3client_mock_additional_methods)
      ->onlyMethods(get_class_methods(S3Client::class))
      ->getMock();

    $this->clientFactoryMock = $this->createMock(S3ClientFactoryInterface::class);
    $this->clientFactoryMock->method('createClient')->with('test', 'test', 'us-east-1', $this->anything())->willReturn($this->s3ClientMock);

    $this->metadataServiceMock = $this->getS3MetadataServiceStub();
    $this->mimeTypeGuesserMocker = $this->createMock(MimeTypeGuesserInterface::class);
    $this->moduleHandlerMock = $this->createMock(ModuleHandlerInterface::class);
    $this->s3fsBucketRefreshBatchMock = $this->createMock(S3fsBucketRefreshCacheBatchInterface::class);

    $this->pluginConfig = [
      '#bucket' => $entityMock,
    ];

    // Mock the logger.factory in the global container for watchdog_exception.
    \Drupal::unsetContainer();
    $container = new ContainerBuilder();
    $loggerFactoryMock = $this->createMock(LoggerChannelFactoryInterface::class);
    $loggerFactoryMock
      ->method('get')
      ->willReturn(
        $this->createMock(LoggerChannelInterface::class)
      );
    $container->set('logger.factory', $loggerFactoryMock);
    $container->set('string_translation', $this->getStringTranslationStub());
    \Drupal::setContainer($container);

    // Make sure every run has purged the static cache. s3Client mock may be
    // replaced without config change.
    drupal_static_reset();

  }

  /**
   * Set up the plugin under test.
   *
   * This function should be replaced by extenders to set up the real plugin.
   *
   * @return \Drupal\s3fs_bucket\S3BucketPluginInterface
   *   An instance of the plugin type under test.
   */
  abstract protected function setupPlugin(): S3BucketPluginInterface;

}
