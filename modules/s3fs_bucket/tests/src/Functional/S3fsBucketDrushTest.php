<?php

namespace Drupal\Tests\s3fs_bucket\Functional;

use Drupal\Tests\TestFileCreationTrait;
use Drush\TestTraits\DrushTestTrait;

/**
 * S3fs_bucket Drush Test.
 *
 * Ensure that drush performs correctly.
 *
 * @group s3fs
 * @group s3fs_bucket
 */
class S3fsBucketDrushTest extends S3fsBucketTestBase {

  use DrushTestTrait;
  use TestFileCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'key',
    's3fs_bucket',
    's3fs_client',
    's3fs_bucket_localstack_config',
  ];

  /**
   * Coverage test for the drush commands.
   *
   * @covers \Drupal\s3fs_bucket\Commands\S3fsBucketCommands
   */
  public function testDrushCommands(): void {
    /*
     * Coverage test for the drush list buckets.
     */
    $this->drush('s3fs:list-buckets', [], ['format' => 'csv']);
    $messages = $this->getOutput();
    $this->assertEquals("ID,Name,Status\nlocalstack,Localstack,Enabled", $messages);

    /*
     * Coverage test for the drush refresh-cache.
     */
    $this->drush('s3fs:refresh-cache', [], [], NULL, NULL, 1);
    $messages = $this->getErrorOutput();
    $this->assertStringContainsString('Must specify a bucket name', $messages, 'Must specify a bucket name');

    $this->drush('s3fs:refresh-cache', [], ['bucket' => 'localstack']);
    $messages = $this->getErrorOutput();
    $this->assertStringContainsString('Message: The cached list of files has been refreshed in', $messages, 'Successfully Refreshed Cache');
  }

}
