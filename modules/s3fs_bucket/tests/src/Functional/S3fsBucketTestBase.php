<?php

namespace Drupal\Tests\s3fs_bucket\Functional;

use Aws\Exception\AwsException;
use Aws\S3\Exception\S3Exception;
use Drupal\Tests\BrowserTestBase;

/**
 * S3 File System Test Base.
 *
 * Provides a base for BrowserTest to execute against.
 *
 * This test is designed to execute against a local localstack instance.
 * Should you need to execute it against another provider the target may be
 * edited inside the s3fs_bucket_localstack_config test module.
 *
 * @group s3fs
 * @group s3fs_bucket
 */
abstract class S3fsBucketTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'key',
    's3fs_bucket',
    's3fs_client',
    's3fs_streamwrapper',
    's3fs_bucket_localstack_config',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The s3fs module config.
   *
   * @var array
   */
  protected $s3Config;

  /**
   * The AWS SDK for PHP S3Client object.
   *
   * @var \Aws\S3\S3ClientInterface
   */
  protected $s3;

  /**
   * S3 Credentials provided and bucket exists.
   *
   * @var bool
   */
  protected $bucketNotFound = FALSE;

  /**
   * Folder name to use for placing tests files.
   *
   * @var string
   */
  protected $remoteTestsFolder = '_s3fs_tests';

  /**
   * Full base key path for tests folder.
   *
   * @var string
   */
  protected $remoteTestsFolderKey = '';

  /**
   * S3fs_bucket config entity.
   *
   * @var \Drupal\s3fs_bucket\S3BucketInterface
   */
  protected $s3Bucket;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->connection = $this->container->get('database');

    $this->remoteTestsFolderKey = $this->remoteTestsFolder . '/' . uniqid('', TRUE);
    /** @var \Drupal\s3fs_bucket\S3BucketInterface s3Bucket */
    $s3Bucket = $this->container->get('entity_type.manager')->getStorage('s3fs_bucket')->load('localstack');
    $this->s3Bucket = $s3Bucket;

    $this->s3 = $this->s3Bucket->getS3Client();

    $this->bucketNotFound = !$this->s3->doesBucketExistV2($this->s3Bucket->getBucketName(), FALSE);

    $connectAttempts = 0;
    while ($this->bucketNotFound && $connectAttempts <= 5) {
      try {
        $this->s3->createBucket([
          'Bucket' => $this->s3Bucket->getBucketName(),
        ]);
        $this->bucketNotFound = FALSE;
      }
      catch (S3Exception $e) {
        // Bucket possibly was created by another script between checking.
        $this->bucketNotFound = !$this->s3->doesBucketExistV2($this->s3Bucket->getBucketName(), FALSE);
      }
      catch (AwsException $e) {
        // No need to continue tests if we can't access the bucket. Either the
        // credentials are incorrect or problem with S3Client.
        $this->fail("Unable to create bucket '{$this->s3Bucket->getBucketName()}'. Please verify the S3 settings.");
      }

      if ($this->bucketNotFound) {
        // Wait before we loop again.
        sleep(5);
      }
      $connectAttempts++;
    }

    if (!$this->bucketNotFound) {
      // Empty out the bucket before the test, to prevent unexpected errors.
      $this->s3->deleteMatchingObjects($this->s3Bucket->getBucketName(), $this->remoteTestsFolderKey);
    }
    else {
      $this->fail("Unable to access bucket '{$this->s3Bucket->getBucketName()}'. Please verify the S3 settings.");
    }
  }

  /**
   * Clean up S3 folder.
   */
  protected function tearDown(): void {
    if (!$this->bucketNotFound) {
      $this->s3->deleteMatchingObjects($this->s3Bucket->getBucketName(), $this->remoteTestsFolderKey);
    }
    parent::tearDown();
  }

}
