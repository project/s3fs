<?php

namespace Drupal\Tests\s3fs_bucket\Traits;

use Drupal\s3fs_bucket\S3fsMetadataServiceInterface;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Provides helpers for mocking the S3fs Metadata Service during tests.
 */
trait S3fsMetadataServiceTestTrait {

  /**
   * Obtain a mock S3fs Metadata service.
   *
   * @return \PHPUnit\Framework\MockObject\MockObject&\Drupal\s3fs_bucket\S3fsMetadataServiceInterface
   *   Mock S3fsMetadata service.
   */
  public function getS3MetadataServiceStub(): MockObject&S3fsMetadataServiceInterface {

    $callback_convert_metadata = function (string $path, array $s3_metadata, string $bucketId): array {

      $metadata = [
        'bucket' => $bucketId,
        'path' => $path,
        'filesize' => 0,
        'timestamp' => 1640000000,
        'dir' => 0,
        'version' => '',
      ];

      if (empty($s3_metadata)) {
        // The caller wants directory metadata.
        $metadata['dir'] = 1;
      }
      else {
        // The filesize value can come from either the Size or ContentLength
        // attribute, depending on which AWS API call built $s3_metadata.
        if (isset($s3_metadata['ContentLength'])) {
          $metadata['filesize'] = $s3_metadata['ContentLength'];
        }
        else {
          if (isset($s3_metadata['Size'])) {
            $metadata['filesize'] = $s3_metadata['Size'];
          }
        }

        if (isset($s3_metadata['LastModified'])) {
          $metadata['timestamp'] = date('U', strtotime($s3_metadata['LastModified']));
        }

        if (isset($s3_metadata['VersionId']) && $s3_metadata['VersionId'] != 'null') {
          $metadata['version'] = $s3_metadata['VersionId'];
        }
      }
      return $metadata;
    };

    $metadata_service_mock = $this->createMock(S3fsMetadataServiceInterface::class);
    $metadata_service_mock->method('convertMetadata')->with($this->isType('string'), $this->isType('array'))->willReturnCallback($callback_convert_metadata);
    return $metadata_service_mock;
  }

}
