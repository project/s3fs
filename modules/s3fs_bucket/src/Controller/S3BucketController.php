<?php

namespace Drupal\s3fs_bucket\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\s3fs_bucket\S3BucketInterface;

/**
 * Returns responses for S3 Bucket routes.
 */
class S3BucketController extends ControllerBase {

  use MessengerTrait;

  /**
   * Enables a search server without a confirmation form.
   *
   * @param \Drupal\s3fs_bucket\S3BucketInterface $s3fs_bucket
   *   The server to be enabled.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response to send to the browser.
   */
  public function bucketBypassEnable(S3BucketInterface $s3fs_bucket) {
    $s3fs_bucket->setStatus(TRUE)->save();

    // Notify the user about the status change.
    $this->messenger()->addStatus($this->t('The %name bucket has been enabled.', ['%name' => $s3fs_bucket->label()]));

    // Redirect to the bucket's "Edit" page.
    $url = $s3fs_bucket->toUrl('edit-form');
    return $this->redirect($url->getRouteName(), $url->getRouteParameters());
  }

}
