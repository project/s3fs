<?php

namespace Drupal\s3fs_bucket\Entity;

use Aws\S3\S3ClientInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\s3fs_bucket\S3BucketInterface;
use Drupal\s3fs_bucket\S3BucketPluginInterface;
use Drupal\s3fs_bucket\S3Exception;

/**
 * Defines the s3 bucket entity type.
 *
 * @ConfigEntityType(
 *   id = "s3fs_bucket",
 *   label = @Translation("S3 Bucket"),
 *   label_collection = @Translation("S3 Buckets"),
 *   label_singular = @Translation("S3 bucket"),
 *   label_plural = @Translation("S3 buckets"),
 *   label_count = @PluralTranslation(
 *     singular = "@count S3 bucket",
 *     plural = "@count S3 buckets",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\s3fs_bucket\S3BucketListBuilder",
 *     "form" = {
 *       "add" = "Drupal\s3fs_bucket\Form\S3BucketForm",
 *       "edit" = "Drupal\s3fs_bucket\Form\S3BucketForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *       "disable" = "Drupal\s3fs_bucket\Form\S3BucketDisableConfirmForm",
 *       "actions" = "Drupal\s3fs_bucket\Form\S3BucketEntityActionsForm"
 *     }
 *   },
 *   config_prefix = "s3fs_bucket",
 *   admin_permission = "administer s3fs_bucket",
 *   links = {
 *     "collection"  = "/admin/config/s3/s3-bucket",
 *     "canonical"   = "/admin/config/s3/s3-bucket/{s3fs_bucket}",
 *     "add-form"    = "/admin/config/s3/s3-bucket/add",
 *     "edit-form"   = "/admin/config/s3/s3-bucket/{s3fs_bucket}",
 *     "delete-form" = "/admin/config/s3/s3-bucket/{s3fs_bucket}/delete",
 *     "disable"     = "/admin/config/s3/s3-bucket/{s3fs_bucket}/disable",
 *     "enable"      = "/admin/config/s3/s3-bucket/{s3fs_bucket}/enable",
 *     "actions"     = "/admin/config/s3/s3-bucket/{s3fs_bucket}/actions"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "status" = "status"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "bucket_type",
 *     "bucket_config",
 *   }
 * )
 */
class S3Bucket extends ConfigEntityBase implements S3BucketInterface {

  /**
   * The s3 bucket ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * The s3fs_bucket description.
   *
   * @var string
   */
  protected string $description = '';

  /**
   * The id of the S3BucketPlugin.
   *
   * @var string
   */
  protected string $bucket_type = '';

  /**
   * Settings for this bucket set during plugin configuration.
   *
   * @var array
   */
  protected array $bucket_config = [];

  /**
   * The bucket plugin instance.
   *
   * @var \Drupal\s3fs_bucket\S3BucketPluginInterface|null
   */
  protected ?S3BucketPluginInterface $bucketPlugin = NULL;

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function hasValidBucketPlugin(): bool {
    $backend_plugin_definition = \Drupal::service('plugin.manager.s3fs_bucket')->getDefinition($this->getPluginId(), FALSE);
    return !empty($backend_plugin_definition);
  }

  /**
   * Returns the bucket type id.
   *
   * @return string
   *   Bucket plugin type id.
   */
  public function getPluginId(): string {
    return $this->bucket_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getBucketPlugin(): S3BucketPluginInterface {
    if (!$this->bucketPlugin) {
      /** @var \Drupal\s3fs_bucket\S3BucketPluginManager $bucket_plugin_manager */
      $bucket_plugin_manager = \Drupal::service('plugin.manager.s3fs_bucket');
      $config = $this->bucket_config;
      $config['#bucket'] = $this;
      try {
        $this->bucketPlugin = $bucket_plugin_manager->createInstance($this->getPluginId(), $config);
      }
      catch (PluginException $e) {
        $pluginId = $this->getPluginId();
        $label = $this->label();
        throw new S3Exception("The plugin with ID '$pluginId' could not be retrieved for bucket '$label'.");
      }
    }
    return $this->bucketPlugin;
  }

  /**
   * {@inheritdoc}
   */
  public function getBucketConfig(): array {
    return $this->bucket_config;
  }

  /**
   * {@inheritdoc}
   */
  public function setBucketConfig(array $bucket_config): static {
    $this->bucket_config = $bucket_config;
    // In case the bucket plugin is already loaded, make sure the configuration
    // stays in sync.
    if ($this->bucketPlugin
      && $this->getBucketPlugin()->getConfiguration() !== $bucket_config) {
      $this->getBucketPlugin()->setConfiguration($bucket_config);
    }
    return $this;
  }

  /**
   * Overrides \Drupal\Core\Entity\Entity::preSave().
   */
  public function preSave(EntityStorageInterface $storage): void {

    if (!$this->isNew()) {
      // Some updates are always disallowed.
      if ($this->id != $this->getOriginalId()) {
        throw new S3Exception("Cannot change an existing bucket's machine name.");
      }
    }

    parent::preSave($storage);
  }

  /*
   * The following functions are fulfilled by the bucket plugin.
   */

  /**
   * Functions for obtaining Plugin configuration.
   */

  /**
   * {@inheritdoc}
   */
  public function getS3Client(): S3ClientInterface {
    return $this->getbucketPlugin()->getS3Client();
  }

  /**
   * {@inheritdoc}
   */
  public function getBucketName(): string {
    return $this->getbucketPlugin()->getBucketName();
  }

  /**
   * {@inheritdoc}
   */
  public function isPathStyleEndpoint(): bool {
    return $this->getbucketPlugin()->isPathStyleEndpoint();
  }

  /**
   * File Stream related functions.
   */

  /**
   * {@inheritdoc}
   */
  public function getCommandParams(string $path, array $params = []): array {
    return $this->getbucketPlugin()->getCommandParams($path, $params);
  }

  /**
   * {@inheritdoc}
   */
  public function getStreamOptions(&$options): void {
    $this->getbucketPlugin()->getStreamOptions($options);
  }

  /**
   * {@inheritdoc}
   */
  public function alterKeyedPath($path): string {
    return $this->getbucketPlugin()->alterKeyedPath($path);
  }

  /**
   * Cache related functions.
   */

  /**
   * {@inheritdoc}
   */
  public function readCache(string $path): array|FALSE {
    return $this->getbucketPlugin()->readCache($path);
  }

  /**
   * {@inheritdoc}
   */
  public function writeCache(array $metadata): void {
    $this->getbucketPlugin()->writeCache($metadata);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteCache(string|array $path): int {
    return $this->getbucketPlugin()->deleteCache($path);
  }

  /**
   * {@inheritdoc}
   */
  public function convertMetadata(string $path, array $s3_metadata): array {
    return $this->getbucketPlugin()->convertMetadata($path, $s3_metadata);
  }

  /**
   * {@inheritdoc}
   */
  public function getS3Metadata(string $path): array|FALSE {
    return $this->getbucketPlugin()->getS3Metadata($path);
  }

  /**
   * {@inheritdoc}
   */
  public function listDir(string $path): array {
    return $this->getbucketPlugin()->listDir($path);
  }

  /**
   * {@inheritdoc}
   */
  public function isDirEmpty(string $path): bool {
    return $this->getbucketPlugin()->isDirEmpty($path);
  }

  /**
   * {@inheritdoc}
   */
  public function waitUntilFileExists(string $path): bool {
    return $this->getbucketPlugin()->waitUntilFileExists($path);
  }

  /**
   * {@inheritdoc}
   */
  public function refreshCache(array|\DrushBatchContext &$context): void {
    $this->getbucketPlugin()->refreshCache($context);
  }

  /**
   * {@inheritdoc}
   */
  public function getMaxPathLength(): int {
    return $this->getBucketPlugin()->getMaxPathLength();
  }

  /**
   * {@inheritdoc}
   */
  public function getS3fsObject(string $path): array|FALSE {
    return $this->getBucketPlugin()->getS3fsObject($path);
  }

  /**
   * {@inheritdoc}
   */
  public function buildActionsForm(array $form, FormStateInterface $form_state): array {
    return $this->getBucketPlugin()->buildActionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateActionsForm(array &$form, FormStateInterface $form_state): void {
    $this->getBucketPlugin()->validateActionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitActionsForm(array &$form, FormStateInterface $form_state): void {
    $this->getBucketPlugin()->submitActionsForm($form, $form_state);
  }

}
