<?php

namespace Drupal\s3fs_bucket;

/**
 * Defines a S3fsService service.
 *
 * @type
 */
interface S3fsMetadataServiceInterface {

  /**
   * Max object length.
   *
   * Max length for object path in a bucket.
   *  This must match the size of the PATH database field.
   *
   * @const
   */
  const MAX_PATH_LENGTH = 255;

  /**
   * Convert file metadata returned from S3 into a metadata cache array.
   *
   * @param string $path
   *   The path of the resource.
   * @param array{'ContentLength'?: non-negative-int, 'Size'?: non-negative-int, 'LastModified'?: string, 'VersionId'?: string}|array{} $s3_metadata
   *   An array containing the collective metadata for the object in S3.
   *   The caller may send an empty array here to indicate that the returned
   *   metadata should represent a directory.
   * @param string $bucketId
   *   Machine_name of the bucket entity for this plugin.
   *     Allows s3fs_file cache entries to be associated with
   *     the correct bucket.
   *
   * @return array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir': 0|1, 'version': string}
   *   A file metadata cache array.
   *
   * @throws \Drupal\s3fs_bucket\Exception\BucketIdEmptyException
   *   The $bucketId parameter must not be empty.
   */
  public function convertMetadata(string $path, array $s3_metadata, string $bucketId): array;

  /**
   * Write an object's (and its ancestor folders') metadata to the cache.
   *
   * @param array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir'?: 0|1, 'version'?: string} $metadata
   *   An associative array of file metadata in this format:
   *     'bucket' => The machine of this bucket's entity.
   *     'path' => The full URI of the file, including the scheme.
   *     'filesize' => The size of the file, in bytes.
   *     'timestamp' => The file's create/update timestamp.
   *     'dir' => A boolean indicating whether the object is a directory.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\InvalidMetadataException
   * @throws \Drupal\s3fs_bucket\Exception\PathLengthExceededException
   */
  public function writeCache(array $metadata): void;

  /**
   * Delete an object's metadata from the cache.
   *
   * @param string|string[] $path
   *   A string (or array of strings) containing the path(s) of the object(s)
   *   to be deleted.
   * @param string $bucketId
   *   Machine_name of the bucket entity for this plugin..
   *     Allows s3fs_file cache entries to be associated with
   *     the correct bucket.
   *
   * @return int
   *   Number of paths deleted from the cache.
   */
  public function deleteCache(string|array $path, string $bucketId): int;

  /**
   * Fetch an object from the file metadata cache table.
   *
   * @param string $path
   *   The path of the resource to check.
   * @param string $bucketId
   *   Machine_name of the bucket entity for this plugin..
   *     Allows s3fs_file cache entries to be associated with
   *     the correct bucket.
   *
   * @return array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir': 0|1, 'version': string}|false
   *   An array of metadata if the $path is in the cache. Otherwise, FALSE.
   */
  public function readCache(string $path, string $bucketId): array|FALSE;

  /**
   * Fetch an object from the file metadata cache table.
   *
   * @param string $path
   *   The path of the directory to list, without trailing slash.
   * @param string $bucketId
   *   Machine_name of the bucket entity for this plugin..
   *     Allows s3fs_file cache entries to be associated with
   *     the correct bucket.
   *
   * @return array
   *   An array of non-recursive paths in a 'directory'.
   */
  public function listDir(string $path, string $bucketId): array;

  /**
   * Determine if the 'directory' is empty.
   *
   * @param string $path
   *   The path of the directory to list, without trailing slash.
   * @param string $bucketId
   *   Machine_name of the bucket entity for this plugin.
   *     Allows s3fs_file cache entries to be associated with
   *     the correct bucket.
   *
   * @return bool
   *   True if the 'directory' is empty.
   */
  public function isDirEmpty(string $path, string $bucketId): bool;

  /**
   * Obtain a list of existing 'directories' from the metadata table.
   *
   * @param string $bucketId
   *   Machine_name of the bucket entity.
   *
   * @return array
   *   An array containing that object paths for existing 'folders'.
   *
   * @throws \Drupal\s3fs_bucket\Exception\BucketIdEmptyException
   *   The $bucketId parameter must not be empty.
   */
  public function getExistingFolders(string $bucketId): array;

  /**
   * Prepare the temporary metadata table to receive new data.
   *
   * @param string $bucketId
   *   Machine_name of the bucket entity.
   */
  public function prepareTempTable(string $bucketId): void;

  /**
   * Write object metadata to the temporary metadata table.
   *
   * @param array $file_metadata_list
   *   An array of convertMetadata objects to be inserted into the database.
   * @param array $folders
   *   Modified to contain a list of all 'folders' necessary for the paths
   *   to exist.
   */
  public function writeTemporaryMetadata(array $file_metadata_list, array &$folders): void;

  /**
   * Create 'directory' entries in the temporary metadata table.
   *
   * @param array $folders
   *   An array containing object paths for 'folders' to be inserted into
   *   the database.
   * @param string $bucketId
   *   Machine_name of the bucket entity to migrate.
   */
  public function writeTemporaryFolders(array $folders, string $bucketId): void;

  /**
   * Migrate data from the temporary metadata table into real metadata table.
   *
   * @param string $bucketId
   *   Machine_name of the bucket entity.
   *     Allows s3fs_file cache entries to be associated with
   *     the correct bucket.
   */
  public function moveTempMetadata(string $bucketId): void;

  /**
   * Count the number of objects in the metadata table.
   *
   * @param string $bucketId
   *   Machine_name of the bucketId to limit search to, otherwise all buckets.
   *
   * @return int
   *   Count of objects in the metadata table.
   */
  public function countStoredObjects(string $bucketId): int;

}
