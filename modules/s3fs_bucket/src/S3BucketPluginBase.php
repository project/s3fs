<?php

namespace Drupal\s3fs_bucket;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\s3fs_bucket\Batch\S3fsBucketRefreshCacheBatchInterface;
use Drupal\s3fs_client\S3ClientFactoryInterface;

/**
 * Base class for s3fs_bucket plugins.
 */
abstract class S3BucketPluginBase extends PluginBase implements S3BucketPluginInterface {

  use StringTranslationTrait;

  /**
   * The S3Bucket Entity.
   *
   * @var \Drupal\s3fs_bucket\S3BucketInterface|null
   */
  protected ?S3BucketInterface $bucket = NULL;

  /**
   * S3fs Client Factory service.
   *
   * @var \Drupal\s3fs_client\S3ClientFactoryInterface
   */
  protected S3ClientFactoryInterface $clientFactory;

  /**
   * S3fs Metadata Service.
   *
   * @var \Drupal\s3fs_bucket\S3fsMetadataServiceInterface
   */
  protected S3fsMetadataServiceInterface $s3fsMetadata;

  /**
   * Bucket cache refresh batch service.
   *
   * @var \Drupal\s3fs_bucket\Batch\S3fsBucketRefreshCacheBatchInterface
   */
  protected S3fsBucketRefreshCacheBatchInterface $refreshCacheBatch;

  /**
   * Base constructor for a Bucket Plugin.
   *
   * @param array $configuration
   *   Plugin Configuration.
   * @param string $plugin_id
   *   Plugin Id.
   * @param array $plugin_definition
   *   Plugin Definition.
   * @param \Drupal\s3fs_client\S3ClientFactoryInterface $s3fs_client_factory
   *   S3fs_client factory service.
   * @param \Drupal\s3fs_bucket\S3fsMetadataServiceInterface $s3fs_metadata
   *   S3fs_bucket metadata service.
   * @param \Drupal\s3fs_bucket\Batch\S3fsBucketRefreshCacheBatchInterface $refresh_cache_batch
   *   S3fs_bucket Refresh Cache Batch service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, S3ClientFactoryInterface $s3fs_client_factory, S3fsMetadataServiceInterface $s3fs_metadata, S3fsBucketRefreshCacheBatchInterface $refresh_cache_batch) {
    if (($configuration['#bucket'] ?? NULL) instanceof S3BucketInterface) {
      $this->setBucket($configuration['#bucket']);
      unset($configuration['#bucket']);
    }
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->clientFactory = $s3fs_client_factory;
    $this->s3fsMetadata = $s3fs_metadata;
    $this->refreshCacheBatch = $refresh_cache_batch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create($container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('s3fs_client.factory'),
      $container->get('s3fs_bucket.metadata'),
      $container->get('s3fs_bucket.refresh_cache_batch'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    $plugin_definition = $this->getPluginDefinition();
    return $plugin_definition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $plugin_definition = $this->getPluginDefinition();
    return $plugin_definition['description'] ?? '';
  }

  /**
   * Gets default configuration for this plugin.
   *
   * @return array{'bucket_name': string, 'region': string}
   *   An associative array with the default configuration.
   */
  public function defaultConfiguration(): array {
    return [
      // The bucket name on the S3 Provider.
      'bucket_name' => '',
      // Region ID to use on remote bucket.
      'region' => 'us-east-1',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration += $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration;

    try {
      $bucket_entity = $this->getBucket();
    }
    catch (\Exception) {
      $bucket_entity = NULL;
    }

    if ($bucket_entity && $bucket_entity->getBucketConfig() !== $configuration) {
      $bucket_entity->setBucketConfig($configuration);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setBucket(S3BucketInterface $bucket): static {
    $this->bucket = $bucket;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPathStyleEndpoint(): bool {
    return FALSE;
  }

  /**
   * Is cache ignored.
   *
   * @return bool
   *   True if cache lookup is ignored.
   */
  protected function isCacheDisabled(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function readCache(string $path): array|FALSE {
    return $this->s3fsMetadata->readCache($path, $this->getBucketId());
  }

  /**
   * {@inheritdoc}
   */
  public function writeCache(array $metadata): void {
    $this->s3fsMetadata->writeCache($metadata);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteCache(string|array $path): int {
    return $this->s3fsMetadata->deleteCache($path, $this->getBucketId());
  }

  /**
   * {@inheritdoc}
   */
  public function listDir($path): array {
    return $this->s3fsMetadata->listDir($path, $this->getBucketId());
  }

  /**
   * {@inheritdoc}
   */
  public function isDirEmpty($path): bool {
    return $this->s3fsMetadata->isDirEmpty($path, $this->getBucketId());
  }

  /**
   * {@inheritdoc}
   */
  public function alterKeyedPath(string $path): string {
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function waitUntilFileExists(string $path): bool {
    // Retry ten times, once every second.
    $attempts = 10;
    $params = $this->getCommandParams($path);
    $params['@waiter'] = [
      'delay' => 1,
      'maxAttempts' => $attempts,
    ];
    try {
      $this->getS3Client()->waitUntil('ObjectExists', $params);
      return TRUE;
    }
    catch (\Exception $e) {
      watchdog_exception('s3fs_bucket', $e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getMaxPathLength(): int {
    // MAX_PATH_LENGTH.
    return S3fsMetadataServiceInterface::MAX_PATH_LENGTH;
  }

  /**
   * {@inheritdoc}
   */
  public function getS3fsObject(string $path): array|FALSE {

    // Trim any trailing '/', in case this is a folder request.
    $path = rtrim($path, '/');

    // For the root directory, return metadata for a generic folder.
    if ($path == '') {
      return $this->convertMetadata('', []);
    }

    if (mb_strlen($path) > $this->getMaxPathLength()) {
      return FALSE;
    }

    // Check if this URI is in the cache. NOTE We do this even if cache is
    // disabled because directories do not exist in S3, only object keys.
    $metadata = $this->readCache($path);

    // If cache ignore is enabled, query S3 for all URIs that are
    // not directories.
    if ($this->isCacheDisabled() && ($metadata == FALSE || !$metadata['dir'])) {
      // If getS3Metadata() returns FALSE, the file doesn't exist.
      $metadata = $this->getS3Metadata($path);
    }

    return $metadata;
  }

  /*
   * Bucket action form related functions.
   */

  /**
   * {@inheritdoc}
   */
  public function buildActionsForm(array $form, FormStateInterface $form_state): array {
    $form['#attached']['library'][] = 's3fs_bucket/s3fs_bucket.actionform';

    $form['refresh_cache'] = [
      '#type' => 'fieldset',
      '#description' => $this->t(
        "The file metadata cache keeps track of every file that S3 File System writes to (and deletes from) the S3 bucket,
      so that queries for data about those files (checks for existence, filetype, etc.) don't have to hit S3.
      This speeds up many operations, most noticeably anything related to images and their derivatives."
      ),
      '#title' => $this->t('File Metadata Cache'),
    ];
    $refresh_description = $this->t(
      "This button queries S3 for the metadata of <i><b>all</b></i> the files in your site's bucket and saves it
    to the database. This may take a while for buckets with many thousands of files. <br>
    It should only be necessary to use this button if you've just installed S3 File System and you need to cache all the
    pre-existing files in your bucket, or if you need to restore your metadata cache from scratch for some other reason."
    );
    $form['refresh_cache']['refresh'] = [
      '#type' => 'submit',
      '#suffix' => '<div class="refresh">' . $refresh_description . '</div>',
      '#name' => 'refresh_cache_btn',
      '#value' => $this->t("Refresh file metadata cache"),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateActionsForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   */
  public function submitActionsForm(array &$form, FormStateInterface $form_state): void {
    $triggerElement = $form_state->getTriggeringElement();
    if (isset($triggerElement['#name'])) {
      switch ($triggerElement['#name']) {
        case 'refresh_cache_btn':
          /** @var \Drupal\s3fs_bucket\S3BucketInterface $entity */
          $entity = $this->getBucket();
          $this->refreshCacheBatch->execute($entity);
          break;

      }
    }
  }

  /**
   * Get the entity that controls this plugin.
   *
   * @return \Drupal\s3fs_bucket\S3BucketInterface
   *   The Bucket entity.
   *
   * @throws \Exception
   */
  protected function getBucket(): S3BucketInterface {
    if ($this->bucket == NULL) {
      throw new \Exception('Bucket has not yet been set');
    }

    return $this->bucket;
  }

  /**
   * Get the machine name of the bucket entity.
   *
   * @return string
   *   The machine_name of the bucket entity.
   */
  protected function getBucketId(): string {
    return $this->getBucket()->id();
  }

}
