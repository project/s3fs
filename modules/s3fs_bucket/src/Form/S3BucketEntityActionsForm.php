<?php

namespace Drupal\s3fs_bucket\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\s3fs_bucket\Batch\S3fsBucketRefreshCacheBatchInterface;

/**
 * Form for Bucket Actions.
 *
 * Provides the ability for admins to:
 *  - Refresh the metadata cache.
 *  - Migrate files from local paths.
 */
class S3BucketEntityActionsForm extends EntityForm {

  /**
   * Bucket cache refresh batch service.
   *
   * @var \Drupal\s3fs_bucket\Batch\S3fsBucketRefreshCacheBatchInterface
   */
  protected S3fsBucketRefreshCacheBatchInterface $refreshCacheBatch;

  /**
   * StreamWrapperManager Service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected StreamWrapperManagerInterface $streamWrapperManager;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 's3fs_bucket_actions_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\s3fs_bucket\Entity\S3Bucket $entity */
    $entity = $this->entity;
    return $entity->buildActionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\s3fs_bucket\Entity\S3Bucket $entity */
    $entity = $this->entity;
    $entity->validateActionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\s3fs_bucket\Entity\S3Bucket $entity */
    $entity = $this->entity;
    $entity->submitActionsForm($form, $form_state);
  }

}
