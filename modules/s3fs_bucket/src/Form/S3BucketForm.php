<?php

namespace Drupal\s3fs_bucket\Form;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Utility\Error;
use Drupal\s3fs_bucket\S3BucketInterface;
use Drupal\s3fs_bucket\S3BucketPluginManager;
use Drupal\s3fs_bucket\Utility\Utility;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * S3 Bucket form.
 *
 * @property \Drupal\s3fs_bucket\S3BucketInterface $entity
 */
class S3BucketForm extends EntityForm {

  /**
   * Plugin manager for s3fs_bucket.
   *
   * @var \Drupal\s3fs_bucket\S3BucketPluginManager
   */
  protected S3BucketPluginManager $s3BucketPluginManager;

  /**
   * Constructor for s3 bucket forms.
   *
   * @param \Drupal\s3fs_bucket\S3BucketPluginManager $s3BucketPluginManager
   *   Plugin Manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger Service.
   */
  public function __construct(S3BucketPluginManager $s3BucketPluginManager, MessengerInterface $messenger) {
    $this->s3BucketPluginManager = $s3BucketPluginManager;
    $this->setMessenger($messenger);
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('plugin.manager.s3fs_bucket'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    // If the form is being rebuilt, rebuild the entity with the current form
    // values.
    if ($form_state->isRebuilding()) {
      /** @var \Drupal\s3fs_bucket\S3BucketInterface $entity */
      $entity = $this->buildEntity($form, $form_state);
      $this->entity = $entity;
    }

    $form = parent::form($form, $form_state);

    /** @var \Drupal\s3fs_bucket\S3BucketInterface $bucket */
    $bucket = $this->getEntity();

    // Set the page title according to whether we are creating or editing the
    // bucket.
    if ($bucket->isNew()) {
      $form['#title'] = $this->t('Add S3 Bucket');
    }
    else {
      $form['#title'] = $this->t('Edit S3 Bucket %label', ['%label' => $bucket->label()]);
    }

    $this->buildEntityForm($form, $form_state, $bucket);
    // Skip adding the backend config form if we cleared the server form due to
    // an error.
    if ($form) {
      $this->buildPluginConfigForm($form, $form_state, $bucket);
    }

    return $form;
  }

  /**
   * Builds the form for the basic server properties.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param \Drupal\s3fs_bucket\S3BucketInterface $bucket
   *   The s3 bucket that is being created or edited.
   */
  public function buildEntityForm(array &$form, FormStateInterface $form_state, S3BucketInterface $bucket): void {
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('Enter a label for this bucket configuration.'),
      '#default_value' => $bucket->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $bucket->isNew() ? NULL : $bucket->id(),
      '#maxlength' => 50,
      '#required' => TRUE,
      '#machine_name' => [
        'exists' => '\Drupal\s3fs_bucket\Entity\S3Bucket::load',
        'source' => ['label'],
      ],
      '#disabled' => !$bucket->isNew(),
    ];
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Only enabled buckets can be accessed by file operations.'),
      '#default_value' => $bucket->status(),
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Enter a description for the bucket.'),
      '#default_value' => $bucket->getDescription(),
    ];

    $plugins = $this->s3BucketPluginManager->getDefinitions();
    $plugin_options = [];
    $descriptions = [];
    foreach ($plugins as $plugin_id => $definition) {
      $config = $plugin_id === $bucket->getPluginId() ? $bucket->getBucketConfig() : [];
      $config['#bucket'] = $bucket;
      try {
        /** @var \Drupal\s3fs_bucket\S3BucketPluginInterface $plugin */
        $plugin = $this->s3BucketPluginManager
          ->createInstance($plugin_id, $config);
      }
      catch (PluginException $e) {
        continue;
      }
      $plugin_options[$plugin_id] = Utility::escapeHtml($plugin->label());
      $descriptions[$plugin_id]['#description'] = Utility::escapeHtml($plugin->getDescription());
    }
    asort($plugin_options, SORT_NATURAL | SORT_FLAG_CASE);
    if (count($plugin_options) === 1) {
      $bucket->set('bucket_type', key($plugin_options));
    }
    $form['bucket_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Bucket type'),
      '#description' => $this->t('Choose a plugin to use for this bucket.'),
      '#options' => $plugin_options,
      '#default_value' => $bucket->getPluginId(),
      '#required' => TRUE,
      '#disabled' => !$bucket->isNew(),
      '#ajax' => [
        'callback' => [get_class($this), 'buildAjaxBucketConfigForm'],
        'wrapper' => 's3-bucket-plugin-config-form',
        'method' => 'replace',
        'effect' => 'fade',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state): array {
    if ($form === []) {
      return [];
    }

    return parent::actions($form, $form_state);
  }

  /**
   * Builds the plugin-specific configuration form.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param \Drupal\s3fs_bucket\S3BucketInterface $bucket
   *   The bucket that is being created or edited.
   */
  public function buildPluginConfigForm(array &$form, FormStateInterface $form_state, S3BucketInterface $bucket): void {
    $form['bucket_config'] = [];
    if ($bucket->hasValidBucketPlugin()) {
      $plugin = $bucket->getBucketPlugin();
      $form_state->set('plugin', $plugin->getPluginId());
      if ($plugin instanceof PluginFormInterface) {
        if ($form_state->isRebuilding()) {
          $this->messenger->addWarning($this->t('Please configure the selected plugin.'));
        }
        // Attach the plugin configuration form.
        $plugin_form_state = SubformState::createForSubform($form['bucket_config'], $form, $form_state);
        $form['bucket_config'] = $plugin->buildConfigurationForm($form['bucket_config'], $plugin_form_state);

        // Modify the bucket plugin configuration container element.
        $form['bucket_config']['#type'] = 'details';
        $form['bucket_config']['#title'] = $this->t('Configure %plugin plugin', ['%plugin' => $plugin->label()]);
        $form['bucket_config']['#open'] = TRUE;
      }
    }
    // Only notify the user of a missing plugin plugin if we're editing an
    // existing bucket.
    elseif (!$bucket->isNew()) {
      $this->messenger->addError($this->t('The bucket plugin is missing or invalid.'));
      return;
    }
    $form['bucket_config'] += [
      '#type' => 'container',
    ];
    $form['bucket_config']['#attributes']['id'] = 's3-bucket-plugin-config-form';
    $form['bucket_config']['#tree'] = TRUE;
  }

  /**
   * Handles switching the selected plugin plugin.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The part of the form to return as AJAX.
   */
  public static function buildAjaxBucketConfigForm(array $form, FormStateInterface $form_state): array {
    // The work is already done in form(), where we rebuild the entity according
    // to the current form values and then create the plugin configuration form
    // based on that. So we just need to return the relevant part of the form
    // here.
    return $form['bucket_config'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    /** @var \Drupal\s3fs_bucket\S3BucketInterface $server */
    $server = $this->getEntity();

    // Check if the backend plugin changed.
    $plugin_id = $server->getPluginId();
    if ($plugin_id != $form_state->get('plugin')) {
      // This can only happen during initial server creation, since we don't
      // allow switching the plugin afterwards. The user has selected a
      // different plugin, so any values entered for the other plugin should
      // be discarded.
      $input = &$form_state->getUserInput();
      $input['bucket_config'] = [];
      $new_backend = $this->s3BucketPluginManager->createInstance($form_state->getValues()['bucket_type']);
      if ($new_backend instanceof PluginFormInterface) {
        $form_state->setRebuild();
      }
    }
    // Check before loading the bucket plugin so we don't throw an exception.
    elseif ($server->hasValidBucketPlugin()) {
      $backend = $server->getBucketPlugin();
      if ($backend instanceof PluginFormInterface) {
        $backend_form_state = SubformState::createForSubform($form['bucket_config'], $form, $form_state);
        $backend->validateConfigurationForm($form['bucket_config'], $backend_form_state);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    /** @var \Drupal\s3fs_bucket\S3BucketInterface $bucket */
    $bucket = $this->getEntity();
    // Check before loading the backend plugin so we don't throw an exception.
    if ($bucket->hasValidBucketPlugin()) {
      $backend = $bucket->getBucketPlugin();
      if ($backend instanceof PluginFormInterface) {
        $backend_form_state = SubformState::createForSubform($form['bucket_config'], $form, $form_state);
        $backend->submitConfigurationForm($form['bucket_config'], $backend_form_state);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // Only save the bucket if the form doesn't need to be rebuilt.
    if (!$form_state->isRebuilding()) {
      try {
        $bucket = $this->getEntity();
        $result = $bucket->save();
        $this->messenger->addStatus($this->t('The bucket was successfully saved.'));
        $form_state->setRedirect('entity.s3fs_bucket.edit_form', ['s3fs_bucket' => $bucket->id()]);
        return $result;
      }
      catch (EntityStorageException $e) {
        $form_state->setRebuild();

        $message = '%type: @message in %function (line %line of %file).';
        $variables = Error::decodeException($e);
        $this->getLogger('s3fs_bucket')->error($message, $variables);

        $this->messenger->addError($this->t('The bucket could not be saved.'));
      }
    }
  }

}
