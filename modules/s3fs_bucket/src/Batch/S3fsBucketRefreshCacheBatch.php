<?php

namespace Drupal\s3fs_bucket\Batch;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\s3fs_bucket\S3BucketInterface;

/**
 * Performs a refresh of the s3fs_file table with data from S3 bucket.
 *
 * @package Drupal\s3fs\Batch
 */
class S3fsBucketRefreshCacheBatch implements S3fsBucketRefreshCacheBatchInterface {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function execute(S3BucketInterface $entity): void {

    // Create batch.
    $batch_builder = $this->getBatch();
    $args = [
      $entity,
    ];
    $batch_builder->addOperation([
      S3fsBucketRefreshCacheBatch::class,
      'refreshCacheOperation',
    ], $args);
    batch_set($batch_builder->toArray());

    $batch =& batch_get();

    // Drush integration.
    if (PHP_SAPI === 'cli') {
      $batch['progressive'] = FALSE;
      drush_backend_batch_process();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getBatch(): BatchBuilder {
    return (new BatchBuilder())
      ->setTitle($this->t('Refresh cached list of files S3'))
      ->setFinishCallback([S3fsBucketRefreshCacheBatch::class, 'finished'])
      ->setInitMessage($this->t('The process to refresh the cached list of files is about to start..'))
      ->setProgressMessage($this->t('Processed batch @current out of an unknown total (AWS S3 does not make the total number of files available without looping through all files).'))
      ->setErrorMessage($this->t('Something wrong happened, please check the logs.'));
  }

  /**
   * {@inheritdoc}
   */
  public static function refreshCacheOperation(S3BucketInterface $entity, array|\DrushBatchContext &$context): void {
    $entity->getBucketPlugin()->refreshCache($context);
  }

  /**
   * Get the elapsed time since start of the batch process.
   *
   * @param int $time_start
   *   Unix timestamp of when function started.
   *
   * @return string
   *   Elapsed time as a string format of 'hh:mm:ss'.
   */
  protected static function getElapsedTimeFormatted($time_start) {
    $time_elapsed_seconds = time() - $time_start;
    return gmdate('H:i:s', $time_elapsed_seconds);
  }

  /**
   * {@inheritdoc}
   */
  public static function finished(bool $success, array $results, array $operations): void {
    \Drupal::messenger()
      ->addStatus(new TranslatableMarkup('The cached list of files has been refreshed in @elapsed_time (hh:mm:ss).', [
        '@elapsed_time' => static::getElapsedTimeFormatted($results['time_start']),
      ]));

  }

}
