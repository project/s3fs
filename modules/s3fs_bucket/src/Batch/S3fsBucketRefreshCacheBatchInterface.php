<?php

namespace Drupal\s3fs_bucket\Batch;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\s3fs_bucket\S3BucketInterface;

/**
 * Performs a refresh of the s3fs_file table with data from S3 bucket.
 *
 * @package Drupal\s3fs\Batch
 */
interface S3fsBucketRefreshCacheBatchInterface {

  /**
   * Refreshes the complete list of objects in the S3 bucket.
   *
   * @param \Drupal\s3fs_bucket\S3BucketInterface $entity
   *   S3fs_bucket entity that this operation will be conducted on.
   */
  public function execute(S3BucketInterface $entity): void;

  /**
   * Initialise a batch builder object.
   *
   * @return \Drupal\Core\Batch\BatchBuilder
   *   The instantiated batch builder.
   */
  public function getBatch(): BatchBuilder;

  /**
   * Batch operation callback to refresh the cached list of S3 bucket objects.
   *
   * @param \Drupal\s3fs_bucket\S3BucketInterface $entity
   *   S3fs_bucket entity that this operation will be conducted on.
   * @param array|\DrushBatchContext $context
   *   Batch context.
   */
  public static function refreshCacheOperation(S3BucketInterface $entity, array|\DrushBatchContext &$context): void;

  /**
   * Finished batch callback.
   *
   * @param bool $success
   *   Whether the batch completed successfully or not.
   * @param array $results
   *   The results key of the batch context.
   * @param array $operations
   *   The operations that were carried out.
   */
  public static function finished(bool $success, array $results, array $operations): void;

}
