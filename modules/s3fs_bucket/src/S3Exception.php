<?php

namespace Drupal\s3fs_bucket;

/**
 * Base class for all exceptions thrown by S3 functions.
 *
 * This class has no functionality of its own other than allowing all
 * S3 exceptions to be caught by a single catch block.
 */
class S3Exception extends \RuntimeException {}
