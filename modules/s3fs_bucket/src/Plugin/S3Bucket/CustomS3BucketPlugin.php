<?php

namespace Drupal\s3fs_bucket\Plugin\S3Bucket;

use Aws\Credentials\CredentialProvider;
use Aws\DoctrineCacheAdapter;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3ClientInterface;
use Doctrine\Common\Cache\FilesystemCache;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\key\KeyRepositoryInterface;
use Drupal\s3fs_bucket\Batch\S3fsBucketRefreshCacheBatchInterface;
use Drupal\s3fs_bucket\S3BucketPluginBase;
use Drupal\s3fs_bucket\S3fsMetadataServiceInterface;
use Drupal\s3fs_client\S3ClientFactoryInterface;
use Symfony\Component\Mime\MimeTypeGuesserInterface;

/**
 * Plugin implementation of the s3fs_bucket for a fully customizable plugin.
 *
 * Provides:
 *   - S3Client - Configured for use with the bucket.
 *   - An interface to Object Metadata Service.
 *
 * @S3Bucket(
 *   id = "s3_custom",
 *   label = @Translation("Custom Bucket"),
 *   description = @Translation("A fully customizable option")
 * )
 *
 * @method array{'keymodule_access_key_name': string, 'keymodule_secret_key_name': string, 'credentials_file': string, 'use_credentials_cache': bool, 'credentials_cache_dir': string, 'disable_shared_config_files': bool, 'use_custom_endpoint': bool, 'endpoint': string, 'use_path_style_endpoint': bool, 'upload_as_private': bool, 'disable_version_sync': bool, 'read_only': bool, 'ignore_cache': bool, 'disable_cert_verify': bool, 'bucket_name': string, 'region': string} getConfiguration()
 */
class CustomS3BucketPlugin extends S3BucketPluginBase implements PluginFormInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * Key module key provider service.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected KeyRepositoryInterface $keyService;

  /**
   * MimeType Guesser Service.
   *
   * @var \Symfony\Component\Mime\MimeTypeGuesserInterface
   */
  protected MimeTypeGuesserInterface $mimeTypeGuesser;

  /**
   * Module Handler Service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Initialize the plugin with dependent services.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin Definition.
   * @param \Drupal\s3fs_client\S3ClientFactoryInterface $s3_client_factory
   *   S3Client Factory.
   * @param \Drupal\s3fs_bucket\S3fsMetadataServiceInterface $s3fs_metadata
   *   S3fs Metadata Service.
   * @param \Drupal\s3fs_bucket\Batch\S3fsBucketRefreshCacheBatchInterface $refresh_cache_batch
   *   S3fs_bucket Refresh Cache Batch service.
   * @param \Drupal\key\KeyRepositoryInterface $key_service
   *   Key Service.
   * @param \Symfony\Component\Mime\MimeTypeGuesserInterface $mime_type_guesser
   *   Mime Type Guesser Service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module Handler Service.
   *
   * @todo Do we want to get rid of hooks and switch to events?
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, S3ClientFactoryInterface $s3_client_factory, S3fsMetadataServiceInterface $s3fs_metadata, S3fsBucketRefreshCacheBatchInterface $refresh_cache_batch, KeyRepositoryInterface $key_service, MimeTypeGuesserInterface $mime_type_guesser, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $s3_client_factory, $s3fs_metadata, $refresh_cache_batch);
    $this->keyService = $key_service;
    $this->mimeTypeGuesser = $mime_type_guesser;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create($container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('s3fs_client.factory'),
      $container->get('s3fs_bucket.metadata'),
      $container->get('s3fs_bucket.refresh_cache_batch'),
      $container->get('key.repository'),
      $container->get('file.mime_type.guesser'),
      $container->get('module_handler')
    );
  }

  /**
   * Gets default configuration for this plugin.
   *
   * @return array{'keymodule_access_key_name': string, 'keymodule_secret_key_name': string, 'credentials_file': string, 'use_credentials_cache': bool, 'credentials_cache_dir': string, 'disable_shared_config_files': bool, 'use_custom_endpoint': bool, 'endpoint': string, 'use_path_style_endpoint': bool, 'upload_as_private': bool, 'disable_version_sync': bool, 'read_only': bool, 'ignore_cache': bool, 'disable_cert_verify': bool, 'bucket_name': string, 'region': string}
   *   An associative array with the default configuration.
   */
  public function defaultConfiguration(): array {
    $config = [
      'keymodule_access_key_name' => '',
      'keymodule_secret_key_name' => '',
      'credentials_file' => '',
      'use_credentials_cache' => FALSE,
      'credentials_cache_dir' => '',
      'disable_shared_config_files' => FALSE,
      'use_custom_endpoint' => FALSE,
      'endpoint' => '',
      'use_path_style_endpoint' => FALSE,
      'upload_as_private' => FALSE,
      'disable_version_sync' => FALSE,
      'read_only' => FALSE,
      'ignore_cache' => FALSE,
      'disable_cert_verify' => FALSE,
    ];
    $config += parent::defaultConfiguration();

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $config = $this->getConfiguration();
    $config += $this->defaultConfiguration();

    $form['bucket_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Bucket'),
      '#open' => TRUE,
      '#parents' => ['bucket_config'],
    ];
    $form['bucket_settings']['bucket_name'] = [
      '#type' => 'textfield',
      '#title' => 'Bucket name',
      '#description' => $this->t('The name of the bucket as defined in AWS'),
      '#default_value' => $config['bucket_name'],
      '#maxlength' => 255,
    ];

    $form['credentials'] = [
      '#type' => 'details',
      '#title' => $this->t('Credentials'),
      '#description' => $this->t(
        "To configure your credentials, enter the values in the appropriate fields."
      ),
      '#open' => TRUE,
      '#parents' => ['bucket_config'],
    ];
    $key_collection_url = Url::fromRoute('entity.key.collection')->toString();
    $form['credentials']['keymodule'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Keys'),
      '#description' => $this->t('Use keys managed by the key module. <a href=":keys">Manage keys</a>', [
        ':keys' => $key_collection_url,
      ]),
      '#parents' => ['bucket_config'],
    ];
    $form['credentials']['keymodule']['keymodule_access_key_name'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Amazon Web Services Access Key'),
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $config['keymodule_access_key_name'],
      '#key_filters' => ['type' => 'authentication'],
      '#description' => $this->t('Select a key containing the AWS access key.'),
    ];
    $form['credentials']['keymodule']['keymodule_secret_key_name'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Amazon Web Services Secret Key'),
      '#description' => $this->t('Select a key containing the AWS secret key.'),
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $config['keymodule_secret_key_name'],
      '#key_filters' => ['type' => 'authentication'],
    ];
    $form['credentials']['credentials_file'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom Credentials File Location'),
      '#default_value' => $config['credentials_file'],
      '#description' => $this->t(
        "A custom profile or ini file location. This will add a custom AWS SDK
         ini provider in addition to the AWS SDK defaultProvider.
         \$settings[\'s3fs.access_key'] and \$settings['s3fs.secret_key'] will
         take priority over this setting."
      ),
    ];
    $form['credentials']['use_credentials_cache'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Credential Caching'),
      '#default_value' => $config['use_credentials_cache'],
      '#description' => $this->t(
        'Allow the AWS SDK to locally cache credentials. This is recommended
        if using EC2, IAM, or similar authentication methods that require an
        API call to AWS Metadata servers to obtain usable credentials.
      '),
    ];
    $form['credentials']['credentials_cache'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Credential Caching'),
      '#states' => [
        'visible' => [
          ':input[id=edit-bucket-config-use-credentials-cache]' => ['checked' => TRUE],
        ],
      ],
      '#parents' => ['bucket_config'],
    ];
    $form['credentials']['credentials_cache']['credentials_cache_dir'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cached Credentials Directory'),
      '#default_value' => $config['credentials_cache_dir'],
      '#description' => $this->t(
        "Copy of Cached credentials will be stored in this directory to
          avoid calling the metadata servers."
      ),
    ];
    if (!class_exists('\Doctrine\Common\Cache\FilesystemCache')) {
      $form['credentials']['credentials_cache']['credentials_cache_dir']['#disabled'] = TRUE;
    }
    $form['credentials']['disable_shared_config_files'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable Shared Config Files'),
      '#default_value' => $config['disable_shared_config_files'],
      '#description' => $this->t(
        "Disables the searching of shared config files
          (such as ~/.aws/config). This may be necessary if open_basedir is
          enabled on the server."
      ),
    ];

    $form['connection'] = [
      '#type' => 'details',
      '#title' => $this->t('Connection'),
      '#description' => $this->t(
        "Bucket provider connection settings."
      ),
      '#open' => TRUE,
      '#parents' => ['bucket_config'],
    ];
    $form['connection']['use_custom_endpoint'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use a custom endpoint'),
      '#default_value' => $config['use_custom_endpoint'],
      '#description' => $this->t('Connect to an S3-compatible storage service other than Amazon.'),
    ];
    $form['connection']['custom_endpoint_settings_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Custom Endpoint Settings'),
      '#states' => [
        'visible' => [
          ':input[id=edit-bucket-config-use-custom-endpoint]' => ['checked' => TRUE],
        ],
      ],
      '#parents' => ['bucket_config'],
    ];
    $form['connection']['custom_endpoint_settings_fieldset']['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hostname'),
      '#default_value' => $config['endpoint'],
      '#description' => $this->t('Custom service hostname with http(s) protocol, e.g. "https://objects.example.com"'),
      '#states' => [
        'visible' => [
          ':input[id=edit-bucket-config-use-custom-endpoint]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['connection']['use_path_style_endpoint'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use path-style endpoint'),
      '#default_value' => $config['use_path_style_endpoint'],
      '#description' => $this->t(
        'Send requests to a path-style endpoint, instead of a virtual-hosted-style
         endpoint. For example, %path_style, instead of %virtual_hosted_style.
         This is no longer common.',
        [
          '%path_style' => 'http://s3.amazonaws.com/bucket',
          '%virtual_hosted_style' => 'http://bucket.s3.amazonaws.com',
        ]
      ),
    ];
    $form['connection']['upload_as_private'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Upload all files as private in S3'),
      '#default_value' => $config['upload_as_private'],
      '#description' => $this->t(
        "Enable to store all files without public viewable permissions.</em>"
      ),
    ];
    $form['connection']['disable_version_sync'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable version sync'),
      '#default_value' => $config['disable_version_sync'],
      '#description' => $this->t('Check if the bucket does not support listObjectVersions.'),
    ];
    $form['connection']['read_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Bucket is read-only'),
      '#default_value' => $config['read_only'],
      '#description' => $this->t(
        "Enable this option if the account used to access the bucket has read-only permissions:"
      ),
    ];
    $form['connection']['ignore_cache'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ignore the file metadata cache'),
      '#default_value' => $config['ignore_cache'],
      '#description' => $this->t(
        "If you need to debug a problem with S3, you may want to temporarily ignore the file metadata cache.
      This will make all file system reads hit S3 instead of the cache.<br>
      <b>This causes s3fs to work extremely slowly, and should never be enabled on a production site.</b>"
      ),
    ];
    $form['connection']['disable_cert_verify'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable SSL/TLS verification'),
      '#default_value' => $config['disable_cert_verify'],
      '#description' => $this->t(
        "Enable if your bucket is using a locally self-signed certificate.<br>
        <b>PLEASE NOTE:</b> This is not recommended as it is a security risk."
      ),
    ];

    $form['region'] = [
      '#type' => 'hidden',
      '#default_value' => $config['region'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $tmpConfig = $form_state->getValues();
    [$access_key, $secret_key,, $config] = $this->processClientSettings($tmpConfig);

    // Region Detection code.
    if (!empty($tmpConfig['use_custom_endpoint']) && !empty($tmpConfig['endpoint'])) {
      // S3Client must always have a region, use a safe default.
      $region = 'us-east-1';
    }
    else {
      try {
        $client = $this->clientFactory->createClient($access_key, $secret_key, 'us-east-1', $config);
        $region = $client->determineBucketRegion($tmpConfig['bucket_name']);
        if (!$region) {
          // Error with trying to determine region, use the default.
          $region = 'us-east-1';
        }
      }
      catch (\Exception $e) {
        $region = 'us-east-1';
      }
    }
    $form_state->setValue('region', $region);

    // Test the bucket access.
    try {
      $s3 = $this->clientFactory->createClient($access_key, $secret_key, $region, $config);
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('obtain client', $this->t(
        'An unexpected error occurred obtaining S3Client . @message',
        ['@message' => $e->getMessage()]
      ));
      return;
    }

    // Test the connection to S3, bucket name and WRITE|READ ACL permissions.
    // These actions will trigger descriptive exceptions if the credentials,
    // bucket name, or region are invalid/mismatched.
    $date = date('dmy-Hi');
    $key_path = "s3fs-tests-results";
    if (!empty($tmpConfig['root_folder'])) {
      $key_path = "{$tmpConfig['root_folder']}/$key_path";
    }
    $key = "{$key_path}/write-test-{$date}.txt";
    $successPut = FALSE;
    $successDelete = FALSE;
    $exceptionCaught = FALSE;
    try {
      $putOptions = [
        'Body' => 'Example file uploaded successfully.',
        'Bucket' => $tmpConfig['bucket_name'],
        'Key' => $key,
      ];
      if (!empty($tmpConfig['encryption'])) {
        $putOptions['ServerSideEncryption'] = $tmpConfig['encryption'];
      }

      $s3->putObject($putOptions);
      $object = $s3->getObject([
        'Bucket' => $tmpConfig['bucket_name'],
        'Key' => $key,
      ]);
      if ($object) {
        $successPut = TRUE;
        $s3->deleteObject(['Bucket' => $tmpConfig['bucket_name'], 'Key' => $key]);
        $successDelete = TRUE;
      }
    }
    catch (\Exception $e) {
      $exceptionCaught = $e;
    }

    if (!empty($tmpConfig['read_only']) && ($successPut || $successDelete)) {
      // We were able to upload or delete a file when bucket is
      // tagged read-only.
      $form_state->setErrorByName('read only', $this->t('The provided credentials are not read-only.'));
    }
    elseif ($exceptionCaught) {
      // Bucket is read+write but we had an exception above.
      $form_state->setErrorByName('read only', $this->t(
        'An unexpected error occurred. @message',
        ['@message' => $exceptionCaught->getMessage()]
      ));
    }

    if (empty($tmpConfig['read_only']) && !empty(($tmpConfig['upload_as_private']))) {
      try {
        $key = "{$key_path}/public-write-test-{$date}.txt";
        $putOptions = [
          'Body' => 'Example public file uploaded successfully.',
          'Bucket' => $tmpConfig['bucket_name'],
          'Key' => $key,
          'ACL' => 'public-read',
        ];
        if (!empty($tmpConfig['encryption'])) {
          $putOptions['ServerSideEncryption'] = $tmpConfig['encryption'];
        }
        $s3->putObject($putOptions);
        if ($object = $s3->getObject([
          'Bucket' => $tmpConfig['bucket_name'],
          'Key' => $key,
        ])) {
          $s3->deleteObject([
            'Bucket' => $tmpConfig['bucket_name'],
            'Key' => $key,
          ]);
        }
      }
      catch (S3Exception $e) {
        $form_state->setErrorByName('upload as private', $this->t(
          "Could not upload file as publicly accessible. If the bucket security
          policy is set to BlockPublicAcl ensure that upload_as_private is enabled
          in your settings.php \$settings['s3fs.upload_as_private'] = TRUE; Error message: @message",
          ['@message' => $e->getMessage()]
        ));
      }
      catch (\Exception $e) {
        $form_state->setErrorByName('upload as private exception', $this->t(
          'An unexpected error occurred. @message',
          ['@message' => $e->getMessage()]
        ));
      }
    }

    if (empty($tmpConfig['disable_version_sync'])) {
      // Set up the args for list objects.
      $args = [
        'Bucket' => $tmpConfig['bucket_name'],
        'MaxKeys' => '1',
      ];

      try {
        $s3->listObjectVersions($args);
      }
      catch (\Exception $e) {
        $form_state->setErrorByName('version sync fault', $this->t(
          'Unable to listObjectVersions. Is listObjectVersions supported
           by your bucket? @message',
          ['@message' => $e->getMessage()]
        ));
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    // @todo Implement submitConfigurationForm() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getS3Client(): S3ClientInterface {
    $config = $this->getConfiguration();

    /** @var ?S3ClientInterface $s3 */
    $s3 = &drupal_static($this->getBucket()->id() . '_S3Client');

    // If the client hasn't been set up yet build the client.
    if (!isset($s3)) {

      [$access_key, $secret_key, $region, $client_config] = $this->processClientSettings($config);

      // Create the Aws\S3\S3Client object.
      $s3 = $this->clientFactory->createClient($access_key, $secret_key, $region, $client_config);
    }
    return $s3;
  }

  /**
   * Helper function to process Bucket Config into an S3 Client Config.
   *
   * @param array $config
   *   An array of plugin config options.
   *
   * @return array{string, string, string, array}
   *   Returns an array containing:
   *   - string access_key
   *   - string secret_key
   *   - string region
   *   - array S3 Client Config.
   */
  protected function processClientSettings(array $config): array {
    $client_config = [];

    $access_key = '';
    $secret_key = '';

    if (!empty($config['keymodule_access_key_name']) && !empty($config['keymodule_secret_key_name'])) {
      $retrievedAccessKey = $this->keyService->getKey($config['keymodule_access_key_name'])?->getKeyValue();
      $retrievedSecretKey = $this->keyService->getKey($config['keymodule_secret_key_name'])?->getKeyValue();

      if (!empty($retrievedAccessKey) && !empty($retrievedSecretKey)) {
        $access_key = $retrievedAccessKey;
        $secret_key = $retrievedSecretKey;
      }
    }

    if (empty($access_key) || empty($secret_key)) {
      // Use the defaultProvider to get all paths in home, env, etc.
      $provider = CredentialProvider::defaultProvider();

      // Use a custom ini file before defaultProvider.
      if (!empty($config['credentials_file'])) {
        $iniProvider = CredentialProvider::ini(NULL, $config['credentials_file']);
        $provider = CredentialProvider::chain($iniProvider, $provider);
      }
      // Cache the results in a memoize function to avoid loading and parsing
      // the ini file on every API operation.
      $provider = CredentialProvider::memoize($provider);

      // Allow SDK to cache results of calls to metadata servers.
      $doctrineInstalled = class_exists('\Doctrine\Common\Cache\FilesystemCache');
      if ($config['use_credentials_cache'] && !empty($config['credentials_cache_dir']) && $doctrineInstalled) {
        $cache = new DoctrineCacheAdapter(new FilesystemCache($config['credentials_cache_dir'] . '/s3fscache', '.doctrine.cache', 0017));
        $provider = CredentialProvider::cache($provider, $cache);
      }
      $client_config['credentials'] = $provider;
    }

    $region = $config['region'];
    $client_config['signature'] = 'v4';

    if ($config['use_custom_endpoint'] && !empty($config['endpoint'])) {
      $client_config['endpoint'] = $config['endpoint'];
    }

    // Use path-style endpoint, if selected.
    if ($config['use_path_style_endpoint']) {
      $client_config['use_path_style_endpoint'] = TRUE;
    }
    // @todo who should be responsible for the API_VERSION?
    $client_config['version'] = '2006-03-01';

    // Disable SSL/TLS verification.
    if (!empty($config['disable_cert_verify'])) {
      $client_config['http']['verify'] = FALSE;
    }

    // Set use_aws_shared_config_files = false to avoid reading ~/.aws/config.
    // If open_basedir restriction is in effect an exception may be thrown
    // without this enabled.
    if ($config['disable_shared_config_files']) {
      $client_config['use_aws_shared_config_files'] = FALSE;
    }

    return [$access_key, $secret_key, $region, $client_config];
  }

  /**
   * {@inheritdoc}
   */
  public function getCommandParams(string $path, array $params = []): array {
    // Allow other modules to change the command param settings.
    $context = [
      'bucket_id' => $this->getBucketId(),
      'key_path' => $path,
    ];
    $this->moduleHandler->alter('s3fs_bucket_command_params', $params, $context);

    // Set bucket name and key path after alter to avoid modification.
    $params['Bucket'] = $this->getBucketName();
    $params['Key'] = $path;

    return $params;
  }

  /**
   * Is bucket configured to upload files as private.
   */
  protected function isBucketPrivate(): bool {
    return $this->getConfiguration()['upload_as_private'];
  }

  /**
   * {@inheritdoc}
   */
  public function getStreamOptions(array &$options): void {
    if ($this->isBucketPrivate()) {
      // We unset the ACL rather than setting 'private' in order to work
      // with the AWS 'bucket owner enforced' feature.
      unset($options['ACL']);
    }
    else {
      $options['ACL'] = 'public-read';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getS3Metadata(string $path): array|FALSE {
    $params = $this->getCommandParams($path);
    try {
      $result = $this->getS3Client()->headObject($params);
      $data = $result->toArray();
    }
    catch (S3Exception $e) {
      // headObject() throws this exception if the requested key doesn't exist
      // in the bucket.
      return FALSE;
    }
    catch (\Exception $e) {
      watchdog_exception('S3FS', $e);
      return FALSE;
    }

    return $this->s3fsMetadata->convertMetadata($path, $data, $this->getBucketId());
  }

  /**
   * {@inheritdoc}
   */
  public function getBucketName(): string {
    $config = $this->getConfiguration();
    return $config['bucket_name'];
  }

  /**
   * {@inheritdoc}
   */
  public function isPathStyleEndpoint(): bool {
    $config = $this->getConfiguration();
    return $config['use_path_style_endpoint'];
  }

  /**
   * {@inheritdoc}
   */
  protected function isCacheDisabled(): bool {
    $config = $this->getConfiguration();
    return $config['ignore_cache'];
  }

  /**
   * Is version syncing disabled.
   *
   * @return bool
   *   True if version sync is disabled.
   */
  protected function isVersionSyncDisabled(): bool {
    $config = $this->getConfiguration();
    return $config['disable_version_sync'];
  }

  /**
   * {@inheritdoc}
   */
  public function convertMetadata($path, array $s3_metadata): array {
    return $this->s3fsMetadata->convertMetadata($path, $s3_metadata, $this->getBucketId());
  }

  /**
   * {@inheritdoc}
   */
  public function refreshCache(&$context): void {
    $s3 = $this->getS3Client();

    if (!isset($context['results']['progress'])) {
      $context['results']['progress'] = 0;
      $context['results']['time_start'] = time();

      $context['sandbox']['NextKeyMarker'] = '';
      $context['sandbox']['NextVersionIdMarker'] = '';
      $context['sandbox']['ContinuationToken'] = '';
      $context['sandbox']['folders'] = $this->s3fsMetadata->getExistingFolders($this->getBucketId());

      $context['sandbox']['current_page'] = 0;

      $context['sandbox']['estimated_max'] = $this->s3fsMetadata->countStoredObjects($this->getBucketId());

      $this->s3fsMetadata->prepareTempTable($this->getBucketId());
    }

    // General batch settings.
    $per_batch = 1000;

    // Make API call.
    $args = [
      'Bucket' => $this->getBucketName(),
      'MaxKeys' => $per_batch,
    ];

    if (!$this->isVersionSyncDisabled()) {
      if (!empty($context['sandbox']['NextKeyMarker'])) {
        $args['KeyMarker'] = $context['sandbox']['NextKeyMarker'];
      }
      if (!empty($context['sandbox']['NextVersionIdMarker'])) {
        $args['VersionIdMarker'] = $context['sandbox']['NextVersionIdMarker'];
      }
      // Get the object versions from the S3 API.
      $response = $s3->listObjectVersions($args);
    }
    else {
      if (!empty($context['sandbox']['ContinuationToken'])) {
        $args['ContinuationToken'] = $context['sandbox']['ContinuationToken'];
      }
      $response = $s3->listObjectsV2($args);
    }

    $finalise_cache_refresh = TRUE;
    if ($response->count()) {
      $result = $response->toArray();

      // Process the objects in this batch.
      $fileMetadataList = [];

      if (array_key_exists('Versions', $result)) {
        foreach ($result['Versions'] as $s3_metadata) {
          $this->parseS3ListResponseObject($fileMetadataList, $context['sandbox']['folders'], $s3_metadata);
          $context['results']['progress']++;
        }
      }
      elseif (array_key_exists('Contents', $result)) {
        foreach ($result['Contents'] as $s3_metadata) {
          $this->parseS3ListResponseObject($fileMetadataList, $context['sandbox']['folders'], $s3_metadata);
          $context['results']['progress']++;
        }
      }

      // Store the results of this batch in the temporary metadata table.
      $this->s3fsMetadata->writeTemporaryMetadata($fileMetadataList, $context['sandbox']['folders']);

      // The API indicates that there are more results through this truncated
      // flag.
      if ($result['IsTruncated'] === TRUE) {
        $finalise_cache_refresh = FALSE;

        // Store pager data for use in the start of the next
        // chunk in the batch.
        if (array_key_exists('NextKeyMarker', $result)) {
          $context['sandbox']['NextKeyMarker'] = $result['NextKeyMarker'];
        }
        if (array_key_exists('NextVersionIdMarker', $result)) {
          $context['sandbox']['NextVersionIdMarker'] = $result['NextVersionIdMarker'];
        }
        if (array_key_exists('NextContinuationToken', $result)) {
          $context['sandbox']['ContinuationToken'] = $result['NextContinuationToken'];
        }

        // Estimate the percentage based on the previous count.
        if ($context['sandbox']['estimated_max'] && $context['sandbox']['estimated_max'] > $context['results']['progress']) {
          $context['finished'] = $context['results']['progress'] / $context['sandbox']['estimated_max'];
          $context['message'] = $this->t('@percent_progress% (@progress/@estimated_max) time elapsed @elapsed_time (hh:mm:ss)', [
            '@estimated_max' => $context['sandbox']['estimated_max'],
            '@percent_progress' => round($context['finished'] * 100),
            '@progress' => $context['results']['progress'],
            '@elapsed_time' => static::getElapsedTimeFormatted($context['results']['time_start']),
          ]);
        }
        else {

          // Just set an arbitrary number as we are unable to calculate how
          // many results there will be in total.
          $context['finished'] = 0.75;
          $context['message'] = $this->t('Iterating through the S3 bucket beyond the previously found total items (currently @progress), time elapsed @elapsed_time (hh:mm:ss)', [
            '@elapsed_time' => static::getElapsedTimeFormatted($context['results']['time_start']),
            '@progress' => $context['results']['progress'],
          ]);
        }
      }
    }

    if ($finalise_cache_refresh) {
      // Store the folders in the database.
      $this->s3fsMetadata->writeTemporaryFolders($context['sandbox']['folders'], $this->getBucketId());
      // Set the final tables.
      $this->s3fsMetadata->moveTempMetadata($this->getBucketId());
      // Mark batch as completed.
      $context['finished'] = 1;
    }

  }

  /**
   * Get the elapsed time since start of the batch process.
   *
   * @param int $time_start
   *   Unix timestamp of when function started.
   *
   * @return string
   *   Elapsed time as a string format of 'hh:mm:ss'.
   */
  protected static function getElapsedTimeFormatted(int $time_start): string {
    $time_elapsed_seconds = time() - $time_start;
    return gmdate('H:i:s', $time_elapsed_seconds);
  }

  /**
   * Parse response from S3 Object listing.
   *
   * @param array $fileMetadataList
   *   The list of files.
   * @param array $folders
   *   The list of folders.
   * @param array $s3Metadata
   *   The individual list object result.
   */
  protected function parseS3ListResponseObject(array &$fileMetadataList, array &$folders, array $s3Metadata): void {
    $key = $s3Metadata['Key'];

    if (mb_strlen(rtrim($key, '/')) > S3fsMetadataServiceInterface::MAX_PATH_LENGTH) {
      return;
    }

    if ($key[strlen($key) - 1] == '/') {
      // Treat objects in S3 whose filenames end in a '/' as folders.
      // But don't store the '/' itself as part of the folder's uri.
      $folders[rtrim($key, '/')] = TRUE;
    }
    else {
      // Only store the metadata for the latest version of the file.
      if (isset($s3Metadata['IsLatest']) && !$s3Metadata['IsLatest']) {
        return;
      }
      // Files with no StorageClass are actually from the DeleteMarkers list,
      // rather then the Versions list. They represent a file which has been
      // deleted, so don't cache them.
      if (!isset($s3Metadata['StorageClass'])) {
        return;
      }
      // Buckets with Versioning disabled set all files' VersionIds to "null".
      // If we see that, unset VersionId to prevent "null" from being written
      // to the DB.
      if (isset($s3Metadata['VersionId']) && $s3Metadata['VersionId'] == 'null') {
        unset($s3Metadata['VersionId']);
      }
      $fileMetadataList[] = $this->s3fsMetadata->convertMetadata($key, $s3Metadata, $this->getBucketId());
    }
  }

}
