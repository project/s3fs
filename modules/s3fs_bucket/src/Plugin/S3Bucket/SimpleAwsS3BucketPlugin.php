<?php

namespace Drupal\s3fs_bucket\Plugin\S3Bucket;

use Aws\S3\S3ClientInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Plugin implementation of the s3fs_bucket.
 *
 * @S3Bucket(
 *   id = "aws_simple",
 *   label = @Translation("Simple AWS bucket"),
 *   description = @Translation("A low-configuration option for AWS buckets that uses common defaults.")
 * )
 */
class SimpleAwsS3BucketPlugin extends CustomS3BucketPlugin implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $config = $this->getConfiguration();
    $config += $this->defaultConfiguration();
    $form['bucket_name'] = [
      '#type' => 'textfield',
      '#title' => 'Bucket name',
      '#description' => t('The name of the bucket as defined in AWS'),
      '#default_value' => $config['bucket_name'],
      '#maxlength' => 63,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    // @todo Implement validateConfigurationForm() method.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    // @todo Implement submitConfigurationForm() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getS3Client(): S3ClientInterface {
    $config = $this->getConfiguration();

    // Create the Aws\S3\S3Client object.
    $s3 = \Drupal::service('s3fs_client.factory')->createClient('', '', $config['region']);

    return $s3;
  }

}
