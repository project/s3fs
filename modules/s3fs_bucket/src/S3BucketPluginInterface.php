<?php

namespace Drupal\s3fs_bucket;

use Aws\S3\S3ClientInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Interface for s3fs_bucket plugins.
 */
interface S3BucketPluginInterface extends PluginInspectionInterface, ConfigurableInterface, ContainerFactoryPluginInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Returns the plugin's description.
   *
   * @return string
   *   A string describing the plugin. Might contain HTML and should be already
   *   sanitized for output.
   */
  public function getDescription();

  /**
   * Sets the bucket entity for this plugin.
   *
   * @param \Drupal\s3fs_bucket\S3BucketInterface $bucket
   *   The bucket entity.
   *
   * @return $this
   */
  public function setBucket(S3BucketInterface $bucket): static;

  /**
   * Functions for obtaining Plugin configuration.
   */

  /**
   * Returns an S3Client based on the plugin config.
   *
   * @return \Aws\S3\S3ClientInterface
   *   The S3 Client.
   */
  public function getS3Client(): S3ClientInterface;

  /**
   * Obtain the bucket name.
   *
   * @return string
   *   The name of the Bucket on the S3 provider.
   */
  public function getBucketName(): string;

  /**
   * Is the bucket access using Path Style instead of domain based.
   *
   * @return bool
   *   True if path style endpoint.
   */
  public function isPathStyleEndpoint(): bool;

  /**
   * File Stream related functions.
   */

  /**
   * Return bucket and key for a command array.
   *
   * @param string $path
   *   Uri to the required object.
   * @param array $params
   *   Existing params.
   *
   * @return array
   *   A modified params array.
   */
  public function getCommandParams(string $path, array $params = []): array;

  /**
   * Modifies context options for use on file operations.
   *
   * @param array $options
   *   Existing context options by reference.
   */
  public function getStreamOptions(array &$options): void;

  /**
   * Allows the bucket plugin to alter a keyed path provided to it.
   *
   * @param string $path
   *   Keyed path to object in bucket. Do not include bucket name.
   *
   * @return string
   *   Keyed path to object with any alteration.
   */
  public function alterKeyedPath(string $path): string;

  /**
   * Cache related functions.
   */

  /**
   * Fetch an object from the file metadata cache table.
   *
   * @param string $path
   *   The path of the resource to check.
   *
   * @return array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir': 0|1, 'version': string}|false
   *   An array of metadata if the $path is in the cache. Otherwise, FALSE.
   */
  public function readCache(string $path): array|FALSE;

  /**
   * Write an object's (and its ancestor folders') metadata to the cache.
   *
   * @param array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir'?: 0|1, 'version'?: string} $metadata
   *   An associative array of file metadata in this format:
   *     'path' => The full URI of the file, including the scheme.
   *     'filesize' => The size of the file, in bytes.
   *     'timestamp' => The file's create/update timestamp.
   *     'dir' => A boolean indicating whether the object is a directory.
   *
   * @throws \Drupal\s3fs\S3fsException
   *   Exceptions which occur in the database call will percolate.
   */
  public function writeCache(array $metadata): void;

  /**
   * Delete an object's metadata from the cache.
   *
   * @param string|string[] $path
   *   A string (or array of strings) containing the path(s) of the object(s)
   *   to be deleted.
   *
   * @return int
   *   Number of paths deleted from the cache.
   */
  public function deleteCache(string|array $path): int;

  /**
   * Convert file metadata returned from S3 into a metadata cache array.
   *
   * @param string $path
   *   The path of the resource.
   * @param array{'ContentLength'?: non-negative-int, 'Size'?: non-negative-int, 'LastModified'?: string, 'VersionId'?: string}|array{} $s3_metadata
   *   An array containing the collective metadata for the object in S3.
   *   The caller may send an empty array here to indicate that the returned
   *   metadata should represent a directory.
   *
   * @return array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir': 0|1, 'version': string}
   *   A file metadata cache array.
   */
  public function convertMetadata(string $path, array $s3_metadata): array;

  /**
   * Returns the converted metadata for an object in S3.
   *
   * @param string $path
   *   The URI for the object in S3.
   *
   * @return array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir': 0|1, 'version': string}|false
   *   An array of DB-compatible file metadata or FALSE if lookup fails.
   */
  public function getS3Metadata(string $path): array|FALSE;

  /**
   * Returns a list of paths in a 'directory'.
   *
   * Files/folders matching $path/% but not $path/%/% will be returned by
   * the metadata service.
   *
   * @param string $path
   *   The path to search for files.
   *
   * @return array
   *   An array of paths in the 'directory'.
   */
  public function listDir(string $path): array;

  /**
   * Determines if the 'directory' is empty.
   *
   * @param string $path
   *   The path to search for files.
   *
   * @return bool
   *   True if the Metadata Cache indicates the directory is empty.
   */
  public function isDirEmpty(string $path): bool;

  /**
   * Wait for the specified file to exist in the bucket.
   *
   * @param string $path
   *   The path of the file.
   *
   * @return bool
   *   Returns TRUE once the waiting finishes, or FALSE if the file does not
   *   begin to exist within $attempt seconds.
   */
  public function waitUntilFileExists(string $path): bool;

  /**
   * Refresh the bucket metadata cache.
   *
   * This function should be called from inside a Batch context.
   *
   * @param array|\DrushBatchContext $context
   *   Batch Context passed by reference.
   */
  public function refreshCache(array|\DrushBatchContext &$context): void;

  /**
   * Returns the max length a URI can be.
   *
   * @return int
   *   Max length a URI can be.
   */
  public function getMaxPathLength(): int;

  /**
   * Try to obtain metadata for an object.
   *
   * If that file isn't in the cache it is assumed that the file doesn't exist
   * however if the cache is disabled this will perform a realtime lookup
   * for the object against the S3 bucket.
   *
   * @param string $path
   *   The uri of the resource to check.
   *
   * @return array|false
   *   An array if the $path exists, otherwise FALSE.
   */
  public function getS3fsObject(string $path): array|FALSE;

  /**
   * Form constructor for bucket Actions form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   *
   * @see EntityFormInterface::buildForm()
   */
  public function buildActionsForm(array $form, FormStateInterface $form_state): array;

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see EntityFormInterface::validateForm()
   */
  public function validateActionsForm(array &$form, FormStateInterface $form_state): void;

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see EntityFormInterface::submitForm()
   */
  public function submitActionsForm(array &$form, FormStateInterface $form_state): void;

}
