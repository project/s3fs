<?php

namespace Drupal\s3fs_bucket\Exception;

/**
 * The length of path exceeded the database storage limit.
 */
class PathLengthExceededException extends S3fsBucketException {}
