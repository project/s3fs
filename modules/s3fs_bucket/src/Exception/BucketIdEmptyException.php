<?php

namespace Drupal\s3fs_bucket\Exception;

/**
 * The bucketId paramater must not be empty.
 */
class BucketIdEmptyException extends S3fsBucketException {}
