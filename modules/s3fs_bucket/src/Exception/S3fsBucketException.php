<?php

namespace Drupal\s3fs_bucket\Exception;

/**
 * Base for exceptions thrown by s3fs_bucket for errors.
 */
class S3fsBucketException extends \Exception {}
