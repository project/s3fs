<?php

namespace Drupal\s3fs_bucket\Exception;

/**
 * Metadata is not valid.
 */
class InvalidMetadataException extends S3fsBucketException {}
