<?php

namespace Drupal\s3fs_bucket\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines s3fs_bucket annotation object.
 *
 * @Annotation
 */
class S3Bucket extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
