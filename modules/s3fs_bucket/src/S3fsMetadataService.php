<?php

namespace Drupal\s3fs_bucket;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\IntegrityConstraintViolationException;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\s3fs_bucket\Exception\BucketIdEmptyException;
use Drupal\s3fs_bucket\Exception\InvalidMetadataException;
use Drupal\s3fs_bucket\Exception\PathLengthExceededException;

/**
 * Provides access to the s3fs_file metadata cache.
 *
 * Utilizes a Drupal cache bin to speed up access with persistent
 * storage maintained in the database.
 */
class S3fsMetadataService implements S3fsMetadataServiceInterface {

  use StringTranslationTrait;

  /**
   * Prefix for all metadata cache bin entries.
   */
  const S3FS_CACHE_PREFIX = 's3fs:metadata:';

  /**
   * Tag for all cache entries.
   */
  const S3FS_CACHE_TAG = 's3fsMetadata';

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * An object for obtaining the system time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Drupal Cache service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * Lock backend service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected LockBackendInterface $lock;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs an S3fsService object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The new database connection object.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   An object to obtain the system time.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend to store metadata in.
   * @param \Drupal\Core\Lock\LockBackendInterface $lockBackend
   *   Lock backend service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   Logger channel.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(Connection $connection, TimeInterface $time, CacheBackendInterface $cacheBackend, LockBackendInterface $lockBackend, TranslationInterface $stringTranslation, LoggerChannelFactoryInterface $loggerFactory, MessengerInterface $messenger) {
    $this->connection = $connection;
    $this->time = $time;
    $this->cache = $cacheBackend;
    $this->lock = $lockBackend;
    $this->logger = $loggerFactory->get('s3fs');
    $this->messenger = $messenger;
    $this->setStringTranslation($stringTranslation);
  }

  /**
   * {@inheritdoc}
   */
  public function convertMetadata(string $path, array $s3_metadata, string $bucketId): array {

    if (empty($bucketId)) {
      throw new BucketIdEmptyException('Parameter $bucketId must not be empty');
    }
    // Need to fill in a default value for everything, so that DB calls
    // won't complain about missing fields.
    /** @var array{'bucket': string, 'path': string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir': 0|1, 'version': string} $metadata*/
    $metadata = [
      'bucket' => $bucketId,
      'path' => $path,
      'filesize' => 0,
      'timestamp' => $this->time->getRequestTime(),
      'dir' => 0,
      'version' => '',
    ];

    if (empty($s3_metadata)) {
      // The caller wants directory metadata.
      $metadata['dir'] = 1;
    }
    else {
      // The filesize value can come from either the Size or ContentLength
      // attribute, depending on which AWS API call built $s3_metadata.
      if (isset($s3_metadata['ContentLength'])) {
        $metadata['filesize'] = $s3_metadata['ContentLength'];
      }
      else {
        if (isset($s3_metadata['Size'])) {
          $metadata['filesize'] = $s3_metadata['Size'];
        }
      }

      if (isset($s3_metadata['LastModified'])) {
        $timestamp = strtotime($s3_metadata['LastModified']);
        if ($timestamp !== FALSE && $timestamp >= 0) {
          /** @var non-negative-int $timestamp */
          $metadata['timestamp'] = $timestamp;
        }
      }

      if (isset($s3_metadata['VersionId']) && $s3_metadata['VersionId'] != 'null') {
        $metadata['version'] = $s3_metadata['VersionId'];
      }
    }
    return $metadata;
  }

  /**
   * {@inheritdoc}
   */
  public function writeCache(array $metadata): void {
    if ($metadata['path'] == '' || $metadata['bucket'] == '') {
      throw new InvalidMetadataException('The $metadata parameter must contain a path and bucket');
    }

    $pathLength = mb_strlen($metadata['path']);
    if ($pathLength > S3fsMetadataServiceInterface::MAX_PATH_LENGTH) {
      throw new PathLengthExceededException('Object key path exceeds max length limit');
    }

    if (strpos($metadata['path'], '/') === 0) {
      throw new InvalidMetadataException('Paths must not start with a slash');
    }

    if (strpos($metadata['path'], '/', $pathLength)) {
      throw new InvalidMetadataException('Paths must not end with a slash');
    }

    $this->connection->merge('s3fs_file')
      ->keys([
        'bucket' => $metadata['bucket'],
        'path' => $metadata['path'],
      ])
      ->fields($metadata)
      ->execute();

    // Clear this URI from the Drupal cache, to ensure the next read isn't
    // from a stale cache entry.
    $cid = self::S3FS_CACHE_PREFIX . $metadata['bucket'] . $metadata['path'];
    $this->cache->delete($cid);

    $dirname = dirname($metadata['path']);
    // If this file isn't in the root directory, also write this file's
    // ancestor folders to the cache.
    while ($dirname != '' && $dirname != '/' && $dirname != '.') {
      $metadata = $this->convertMetadata($dirname, [], $metadata['bucket']);
      $this->writeCache($metadata);
      $dirname = dirname($metadata['path']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteCache(string|array $path, string $bucketId): int {

    if (empty($bucketId)) {
      throw new BucketIdEmptyException('Parameter $bucketId must not be empty');
    }

    if (!is_array($path)) {
      $path = [$path];
    }

    $cids = [];

    // Build an OR query to delete all the URIs at once.
    $delete_query = $this->connection->delete('s3fs_file');
    $or = $delete_query->orConditionGroup();
    foreach ($path as $p) {
      $or->condition('path', $p, '=');
      // Add URI to cids to be cleared from the Drupal cache.
      $cids[] = self::S3FS_CACHE_PREFIX . $bucketId . $p;
    }

    // Clear URIs from the Drupal cache.
    $this->cache->deleteMultiple($cids);

    // Matches: (uri=$path1 or uri=$path2)  && bucket=$bucketId.
    $delete_query->condition($or)->condition('bucket', $bucketId, '=');
    return $delete_query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function readCache(string $path, string $bucketId): array|FALSE {

    if (empty($bucketId)) {
      throw new BucketIdEmptyException('Parameter $bucketId must not be empty');
    }

    // Cache DB reads so that faster caching mechanisms (e.g. redis, memcache)
    // can further improve performance.
    $cid = self::S3FS_CACHE_PREFIX . $bucketId . $path;

    /**  @var object{'cid': string, 'data': array}|false $cached */
    $cached = $this->cache->get($cid);
    if ($cached) {
      /** @var array{'bucket': non-empty-string, 'path': non-empty-string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir': 0|1, 'version': string} $record */
      $record = $cached->data;
    }
    else {
      // Cache miss. Avoid a stampede.
      if (!$this->lock->acquire($cid, 1)) {
        // Another request is building the variable cache. Wait, then re-run
        // this function.
        $this->lock->wait($cid);
        $record = $this->readCache($path, $bucketId);
      }
      else {
        /** @var array{'bucket': non-empty-string, 'path': non-empty-string, 'filesize': non-negative-int, 'timestamp': non-negative-int, 'dir': 0|1, 'version': string}|FALSE|NULL $record */
        $record = $this->connection->select('s3fs_file', 's')
          ->fields('s')
          ->condition('path', $path, '=')
          ->condition('bucket', $bucketId, '=')
          ->execute()
          ?->fetchAssoc();

        $this->cache->set($cid, $record, CacheBackendInterface::CACHE_PERMANENT, [self::S3FS_CACHE_TAG]);
        $this->lock->release($cid);
      }
    }

    return $record ?: FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function listDir(string $path, string $bucketId): array {

    if (empty($bucketId)) {
      throw new BucketIdEmptyException('Parameter $bucketId must not be empty');
    }

    // Get the list of paths for files and folders which are children of the
    // specified folder, but not grandchildren.
    $query = $this->connection->select('s3fs_file', 's');
    $query->fields('s', ['path'])
      ->condition('bucket', $bucketId, '=');

    // If the path is empty we need to check the root only.
    if ($path == '') {
      $query->condition('path', '%', 'LIKE')
        ->condition('path', '%/%', 'NOT LIKE');
    }
    else {
      $query->condition('path', $query->escapeLike($path) . '/%', 'LIKE')
        ->condition('path', $query->escapeLike($path) . '/%/%', 'NOT LIKE');
    }

    $child_paths = $query->execute()?->fetchCol(0);

    return $child_paths ?: [];
  }

  /**
   * {@inheritdoc}
   */
  public function isDirEmpty(string $path, string $bucketId): bool {

    if (empty($bucketId)) {
      throw new BucketIdEmptyException('Parameter $bucketId must not be empty');
    }

    // Check if the folder is empty.
    $query = $this->connection->select('s3fs_file', 's');
    $query->fields('s')
      ->condition('bucket', $bucketId, '=')
      ->condition('path', $query->escapeLike($path) . '/%', 'LIKE');

    $file_count = $query->countQuery()->execute()?->fetchField();

    return ($file_count == 0);
  }

  /**
   * {@inheritdoc}
   */
  public function getExistingFolders(string $bucketId): array {

    if (empty($bucketId)) {
      throw new BucketIdEmptyException('Parameter $bucketId must not be empty');
    }

    $folders = [];
    $existing_folders = $this->connection->select('s3fs_file', 's')
      ->fields('s', ['path'])
      ->condition('dir', '1', '=')
      ->condition('bucket', $bucketId, '=');

    $existing_folders = $existing_folders->execute()?->fetchCol(0);
    $existing_folders = $existing_folders ?: [];
    foreach ($existing_folders as $folder_uri) {
      $folders[rtrim($folder_uri, '/')] = TRUE;
    }
    return $folders;
  }

  /**
   * {@inheritdoc}
   */
  public function countStoredObjects(string $bucketId = ''): int {
    $query = $this->connection->select('s3fs_file');

    if (!empty($bucketId)) {
      $query->condition('bucket', $bucketId);
    }

    /** @var int|null $result */
    $result = $query
      ->countQuery()
      ->execute()
      ?->fetchField();

    return $result ?: 0;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareTempTable(string $bucketId): void {

    if (empty($bucketId)) {
      throw new BucketIdEmptyException('Parameter $bucketId must not be empty');
    }

    $this->connection->delete('s3fs_file_temp')
      ->condition('bucket', $bucketId, '=')
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function writeTemporaryMetadata(array $file_metadata_list, array &$folders): void {
    if ($file_metadata_list) {
      $insert_query = $this->connection->insert('s3fs_file_temp')
        ->fields(['bucket', 'path', 'filesize', 'timestamp', 'dir', 'version']);

      foreach ($file_metadata_list as $metadata) {

        // @todo prevent writing paths with / ending.
        // Write the file metadata to the DB.
        $insert_query->values($metadata);

        // Add the ancestor folders of this file to the $folders array.
        // Since ObjectPaths are path/to/object we can leverage dirname().
        $path = dirname($metadata['path']);
        // Loop through each ancestor folder until we get to the root uri.
        // Risk exists that dirname() returns a malformed uri if a
        // StreamWrapper is disabled causing a loop. Use isValidUri to avoid.
        while ($path != '' && $path != '/' && $path != '.') {
          $folders[$path] = TRUE;
          $path = dirname($path);
        }
      }
      $insert_query->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function writeTemporaryFolders(array $folders, string $bucketId): void {
    $needsRetry = [];
    // Now that the $folders array contains all the ancestors of every file in
    // the cache, as well as the existing folders from before the refresh,
    // write those folders to the DB.
    if ($folders) {
      // Splits the data into manageable parts for the database.
      $chunks = array_chunk($folders, 5000, TRUE);
      foreach ($chunks as $chunk) {
        $insertQuery = $this->connection->insert('s3fs_file_temp')
          ->fields(['bucket', 'path', 'filesize', 'timestamp', 'dir', 'version']);
        foreach ($chunk as $folderPath => $ph) {
          $metadata = $this->convertMetadata($folderPath, [], $bucketId);
          $insertQuery->values($metadata);
        }
        // If this throws an integrity constraint violation, then the user's
        // S3 bucket has objects that represent folders using a different
        // scheme than the one we account for above.
        try {
          $insertQuery->execute();
        }
        catch (IntegrityConstraintViolationException $e) {
          $this->messenger->addError($this->t(
            'An Integrity Constraint violation occurred while attempting to
                process folder records. Please see the troubleshooting section
                of the s3fs README.txt for more info.'
          ));
          $needsRetry = array_merge($needsRetry, $chunk);
        }
      }

      // We had an IntegrityConstraintViolationException above. Try each
      // record individually and report failures. We keep the existing record
      // and do not insert the folder record to reduce the risk of data loss.
      if (!empty($needsRetry)) {
        foreach ($needsRetry as $folder => $ph) {
          $insert_query = $this->connection->insert('s3fs_file_temp')
            ->fields(
              ['bucket', 'path', 'filesize', 'timestamp', 'dir', 'version']
            );
          $metadata = $this->convertMetadata($folder, [], $bucketId);
          $insert_query->values($metadata);
          try {
            $insert_query->execute();
          }
          catch (IntegrityConstraintViolationException $e) {
            $this->messenger->addError($this->t("Integrity Constraint on folder '%folder'", ['%folder' => $folder]));
            $this->logger->error("Integrity Constraint on folder '%folder'", ['%folder' => $folder]);
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function moveTempMetadata(string $bucketId): void {
    // Delete the existing entries in s3fs_file.
    $this->connection->delete('s3fs_file')
      ->condition('bucket', $bucketId, '=')
      ->execute();

    // Insert a copy from s3fs_file_temp to s3fs_file.
    $query = $this->connection->select('s3fs_file_temp', 't')
      ->condition('bucket', $bucketId);
    $query->addField('t', 'bucket');
    $query->addField('t', 'path');
    $query->addField('t', 'filesize');
    $query->addField('t', 'timestamp');
    $query->addField('t', 'dir');
    $query->addField('t', 'version');

    $this->connection->insert('s3fs_file')
      ->from($query)
      ->execute();

    // Cleanup the temp table.
    $this->connection->delete('s3fs_file_temp')
      ->condition('bucket', $bucketId, '=')
      ->execute();

    // Clear every s3fs entry in the Drupal cache.
    $this->cache->invalidateAll();

  }

}
