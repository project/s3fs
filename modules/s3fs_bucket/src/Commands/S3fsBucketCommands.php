<?php

namespace Drupal\s3fs_bucket\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\s3fs_bucket\Batch\S3fsBucketRefreshCacheBatchInterface;
use Drush\Commands\DrushCommands;

/**
 * S3fs_bucket Drush commands handler.
 *
 * @package Drupal\s3fs\Commands
 */
class S3fsBucketCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * Entity Type Manager Service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * S3fs RefreshCacheBatch service.
   *
   * @var \Drupal\s3fs_bucket\Batch\S3fsBucketRefreshCacheBatchInterface
   */
  private S3fsBucketRefreshCacheBatchInterface $refreshCacheBatch;

  /**
   * S3fsCommands constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   S3fs Bucket Provider service.
   * @param \Drupal\s3fs_bucket\Batch\S3fsBucketRefreshCacheBatchInterface $refreshCacheBatch
   *   S3fs Bucket RefreshCacheBatch service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    S3fsBucketRefreshCacheBatchInterface $refreshCacheBatch
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->refreshCacheBatch = $refreshCacheBatch;
  }

  /**
   * Refresh the S3 File System metadata cache.
   *
   * @param array $options
   *   Options for command.
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   List of buckets.
   *
   * @command s3fs:list-buckets
   * @aliases s3fs-lb, s3fs-list-buckets
   */
  public function listBuckets(array $options = [
    'format' => 'table',
  ]): RowsOfFields {

    $buckets = $this->entityTypeManager->getStorage('s3fs_bucket')->loadMultiple();

    $bucketList = [];
    /** @var \Drupal\s3fs_bucket\S3BucketInterface $bucket */
    foreach ($buckets as $bucket) {
      $bucketList[] = [
        'ID' => $bucket->id(),
        'Name' => $bucket->label(),
        'Status' => $bucket->status() ? 'Enabled' : 'Disabled',
      ];
    }
    return new RowsOfFields($bucketList);
  }

  /**
   * Refresh the S3 File System metadata cache.
   *
   * @param array $options
   *   Options for command.
   *
   * @throws \Exception
   *
   * @command s3fs:refresh-cache
   * @aliases s3fs-rc, s3fs-refresh-cache
   * @option bucket The bucket to refresh.
   * @usage drush s3fs:refresh-cache --bucket=<bucket nam
   */
  public function refreshCache(array $options = [
    'bucket' => NULL,
  ]): void {

    if (!empty($options['bucket'])) {
      $bucket = $options['bucket'];
    }
    else {
      throw new \Exception("Must specify a bucket name");
    }

    /** @var \Drupal\s3fs_bucket\S3BucketInterface|null $entity */
    $entity = $this->entityTypeManager->getStorage('s3fs_bucket')->load($bucket);

    if (!empty($entity)) {
      $this->refreshCacheBatch->execute($entity);
    }
    else {
      throw new \Exception('Unable to load entity.');
    }
  }

}
