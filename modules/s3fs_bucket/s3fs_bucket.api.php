<?php

/**
 * @file
 * This file contains no working PHP code.
 *
 * It exists to provide additional documentation for doxygen as well as to
 * document hooks in the standard Drupal manner.
 */

/**
 * @defgroup s3fs_bucket_hooks S3 File System hooks
 * Hooks that can be implemented by other modules to extend s3fs_bucket module.
 */

/**
 * Alters the S3 parameters returned by getCommandParams().
 *
 * This impacts calls such obtaining metadata.
 *
 * @param array $command_params
 *   Associative array of upload settingshe
 *   The 'Bucket' and 'Key' array keys will be added/replaced after alter.
 *   If 'Bucket' or 'Key' are present they should be ignored.
 * @param array $context
 *   !!! DO NOT ALTER REFERENCE !!!
 *   An array of context information:
 *     - 'bucket_id' - The machine_name of the entity for the Bucket config.
 *     - 'key_path' - The path being operated against.
 *   Passed by reference however alteration is not permitted or honored.
 *
 * @see https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-s3-2006-03-01.html#headobject
 */
function hook_s3fs_bucket_command_params_alter(array &$command_params, array &$context) {
  if (strpos($context['key_path'], 'private/') !== FALSE) {
    $command_params['SSECustomerAlgorithm'] = 'AES256';
    $command_params['SSECustomerKey'] = 'My_Secure_SSE-C_Key';
  }
}
