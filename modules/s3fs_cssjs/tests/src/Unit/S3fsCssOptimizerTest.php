<?php

namespace Drupal\Tests\s3fs_cssjs\Unit;

use Drupal\s3fs_cssjs\Asset\S3fsCssOptimizer;
use Drupal\Tests\UnitTestCase;

require_once __DIR__ . '/../../fixtures/S3fsCssOptimizerMock.php';

/**
 * Tests the S3fsCssOptimizer.
 *
 * @group s3fs
 * @group s3fs_cssjs
 * @covers \Drupal\s3fs_cssjs\Asset\S3fsCssOptimizer
 */
class S3fsCssOptimizerTest extends UnitTestCase {

  /**
   * D9.3+ file_url_generator service mock.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $fileUrlGeneratorServiceMock;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->fileUrlGeneratorServiceMock = $this->getMockBuilder('\Drupal\Core\File\FileUrlGeneratorInterface')
      ->disableOriginalConstructor()
      ->getMock();
    $this->fileUrlGeneratorServiceMock->expects($this->any())
      ->method('generateAbsoluteString')
      ->will(
        $this->returnCallback(function ($arg) {
          return 'http://www.example.org' . $arg;
        }
        )
      );
  }

  /**
   * Test general asset link re-writing.
   */
  public function testRewriteUri(): void {

    /** @var \PHPUnit\Framework\MockObject\MockObject&\Drupal\Core\Config\ConfigFactoryInterface $configFactory */

    $configFactory = $this->getConfigFactoryStub([
      's3fs_cssjs.settings' => [
        'use_https' => FALSE,
        'use_cssjs_host' => FALSE,
        'cssjs_host' => '',
      ],
    ]);

    $cssOptimizer = new S3fsCssOptimizer($configFactory, $this->fileUrlGeneratorServiceMock);

    $cssOptimizer->rewriteFileURIBasePath = '';

    $this->assertEquals(
      'url(//www.example.org/test/file.txt)',
      $cssOptimizer->rewriteFileURI(['', '/test/file.txt'])
    );
    $this->assertEquals(
      'url(//www.example.org/test/file.txt)',
      $cssOptimizer->rewriteFileURI(['', '/core/../test/file.txt'])
    );
    $this->assertEquals(
      'url(//www.example.org/test/file.txt)',
      $cssOptimizer->rewriteFileURI(['', '/core/data/../../test/file.txt'])
    );

  }

  /**
   * Test asset links generated using HTTPS://.
   */
  public function testRewriteUriAlwaysHttps(): void {

    /** @var \PHPUnit\Framework\MockObject\MockObject&\Drupal\Core\Config\ConfigFactoryInterface $configFactory */

    $configFactory = $this->getConfigFactoryStub([
      's3fs_cssjs.settings' => [
        'use_https' => TRUE,
        'use_cssjs_host' => FALSE,
        'cssjs_host' => '',
      ],
    ]);

    $cssOptimizer = new S3fsCssOptimizer($configFactory, $this->fileUrlGeneratorServiceMock);

    $cssOptimizer->rewriteFileURIBasePath = '';

    $this->assertEquals(
      'url(https://www.example.org/test/file.txt)',
      $cssOptimizer->rewriteFileURI(['', '/test/file.txt'])
    );
  }

  /**
   * Test asset links with custom host.
   */
  public function testRewriteUriCustomCssHost(): void {

    /** @var \PHPUnit\Framework\MockObject\MockObject&\Drupal\Core\Config\ConfigFactoryInterface $configFactory */
    $configFactory = $this->getConfigFactoryStub([
      's3fs_cssjs.settings' => [
        'use_https' => FALSE,
        'use_cssjs_host' => TRUE,
        'cssjs_host' => 'test.example.org',
      ],
    ]);

    $cssOptimizer = new S3fsCssOptimizer($configFactory, $this->fileUrlGeneratorServiceMock);

    $cssOptimizer->rewriteFileURIBasePath = '';

    $this->assertEquals(
      'url(//test.example.org/test/file.txt)',
      $cssOptimizer->rewriteFileURI(['', '/test/file.txt'])
    );
  }

}
