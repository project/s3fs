<?php

namespace Drupal\s3fs_cssjs\EventSubscriber;

use Drupal\advagg\Asset\AssetOptimizationEvent;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\s3fs_cssjs\Asset\S3fsCssOptimizer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribe to asset optimization events and update assets urls.
 */
class S3fsAdvAggSubscriber implements EventSubscriberInterface {

  /**
   * Base path to use for URI rewrite.
   *
   * @var string
   */
  protected $rewriteFileURIBasePath;

  /**
   * Construct the optimizer instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file URL generator service.
   */
  public function __construct(protected ConfigFactoryInterface $configFactory, protected FileUrlGeneratorInterface $fileUrlGenerator) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [AssetOptimizationEvent::CSS => ['updateUrls', 0]];
  }

  /**
   * Update asset urls to access static files that they aren't in S3 bucket.
   *
   * @param \Drupal\advagg\Asset\AssetOptimizationEvent $asset
   *   The asset optimization event.
   */
  public function updateUrls(AssetOptimizationEvent $asset): void {
    $content = $this->processAssetContent($asset);
    $asset->setContent($content);
  }

  /**
   * Process asset content for make urls compatible.
   *
   * @param \Drupal\advagg\Asset\AssetOptimizationEvent $asset
   *   Asset to be processed.
   *
   * @return string
   *   Formatted return.
   *
   * @see \Drupal\Core\Asset\CssOptimizer::processFile()
   */
  public function processAssetContent(AssetOptimizationEvent $asset): string {
    $content = $asset->getContent();
    $css_asset = $asset->getAsset();
    // Get the parent directory of this file, relative to the Drupal root.
    $position = strrpos($css_asset['data'], '/') ?: NULL;
    $css_base_path = substr($css_asset['data'], 0, $position);
    // Store base path.
    $this->rewriteFileURIBasePath = $css_base_path . '/';
    // Restore asset urls.
    $content = str_replace('/' . $this->rewriteFileURIBasePath, "", $content);

    /** @var string|null $result */
    $result = preg_replace_callback(
      '/url\(\s*[\'"]?(?![a-z]+:|\/+)([^\'")]+)[\'"]?\s*\)/i',
      [$this, 'rewriteFileURI'],
      $content
    );

    // If there is an error during replacement use the original data.
    return $result ?: $asset->getContent();
  }

  /**
   * Return absolute urls to access static files that aren't in S3 bucket.
   *
   * @param array $matches
   *   An array of matches by a preg_replace_callback() call that scans for
   *   url() references in CSS files, except for external or absolute ones.
   *
   * @return string
   *   The file path.
   */
  // phpcs:ignore Drupal.NamingConventions.ValidFunctionName.ScopeNotCamelCaps, Drupal.Commenting.FunctionComment.Missing
  public function rewriteFileURI(array $matches): string {

    $reWriter = new S3fsCssOptimizer($this->configFactory, $this->fileUrlGenerator);

    $reWriter->rewriteFileURIBasePath = $this->rewriteFileURIBasePath;
    return $reWriter->rewriteFileURI($matches);
  }

}
