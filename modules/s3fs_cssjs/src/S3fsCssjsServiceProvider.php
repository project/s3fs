<?php

namespace Drupal\s3fs_cssjs;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Registers overrides to CSS/JS file creation.
 *
 * Allows for relative links in CSS/JS to be written to reefer to the Drupal
 * site or S3 bucket.
 */
class S3fsCssjsServiceProvider extends ServiceProviderBase {

  /**
   * Modifies existing service definitions.
   *
   * @param \Drupal\Core\DependencyInjection\ContainerBuilder $container
   *   The ContainerBuilder whose service definitions can be altered.
   */
  public function alter(ContainerBuilder $container): void {
    // Modify CSS static urls.
    $container->getDefinition('asset.css.optimizer')
      ->setClass('Drupal\s3fs_cssjs\Asset\S3fsCssOptimizer')
      ->setArguments(
        [
          new Reference('config.factory'),
          new Reference('file_url_generator'),
        ]
      );

  }

  /**
   * Register dynamic service definitions.
   *
   * @param \Drupal\Core\DependencyInjection\ContainerBuilder $container
   *   The ContainerBuilder whose service definitions can be checked.
   */
  public function register(ContainerBuilder $container): void {
    if ($container->hasDefinition('advagg.optimizer.css')) {
      $container
        ->register('s3fs_cssjs.advagg.css_subscriber', 'Drupal\s3fs_cssjs\EventSubscriber\S3fsAdvAggSubscriber')
        ->addTag('event_subscriber')
        ->setArguments(
          [
            new Reference('config.factory'),
            new Reference('file_url_generator'),
          ]
        );
    }
  }

}
