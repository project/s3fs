<?php

namespace Drupal\s3fs_cssjs\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures s3fs settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructor for s3fs settings form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_Handler
   *   Module Handler Service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, ModuleHandlerInterface $module_Handler) {
    parent::__construct($configFactory);
    $this->moduleHandler = $module_Handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 's3fs_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      's3fs_cssjs.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('s3fs_cssjs.settings');

    $form['use_https'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Always rewrite links to serve files via HTTPS'),
      '#default_value' => $config->get('use_https'),
      '#description' => $this->t(
        'Forces S3 File System to always generate HTTPS URLs when rewriting links,
      e.g. "https://mybucket.s3.amazonaws.com/smiley.jpg".<br>
      Without this setting enabled, URLs for your files will use the same scheme as the page they are first served from.'
      ),
    ];

    $form['use_cssjs_host'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Custom CSS/JS Asset Host.'),
      '#default_value' => $config->get('use_cssjs_host'),
      '#description' => $this->t('Use a custom host for assets links inside
      CSS/JS files.'),
    ];
    $form['use_cssjs_host_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Custom CSS/JS Host Settings'),
      '#states' => [
        'visible' => [
          ':input[id=edit-use-cssjs-host]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['use_cssjs_host_fieldset']['cssjs_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hostname'),
      '#default_value' => $config->get('cssjs_host'),
      '#description' => $this->t('Custom hostname, e.g. "objects.example.com" without http(s) protocol.'),
      '#states' => [
        'visible' => [
          ':input[id=edit-use-cssjs-host]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();

    $config = $this->config('s3fs_cssjs.settings');

    $config
      ->set('use_https', $values['use_https'])
      ->set('use_cssjs_host', $values['use_cssjs_host'])
      ->set('cssjs_host', $values['cssjs_host']);

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
