<?php

namespace Drupal\Tests\s3fs_client\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\s3fs_client\S3ClientFactoryInterface;

/**
 * S3fs client service test.
 *
 * Verify we can obtain a client from the service.
 *
 * @group s3fs
 * @group s3fs_client
 */
class S3fsClientServiceTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    's3fs',
    's3fs_client',
    's3fs_bucket',
    's3fs_streamwrapper',
  ];

  /**
   * Ensures the s3_client service is loaded and returns a client.
   */
  public function testS3ClientFactory(): void {
    $s3Factory = $this->container->get('s3fs_client.factory');
    $this->assertInstanceOf(S3ClientFactoryInterface::class, $s3Factory, 'Unable to obtain S3ClientFactory');
    $this->assertTrue(method_exists($s3Factory, 'getDefaultClient'), 'getDefaultClient method does not exist');
    $this->assertTrue(method_exists($s3Factory, 'createClient'), 'getDefaultClient method does not exist');
  }

}
