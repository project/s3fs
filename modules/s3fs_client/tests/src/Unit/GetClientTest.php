<?php

namespace Drupal\Tests\s3fs_client\Unit;

use Aws\Exception\InvalidRegionException;
use Aws\S3\S3ClientInterface;
use Drupal\s3fs_client\S3ClientFactory;
use Drupal\Tests\UnitTestCase;

/**
 * Tests S3ClientFactory.
 *
 * @group s3fs
 * @group s3fs_client
 */
class GetClientTest extends UnitTestCase {

  /**
   * S3FS Client factory.
   *
   * @var \Drupal\s3fs_client\S3ClientFactory
   */
  protected S3ClientFactory $clientFactory;

  /**
   * {@inheritDoc}
   */
  public function setup(): void {
    parent::setUp();
    $this->clientFactory = new S3ClientFactory();
  }

  /**
   * Test getDefaultClient()
   */
  public function testGetDefaultClient(): void {
    $this->markTestSkipped('todo: Determine how region is handled for this method');
    $s3Client = $this->clientFactory->getDefaultClient();
    $this->assertInstanceOf(S3ClientInterface::class, $s3Client);
  }

  /**
   * Test createClient() with access/secret keys.
   */
  public function testCreateClientWithKeys(): void {
    $s3Client = $this->clientFactory->createClient('test', 'test', 'us-east1');
    $this->assertInstanceOf(S3ClientInterface::class, $s3Client, 'Unable to obtain an S3Client');
  }

  /**
   * Test createClient() with empty keys.
   */
  public function testCreateClientWithEmptyKeys(): void {
    $s3Client = $this->clientFactory->createClient('', '', 'us-east1');
    $this->assertInstanceOf(S3ClientInterface::class, $s3Client, 'Unable to obtain an S3Client');
  }

  /**
   * Test createClient() with only a custom config.
   */
  public function testCreateClientWithCustomConfigAndNoKeys(): void {
    $config = [
      'credentials' => [
        'key' => 'test',
        'secret' => 'test',
      ],
    ];

    $s3Client = $this->clientFactory->createClient('', '', 'us-east1', $config);
    $this->assertInstanceOf(S3ClientInterface::class, $s3Client, 'Unable to obtain an S3Client');
  }

  /**
   * Test createClient() without a region.
   */
  public function testCreateClientWithNoRegion(): void {
    $this->expectException(InvalidRegionException::class);
    // Newer AWS API will throw an InvalidArgumentException.
    $this->expectException(\InvalidArgumentException::class);
    $this->clientFactory->createClient('test', 'test', '',);
  }

}
