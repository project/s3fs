<?php

namespace Drupal\s3fs_client;

/**
 * Provides an interface for obtaining an AWS S3 client.
 */
interface S3ClientFactoryInterface {

  /**
   * Get a stock-standard S3 client.
   *
   * This utilises the AWS SDK utility that searches for credentials in your
   * environment variables and aws config files.
   *
   * @see \Aws\S3\RegionalEndpoint\ConfigurationProvider::defaultProvider
   *
   * @return \Aws\S3\S3ClientInterface
   *   AWS S3 Client.
   */
  public function getDefaultClient();

  /**
   * Create and return a new S3 client using the AWS SDK.
   *
   * @param string $access_key
   *   Access Key credential.
   *   If an empty string Secret Key will be ignored.
   * @param string $secret_key
   *   Secret Key credential.
   *   If an empty string access key will be ignored.
   * @param string $region
   *   S3 Bucket Region.
   * @param array $client_configuration
   *   Optional array of client configuration options.
   *
   * @return \Aws\S3\S3ClientInterface
   *   AWS S3 Client.
   *
   * @see \Aws\S3\S3Client::__construct
   *   for client configuration options.
   */
  public function createClient(string $access_key, string $secret_key, string $region, array $client_configuration);

}
