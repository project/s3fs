<?php

namespace Drupal\s3fs_client;

use Aws\S3\S3Client;

/**
 * Factory to provide S3 Clients.
 */
class S3ClientFactory implements S3ClientFactoryInterface {

  /**
   * Default version of the AWS API to use.
   */
  const DEFAULT_API_VERSION = '2006-03-01';

  /**
   * The default client.
   *
   * @var \Aws\S3\S3ClientInterface
   */
  protected $defaultClient;

  /**
   * {@inheritdoc}
   */
  public function getDefaultClient() {

    // @todo What do we want to do about region on this and maybe other config options?
    $config = [
      'version' => self::DEFAULT_API_VERSION,
    ];

    if ($this->defaultClient === NULL) {
      $this->defaultClient = new S3Client($config);
    }

    return $this->defaultClient;
  }

  /**
   * {@inheritdoc}
   */
  public function createClient(string $access_key, string $secret_key, string $region, array $client_config = []) {

    // $access_key and $secret_key may not be provided in environments using
    // IAM credentials or other custom configuration.
    if (!empty($access_key) && !empty($secret_key)) {
      $client_config['credentials'] = [
        'key' => $access_key,
        'secret' => $secret_key,
      ];
    }
    $client_config['region'] = $region;

    if (empty($client_config['version'])) {
      $client_config['version'] = self::DEFAULT_API_VERSION;
    }

    return new S3Client($client_config);
  }

}
